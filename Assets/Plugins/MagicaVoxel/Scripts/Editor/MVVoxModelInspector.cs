﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

[CustomEditor(typeof(MVVoxModel))]
public class MVVoxModelInspector : Editor {

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        MVVoxModel voxModel = this.target as MVVoxModel;

        if (voxModel.vox != null)
            AU.AUEditorUtility.ColoredLabel(string.Format("Controls ({3}: {0}x{1}x{2})", voxModel.vox.sizeX, voxModel.vox.sizeY, voxModel.vox.sizeZ, System.IO.Path.GetFileNameWithoutExtension(voxModel.ed_filePath)), Color.green);
        else
            AU.AUEditorUtility.ColoredLabel("Controls", Color.green);

        AU.AUEditorUtility.ColoredHelpBox(Color.yellow, "Enabling this may create lots of GameObjects, careful when the vox model is big");
        voxModel.ed_importAsIndividualVoxels = EditorGUILayout.Toggle("Import as Individual Voxels", voxModel.ed_importAsIndividualVoxels);

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Load")) {

            string path = EditorUtility.OpenFilePanel(
                "Open VOX model",
                "Assets/MagicaVoxel/Vox",
                "vox"
            );

            if (!string.IsNullOrEmpty(path)) {
                string alphaMaskPath = string.Empty;
                if (EditorUtility.DisplayDialog("Question", "Do you want to load an alpha mask model file?", "yes", "no")) {
                    alphaMaskPath = EditorUtility.OpenFilePanel(
                    "Open VOX model for alpha mask",
                    "Assets/MagicaVoxel/Vox",
                    "vox"
                );
                }

                voxModel.ed_filePath = path;
                voxModel.ed_alphaMaskFilePath = alphaMaskPath;
                voxModel.LoadVOXFile(path, alphaMaskPath, voxModel.ed_importAsIndividualVoxels);
            }
        }
        if (GUILayout.Button("Reimport")) {

            voxModel.LoadVOXFile(voxModel.ed_filePath, voxModel.ed_alphaMaskFilePath, voxModel.ed_importAsIndividualVoxels);
        }

        if (GUILayout.Button("Clear")) {

            voxModel.ClearVoxMeshes();

        }
        EditorGUILayout.EndHorizontal();
        if (GUILayout.Button("Generate Blockers")) {
            var blockerP = voxModel.transform.FindChild("blockers");
            if (blockerP == null) {
                blockerP = new GameObject().transform;
                blockerP.name = "blockers";
                blockerP.transform.SetParent(voxModel.transform);
                blockerP.localPosition = new Vector3(0, 0, 0);
                
            }
            MVMainChunk mc = MVImporter.LoadVOX(voxModel.ed_filePath, null);
            var voxels = mc.voxelChunk.voxels;
            var xm = voxels.GetLength(0);
            var ym = voxels.GetLength(1);
            var zm = voxels.GetLength(2);
            Debug.Log(voxels.GetLength(0));
            Debug.Log(voxels.GetLength(1));
            Debug.Log(voxels.GetLength(2));
            int[,] directions =  {
                {0,1},
                {0,-1},
                {1,0},
                {-1,0}
            };
            int[] rotations = { 270, 90, 0, 180 };
            for (int x = 0; x < xm; x++) {

                for (int y = 0; y < ym; y++) {

                    for (int z = 0; z < zm; z++) {

                        if (y < ym - 1) {
                            int thisVoxel = voxels[x, y, z];
                            Func<int, int, int, bool> CheckIfCanStand = (x2, y2, z2) => {
                                return voxels[x2, y2, z2] != 0 && voxels[x2, y2 + 1, z2] == 0;
                            };
                            //if is walkable
                            if (CheckIfCanStand(x, y, z)) {
                                //check for dangerous falls in all directions
                                for (int d = 0; d < directions.GetLength(0); d++) { //loops all directions
                                    int acceptedFall = 1;
                                    bool dangerousFall = true;
                                    //this loop goes from 
                                    //one block above to 
                                    //how far you can fall
                                    for (int j = -1; j < acceptedFall+1; j++) { //checks if dangerousfall or not
                                        var x2 = x + directions[d, 0];
                                        var y2 = y - j;
                                        var z2 = z + directions[d, 1];
                                        if(x2 > voxels.GetLength(0) || x2< 0 ||
                                            y2 > voxels.GetLength(1) || y2< 0 ||
                                                z2 > voxels.GetLength(2) || z2< 0) {
                                            continue;
                                        }
                                        if (voxels[x2, y2, z2] != 0) {
                                            dangerousFall = false;
                                            break;
                                        }
                                    }
                                    if (dangerousFall) {
                                        //instantiate blocker to stop dangerous fall
                                        var blocker = voxModel.blocker;
                                        //var b = Instantiate(blocker);
                                        var b = PrefabUtility.InstantiatePrefab(blocker) as GameObject;
                                        b.transform.SetParent(blockerP);
                                        b.transform.localPosition = new Vector3(x + 0.5f, y + 1.5f, z + 0.5f);
                                        b.transform.localRotation = Quaternion.Euler(0,rotations[d],0);
                                        b.transform.localScale = new Vector3(1, 1, 1);
                                    }

                                    //voxels[x, y, z]
                                }
                            }
                            /*
                            if (thisVoxel == 0) { //empty voxel
                                if (voxels[x, y + 1, z] == 0) { //vexel on top also empty 

                                    if (
                                        //checking if any adjascent vexel is walkable (not empty and that the one above it is empty)
                                        (x > 0 && voxels[x - 1, y, z] != 0 && voxels[x - 1, y + 1, z] == 0) ||
                                        (z < zm - 1 && voxels[x, y, z + 1] != 0 && voxels[x, y + 1, z + 1] == 0) ||
                                        (x < xm - 1 && voxels[x + 1, y, z] != 0 && voxels[x + 1, y + 1, z] == 0) ||
                                        (z > 0 && voxels[x, y, z - 1] != 0 && voxels[x, y + 1, z - 1] == 0)) {

                                        int allowedHeight = 2;
                                        bool dangerousHeight = true;
                                        for (int i = 1; i < allowedHeight; i++) {
                                            int yTemp = y - i;
                                            if (yTemp < 0) { //fall in to the depths, so cannot allow
                                                break;
                                            } else {
                                                if(voxels[x, yTemp, z] != 0) { //foundwalkable within allowedHeight
                                                    dangerousHeight = false;
                                                    break;
                                                }
                                            }
                                        }

                                        if (dangerousHeight) {
                                            var blocker = voxModel.blocker;
                                            var b = Instantiate(blocker);
                                            b.transform.SetParent(blockerP);
                                            b.transform.localPosition = new Vector3(x+0.5f, y+1.5f, z+0.5f);
                                            b.transform.localRotation = Quaternion.identity;
                                            b.transform.localScale = new Vector3(1,1,1);
                                        }


                                    }
                                }
                            }
                            */
                        }


                    }
                }
                blockerP.localPosition = new Vector3(xm*0.5f, 0, zm*0.5f);
                blockerP.localRotation = Quaternion.Euler(0, 180, 0);
                blockerP.localScale = new Vector3(1,1,1);
            }

        }
    }

}
