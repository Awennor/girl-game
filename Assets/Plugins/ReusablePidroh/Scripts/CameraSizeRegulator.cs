﻿using UnityEngine;
using System.Collections;

public class CameraSizeRegulator : MonoBehaviour {

    public float pixelPerSize;
    public float minSize;
    public Camera cameraToUse;
    float lastHeight;
    public MyFloatEvent cameraSizeChange;

    // Use this for initialization
    void Start() {
        UpdateMethod();
    }

    private void UpdateMethod() {
        
        float height = Screen.height;
        lastHeight = height;
        var size = height / pixelPerSize;
        if (size < minSize) size = minSize;
        cameraToUse.orthographicSize = size;
        cameraSizeChange.Invoke(size);
    }

    // Update is called once per frame
    void Update() {
        if(lastHeight !=  Screen.height) {
            UpdateMethod();
        }
    }
}
