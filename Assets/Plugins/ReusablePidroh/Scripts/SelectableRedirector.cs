﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SelectableRedirector : Selectable {

    public Selectable redirectTo;
    public bool RedirectRight;
    public bool RedirectLeft;


    // Update is called once per frame
    void Update() {

        if (EventSystem.current != null && EventSystem.current.currentSelectedGameObject ==
            this.gameObject) {
            if (redirectTo != null)
                redirectTo.Select();
            if (RedirectLeft) {
                FindSelectableOnLeft().Select();

            }
            if (RedirectRight) {

                FindSelectableOnRight().Select();

            }
        }
    }
}
