﻿using UnityEngine;
using System.Collections;
using System;

public class MoveTowardsTarget : MonoBehaviour {

    [SerializeField]
    Transform target;
    Vector3 targetAsVector = new Vector3(0,0,0);
    public float speed = 10;
    Vector3 aux = new Vector3();
    public Action hit;
    Vector3 offset = new Vector3(0,0,0);
    Vector3 offsetLocal = new Vector3(0,0,0);
    public bool unscaledDelta = false;

    public Transform Target
    {
        get
        {
            return target;
        }

        set
        {
            target = value;
        }
    }

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }

    public Vector3 TargetAsVector
    {
        get
        {
            return targetAsVector;
        }

        set
            {
            targetAsVector = value;
        }
    }

    public Vector3 OffsetLocal {
        get {
            return offsetLocal;
        }

        set {
            //offset += transform.InverseTransformPoint(value);
            offsetLocal = value;
        }
    }

    // Use this for initialization
    void Start () {
	    
	}

    public void SetOffset(float x, float y) {
        offset.x = x;
        offset.y = y;
    }

    public void Teleport() {
        this.transform.position = TargetPosition();
    }

    // Update is called once per frame
    void Update () {
        if (!isActiveAndEnabled) return;

        Vector3 tpos = TargetPosition();
        Vector3 originalTpos = tpos;
        tpos = tpos - transform.position;
        float dis2 = tpos.sqrMagnitude;
        if (unscaledDelta) {
            tpos = tpos.normalized * Speed * Time.unscaledDeltaTime;
        } else {
            tpos = tpos.normalized * Speed * Time.deltaTime;
        }

        bool collision = dis2 < tpos.sqrMagnitude;
        transform.position = transform.position + tpos;

        if (collision) {
            transform.position = originalTpos;
        }
        if (collision && hit != null) {
            hit();
        }
    }

    private Vector3 TargetPosition() {
        var tpos = TargetAsVector;
        if (target != null)
            tpos = Target.position;
        var localp = offsetLocal;
        var scale = transform.lossyScale;
        localp.x *= scale.x;
        localp.z *= scale.z;
        localp.y *= scale.y;
        tpos += transform.rotation* localp;
        
        
        tpos += offset;
        return tpos;
    }
}
