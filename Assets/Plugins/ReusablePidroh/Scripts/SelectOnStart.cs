﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectOnStart : MonoBehaviour {
    private bool update;

    // Use this for initialization
    void Start () {
        
        GetComponent<Selectable>().Select();
	}

    void OnEnable() {
        Debug.Log("ON ENABLEEE");
        update = true;
        //GetComponent<Animator>().updateMode
    }
	
	// Update is called once per frame
	void Update () {
        if (update) {
            var onRight = GetComponent<Selectable>().FindSelectableOnRight();
            if (onRight != null)
                onRight.Select();
            GetComponent<Selectable>().Select();
            //GetComponent<Animator>().SetTrigger("Highlighted");
            update = false;
        }
	}
}
