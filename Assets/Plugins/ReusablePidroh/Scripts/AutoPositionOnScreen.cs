﻿using UnityEngine;
using System.Collections;

public class AutoPositionOnScreen : MonoBehaviour {

    public float xPositionRatio;
    public float yPositionRatio;


	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    float wishedX = xPositionRatio * Screen.width;
        float wishedY = yPositionRatio * Screen.height;
        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(wishedX, wishedY));

	}
}
