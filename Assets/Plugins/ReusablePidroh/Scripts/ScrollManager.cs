﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollManager : MonoBehaviour {

    [SerializeField]
    protected ScrollRect scrollRect;

    float SCROLL_MARGIN = 0.1f;

    protected RectTransform contentPanel;
    private RectTransform scrollRectTransform;
    public Scrollbar scrollbarY;

    

    public void Awake() {
        scrollRectTransform = scrollRect.GetComponent<RectTransform>();


    }

    public void Update() {

    }

    

    public void SnapTo( RectTransform target) {

        var sr = scrollRect;
        
        // helper vars
		float contentHeight2 = sr.content.rect.height;
		float viewportHeight = sr.GetComponent<RectTransform>().rect.height;
 
		// what bounds must be visible?
		float centerLine = target.transform.localPosition.y; // selected item's center
		float upperBound = centerLine + (target.GetComponent<RectTransform>().rect.height / 2f); // selected item's upper bound
		float lowerBound = centerLine - (target.GetComponent<RectTransform>().rect.height / 2f); // selected item's lower bound
 
		// what are the bounds of the currently visible area?
		float lowerVisible = (contentHeight2 - viewportHeight) * sr.normalizedPosition.y - contentHeight2;
		float upperVisible = lowerVisible + viewportHeight;
 
		// is our item visible right now?
		float desiredLowerBound;
		if (upperBound > upperVisible) {
			// need to scroll up to upperBound
			desiredLowerBound = upperBound - viewportHeight + target.GetComponent<RectTransform>().rect.height * SCROLL_MARGIN;
		} else if (lowerBound < lowerVisible) {
			// need to scroll down to lowerBound
			desiredLowerBound = lowerBound - target.GetComponent<RectTransform>().rect.height * SCROLL_MARGIN;
		} else {
			// item already visible - all good
			return;
		}
 
		// normalize and set the desired viewport
		float normalizedDesired = (desiredLowerBound + contentHeight2) / (contentHeight2 - viewportHeight);
		sr.normalizedPosition = new Vector2(0f, Mathf.Clamp01(normalizedDesired));


        
    }
}
