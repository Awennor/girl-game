﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnityEventOnGameObjectActive : MonoBehaviour {

    public UnityEvent unityEvent;
    public bool AutoDeactivateAftercallback = true;

    public void TryToCallback() {
        if(this.gameObject.activeSelf) {
            unityEvent.Invoke();
            if(AutoDeactivateAftercallback) {
                this.gameObject.SetActive(false);
            }
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
