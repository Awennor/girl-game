﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class RandomSpecial{

    public static T RandomElementSpecial<T>(this List<T> list){
        if(list == null || list.Count == 0) return default(T);
        
        int count = list.Count;
        int index = Range(0,count);
        return list[index];
    }

    public static int Seed { get {
            return UnityEngine.Random.seed;
        } set {
            UnityEngine.Random.seed = value;
        } }

    public static bool RandomBool() {
        return RandomBool(0.5f);

    }

    public static float randomFloatOnZero(float v)
    {
        return UnityEngine.Random.Range(-v, v);
    }

    public static int PriorityArray(List<int> ps) {
        int sum = 0;
        for (int i = 0; i < ps.Count; i++) {
            sum += ps[i];
        }
        if(sum == 0) {
            return -1;
        }
        int v = Range(1,sum);
        for (int i = 0; i < ps.Count; i++) {
            v -= ps[i];
            if(v <= 0) return i;
        }
        return -1;
    }

    public static float randomFloat(float max) {
        return UnityEngine.Random.Range(0, max);
    }

    public static bool RandomBool(float v) {
        return UnityEngine.Random.Range(0f, 1f) < v;
    }

    public static float Range(float v1, float v2) {
        return UnityEngine.Random.Range(v1, v2);
    }

    public static Vector2 Range(Vector2 v1, Vector2 v2) {
        Vector2 v3 = new Vector2(UnityEngine.Random.Range(v1.x, v2.x), UnityEngine.Random.Range(v1.y, v2.y));
        return v3;
        
    }

    public static int Range(int v1, int v2) {
        return UnityEngine.Random.Range(v1, v2);
    }
}


