﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public class Countdown : MonoBehaviour {
    [SerializeField]
    float seconds;

    bool paused = true;

    public float timeScale = 1;

    float warningTime = 0;

    public MyIntEvent secondChanged;
    public UnityEvent countStart = new UnityEvent();
    public UnityEvent warningEvent = new UnityEvent();
    public UnityEvent timeUpEvent = new UnityEvent();
    public UnityEvent onlyOneMinuteLeft = new UnityEvent();
    private float lastTime;

    public float Seconds {
        get {
            return seconds;
        }

        set {
            seconds = value;
        }
    }

    public bool Paused {
        get {
            return paused;
        }

        set {
            paused = value;
        }
    }

    // Use this for initialization
    void Start() {

    }

    void Awake() {
        lastTime = Time.time;
    }

    public void Interrupt() {
        paused = true;
    }

    public void StartCount(float time) {
        seconds = time;
        paused = false;
        countStart.Invoke();
    }


    // Update is called once per frame
    void Update() {
        if (!Paused) {
            if(seconds >= 0) {
                float timeNow = Time.time;
                var dT = Time.deltaTime;
                lastTime = timeNow;
                int minutesBefore = (int)Math.Floor(seconds / 60f);

                int i = (int)Seconds;
                Seconds -= dT * timeScale;
                if (i != (int)Seconds) {
                    secondChanged.Invoke((int) Seconds);
                }

                var minutesNow = (int)Math.Floor(seconds / 60f);
                if (minutesBefore != minutesNow) {
                    if(minutesNow == 1) {
                        onlyOneMinuteLeft.Invoke();
                    }
                    //MinuteEvent.Invoke(minutesNow);
                }

                if(seconds <= 0) {
                    warningTime = 0;
                    timeUpEvent.Invoke();
                }

            } else {
                var dT = Time.deltaTime;
                int pastWarningMinute = (int) warningTime /60;
                //Debug.Log(pastWarningMinute +"past minute");
                warningTime += dT * timeScale;
                var newMinute = (int)warningTime / 60;
                //Debug.Log(newMinute +" new minute");
                if (pastWarningMinute != newMinute) {
                    //Debug.Log("WARNING EVENT!!!!!!");
                    warningEvent.Invoke();
                }
            }
        }
        

    }
}
