﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EventSystemAlwaysSelect : MonoBehaviour {

    public EventSystem es;
    GameObject lastSelected;

	// Use this for initialization
	void Start () {
        if (es == null)
            es = GetComponent<EventSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if (es.currentSelectedGameObject != null)
        {
            lastSelected = es.currentSelectedGameObject;
        }
        else {
            if (lastSelected != null) {
                lastSelected.GetComponent<Selectable>().Select();
            }
        }
	}
}
