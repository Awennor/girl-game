﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class RotationToMouse {

    public static Vector3 GetPositionInTransformPlane(Transform transform) {
        Plane playerPlane = new Plane(Vector3.up, transform.position + new Vector3(0, 2, 0));
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float hitdist = 0.0f;
        if (playerPlane.Raycast(ray, out hitdist)) {
            //  Debug.Log("Camera ray HIT ROTATE");
            //Debug.Log("HIT RAY CAST");
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);
            return targetPoint;
        }
        return transform.position;
    }

    public static void LookAtMouse(Transform transform, float offsetRotationY = 0) {
        Plane playerPlane = new Plane(Vector3.up, transform.position + new Vector3(0, 2, 0));
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //Debug.Log("Camera ray");
        //Debug.Log(Input.mousePosition+"MOUSE POSITION");
        float hitdist = 0.0f;
        //Debug.Log("try to hit ray cast");
        if (playerPlane.Raycast(ray, out hitdist)) {
            //  Debug.Log("Camera ray HIT ROTATE");
            //Debug.Log("HIT RAY CAST");
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            var euler = targetRotation.eulerAngles;
            euler.z = 0;
            euler.x = 0;
            euler.y += offsetRotationY;
            targetRotation = Quaternion.Euler(euler);
            //euler.

            // Smoothly rotate towards the target point.
            transform.rotation = targetRotation;
        }
    }
}
