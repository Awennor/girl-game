﻿using UnityEngine;
using System.Collections;

public class ParticleEnder : MonoBehaviour {

    [SerializeField]
    ParticleSystem[] particleSystems;

    ParticleSystem.Particle[] particles = new ParticleSystem.Particle[6000];

    // Use this for initialization
    void Start() {

    }

    public void EndParticles() {
        for (int i = 0; i < particleSystems.Length; i++) {
            var pE = particleSystems[i];
            pE.GetParticles(particles);
            pE.enableEmission = false;
            //pE.emissionRate = 0;
            for (int j = 0; j < pE.particleCount; j++) {
                //particles[i].velocity = new Vector3(0,0,0);
                particles[j].remainingLifetime = UnityEngine.Random.Range(0.4f,0.5f);
                //particles[j].velocity *= 0.1f;
                //particles[j].size *= 0.1f;
            }
            pE.SetParticles(particles, pE.particleCount);
        }

    }

    // Update is called once per frame
    void Update() {

    }
}
