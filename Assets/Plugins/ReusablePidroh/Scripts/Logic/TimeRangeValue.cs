﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.ReusablePidroh.Scripts.Logic
{
    public class TimedRangeValue
    {
        bool pause;
        public RangedValue Value { get; set; }
        public float SpeedRatio { get; set; }

        public bool Pause {
            get {
                return pause;
            }

            set {
                pause = value;
            }
        }

        public TimedRangeValue(RangedValue v){
            Value = v;
            MainDataHolder.Instance.addUpdate(Update);
        }

        public void Update(float d) {
            if(!pause)
                Value.Value += SpeedRatio * d;

        }


    }
}
