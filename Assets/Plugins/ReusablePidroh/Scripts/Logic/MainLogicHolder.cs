﻿using UnityEngine;
using System.Collections;
using System;

namespace Assets.ReusablePidroh.Scripts.Logic
{
    public class MainDataHolder
    {

        UpdateCenter updateCenter = new UpdateCenter();
        private static MainDataHolder instance;

        static public MainDataHolder Instance
        {
            get
            {
                if (instance == null) instance = new MainDataHolder(); return instance;
            }
        }

        internal void addUpdate(Action<float> update)
        {
            //Debug.Log("ADD UPDATE");
            updateCenter.updateEvent += update;

        }


        public UpdateCenter UpdateCenter
        {
            get
            {
                return updateCenter;
            }

            set
            {
                updateCenter = value;
            }
        }
    }
}
