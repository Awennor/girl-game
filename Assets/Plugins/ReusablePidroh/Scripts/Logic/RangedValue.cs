﻿using UnityEngine;
using System.Collections;
using System;

namespace Assets.ReusablePidroh.Scripts.Logic {
    [Serializable]
    public class RangedValue {
        public bool DebugB { get; set; }
        [SerializeField]
        private float value;

        public float Max { get; set; }

        public float Ratio { get { return Value / Max; } }
        public Action<RangedValue> MaxValueReached;
        public Action<RangedValue> ValueChanged;
        float oldValue = 0;

        public float Value {
            get {
                return value;
            }

            set {
                oldValue = this.value;
                if (value > Max) {
                    value = Max;
                }
                if (value < 0) {
                    value = 0;
                }

                this.value = value;
                if (oldValue < Max && value >= Max) {
                    if (MaxValueReached != null) {
                        MaxValueReached(this);
                    }
                }
                if (this.value != oldValue) {
                    if (ValueChanged != null) {
                        ValueChanged(this);
                    }
                }
            }
        }

        public float OldScale {
            get {
                return oldValue / Max;
            }
        }

        public bool IsFull() {
            return (value >= Max);
        }

        public float Scale() {
            
            return value / Max;
        }

        public bool isZero() { return Value == 0; }

        public bool RatioGoneBelow(float v) {
            if (Scale() < v && OldScale >= v) {
                return true;
            } 
            return false;
        }
    }
}
