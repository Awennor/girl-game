﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
namespace Assets.ReusablePidroh.Scripts.Logic
{
    public class UpdateCenter
    {

        public event Action<float> updateEvent;

        public void Update(float f)
        {
            //Debug.Log("UPDATE CENTER UPDATING");

            if (updateEvent != null)
            {
                //Debug.Log("UPDATE CENTER UPDATING EVENT");
                updateEvent(f);
            }
        }
    }
}
