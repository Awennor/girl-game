﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class AmbientLightSetter : MonoBehaviour {

    public Color skyColor;
    public Color equatorColor;
    public Color groundColor;
    public float intensity = -1;
    //public Color cameraColor;

    void OnEnable() {
        //Start();
        //    CentralResource.Instance.CameraControl.Camera.backgroundColor = cameraColor;
    }

    // Use this for initialization
    void Start() {
        RenderSettings.ambientEquatorColor = equatorColor;
        RenderSettings.ambientGroundColor = groundColor;
        RenderSettings.ambientSkyColor = skyColor;
        if(intensity >=0)
            RenderSettings.ambientIntensity = intensity;

    }

    // Update is called once per frame
    void Update() {

    }
}
