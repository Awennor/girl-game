﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;

public class EventTriggerCollider : MonoBehaviour {
    //public UnityAction u;
    public UnityEvent u2 = new UnityEvent();
    public UnityEvent OnExit = new UnityEvent();
    public MyBoolEvent InsideTrigger;
    public bool triggerOnStart = false;
    bool ignoreOnce = false;

    public bool IgnoreOnce {
        get {
            return ignoreOnce;
        }

        set {
            ignoreOnce = value;
        }
    }

    public void Start() {
        if (triggerOnStart) {
            TriggerPositive();
        }
    }

    private void TriggerPositive() {
        u2.Invoke();
        InsideTrigger.Invoke(true);
    }

    void OnTriggerEnter(Collider col) {
        if (IgnoreOnce) {
            IgnoreOnce = false;
            return;
        }
        TriggerPositive();
    }

    void OnTriggerExit(Collider col) {
        TriggerNegative();
    }

    private void TriggerNegative() {
        OnExit.Invoke();
        InsideTrigger.Invoke(false);
    }
}
