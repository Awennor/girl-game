﻿using System;
using UnityEngine;

public interface CharacterControllerWrapperModifier
{
    Vector3 TryModify(Vector3 motion);
}