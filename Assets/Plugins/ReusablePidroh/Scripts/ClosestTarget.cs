﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ClosestTarget : MonoBehaviour {
    private GameObject chosenTarget;
    List<GameObject> possibleTargets;
    public Action<GameObject> targetChanged;

    public GameObject ChosenTarget {
        get {
            return chosenTarget;
        }

        set {
            if (value != chosenTarget) {
                chosenTarget = value;
                if (targetChanged != null) {
                    targetChanged(chosenTarget);
                }
            }

        }
    }

    public List<GameObject> PossibleTargets {
        get {
            if (possibleTargets == null) return null;
            while (possibleTargets.Remove(null)) {

            }
            return possibleTargets;
        }

        set {
            possibleTargets = value;
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        float len = 999999;

        if (PossibleTargets == null) {
            ChosenTarget = null;
            return;
        }



        for (int i = 0; i < PossibleTargets.Count; i++) {

            GameObject obj = PossibleTargets[i];
            if (obj == null) continue;
            Vector3 p = transform.position;
            p -= obj.transform.position;
            float len2 = p.sqrMagnitude;
            if (len2 < len) {
                len = len2;

                ChosenTarget = obj;



            }
        }
        if (possibleTargets.Count == 0)
            ChosenTarget = null;
        //Debug.Log("CLOSEST TARGET "+possibleTargets.Count);

    }

    public GameObject ClosestTargetTo(Vector3 thisPosition, float minDis) {
        float len = 999999;

        if (PossibleTargets == null) {
            return null;
        }
        GameObject bestTarget = null;
        for (int i = 0; i < PossibleTargets.Count; i++) {

            GameObject target = PossibleTargets[i];
            if (target == null) continue;
            Vector3 p = thisPosition;
            p -= target.transform.position;
            float len2 = p.sqrMagnitude;
            if (len2 < len && len2 < minDis*minDis) {
                len = len2;

                bestTarget = target;
            }
        }
        return bestTarget;
    }
}
