﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ParticleCircle : MonoBehaviour {

    public ParticleSystem ps;
    public bool automaticLocalPosition = false;
    [SerializeField]
    float radius = 10f;
    public bool Invert;
    public bool DoSpeedOnUpdate;
	// Use this for initialization
	void Start () {
        DoSpeed();

    }

    [ExposeMethodInEditor]
    private void DoSpeed() {
        float multiplierInvert = 1;
        if(Invert) multiplierInvert *= -1;
        float periodLife = ps.main.startLifetimeMultiplier;
        float speed = 2 * Mathf.PI * radius / periodLife;
        if(automaticLocalPosition)
            ps.transform.localPosition = new Vector3(-radius*multiplierInvert,0,0);
        float pFake = 1f; //always 1F, real period depends on lifetime of particle
        var velocityOverLifetimex = ps.velocityOverLifetime;

        var curveX = new AnimationCurve();
        curveX.AddKey(0, 0);
        curveX.AddKey(pFake * 0.25f, 1*multiplierInvert);
        curveX.AddKey(pFake * 0.5f, 0);
        curveX.AddKey(pFake * 0.75f, -1*multiplierInvert);
        curveX.AddKey(pFake * 1.00f, 0);
        velocityOverLifetimex.x = new ParticleSystem.MinMaxCurve(speed, curveX);

        var velocityOverLifetimey = ps.velocityOverLifetime;
        var curveY = new AnimationCurve();
        curveY.AddKey(0, 1);
        curveY.AddKey(pFake * 0.25f, 0);
        curveY.AddKey(pFake * 0.5f, -1);
        curveY.AddKey(pFake * 0.75f, 0);
        curveY.AddKey(pFake * 1.00f, 1);
        velocityOverLifetimex.y = new ParticleSystem.MinMaxCurve(speed, curveY);
        
    }

    // Update is called once per frame
    void Update () {
        if(DoSpeedOnUpdate) {
            DoSpeed();
        }
	}
}
