﻿using UnityEngine;


public class SmoothFollowC : MonoBehaviour {

    // The target we are following
    [SerializeField]
    private Transform target;
    // The distance in the x-z plane to the target
    [SerializeField]
    private float distance = 10.0f;
    // the height we want the camera to be above the target
    [SerializeField]
    private float height = 5.0f;
    

    [SerializeField]
    private float rotationDamping;
    [SerializeField]
    private float heightDamping;

    bool followPosition = false;
    bool followObject = true;


    Vector3 targetPosition;
    private float initialY;
    [SerializeField]
    private float maxDistanceSmooth = 9999999f;

    public Transform Target {
        get {
            return target;
        }

        set {
            target = value;
        }
    }

    public Vector3 TargetPosition {
        get {
            return targetPosition;
        }

        set {
            targetPosition = value;
        }
    }

    public bool FollowObject {
        get {

            return followObject;
        }

        set {
            followObject = value;
            FollowPosition = !followObject;
        }
    }

    public bool FollowPosition {
        get {
            return followPosition;
        }

        set {
            followPosition = value;
            followObject = !followPosition;
        }
    }

    public bool Pause { get; set; }
    public bool AutomaticFollow { get; set; }

    public Vector3 GetFocusPoint() {
        if (followObject) {
            return target.transform.position;
        } else {
            return targetPosition;
        }
    }

    // Use this for initialization
    void Start() {
        initialY = transform.position.y;
        //AutomaticFollow = true;
    }



    // Update is called once per frame
    void LateUpdate() {
        // Early out if we don't have a target
        if (!Target || Pause)
            return;

        // Calculate the current rotation angles
        var wantedRotationAngle = Target.eulerAngles.y;
        var wantedHeight = Target.position.y + height;

        var currentRotationAngle = transform.eulerAngles.y;
        var currentHeight = transform.position.y;

        // Damp the rotation around the y-axis
        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.unscaledDeltaTime);

        // Damp the height
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.unscaledDeltaTime);


        // Convert the angle into a rotation
        var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

        // Set the position of the camera on the x-z plane to:
        // distance meters behind the target

        Vector3 wantedPos;
        wantedPos = Target.position;
        if (followPosition) {
            wantedPos = targetPosition;
        }
        //transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime*2);

        wantedPos -= currentRotation * Vector3.forward * distance;



        // Set the height of the camera
        wantedPos = new Vector3(wantedPos.x, currentHeight, wantedPos.z);
        float dis2 = (wantedPos - transform.position).sqrMagnitude;
        if (transform.position != wantedPos) {
            if(!AutomaticFollow && dis2 < maxDistanceSmooth*maxDistanceSmooth)
                transform.position = Vector3.Lerp(transform.position, wantedPos, Time.unscaledDeltaTime * 4);
            else
                transform.position = wantedPos;
        }
            
        // Always look at the target
        //transform.LookAt(target);
    }
}
    