﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UVChanger : MonoBehaviour {

    [SerializeField]
    private List<MeshFilter> meshFilters = new List<MeshFilter>();
    public float offSetX;
    public float offSetY;
    public float scale;

    public List<MeshFilter> MeshFilters {
        get {
            return meshFilters;
        }

        set {
            meshFilters = value;
        }
    }

    //[ExecuteInEditMode]
    // Use this for initialization
    void Start () {
        //Debug.Log("START CALLED");
        Change();

    }

    public void Change() {
        for (int i = 0; i < MeshFilters.Count; i++) {
            var m = MeshFilters[i].mesh;
            ChangeThisMesh(m);
            //uv = m.uv;

        }
    }

    public void ChangeThisMesh(Mesh m) {
        var uv = m.uv;
        for (int j = 0; j < uv.Length; j++) {
            //Debug.Log("MESH CHANGE");
            //                if(offsetMode)
            uv[j] += new Vector2(offSetX, offSetY) * scale;

            //v[j] = new Vector2(0.8f,0.8f);
            //Debug.Log(uv[i]);

        }
        m.uv = uv;
    }






    // Update is called once per frame
    void Update () {
	
	}
}
