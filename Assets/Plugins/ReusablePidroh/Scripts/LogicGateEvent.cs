﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LogicGateEvent : MonoBehaviour {

    [SerializeField]
    List<bool> inputs = new List<bool>();
    bool lastOutput = false;
    public GateType type;
    public bool automaticOutputUpdate = true;

    public MyIntEvent OnInputTrue;

    public MyBoolEvent outputChange;
    public UnityEvent outputChange_True;

	// Use this for initialization
	void Start () {
    }

    public void ChangeInput(int pos, bool state) {
        if(inputs[pos] != state) {
            inputs[pos] = state;
            if(state)
                OnInputTrue.Invoke(pos);
        }
        
        if(automaticOutputUpdate)
            UpdateOutput();
    }

    public void ChangeInput_true(int pos) {
        ChangeInput(pos, true);
    }

    public void ChangeInput_0(bool state) {
        ChangeInput(0, state);
    }

    public void UpdateOutput() {
        bool outputNow = false;
        if (type == GateType.AND) {
            outputNow = !inputs.Contains(false);
        }
        if (type == GateType.OR) {
            outputNow = inputs.Contains(true);
        }
        if (outputNow != lastOutput) {
            lastOutput = outputNow;

            outputChange.Invoke(outputNow);
            if(outputNow) {
                outputChange_True.Invoke();
            }
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
    [Serializable]
    public enum GateType {
        AND, OR,
    }
}
