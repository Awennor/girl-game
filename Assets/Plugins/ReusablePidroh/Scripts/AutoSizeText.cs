﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class AutoSizeText : MonoBehaviour {

    public RectTransform rectT;
    public Text text;
    public int additionalWidth;
    public int additionalHeight;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void LateUpdate() {
        if (rectT != null && text != null) {
            Canvas.ForceUpdateCanvases();
            var rect = text.rectTransform.rect;
            rect.width += additionalWidth;
            rect.height += additionalHeight;
            rectT.SetLocalWidth(rect.width);
            rectT.SetLocalHeight(rect.height);
        }

    }
}
