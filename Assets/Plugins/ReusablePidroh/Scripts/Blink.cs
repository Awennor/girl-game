﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Blink : MonoBehaviour {


    public float blinkPeriod;
    private Sequence blinkSequence;
    bool finalState;

    public void BlinkStartFinalActive(float totalTime) {
        EndBlink();
        blinkSequence = DOTween.Sequence();
        float blinkAdvance = blinkPeriod;
        bool currentState = gameObject.activeSelf;
        TweenCallback firstMethod = StateTrue;
        TweenCallback secondMethod = StateFalse;
        if (currentState) {
            firstMethod = StateFalse;
            secondMethod = StateTrue;
        }

        blinkSequence
            .AppendCallback(firstMethod).AppendInterval(blinkPeriod / 2)
            .AppendCallback(secondMethod).AppendInterval(blinkPeriod/2)
            .SetLoops(-1).SetUpdate(false);
        DOVirtual.DelayedCall(totalTime, EndBlink);
        finalState = true;

    }

    private void EndBlink() {
        if(blinkSequence != null) {
            blinkSequence.Kill();
            blinkSequence = null;
        }
            
        gameObject.SetActive(finalState);
    }

    private void StateFalse() {
        gameObject.SetActive(false);
    }

    private void StateTrue() {
        gameObject.SetActive(true);
    }

}
