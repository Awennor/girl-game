﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ButtonMethod_GUI{

    Action action;

    public ButtonMethod_GUI() {
    }

    public ButtonMethod_GUI(Action a) {
        action = a;
    }

    public void CallAction() {
        action();
    }

}
