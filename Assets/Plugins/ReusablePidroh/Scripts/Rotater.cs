﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotater : MonoBehaviour {

    public float period;
    public bool RotateOnStart;
    private bool rotating;

    // Use this for initialization
    void Start () {
		if(RotateOnStart) {
            BeginRotationState();
        }
	}

    public void BeginRotationState() {
        rotating = true;
        //transform.DORotate(new Vector3(0, 360), period/2).SetEase(Ease.InExpo);
        //transform.DORotate(new Vector3(0, 360), 1f).SetDelay(1f);
        //DOTween.Sequence()
          //  .Append(transform.DORotate(new Vector3(0, 360), period, RotateMode.LocalAxisAdd)).
            //SetLoops(-1).SetEase(Ease.Unset);

        
    }

    public void StopRotation() {
        rotating = false;
    }
	
	// Update is called once per frame
	void Update () {
        if(rotating)
		    transform.Rotate(new Vector3(0, 360*Time.deltaTime/period));
	}
}
