﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;

public class MovementLinearCtrl : MonoBehaviour {
    private Invoker invoker;
    Vector3 velocity = new Vector3();
    //private Rigidbody rigidBody;
    private CharacterControllerWrapper CharacterControllerWrapper;

    public Vector3 Velocity
    {
        get
        {
            return velocity;
        }

        set
        {
            velocity = value;
        }
    }

    // Use this for initialization
    void Start () {
        if (invoker == null)
            invoker = gameObject.GetComponent<Invoker>();
        
        //rigidBody = GetComponent<Rigidbody>();
        CharacterControllerWrapper = GetComponent<CharacterControllerWrapper>();
	}

    public void OnDisable() {
        if(CharacterControllerWrapper != null)
            CharacterControllerWrapper.ZeroSpeed();
    }

    public void Disable(float delay) {
        if(invoker== null)
            invoker = gameObject.GetComponent<Invoker>();
        
        invoker.AddActionUnique(Disable, delay);
    }

    public void Disable() {
        enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        CharacterControllerWrapper.SetSpeed(velocity);
        //transform.position = transform.position + velocity * Time.deltaTime;
	}
}
