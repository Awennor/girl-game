﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedParticleAccess : MonoBehaviour {
    private ParticleSystem system;
    public bool playOnEnable;
    public bool deactivateOnDone;
    public bool deactivateParentOnDone;


    // Use this for initialization
    void Awake() {
        system = GetComponent<ParticleSystem>();

    }

    void OnEnable() {
        if (playOnEnable) {
            system.Play();
            Debug.Log("TRY PLAY HAPPENS");
        }

    }

    public void SetEmission(float rate) {
        if(system == null) Awake();
        var emission = system.emission;
        emission.rateOverTime = new ParticleSystem.MinMaxCurve(rate);

    }

    public void SetStartSize(int s) {
        if(system == null) Awake();
        var m = system.main;
        m.startSize = new ParticleSystem.MinMaxCurve(s);
    }

    public void PlayDelay(float delay) {
        DOVirtual.DelayedCall(delay, system.Play);
    }

    // Update is called once per frame
    void Update() {
        if (!system.isPlaying) {
            if (deactivateOnDone) {
                gameObject.SetActive(false);
            }
            if(deactivateParentOnDone) {
                transform.parent.gameObject.SetActive(false);
            }
        }

    }
}
