﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;




public static class ListShuffleExtension {

    public static void Shuffle<T>(this List<T> list) {
        //Random rng = new Random();
        int n = list.Count;
        while (n > 1) {
            n--;
            int k = UnityEngine.Random.Range(0, n);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}

