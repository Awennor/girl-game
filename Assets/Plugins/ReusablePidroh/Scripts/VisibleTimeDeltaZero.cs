﻿using UnityEngine;
using System.Collections;

public class VisibleTimeDeltaZero : MonoBehaviour {

    new public GameObject gameObject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    gameObject.SetActive(Time.deltaTime == 0);
	}
}
