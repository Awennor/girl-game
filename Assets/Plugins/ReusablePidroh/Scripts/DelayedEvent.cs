﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class DelayedEvent{
    public float delay;
    public UnityEvent unityEvent;

    public void Call() {
        DOVirtual.DelayedCall(delay, Invokeevent);
    }

    private void Invokeevent() {
        unityEvent.Invoke();
    }
}
