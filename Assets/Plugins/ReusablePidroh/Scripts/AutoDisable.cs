﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class AutoDisable : MonoBehaviour {

    [SerializeField]
    bool disableOnStart = false;
    [SerializeField]
    bool disableOnEnable = false;
    [SerializeField]
    float timeToDisableOnEnable = 0;

    void OnEnable() {
        if(disableOnEnable) {
            if(timeToDisableOnEnable == 0) {
                Disable();
            } else {
                DOVirtual.DelayedCall(timeToDisableOnEnable, Disable);
            }
        }
    }

    private void Disable() {
        gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start () {
        if(disableOnStart)
            gameObject.active = false;
	}


	
	// Update is called once per frame
	void Update () {
	
	}
}
