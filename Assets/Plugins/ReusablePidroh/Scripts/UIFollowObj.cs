﻿using UnityEngine;
using System.Collections;

public class UIFollowObj : MonoBehaviour {
    [SerializeField]
    private GameObject target;
    [SerializeField]
    public Vector3 offSet;
    public Canvas canvas;

    public GameObject Target {
        get {
            return target;
        }

        set {
            target = value;
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (target == null) return;
        var position = Target.transform.position;
        position += offSet;
        var renderM = canvas.renderMode;
        var wantedPos = position;
        //if (renderM == RenderMode.ScreenSpaceOverlay) {
        var main = Camera.main;

        wantedPos = main.WorldToScreenPoint(position);

        if (renderM == RenderMode.ScreenSpaceCamera) {
            wantedPos = canvas.worldCamera.ScreenToWorldPoint(wantedPos);
        }

        //wantedPos += offSet;
        this.transform.position = wantedPos;
        var lP = this.transform.localPosition;
        lP.z = 0;
        this.transform.localPosition = lP;

    }
}
