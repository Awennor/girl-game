﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnityEventSequenceBehaviour : MonoBehaviour {

    [SerializeField]
    UnityEventSequence events = new UnityEventSequence();

    public bool RunOnStart = true;
    public bool RunOnEnable = false;


    public void Invoke() {
        events.Invoke();
    }

    public void AddUnit(Action p, float delay) {
        events.AddUnit(p, delay);
    }

    // Use this for initialization
    void Start() {
        if (RunOnStart) {
            Invoke();
        }
            
    }

    void OnEnable() {
        if(RunOnEnable) {
            Invoke();
        }
    }

    // Update is called once per frame
    void Update() { 

    }
}

[Serializable]
public class UnityEventSequence {
    [SerializeField]
    List<UnityEventWithDelay> Events = new List<UnityEventWithDelay>();

    public void Invoke() {
        Run();
    }

    public List<UnityEventWithDelay> Units {
        get {
            return Events;
        }

        set {
            Events = value;
        }
    }

    public bool IgnoreTimeScale {
        get {
            return ignoreTimeScale;
        }

        set {
            ignoreTimeScale = value;
        }
    }

    [SerializeField]
    bool ignoreTimeScale;
    

    internal void AddUnit(Action p, float delay) {
        Events.Add(new UnityEventWithDelay(p, delay));
    }

    public void Run() {
        var seq = DOTween.Sequence();
        seq.SetUpdate(IgnoreTimeScale);
        int countOnSeq = 0;
        for (int i = 0; i < Events.Count; i++) {
            //Debug.Log("Adding event!!!");
            var e = Events[i];
            if (e.Delay != 0) {
                seq.AppendInterval(e.Delay);
                countOnSeq++;
            }
            if(countOnSeq > 0) {
                seq.AppendCallback(e.Event.Invoke);
                countOnSeq++;
            } else {
                e.Event.Invoke(); //avoids using Tween when there is no delay
                //Debug.Log("INVOKE NOW");
            }
            

        }
    }
}

[Serializable]
public class UnityEventWithDelay {

    [SerializeField]
    UnityEvent e;
    [SerializeField]
    float delay;

    public UnityEventWithDelay(Action p, float delay) {
        e = new UnityEvent();
        e.AddListener(() => {
            p();
            //Debug.Log("EVENT!!!");
        });
        Delay = delay;
    }

    public float Delay {
        get {
            return delay;
        }

        set {
            delay = value;
        }
    }

    public UnityEvent Event {
        get {
            return e;
        }

        set {
            e = value;
        }
    }
}
