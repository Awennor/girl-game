﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToggleGroupHelper : MonoBehaviour {

    public Toggle[] toggles;

	// Use this for initialization
	void Start () {
	
	}

    public void ActivateToggle(int which) {
        for (int i = 0; i < toggles.Length; i++) {
            toggles[i].isOn = false;
        }
        toggles[which].isOn = true;

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
