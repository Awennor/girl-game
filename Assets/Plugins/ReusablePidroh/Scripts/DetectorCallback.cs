﻿using UnityEngine;
using System.Collections;
using System;

public class DetectorCallback : MonoBehaviour {

    public Action<DetectorCallback, GameObject> detected;

    void OnTriggerEnter(Collider other) {
        //Destroy(other.gameObject);
        if (detected != null)
            detected(this, other.gameObject);
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
