﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;

public class MovementLinearPhysical : MonoBehaviour {
    private Invoker invoker;
    Vector3 velocity = new Vector3();
    private Rigidbody rigidBody;

    public Vector3 Velocity
    {
        get
        {
            return velocity;
        }

        set
        {
            velocity = value;
        }
    }

    // Use this for initialization
    void Start () {
        if (invoker == null)
            invoker = gameObject.GetComponent<Invoker>();
        rigidBody = GetComponent<Rigidbody>();
	}

    public void Disable(float delay) {
        if(invoker== null)
            invoker = gameObject.GetComponent<Invoker>();
        
        invoker.AddAction(Disable, delay);
    }

    public void Disable() {
        enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        rigidBody.velocity = velocity;
        //transform.position = transform.position + velocity * Time.deltaTime;
	}
}
