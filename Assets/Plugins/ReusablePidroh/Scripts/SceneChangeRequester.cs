﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
namespace ReusablePidroh.Scenes {
    public class SceneChangeRequester : MonoBehaviour {

        public string scenename;


        public UnityEvent sceneUnloadedAfterRequest;
        private bool awaitingUnload;

        // Use this for initialization
        void Start() {
            SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;
        }
        public void UnloadScene() {
            SceneManager.UnloadSceneAsync(scenename);
        }
        public void StartScene() {
            awaitingUnload = true;
            SceneManager.LoadScene(scenename, LoadSceneMode.Additive);
        }
        //public void StartScene(Scene scene) {}

        private void SceneManager_sceneUnloaded(Scene arg0) {
            if(arg0.name.Equals(scenename) && awaitingUnload) {
                awaitingUnload = false;
                sceneUnloadedAfterRequest.Invoke();
            }
        }

        // Update is called once per frame
        void Update() {

        }
    }
}
