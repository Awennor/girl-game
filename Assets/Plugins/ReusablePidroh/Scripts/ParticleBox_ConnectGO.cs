﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ParticleBox_ConnectGO : MonoBehaviour {

    public GameObject obj;
    public GameObject obj2;
    private ParticleSystem particle;
    public bool executeOnce;

    public void Connect(GameObject obj, GameObject obj2) {
        

        var dis = obj.transform.position - obj2.transform.position;
        var shape = particle.shape;
        var box = shape.box;
        box.z = dis.magnitude;
        shape.box = box;

        var newPos = (obj.transform.position + obj2.transform.position)/2;
        this.transform.position = newPos;
        this.transform.LookAt(obj.transform);
    }

    public void Connect() {
        Connect(obj,obj2);
    }

	// Use this for initialization
	void Start () {
		particle = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		if(executeOnce) {
            executeOnce = false;
            Connect();
        }
	}
}
