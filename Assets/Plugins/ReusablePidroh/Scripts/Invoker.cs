﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.ReusablePidroh.Scripts {
    public class Invoker : MonoBehaviour {
        List<float> delays = new List<float>();
        List<Action> actions = new List<Action>();
        List<int> tags = new List<int>();

        public void AddAction(Action a, float d) {
            int tag = 0;
            AddAction(a, d, tag);
        }

        public void AddAction(Action a, float d, int tag) {
            delays.Add(d);
            actions.Add(a);
            tags.Add(tag);
        }

        public bool AddActionUniqueReplaceHigherDelay(Action a, float delay) {
            for (int i = 0; i < actions.Count; i++) {
                if (actions[i] == a) {
                    if (delays[i] < delay) {
                        RemoveAt(i);
                    } else {
                        return false;
                    }
                }
            }
            AddAction(a, delay);
            return true;
        }

        private void RemoveAt(int i) {
            actions.RemoveAt(i);
            delays.RemoveAt(i);
            tags.RemoveAt(i);
        }

        public void AddActionUnique(Action a, float d) {
            removeAllActionsLikeThis(a);
            AddAction(a, d);
        }

        public void AddAction(float v, object stop) {
            throw new NotImplementedException();
        }

        void Update() {
            float delta = Time.deltaTime;
            for (int i = 0; i < delays.Count; i++) {
                float f = delays[i];
                f -= delta;
                if (f < 0) {
                    Action a = actions[i];
                    RemoveAt(i);
                    a();
                } else {
                    delays[i] = f;
                }

            }
        }

        public void Invoke(object enable, float v) {
            throw new NotImplementedException();
        }

        public void removeAllActionsLikeThis(Action action) {
            while (true) {
                int indexOf = actions.IndexOf(action);
                if (indexOf >= 0) {
                    RemoveAt(indexOf);
                } else {
                    break;
                }
            }

        }

        public void ClearAll() {
            actions.Clear();
            delays.Clear();
            tags.Clear();
        }

        public void ClearAllTagged(int tag) {
            for (int i = 0; i < tags.Count; i++) {
                if(tags[i] == tag) {
                    RemoveAt(i);
                }
            }
        }
    }
}
