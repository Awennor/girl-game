﻿using UnityEngine;
using System.Collections;
using System;

public class FaceGameObject : MonoBehaviour {
    private Rigidbody rigidBody;
    private Quaternion rotOld;
    GameObject target;
    public float rotationSpeed = 12f;
    public bool instantRotation = false;

    public GameObject Target
    {
        get
        {
            return target;
        }

        set
        {
            target = value;
        }
    }

    public float RotationSpeed
    {
        get
        {
            return rotationSpeed;
        }

        set
        {
            rotationSpeed = value;
        }
    }

    // Use this for initialization
    void Start () {
        rigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Target == null)
            return;
        Quaternion newRotation = GetTargetRotation();
        if(rigidBody != null) 
            rotOld = rigidBody.rotation;
        else
            rotOld = transform.rotation;
        
        var euler = rotOld.eulerAngles;
        euler.y = newRotation.eulerAngles.y;
        newRotation.eulerAngles =  euler;
        if (instantRotation == true) {
        } else {
            newRotation = Quaternion.Slerp(rotOld, newRotation, Time.deltaTime * rotationSpeed);
        }

        //newRotation = (rotOld + newRotation) / 2;
        if(rigidBody != null)
            rigidBody.MoveRotation(newRotation);
        else
            transform.rotation = newRotation;

    }

    private Quaternion GetTargetRotation() {
        if(target == null) return transform.rotation;
        Vector3 v = transform.position;
        if (Target != null)
            v = Target.transform.position - v;
        Quaternion newRotation = Quaternion.LookRotation(v);
        return newRotation;
    }

    public void FaceNow() {
        var q = GetTargetRotation();
        
        var euler = transform.localRotation.eulerAngles;
        euler.y = q.eulerAngles.y;
        q.eulerAngles = euler;
        transform.localRotation = q;
    }
}
