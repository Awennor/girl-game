﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.ReusablePidroh.Scripts;
using DG.Tweening;

public class SoundSingleton{

    static SoundSingleton instance;
    private GameObject gameObject;
    private Invoker invoker;
    Dictionary<string, AudioSource> sounds = new Dictionary<string, AudioSource>();

    public static SoundSingleton Instance {
        get {
            if(instance == null) {
                instance = new SoundSingleton();
                var gameObject = new GameObject();
                gameObject.name = "SoundAuxiliar";
                GameObject.DontDestroyOnLoad(gameObject);
                instance.invoker = gameObject.AddComponent<Invoker>();
            }
            return instance;
        }

        set {
            instance = value;
        }
    }

    public void AddSound(string key, AudioSource aS) {
        sounds.Add(key, aS);
    }

    public bool hasAny() {
        return sounds.Count > 0;
    }

    public void StopSoundDelay(string sound, float delay) {
        invoker.AddAction(()=> {StopSound(sound); }, delay);

    }

    private void StopSound(string v) {
         if(sounds.ContainsKey(v)) {
            sounds[v].Stop();
            
        } 
    }

    public void PlaySound(string v, float delay) {
        
        if(sounds.ContainsKey(v)) {
            invoker.AddAction(()=> {sounds[v].Play(); }, delay);    
            
        } else {
            Debug.Log("SOUND NOT FOUND!!! key is "+v);
        }
        
    }

    public void PlaySoundOnPause(string v, float delay) {
        
        if(sounds.ContainsKey(v)) {
            DOVirtual.DelayedCall(delay, ()=> {
                sounds[v].ignoreListenerPause = true;
                sounds[v].Play();
            });
            //invoker.AddAction(, delay);    
            
        } else {
            Debug.Log("SOUND NOT FOUND!!! key is "+v);
        }
        
    }

    public void PlaySoundOnPause(string v) {
        if(sounds.ContainsKey(v)) {
            sounds[v].ignoreListenerPause = true;
            sounds[v].Play();
            //sounds[v].ignoreListenerPause = false;
            
        } else {
            Debug.Log("SOUND NOT FOUND!!! key is "+v);
        }
        
    }

    public void PlaySound(string v) {
        if(sounds.ContainsKey(v)) {
            sounds[v].Play();
            
        } else {
            Debug.Log("SOUND NOT FOUND!!! key is "+v);
        }
        
    }
}
