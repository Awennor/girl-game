﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class AlwaysFaceMainCamera : MonoBehaviour {

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if(Camera.main != null && gameObject.activeSelf) {
            transform.LookAt(-Camera.main.transform.forward+transform.position);
            //transform.LookAt
            //transform.rotation = Camera.main.transform.rotation;
        }
	        
            
	}
}
