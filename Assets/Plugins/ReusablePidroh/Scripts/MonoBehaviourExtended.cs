﻿using UnityEngine;
using System.Collections;
using System;

public class MonoBehaviourExtended : MonoBehaviour {
    private bool started;
    // Use this for initialization
    private Action afterStart;

	protected void EndStart () {
        
        started = true;
        if (afterStart != null) {
            
            afterStart();
        } 
	}

    public void AfterStartCallback(Action a) {
        if (started) a();
        else {
            afterStart += a;
            
        }
            
    }


}
