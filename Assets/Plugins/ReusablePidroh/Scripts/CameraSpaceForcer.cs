﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class CameraSpaceForcer : MonoBehaviour {

    public List<GameObjectPair> pairs = new List<GameObjectPair>();

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    var camera = Camera.main;
        if(camera == null) return;
        transform.position = camera.transform.position;
        transform.rotation = camera.transform.rotation;
        transform.localScale = camera.transform.localScale;

        for (int i = 0; i < pairs.Count; i++) {
            if(pairs[i].obj1 == null) continue;
            var positionOf2 = pairs[i].obj2.transform.position;
            positionOf2 = transform.InverseTransformPoint(positionOf2);
            positionOf2.z = 1;
            positionOf2 = transform.TransformPoint(positionOf2);
            pairs[i].obj1.transform.position = positionOf2;

            
        }
	}
}

[Serializable]
public class GameObjectPair{
    public GameObject obj1;
    public GameObject obj2;
}
