﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class MonobehaviourExtensionMethods {

    static public void FillDictionaryWithChildrenComponent<T>(this GameObject obj, Dictionary<string, T> dic) where T : Component {
        int cc = obj.transform.childCount;
        for (int i = 0; i < cc; i++) {
            var child = obj.transform.GetChild(i);
            var comp = child.GetComponent<T>();
            dic.Add(child.name, comp);
        }
    }

    static public T GetOrAddComponent<T>(this Component child) where T : Component {
        T result = child.GetComponent<T>();
        if (result == null) {
            result = child.gameObject.AddComponent<T>();
        }
        return result;
    }

    static public GameObject GetOrAddChildWithName(this Transform t, string name) {
        var result = t.FindChild(name);
        GameObject gameObject;
        if (result == null) {
            gameObject = new GameObject();
            gameObject.name = name;
            gameObject.transform.SetParent(t);
        } else {
            gameObject = result.gameObject;
        }
        return gameObject;
    }

    static public void DestroyAllChildren(this Transform t) {
        while (t.childCount > 0) {
            Transform child = t.GetChild(0);
            child.parent = null;
            GameObject.DestroyImmediate(child.gameObject);
        }
    }

    static public void RotateYAxisToTarget(this Transform transform, Transform t) {
        Quaternion targetRotation = Quaternion.LookRotation(t.transform.position - transform.position);
        var originalEuler = transform.rotation.eulerAngles;
        var euler = targetRotation.eulerAngles;
        euler.z = originalEuler.z;
        euler.x = originalEuler.x;
        euler.y -= 90;
        Debug.Log(euler);
        targetRotation = Quaternion.Euler(euler);
        //euler.

        // Smoothly rotate towards the target point.
        transform.rotation = targetRotation;
    }

    static public void ActionOnAllChildrenComponent<T>(this GameObject obj, Action<T> action) where T : Component {
        int cc = obj.transform.childCount;
        for (int i = 0; i < cc; i++) {
            var child = obj.transform.GetChild(i);
            var comp = child.GetComponent<T>();
            if (comp != null && action != null)
                action(comp);
        }
    }
}
