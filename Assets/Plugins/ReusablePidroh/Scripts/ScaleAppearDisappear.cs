﻿using UnityEngine;
using System.Collections;
using System;

public class ScaleAppearDisappear : MonoBehaviour {

    [SerializeField]
    float scaleBySecond;
    Vector3 scaleTarget;

    public void Disappear() {
        this.enabled = true;
        this.scaleTarget = new Vector3(0, 0, 0);
    }

    public void Appear() {
        this.gameObject.SetActive(true);
        var scaleTargetLocal = new Vector3(1, 1, 1);
        var selfScale = transform.localScale;
        bool needGrowX = NeedGrowX(scaleTargetLocal, selfScale);
        bool needGrowY = NeedGrowY(scaleTargetLocal, selfScale);
        bool needGrowZ = NeedGrowZ(scaleTargetLocal, selfScale);
        if (needGrowX || needGrowY || needGrowZ) {

            scaleTarget = scaleTargetLocal;
            enabled = true;
        }
    }

    public void Show(bool b) {
        if (b) Appear();
        else Disappear();
    }



    // Update is called once per frame
    void Update() {


        var selfScale = transform.localScale;
        if (selfScale != scaleTarget) {

            var diff = scaleTarget - selfScale;
            selfScale += diff.normalized * scaleBySecond * Time.deltaTime;
            var diff2 = scaleTarget - selfScale;
            Debug.Log("truy1 " + diff);
            //Debug.Log("truy1 " + local);
            if (diff.normalized != diff2.normalized) {
                selfScale = endScaling();
            }
        } else {
            selfScale = endScaling();
        }
        this.transform.localScale = selfScale;





    }

    private Vector3 endScaling() {
        Vector3 selfScale = scaleTarget;
        if (selfScale == Vector3.zero) {
            this.gameObject.SetActive(true);
        }
        enabled = false;
        return selfScale;
    }

    private static bool NeedGrowZ(Vector3 scaleTargetLocal, Vector3 selfScale) {
        return selfScale.z < scaleTargetLocal.z;
    }

    private static bool NeedGrowY(Vector3 scaleTargetLocal, Vector3 selfScale) {
        return selfScale.y < scaleTargetLocal.y;
    }

    private bool NeedGrowX(Vector3 scaleTargetLocal, Vector3 selfScale) {
        return selfScale.x < scaleTargetLocal.x;
    }


}
