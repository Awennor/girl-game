﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

public class SelectableWrapper : MonoBehaviour, 
    ISelectHandler, IPointerEnterHandler, IPointerExitHandler {
    //private EventTrigger eventTrigger;
    public string selectedSound;
    public UnityEvent Selected;
    public UnityEvent MouseOver;
    public UnityEvent MouseOverLeave;

    // Use this for initialization
    void Start() {
        //if()
        //  eventTrigger = GetComponent<EventTrigger>();



    }

    public void OnSelect(BaseEventData eventData) {
        Selected.Invoke();
        if (selectedSound.Length > 0) {
            SoundSingleton.Instance.PlaySoundOnPause(selectedSound);
        }
    }

    // Update is called once per frame
    void Update() {

    }

    public void OnPointerEnter(PointerEventData eventData) {
        MouseOver.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData) {
        MouseOverLeave.Invoke();
    }
}
