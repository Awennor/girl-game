﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

class DOTweenCustomComponent : MonoBehaviour {

    public TweenUnit[] tweenUnits;

    public bool tweenOnEnable = true;
    public bool loop = false;
    private SpriteRenderer spriteRenderer;
    private Graphic graphic;

    public void OnEnable() {
        var seq = DOTween.Sequence().SetTarget(this);
        if (loop) seq.SetLoops(-1);
        for (int i = 0; i < tweenUnits.Length; i++) {
            var tU = tweenUnits[i];
            var type = tU.tweenType;
            graphic = GetComponent<Graphic>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            switch (type) {
                case DOTweenCompEnum.FadeIn:
                    if (graphic != null) seq.Append(graphic.DOFade(1, tU.time));
                    if (spriteRenderer != null) seq.Append(spriteRenderer.DOFade(1, tU.time));
                    break;
                case DOTweenCompEnum.FadeOut:
                    if (graphic != null) seq.Append(graphic.DOFade(0, tU.time));
                    if (spriteRenderer != null) seq.Append(spriteRenderer.DOFade(0, tU.time));
                    break;
                case DOTweenCompEnum.Fade:
                    if (graphic != null) seq.Append(graphic.DOFade(tU.value, tU.time));
                    if (spriteRenderer != null) seq.Append(spriteRenderer.DOFade(tU.value, tU.time));
                    break;
                case DOTweenCompEnum.Callback:
                    if (tU.time > 0)
                        seq.AppendInterval(tU.time);
                    seq.AppendCallback(tU.uE.Invoke);
                    break;
                case DOTweenCompEnum.TimeScale:
                    if (tU.time > 0)
                        seq.AppendInterval(tU.time);
                    seq.AppendCallback(tU.EnforceTimescale);
                    break;
                case DOTweenCompEnum.Active:
                    if (tU.time > 0)
                        seq.AppendInterval(tU.time);
                    seq.AppendCallback(() => {
                        gameObject.SetActive(tU.valueB);
                    });
                    break;

                default:
                    break;
            }
        }
    }

    [Serializable]
    public class TweenUnit {

        public DOTweenCompEnum tweenType;
        public UnityEvent uE;
        public float time;
        public float value;
        public bool valueB;

        public void EnforceTimescale() {
            Time.timeScale = value;
        }

        internal void EnforceActive() {

        }
    }

    public void OnDestroy() {
        this.DOKill();
    }
}



public enum DOTweenCompEnum {
    FadeIn, Callback, TimeScale, Active, FadeOut,
    Fade,
}


