﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class CharacterControllerWrapper : MonoBehaviour {
    CharacterController characterController;

    Vector3 motion = new Vector3();
    private bool gravityAffect = true;
    float gravityScale = 1;

    public void AddModifier(CharacterControllerWrapperModifier modi)
    {
        modifiers.Add(modi);
    }

    List<CharacterControllerWrapperModifier> modifiers = new List<CharacterControllerWrapperModifier>();

    public bool GravityAffect
    {
        get
        {
            return gravityAffect;
        }

        set
        {
            gravityAffect = value;
        }
    }

    public float GravityScale {
        get {
            return gravityScale;
        }

        set {
            gravityScale = value;
        }
    }

    public Vector3 Speed {
        get {
            return motion;
        }

        set {
            motion = value;
        }
    }

    void Start () {
        //Vector3.zero = 
        characterController = GetComponent<CharacterController>();
	}

	void Update () {
        if (GravityAffect )
        {
            //Debug.Log("Cancel motion3");
            
            if (characterController.isGrounded)
            {
                if (Physics.gravity.y < 0 && Speed.y < 0) {
                    //motion.y -= Physics.gravity.y*Time.deltaTime;
                    //Debug.Log("Cancel motion");
                }
                //Debug.Log("Cancel motion2");
            }
            else {
                Speed += Physics.gravity*Time.deltaTime*gravityScale;
            }
            
        }
        for (int i = 0; i < modifiers.Count; i++)
        {
            Speed = modifiers[i].TryModify(Speed);
        }
        //Debug.Log(motion);
        characterController.Move(Speed * Time.deltaTime);
        
	}

    public void SetSpeed(Vector3 movement)
    {
        Speed = movement;
    }

    public void ZeroSpeed()
    {
        Speed = new Vector3(0,0,0);
    }

    public float getSpeedY() {
        return Speed.y;
    }

    public bool isGround() {
        return characterController.isGrounded;
    }
}
