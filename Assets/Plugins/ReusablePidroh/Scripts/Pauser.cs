﻿using UnityEngine;
using System.Collections;

public class Pauser : MonoBehaviour {

    public bool PauseOnEnable;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnEnable() {
        if(PauseOnEnable) {
            Pause(true);
        }
    }

    public void Pause(bool p) {
        if(p) {
            Time.timeScale = 0;
        } else {
            Time.timeScale = 1;
        }
    }
}
