﻿using UnityEngine;
using System.Collections;

public class HidableGUI : MonoBehaviour {

    public bool hideOnStart;
    bool hidden = true;

    public bool Hidden
    {
        get
        {
            return hidden;
        }

        set
        {
            hidden = value;
        }
    }

    public void Hide(bool hide) {
        Hidden = hide;
        gameObject.SetActive(!hide);
        if (hide)
        {
            
            //transform.localScale = new Vector3(0, 0, 0);
        }
        else {
            //transform.localScale = new Vector3(1,1,1);
        }
    }

	// Use this for initialization
	void Start () {
        if (hideOnStart)
            Hide(true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
