﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Colliding : MonoBehaviour {

    [SerializeField]
    List<GameObject> colliding = new List<GameObject>();

    void OnTriggerEnter(Collider col) {
        if(!colliding.Contains(col.gameObject))
            colliding.Add(col.gameObject);
    }

    void OnTriggerExit(Collider col) {
        colliding.Remove(col.gameObject);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < colliding.Count; i++) {
            if(colliding[i] == null) {
                colliding.RemoveAt(i);
                i--;
            }
        }
	}

    public bool CollidingSomething() {
        return colliding.Count > 0;
    }

    public void Clear() {
        colliding.Clear();
    }
}
