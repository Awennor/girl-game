﻿using UnityEngine;
using System.Collections;
using System;
using Assets.ReusablePidroh.Scripts.Logic;

namespace Assets.ReusablePidroh.Scripts
{
    public class MainDataTimeConnect : MonoBehaviour {

    Action<float> update;

	// Use this for initialization
	void Start () {
        update = MainDataHolder.Instance.UpdateCenter.Update;


	
	}
	
	// Update is called once per frame
	void Update () {
        if(update != null)
            update(Time.deltaTime);
	}
}
}