﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class EnableCallback : MonoBehaviour {

    public UnityEvent OnEnableCallback;

	// Use this for initialization
	void Start () {
	
	}

    void OnEnable() {
        OnEnableCallback.Invoke();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
