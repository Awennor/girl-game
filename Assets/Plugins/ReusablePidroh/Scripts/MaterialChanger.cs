﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;
using DG.Tweening;

public class MaterialChanger : MonoBehaviour {

    [SerializeField]
    private Renderer rendererMain;
    private Material mainMaterial;


    public Renderer RendererMain {
        get {
            return RendererMain1;
        }

        set {
            RendererMain1 = value;
            
            MainMaterial = RendererMain.material;
        }
    }

    public Material MainMaterial {
        get {
            return mainMaterial;
        }

        set {
            mainMaterial = value;
        }
    }

    public Renderer RendererMain1 {
        get {
            return rendererMain;
        }

        set {
            rendererMain = value;
        }
    }

    public void ChangeMaterial(Material m) {
        if (RendererMain1 != null)
            RendererMain1.material = m;
    }

    public void ChangeMaterialShared(Material m) {
        if (RendererMain1 != null)
            RendererMain1.sharedMaterial = m;
    }

    public void revertMaterial(float delay, bool ignoreUpdate = false) {
        DOVirtual.DelayedCall(delay, revertMaterial).SetUpdate(ignoreUpdate);
        //invoker.AddAction(revertMaterial, delay);
    }

    public void revertMaterial() {
        if (RendererMain1 != null)
            RendererMain1.material = MainMaterial;
    }

    // Use this for initialization
    void Start() {


    }

    // Update is called once per frame
    void Update() {

    }

    public bool HasRenderer() {
        return RendererMain1 != null;
    }
}
