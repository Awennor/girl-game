﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class UpdateSupport : MonoBehaviour {

    public Action<float> Updates;

    internal void Update() {
        if(Updates != null)
            Updates(Time.deltaTime);
    }
    
}
