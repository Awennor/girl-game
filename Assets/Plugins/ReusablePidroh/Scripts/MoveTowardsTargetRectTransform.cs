﻿using UnityEngine;
using System.Collections;

public class MoveTowardsTargetRectTransform : MonoBehaviour {

    [SerializeField]
    RectTransform target;
    Vector3 targetAsVector = new Vector3(0, 0, 0);
    public float speed = 10;
    Vector3 aux = new Vector3();
    //public Action hit;
    Vector3 offset = new Vector3(0, 0, 0);
    Vector3 offsetLocal = new Vector3(0, 0, 0);
    public bool unscaledDelta = false;
    RectTransform rectTransform;

    public RectTransform Target {
        get {
            return target;
        }

        set {
            target = value;
        }
    }

    public float Speed {
        get {
            return speed;
        }

        set {
            speed = value;
        }
    }

    public Vector3 TargetAsVector {
        get {
            return targetAsVector;
        }

        set {
            targetAsVector = value;
        }
    }

    public Vector3 OffsetLocal {
        get {
            return offsetLocal;
        }

        set {
            offsetLocal = value;
        }
    }


    // Use this for initialization
    void Start() {
        rectTransform = GetComponent<RectTransform>();
    }

    public void SetWorldOffset(float x, float y) {
        offset.x = x;
        offset.y = y;
    }

    

    internal void Teleport() {
        this.rectTransform.position = TargetPosition();
    }

    // Update is called once per frame
    void Update() {

        if (target != null) {
            rectTransform.position = TargetPosition();
            //var lP = rectTransform.localPosition;
            rectTransform.localPosition = rectTransform.localPosition + offsetLocal;
            
        }
            //transform.position = target.position;
            

        /*
        if (!isActiveAndEnabled) return;

        Vector3 tpos = TargetPosition();
        Vector3 originalTpos = tpos;
        tpos = tpos - rectTransform.position;
        float dis2 = tpos.sqrMagnitude;
        if (unscaledDelta) {
            tpos = tpos.normalized * Speed * Time.unscaledDeltaTime;
        } else {
            tpos = tpos.normalized * Speed * Time.deltaTime;
        }

        bool collision = dis2 < tpos.sqrMagnitude;
        rectTransform.position = rectTransform.position + tpos;

        if (collision) {
            rectTransform.position = originalTpos;
        }
        /*if (collision && hit != null) {
            hit();
        }*/
    }

    private Vector3 TargetPosition() {
        var tpos = TargetAsVector;
        if (target != null)
            tpos = Target.position;
        tpos += offset;

        //if(offsetLocal.sqrMagnitude != 0)
          //  tpos += rectTransform.InverseTransformPoint(OffsetLocal);
        return tpos;
    }
}
