﻿using UnityEngine;
using System.Collections;

public class CopyRectTransform : MonoBehaviour {

    public RectTransform own;
    public RectTransform target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    own.position = target.position;
        own.anchorMin = target.anchorMin;
        own.anchorMax = own.anchorMax;
        own.sizeDelta = target.sizeDelta;
	}
}
