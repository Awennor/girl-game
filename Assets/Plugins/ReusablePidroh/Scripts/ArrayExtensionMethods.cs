﻿using UnityEngine;

public static class ArrayExtensionMethods {

    public static void AEXAddValue(this float[] v, float value) {
        for (int i = 0; i < v.Length; i++) {
            v[i] += value;
        }
    }

    public static void AEXZeroArray(this int[] v) {
        for (int i = 0; i < v.Length; i++) {
            v[i] = 0;
        }
    }

    public static void AEXZeroArray(this float[] v) {
        AEXSetArray(v, 0);
    }

    public static void AEXSetArray(this float[] v, float value) {
        for (int i = 0; i < v.Length; i++) {
            v[i] = value;
        }
    }

    public static void AEXCopyElementsThatAreBigger(this float[] v, float[] v2) {
        for (int i = 0; i < v.Length && i < v2.Length; i++) {
            if (v[i] < v2[i]) {
                v[i] = v2[i];
            }
        }
    }

    public static void AEXMultiply(this float[] v, float[] v2) {
        for (int i = 0; i < v.Length && i < v2.Length; i++) {

            v[i] *= v2[i];

        }
    }
}