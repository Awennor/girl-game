﻿using Assets.ReusablePidroh.Scripts.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.ReusablePidroh.Scripts
{
    public class ScaleToRangeValue 
    {
        RangedValue rv;
     
        public RectTransform RectT { get; set; }

        public RangedValue RV
        {
            get
            {
                return rv;
            }

            set
            {
                if(rv != null)
                    rv.ValueChanged -= update;
                rv = value;
                rv.ValueChanged += update;
                update(RV);
            }
        }


        private void update(RangedValue rv)
        {
            if (RectT != null) {
                Vector3 lC = RectT.localScale;
                lC.x = rv.Scale();
                
                RectT.localScale = lC;
            }
            
        }
    }
}
