﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class Vector3Special
    {
    public static Vector3 vectorXZRandomDirection() {
        var rot = Quaternion.AngleAxis(UnityEngine.Random.Range(0,360), Vector3.up);
        
        return rot * Vector3.forward;
    }
    }