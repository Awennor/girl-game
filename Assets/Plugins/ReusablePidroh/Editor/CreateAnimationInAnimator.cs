﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class CreateAnimationToAnimator : ScriptableWizard {

    public Object animator;
    public string animationName = "";

    [MenuItem("Tools Ex/Create Anim in Animator")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard("Create Anim in Animator", typeof(CreateAnimationToAnimator));
    }

    void OnWizardCreate()
    {
        if(animator == null) {
            var obj = Selection.activeObject;
            if(AssetDatabase.IsMainAsset(obj)) {
                animator = obj;
            }
        }
        if (animator != null && animationName != null)
        {
            var animation = new AnimationClip();
            //Debug.Log(animation+"a");
		    //AssetDatabase.CreateAsset(animation, "Assets/"+animationPath);
            animation.name = animationName;
            AssetDatabase.AddObjectToAsset(animation, animator);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(animation));
            

        }
    }
}
