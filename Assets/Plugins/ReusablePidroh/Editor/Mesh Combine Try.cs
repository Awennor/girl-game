﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class MeshCombineTry : ScriptableWizard {

    public GameObject meshToCombine;

    [MenuItem("Tools/Combine Meshes")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard("Combine Mesh", typeof(MeshCombineTry));
    }

    void OnWizardCreate()
    {

        
        if (meshToCombine == null) {
            meshToCombine = Selection.activeObject as GameObject;
        }
        
        return;
        if (meshToCombine != null)
        {
            //Debug.Log("Mesh To Combine name "+meshToCombine.name);
            MeshFilter[] meshFilters = meshToCombine.GetComponentsInChildren<MeshFilter>();
            CombineInstance[] combine = new CombineInstance[meshFilters.Length];

            List<Material> mats = new List<Material>();

            int i = 0;
            while (i < meshFilters.Length)
            {
                combine[i].mesh = meshFilters[i].sharedMesh;
                combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
                //meshFilters[i].gameObject.active = false;
                var meshRenderer = meshFilters[i].gameObject.GetComponent<MeshRenderer>();
                meshRenderer.enabled = false;
                Material mat = meshRenderer.sharedMaterial;
                if (mats.Count < 1) mats.Add(mat);
                //if (!mats.Contains(mat)) mats.Add(mat);
                i++;
            }
            

            GameObject combinedObject = new GameObject("CombinedMesh");
            combinedObject.transform.SetParent(meshToCombine.transform);
            Mesh combinedMesh = new Mesh();
            combinedMesh.CombineMeshes(combine, (mats.Count <= 1));
            var objectName = meshToCombine.name;
            var assetName = "Assets/Graphics&Animation/MeshData/" + objectName + ".asset";
            AssetDatabase.CreateAsset(combinedMesh, assetName);

            MeshFilter filter = combinedObject.AddComponent<MeshFilter>();
            filter.mesh = AssetDatabase.LoadAssetAtPath<Mesh>(assetName) as Mesh;
            MeshRenderer renderer = combinedObject.AddComponent<MeshRenderer>();
            renderer.sharedMaterials = mats.ToArray();
            //combinedObject.GetComponent<MeshFilter>().sharedMesh.;
            //Debug.Log("Appear here!!!");

            //Object prefab = PrefabUtility.CreateEmptyPrefab("Assets/" + "CombinedObject" + ".prefab");
            //PrefabUtility.ReplacePrefab(combinedObject, prefab, ReplacePrefabOptions.ConnectToPrefab);

        }
    }
}
