﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public class MethodExecuteEditor_Window : EditorWindow {
    private GameObject selection;

    [MenuItem("Window/MethodExecuteEditor_Window")]

    public static void ShowWindow() {
        EditorWindow.GetWindow(typeof(MethodExecuteEditor_Window));
    }

    public void OnInspectorUpdate() {

        if (selection != Selection.activeGameObject) {
            selection = Selection.activeGameObject;
            Repaint();
        }


    }

    void OnGUI() {
        var target = Selection.activeGameObject;
        if (target == null) return;
        var comps = target.GetComponents<Component>();
        foreach (var comp in comps) {
            if (comp != null) {
                var type = comp.GetType();
                //Debug.Log("IAM HERE");
                // Iterate over each private or public instance method (no static methods atm)
                foreach (var method in type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)) {
                    //Debug.Log("IAM HERE2");
                    // make sure it is decorated by our custom attribute
                    var attributes = method.GetCustomAttributes(typeof(ExposeMethodInEditorAttribute), true);
                    if (attributes.Length > 0) {

                        if (GUILayout.Button(method.Name + " - " + comp.GetType())) {
                            method.Invoke(comp, null);
                        }
                    }
                }
            }

        }

    }

}
