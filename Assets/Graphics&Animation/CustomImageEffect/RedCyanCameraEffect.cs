﻿using UnityEngine;
using System.Collections;
 
[ExecuteInEditMode]
public class RedCyanCameraEffect : MonoBehaviour {
 
	public float intensity;
	private Material material;
 
	// Creates a private material used to the effect
	void Start ()
	{
		material = new Material( Shader.Find("Hidden/REDCYANeffect") );
	}

    void OnDestroy () {
        DestroyImmediate(material);
    }
	
	// Postprocess the image
	void OnRenderImage (RenderTexture source, RenderTexture destination)
	{
        if(material == null) return;
		if (intensity == 0 )
		{
			Graphics.Blit (source, destination);
			return;
		}
 
		material.SetFloat("_bwBlend", intensity);
		Graphics.Blit (source, destination, material);
	}
}