﻿
Shader "Hidden/ComplimentaryColorShader" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_bwBlend("Black & White blend", Range(0, 1)) = 0
			_yInfluence("", Range(0, 1)) = 0
			_xInfluence("", Range(0, 1)) = 0
		_Color("Main Color", Color) = (1,0,0,1)
	}
		SubShader{
		Pass{
		ZTest Always Cull Off ZWrite Off
		CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"

		uniform sampler2D _MainTex;
	uniform float _bwBlend;
	uniform float _yInfluence;
	uniform float _xInfluence;
	fixed4 _Color;

	float4 frag(v2f_img i) : COLOR{
		float4 c = tex2D(_MainTex, i.uv);

		float lum = c.r*.3 + c.g*.59 + c.b*.11;
		
		float3 bw = float3(lum, lum, lum);
		
		//bw = c * i.pos;
		float yratio = i.pos.y / _ScreenParams.y;
		float xratio = i.pos.x / _ScreenParams.x;
		
		/*true code*/
		float factor = 
			(yratio - 0.5)*_yInfluence + (xratio-0.5)*_xInfluence + 1;

		bw = _Color * c * factor + (1 - _Color) * bw;
		/*true code*/
		


		float4 result = c;
		result.rgb = lerp(c.rgb, bw, _bwBlend);
		return result;
	}
		ENDCG
	}
	}
}