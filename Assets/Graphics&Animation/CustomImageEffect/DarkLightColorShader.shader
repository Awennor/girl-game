﻿
Shader "Hidden/LightDarknessColors" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		//_ColorL("Light Color", Color) = (0.63,0.96,0,1)
			_ColorL("Light Color", Color) = (1,0,0,1)
		//_ColorS("Shadow", Color) = (0.14,0.09,0.12,1)
				_ColorS("Shadow", Color) = (0,0,0,1)

		_bwBlend("Black & White blend", Range(0, 1)) = 0
	}
		SubShader{
		Pass{
		ZTest Always Cull Off ZWrite Off
		CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"

		uniform sampler2D _MainTex;
	uniform float _bwBlend;
	
	fixed4 _ColorL;
	fixed4 _ColorS;

	float4 frag(v2f_img i) : COLOR{
		float4 c = tex2D(_MainTex, i.uv);

		float lum = c.r*.3 + c.g*.59 + c.b*.11;

		//float3 bw = float3(lum, lum, lum);
		float3 bw = c.rgb;

		//lum *= lum;
		//bw = _ColorS;
		float lum2 = lum*5;
		lum2 = clamp(lum2, 0, 1);

		float lum3 = lum / 5;
		lum3 = clamp(lum3, 0, 1);
		
		bw = bw * lum2 + _ColorS*(1 - lum2);
		bw = bw * (1-lum3) + _ColorL*(lum3);

		//bw = c * i.pos;
		//float y = i.pos.y / _ScreenParams.y;
		//float x = i.pos.x / _ScreenParams.x;


		//bw.r = c.r;
		//bw.r = c.r*((y+x));
		//bw.g = c.g*((y + x));
		//bw.b = c.b*((y + x));
		//bw.r = c.r*(1 * i.pos.y / _ScreenParams.y);
		//bw.g = c.g*(1 * i.pos.y / _ScreenParams.y);
		//bw.b = c.b*( i.pos.y / _ScreenParams.y);

		float4 result = c;
		result.rgb = lerp(c.rgb, bw, _bwBlend);
		return result;
	}
		ENDCG
	}
	}
}