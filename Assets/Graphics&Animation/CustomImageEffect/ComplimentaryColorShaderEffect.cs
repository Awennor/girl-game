﻿using UnityEngine;
using System.Collections;
 
[ExecuteInEditMode]
public class ComplimentaryColorShaderEffect : MonoBehaviour {
 
	public float intensity;
	private Material material;
    public Color mainColor;
    public float xInfluence;
    public float yInfluence;
 
	// Creates a private material used to the effect
	void Start ()
	{
		material = new Material( Shader.Find("Hidden/ComplimentaryColorShader") );
	}

    void OnDestroy () {
        DestroyImmediate(material);
    }
	
	// Postprocess the image
	void OnRenderImage (RenderTexture source, RenderTexture destination)
	{
        if(material == null) return;
		if (intensity == 0 )
		{
			Graphics.Blit (source, destination);
			return;
		}
 
		material.SetFloat("_bwBlend", intensity);
        material.SetFloat("_yInfluence", yInfluence);
        material.SetFloat("_xInfluence", xInfluence);
        material.SetColor("_Color", mainColor);
		Graphics.Blit (source, destination, material);
	}
}