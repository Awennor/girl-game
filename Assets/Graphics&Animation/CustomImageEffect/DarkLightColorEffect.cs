﻿using UnityEngine;
using System.Collections;
 
[ExecuteInEditMode]
public class DarkLightColorEffect : MonoBehaviour {
 
	public float intensity;
	private Material material;
    public Color LightColor;
    public Color ShadowColor;
 
	// Creates a private material used to the effect
	void Start ()
	{
		material = new Material( Shader.Find("Hidden/LightDarknessColors") );
	}

    void OnDestroy () {
        DestroyImmediate(material);
    }
	
	// Postprocess the image
	void OnRenderImage (RenderTexture source, RenderTexture destination)
	{
        if(material == null) return;
		if (intensity == 0 )
		{
			Graphics.Blit (source, destination);
			return;
		}
 
		material.SetFloat("_bwBlend", intensity);
        material.SetColor("_ColorS", ShadowColor);
        material.SetColor("_ColorL", LightColor);
		Graphics.Blit (source, destination, material);
	}
}