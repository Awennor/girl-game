﻿
Shader "Hidden/REDCYANeffect" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_bwBlend("Black & White blend", Range(0, 1)) = 0
	}
		SubShader{
		Pass{
		ZTest Always Cull Off ZWrite Off
		CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"

		uniform sampler2D _MainTex;
	uniform float _bwBlend;

	float4 frag(v2f_img i) : COLOR{
		float4 c = tex2D(_MainTex, i.uv);

		float lum = c.r*.3 + c.g*.59 + c.b*.11;
		
		float3 bw = float3(lum, lum, lum);
		
		//bw = c * i.pos;
		float yratio = i.pos.y / _ScreenParams.y;
		float xratio = i.pos.x / _ScreenParams.x;
		
		//bw.r = c.r;
		bw.r = c.r*((yratio+xratio))/2;
		//bw.r = c.r;
		//bw.g = c.g*((y + x));
		//bw.b = c.b*((y + x));
		//bw.r = c.r*(1 * i.pos.y / _ScreenParams.y);
		//bw.g = c.g*(1 * i.pos.y / _ScreenParams.y);
		//bw.b = c.b*( i.pos.y / _ScreenParams.y);

		float4 result = c;
		result.rgb = lerp(c.rgb, bw, _bwBlend);
		return result;
	}
		ENDCG
	}
	}
}