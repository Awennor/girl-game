﻿//Shows the grayscale of the depth from the camera.

Shader "Hidden/BWDiffuse"
{
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {}
	}
	SubShader
	{
		Tags{ "RenderType" = "Opaque" }

		Pass
	{

		CGPROGRAM
#pragma target 3.0
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

		uniform sampler2D _CameraDepthTexture; //the depth texture

	struct v2f
	{
		float4 pos : SV_POSITION;
		float4 projPos : TEXCOORD1; //Screen position of pos
		half2 uv : TEXCOORD0;
	};

	v2f vert(appdata_base v)
	{
		v2f o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		o.projPos = ComputeScreenPos(o.pos);
		o.uv = MultiplyUV(UNITY_MATRIX_TEXTURE0, v.texcoord);

		return o;
	}

	uniform sampler2D _MainTex;
	fixed4 _Color;

	half4 frag(v2f i) : COLOR
	{
		//Grab the depth value from the depth texture
		//Linear01Depth restricts this value to [0, 1]
		float depth = tex2Dproj(_CameraDepthTexture,
		UNITY_PROJ_COORD(i.projPos)).r;
	float4 c = tex2D(_MainTex, i.uv);
	//half4 c;
	//float f = 200;
	//if (depth > 0.3 ) {
		//float depth2 = 2 * depth;
		//c.rgb = c.rgb *(1-depth2)+_Color.rgb *(depth2);
		
	//}
	depth = depth * 4;
	if (depth > 1) depth = 1;
	c.r = lerp(c.r, _Color.r, depth);
	c.g = lerp(c.g, _Color.g, depth);
	c.b = lerp(c.b, _Color.b, depth);
		//c.r = c.r*(1-depth);
		//c.g = c.g*(1-depth);
		//c.b = c.b*(1-depth);
	
	c.a = 1;
	

	return c;
	}

		ENDCG
	}
	}
		FallBack "VertexLit"
}