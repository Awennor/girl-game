﻿
Shader "Hidden/DualHue" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_bwBlend("Black & White blend", Range(0, 1)) = 0
			_yInfluence("", Range(0, 1)) = 0
			_xInfluence("", Range(0, 1)) = 0
		_Color("Main Color", Color) = (1,0,0,1)
	}
		SubShader{
		Pass{
		ZTest Always Cull Off ZWrite Off
		CGPROGRAM
	#pragma vertex vert_img
	#pragma fragment frag

	#include "UnityCG.cginc"

			float3 rgb_to_hsv_no_clip(float3 RGB)
		{
			float3 HSV = 0;

			float minChannel, maxChannel;

			maxChannel = max(RGB.x, RGB.y);
			minChannel = min(RGB.x, RGB.y);

			maxChannel = max(RGB.z, maxChannel);
			minChannel = min(RGB.z, minChannel);

			HSV.z = maxChannel;

			float delta = maxChannel - minChannel;             //Delta RGB value

															   //if ( delta > 0 )
															   //{                    // If gray, leave H  S at zero
			HSV.y = delta / HSV.z;
			float3 delRGB = (HSV.zzz - RGB + 3 * delta) / (6 * delta);
			if (RGB.x == HSV.z) HSV.x = delRGB.z - delRGB.y;
			if (RGB.y == HSV.z) HSV.x = (1.0 / 3.0) + delRGB.x - delRGB.z;
			if (RGB.z == HSV.z) HSV.x = (2.0 / 3.0) + delRGB.y - delRGB.x;
			//}

			return (HSV);
		}

		float3 hsv_to_rgb(float3 HSV)
		{
			float var_h = HSV.x * 6;
			//float var_i = floor(var_h);   // Or ... var_i = floor( var_h )
			float var_1 = HSV.z * (1.0 - HSV.y);
			float var_2 = HSV.z * (1.0 - HSV.y * (var_h - floor(var_h)));
			float var_3 = HSV.z * (1.0 - HSV.y * (1 - (var_h - floor(var_h))));

			float3 RGB = float3(HSV.z, var_1, var_2);

			if (var_h < 5) { RGB = float3(var_3, var_1, HSV.z); }
			if (var_h < 4) { RGB = float3(var_1, var_2, HSV.z); }
			if (var_h < 3) { RGB = float3(var_1, HSV.z, var_3); }
			if (var_h < 2) { RGB = float3(var_2, HSV.z, var_1); }
			if (var_h < 1) { RGB = float3(HSV.z, var_3, var_1); }

			return (RGB);
		}

	uniform sampler2D _MainTex;
	uniform float _bwBlend;
	uniform float _yInfluence;
	uniform float _xInfluence;
	fixed4 _Color;

	float4 frag(v2f_img i) : COLOR{
		float4 c = tex2D(_MainTex, i.uv);
		float yratio = i.pos.y / _ScreenParams.y;
		float xratio = i.pos.x / _ScreenParams.x;
		
		float3 hsv = rgb_to_hsv_no_clip(c.rgb);
		float luminance = hsv.z;
		//hsv.x = 0;
		//hsv.y = 1;
		//float luminance = hsv.z;
		//hsv.x = (0.8-luminance*0.5f);

		//hsv.x = ((yratio)*1 + (1-yratio)*0.3f);
		float yratiomax = 0.8f;
		float yratiomin = 0.2f;
		float hue1 = 0;
		float hue2 = 0.5;
		if (yratio > yratiomax) {
			hsv.x = hue1;
		}
		else {
			if (yratio < yratiomin) {
				hsv.x = hue2;
			}
			else {
				//hsv.y = 0;
				float delta = yratiomax - yratiomin;
				float yBetweenRatio =(yratio - yratiomin) / delta;
				hsv.x = hue2 * (1-yBetweenRatio) + hue1 * yBetweenRatio;

				float sat = (yBetweenRatio - 0.5f)*2;
				if (sat < 0)sat *= -1;
				hsv.y = sat * hsv.y;
			}
		}
		
		//float sat = yratio - 0.5;
		//if (sat < 0) sat = -sat;
		//hsv.y = sat * 0.5 + hsv.y *0.5;
		//hsv.y = 1-luminance;

		c.rgb = hsv_to_rgb(hsv) * _bwBlend + c.rgb *(1-_bwBlend);

		float4 result = c;
		//result.rgb = lerp(c.rgb, bw, _bwBlend);
		return result;
	}
		ENDCG
	}
		}
}