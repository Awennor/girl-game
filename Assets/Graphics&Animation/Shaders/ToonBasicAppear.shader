Shader "Toon/BasicAppear" {
	Properties {
		_Color ("Main Color", Color) = (.5,.5,.5,1)
		_MainTex("Color (RGB) Alpha (A)", 2D) = "white"
		_ToonShade ("ToonShader Cubemap(RGB)", CUBE) = "" { }
		_HeightShown("Height Shown", Range(-20, 20)) = 0
	}


	SubShader {
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Pass {
			Name "BASE"
			Cull Off
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			samplerCUBE _ToonShade;
			float4 _MainTex_ST;
			float4 _Color;
			float _HeightShown;
			float maxY = 0;

			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float3 normal : NORMAL;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				
				float2 texcoord : TEXCOORD0;
				float3 cubenormal : TEXCOORD1;
				float4 lpos : TEXCOORD2;
				UNITY_FOG_COORDS(2)
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.lpos = v.vertex;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				//float y = o.pos.y;
				//if (y > maxY) maxY = y;
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.cubenormal = mul (UNITY_MATRIX_MV, float4(v.normal,0));
				UNITY_TRANSFER_FOG(o,o.pos);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				float yPos = i.lpos.z;
				
				if(yPos < _HeightShown) return fixed4(0,0,0,0);
				if (yPos > _HeightShown && yPos < _HeightShown+0.5) return fixed4(1, 0, 0, 1);
				fixed4 col = _Color * tex2D(_MainTex, i.texcoord);
				fixed4 cube = texCUBE(_ToonShade, i.cubenormal);
				fixed4 c = fixed4(2.0f * cube.rgb * col.rgb, col.a);
				UNITY_APPLY_FOG(i.fogCoord, c);
				return c;
			}
			ENDCG			
		}
	} 

	Fallback "VertexLit"
}
