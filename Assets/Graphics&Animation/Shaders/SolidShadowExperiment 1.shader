﻿Shader "Experiment/SolidColor2" {

	Properties{
		_Color("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
		_Color3("Shadow Color", Color) = (1,1,1,1)
		_Color4("Shadow Color 2", Color) = (1,1,1,1)
		_Rate("Oscillation Rate", Range(5, 200)) = 50
		_Scale("Scale", Range(0.02, 0.5)) = 0.25
		_Distortion("Distortion", Range(0.1, 20)) = 1
		_LuminanceBonus("LightBonus", Range(0.1, 1)) = 1

	}

	SubShader{
		Pass{

		// 1.) This will be the base forward rendering pass in which ambient, vertex, and
		// main directional light will be applied. Additional lights will need additional passes
		// using the "ForwardAdd" lightmode.
		// see: http://docs.unity3d.com/Manual/SL-PassTags.html
		Tags{ "LightMode" = "ForwardBase" }

		CGPROGRAM

#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

		// 2.) This matches the "forward base" of the LightMode tag to ensure the shader compiles
		// properly for the forward bass pass. As with the LightMode tag, for any additional lights
		// this would be changed from _fwdbase to _fwdadd.
#pragma multi_compile_fwdbase

		// 3.) Reference the Unity library that includes all the lighting shadow macros
#include "AutoLight.cginc"



		float4 _Color;
	float4 _Color2;
	float4 _Color3;
	float4 _Color4;
	float _Rate;
	float _Scale;
	float _Distortion;
	float _LuminanceBonus;

	struct v2f
	{
		float4 pos : SV_POSITION;
		float3 uv : TEXCOORD0;
		float3 vert : TEXCOORD1;

		// 4.) The LIGHTING_COORDS macro (defined in AutoLight.cginc) defines the parameters needed to sample 
		// the shadow map. The (0,1) specifies which unused TEXCOORD semantics to hold the sampled values - 
		// As I'm not using any texcoords in this shader, I can use TEXCOORD0 and TEXCOORD1 for the shadow 
		// sampling. If I was already using TEXCOORD for UV coordinates, say, I could specify
		// LIGHTING_COORDS(1,2) instead to use TEXCOORD1 and TEXCOORD2.
		LIGHTING_COORDS(2, 3)
	};


	v2f vert(appdata_base v) {
		v2f o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

		float s = 1 / _Scale;
		float t = _Time[0] * _Rate*_Scale;
		o.vert = sin((v.vertex.xyz + t) * s);
		o.uv = sin((v.texcoord.xyz + t) * s);

		// 5.) The TRANSFER_VERTEX_TO_FRAGMENT macro populates the chosen LIGHTING_COORDS in the v2f structure
		// with appropriate values to sample from the shadow/lighting map
		TRANSFER_VERTEX_TO_FRAGMENT(o);

		return o;
	}

	fixed4 frag(v2f i) : COLOR{
		float3 vert = i.vert;
		float3 uv = i.uv;
		float mix = 1 + sin((vert.x - uv.x) + (vert.y - uv.y) + (vert.z - uv.z));
		//float mix2 = 1 + sin((vert.x + uv.x) - (vert.y + uv.y) - (vert.z + uv.z)) / _Distortion;
		float mix2 = 3 - mix;
		//float mix = ((vert.y + uv.y) % 0.3) / 0.3;
		//float mix2 = ((vert.y + uv.y) % 0.3 + 0.3) / 0.3;
		half4 c = half4((_Color * mix * 0.5) + (_Color2 * mix2 * 0.5));
		//half4 c = fixed4(1.0, 0.0, 0.0, 1.0);
		// 6.) The LIGHT_ATTENUATION samples the shadowmap (using the coordinates calculated by TRANSFER_VERTEX_TO_FRAGMENT
		// and stored in the structure defined by LIGHTING_COORDS), and returns the value as a float.
		float attenuation = LIGHT_ATTENUATION(i);
		//attenuation = 1 - attenuation;

		attenuation = attenuation+_LuminanceBonus;
		if (attenuation > 1) attenuation = 1;
		c = c * attenuation;
		/*if (attenuation < 0.5) {
			c = half4((_Color3 * mix * 0.5) + (_Color4 * mix2 * 0.5));
		}*/
		return c;
	}

		ENDCG
	}
	}

		// 7.) To receive or cast a shadow, shaders must implement the appropriate "Shadow Collector" or "Shadow Caster" pass.
		// Although we haven't explicitly done so in this shader, if these passes are missing they will be read from a fallback
		// shader instead, so specify one here to import the collector/caster passes used in that fallback.
		Fallback "VertexLit"
}