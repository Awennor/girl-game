﻿Shader "Custom/CelShadScenarioForward" {
	Properties {
		_Color("Color", Color) = (1, 1, 1, 1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
		}
		LOD 200

		CGPROGRAM#pragma surface surf CelShadingForward #pragma target 3.0

		half4 LightingCelShadingForward(SurfaceOutput s, half3 lightDir, half atten) {
			
			half NdotL = dot(s.Normal, lightDir);
			//int i = ((NdotL+0.1) *5);
			//NdotL = i;
			//NdotL = NdotL/5 - 0.1;
			if(NdotL < 0.3) NdotL = 0;
			
			
			//GREAT FUNCTION START
			if(false){
			if(NdotL <= 0) NdotL = 0;
			else 
				if(NdotL <= 0.3) NdotL = 0;
				else
					if(NdotL <= 0.6) NdotL = 0.6;
				else
					if(NdotL <= 0.8) NdotL = 0.8;
				//else
				//	if(NdotL <= 0.6) NdotL = 0.6;
				//else
					//if(NdotL <= 0.9) NdotL = 0.65;
				else
					NdotL = 1;
			}
			//GREAT FUNCTION END
			
			
			
			//NdotL = 1;
			
			//else if (NdotL <= 0.9 ) NdotL = 0.5;
				//else NdotL = 1;
			//if (NdotL <= 0.0) NdotL = 0;
			//if (NdotL >= 0.25) NdotL = 1;
			//NdotL = smoothstep(0, 0.25f, NdotL);
			//NdotL = smoothstep(0.25, 0.5f, NdotL);
			
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten * 2);
			c.a = s.Alpha;
			return c;
		}

		sampler2D _MainTex;
		fixed4 _Color;

		struct Input {
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}