Shader "Standard-ganb"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo", 2D) = "white" {}

		_Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

		_Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5
		_GlossMapScale("Smoothness Scale", Range(0.0, 1.0)) = 1.0
		[Enum(Metallic Alpha,0,Albedo Alpha,1)] _SmoothnessTextureChannel("Smoothness texture channel", Float) = 0

		[Gamma] _Metallic("Metallic", Range(0.0, 1.0)) = 0.0
		_MetallicGlossMap("Metallic", 2D) = "white" {}

		[ToggleOff] _SpecularHighlights("Specular Highlights", Float) = 1.0
		[ToggleOff] _GlossyReflections("Glossy Reflections", Float) = 1.0

		_BumpScale("Scale", Float) = 1.0
		_BumpMap("Normal Map", 2D) = "bump" {}

		_Parallax("Height Scale", Range(0.005, 0.08)) = 0.02
		_ParallaxMap("Height Map", 2D) = "black" {}

		_OcclusionStrength("Strength", Range(0.0, 1.0)) = 1.0
		_OcclusionMap("Occlusion", 2D) = "white" {}

		_EmissionColor("Color", Color) = (0,0,0)
		_EmissionMap("Emission", 2D) = "white" {}

		_DetailMask("Detail Mask", 2D) = "white" {}

		_DetailAlbedoMap("Detail Albedo x2", 2D) = "grey" {}
		_DetailNormalMapScale("Scale", Float) = 1.0
		_DetailNormalMap("Normal Map", 2D) = "bump" {}

		_MaxValue("Max Color Value", Float) = 1.0
		_MinValue("Min Color Value", Float) = 0
		_MaxSat("Max Color Sat", Float) = 1.0
		_MinSat("Min Color Sat", Float) = 0
		_maxHue("Max Color Hue", Float) = 1.0
		_minHue("Min Color Hue", Float) = 0
		_noiseZ("Noise 1 zoom", Float) = 0
		_BoostSat("Boost Sat", Float) = 0
		_BoostValue("Boost Value", Float) = 0
		_BoostHue("Boost Hue", Float) = 0
		_ValueRemapStrength("Value Remap Strength", Float) = 0
		_ValueRemapPremap("Value Remap Premap multiplier", Float) = 0.5
		_noiseColor("Noise Color", Color) = (0,0,0,0)
		_noiseZB("Noise 2 zoom", Float) = 0
		_noiseColorB("Noise Color 2", Color) = (0,0,0,0)
		_BoostContrast("Boost Contrast", Float) = 0
		_MidValue("Mid Value", Float) = 0

		//_Indicator("Indicator Color", Color) = (1,1,1,1)

		[Enum(UV0,0,UV1,1)] _UVSec("UV Set for secondary textures", Float) = 0


			// Blending state
			[HideInInspector] _Mode("__mode", Float) = 0.0
			[HideInInspector] _SrcBlend("__src", Float) = 1.0
			[HideInInspector] _DstBlend("__dst", Float) = 0.0
			[HideInInspector] _ZWrite("__zw", Float) = 1.0
	}

		CGINCLUDE
#define UNITY_SETUP_BRDF_INPUT MetallicSetup
			ENDCG

			SubShader
		{
			Tags { "RenderType" = "Opaque" "PerformanceChecks" = "False" }
			LOD 300


			// ------------------------------------------------------------------
			//  Base forward pass (directional light, emission, lightmaps, ...)
			Pass
			{
				Name "FORWARD"
				Tags { "LightMode" = "ForwardBase" }

				Blend[_SrcBlend][_DstBlend]
				ZWrite[_ZWrite]

				CGPROGRAM
				#pragma target 3.0

			// -------------------------------------

			#pragma shader_feature _NORMALMAP
			#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
			#pragma shader_feature _EMISSION
			#pragma shader_feature _METALLICGLOSSMAP
			#pragma shader_feature ___ _DETAIL_MULX2
			#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
			#pragma shader_feature _ _SPECULARHIGHLIGHTS_OFF
			#pragma shader_feature _ _GLOSSYREFLECTIONS_OFF
			#pragma shader_feature _PARALLAXMAP

			#pragma multi_compile_fwdbase
			#pragma multi_compile_fog

			#pragma vertex vertBase
			#pragma fragment fragBase
			#include "UnityStandardCoreForward.cginc"

			ENDCG
		}
			// ------------------------------------------------------------------
			//  Additive forward pass (one light per pass)
			Pass
			{
				Name "FORWARD_DELTA"
				Tags { "LightMode" = "ForwardAdd" }
				Blend[_SrcBlend] One
				Fog { Color(0,0,0,0) } // in additive pass fog should be black
				ZWrite Off
				ZTest LEqual

				CGPROGRAM
				#pragma target 3.0

			// -------------------------------------


			#pragma shader_feature _NORMALMAP
			#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
			#pragma shader_feature _METALLICGLOSSMAP
			#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
			#pragma shader_feature _ _SPECULARHIGHLIGHTS_OFF
			#pragma shader_feature ___ _DETAIL_MULX2
			#pragma shader_feature _PARALLAXMAP

			#pragma multi_compile_fwdadd_fullshadows
			#pragma multi_compile_fog


			#pragma vertex vertAdd
			#pragma fragment fragAdd
			#include "UnityStandardCoreForward.cginc"

			ENDCG
		}
			// ------------------------------------------------------------------
			//  Shadow rendering pass
			Pass {
				Name "ShadowCaster"
				Tags { "LightMode" = "ShadowCaster" }

				ZWrite On ZTest LEqual

				CGPROGRAM
				#pragma target 3.0

			// -------------------------------------


			#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
			#pragma shader_feature _METALLICGLOSSMAP
			#pragma shader_feature _PARALLAXMAP
			#pragma multi_compile_shadowcaster

			#pragma vertex vertShadowCaster
			#pragma fragment fragShadowCaster

			#include "UnityStandardShadow.cginc"

			ENDCG
		}
			// ------------------------------------------------------------------
			//  Deferred pass
			Pass
			{
				Name "DEFERRED"
				Tags { "LightMode" = "Deferred" }

				CGPROGRAM
				#pragma target 3.0
				#pragma exclude_renderers nomrt


			// -------------------------------------

			#pragma shader_feature _NORMALMAP
			#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
			#pragma shader_feature _EMISSION
			#pragma shader_feature _METALLICGLOSSMAP
			#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
			#pragma shader_feature _ _SPECULARHIGHLIGHTS_OFF
			#pragma shader_feature ___ _DETAIL_MULX2
			#pragma shader_feature _PARALLAXMAP

			#pragma multi_compile ___ UNITY_HDR_ON
			#pragma multi_compile ___ LIGHTMAP_ON
			#pragma multi_compile ___ DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
			#pragma multi_compile ___ DYNAMICLIGHTMAP_ON

			#pragma vertex vertDeferred
			#pragma fragment fragDeferred

			#include "UnityStandardCore.cginc"

			ENDCG
		}

			// ------------------------------------------------------------------
			// Extracts information for lightmapping, GI (emission, albedo, ...)
			// This pass it not used during regular rendering.
			Pass
			{
				Name "META"
				Tags { "LightMode" = "Meta" }

				Cull Off

				CGPROGRAM
				#pragma vertex vert_meta
				#pragma fragment frag_meta

				#pragma shader_feature _EMISSION
				#pragma shader_feature _METALLICGLOSSMAP
				#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
				#pragma shader_feature ___ _DETAIL_MULX2

				#include "UnityStandardMeta.cginc"
				ENDCG
			}

			//Pass: Surface SHADER
				 //ZWrite Off
				 //ZTest Greater
				 //Blend DstColor Zero

			CGPROGRAM
			//#pragma surface surf BlinnPhong
			#pragma surface surf Lambert finalcolor:mycolor //vertex:vert 
			uniform float4 _Color;
			uniform float _BoostValue;
			uniform float _BoostSat;
			uniform float _BoostHue;
			uniform float _MaxValue;
			uniform float _MinValue;
			uniform float _MaxSat;
			uniform float _MinSat;
			uniform float _maxHue;
			uniform float _minHue;
			uniform float _noiseZ;
			uniform float _noiseZB;
			uniform float4 _noiseColor;
			uniform float4 _noiseColorB;
			uniform float _BoostContrast;
			uniform float _MidValue;
			uniform float _ValueRemapStrength;
			uniform float _ValueRemapPremap;
			
			uniform float4 _Indicator;
			uniform sampler2D _MainTex;

			struct Input
			{
				float2 uv_MainTex;
				float3 viewDir;
				float3 worldPos;
				float3 localPos;
				float3 worldNormal;
				
			};

			float3 HUEtoRGB(in float H)
			{
				float R = abs(H * 6 - 3) - 1;
				float G = 2 - abs(H * 6 - 2);
				float B = 2 - abs(H * 6 - 4);
				return saturate(float3(R, G, B));
			}

			float Epsilon = 1e-10;

			float3 RGBtoHCV(in float3 RGB)
			{
				// Based on work by Sam Hocevar and Emil Persson
				float4 P = (RGB.g < RGB.b) ? float4(RGB.bg, -1.0, 2.0 / 3.0) : float4(RGB.gb, 0.0, -1.0 / 3.0);
				float4 Q = (RGB.r < P.x) ? float4(P.xyw, RGB.r) : float4(RGB.r, P.yzx);
				float C = Q.x - min(Q.w, Q.y);
				float H = abs((Q.w - Q.y) / (6 * C + Epsilon) + Q.z);
				return float3(H, C, Q.x);
			}

			float3 RGBtoHSV(in float3 RGB)
			{
				float3 HCV = RGBtoHCV(RGB);
				float S = HCV.y / (HCV.z + Epsilon);
				return float3(HCV.x, S, HCV.z);
			}

			float3 HSVtoRGB(in float3 HSV)
			{
				float3 RGB = HUEtoRGB(HSV.x);
				return ((RGB - 1) * HSV.y + 1) * HSV.z;
			}

			float rand(float3 co)
			{
				//return sin(dot(co, float3(2,-2,2)));
				//return sin(dot(co, float3(12.9898, 78.233, 45.5432)));
				float noise = frac(sin(dot(co.xyz, float3(12.9898, 78.233, 45.5432))) * 43758.5453);
				//float t = dot(co, float3(2, 2, 2));
				//float noise = t * t * t * (t * (t * 6 - 15) + 10);
				//floor(noise);
				//noise = noise / 100;
				return noise;
			}

			float smoothRand(float3 co) {
				float3 answer;

				int i = 0;
				int j = 0;
				int l = 0;
	
				for (i =  0; i < 8; i++)
				{
					if (i > 3) j = 1;
					l = i / 2;
					float3 seed = floor(co) + float3(i%2, j, l%2);

					float number = abs(rand(seed));
					//return rand(floor(co) + float3(1, 1, 1));
					float diff = abs(length(co - seed));
							
					float closeOff = 1;
					if (diff > closeOff) diff = closeOff;
					answer += (closeOff-diff) * number;
					//answer += number;

				}
				//return 0;
				return answer;

			}


			void mycolor(Input IN, SurfaceOutput o, inout fixed4 color)
			{

				
				float3 localPos = IN.localPos;
				float3 worldPos = IN.worldPos;

				float noiseFinalValue = _noiseColor.a * (smoothRand(worldPos*_noiseZ) - 0.4);
				if (noiseFinalValue < 0) noiseFinalValue = 0;
				//noiseFinalValue = 0;
				color.rgb *= 1 - noiseFinalValue;
				color.rgb += (_noiseColor.rgb  * noiseFinalValue);

				noiseFinalValue = _noiseColorB.a * ((smoothRand(worldPos*_noiseZB))-0.4);
				if (noiseFinalValue < 0) noiseFinalValue = 0;
				//noiseFinalValue = 0;
				color.rgb *= 1 - noiseFinalValue;
				color.rgb += (_noiseColorB.rgb  * noiseFinalValue);
				
				float3 boostTexture = 0;
				color.rgb = (1 - boostTexture) * color.rgb + tex2D(_MainTex, IN.uv_MainTex).rgb * boostTexture;

				float3 hsv = RGBtoHSV(color.rgb);

				//float _BoostLight = 0.3;
				//float _BoostDark = 0.3;
				if (_BoostContrast != 0) {
					float _MidLight = _MidValue;
					if (hsv[2] > _MidLight) {
						hsv[2] = hsv[2] + (hsv[2] - _MidLight)* _BoostContrast / (1 - _MidLight);
						if (hsv[2] > 1) hsv[2] = 1;
					}
					else {
						hsv[2] = hsv[2] - (_MidLight - hsv[2])* _BoostContrast / (_MidLight);
						if (hsv[2] < 0) hsv[2] = 0;
					}
				}
				
					
				

				hsv[0] += _BoostHue;
				hsv[1] += _BoostSat;
				hsv[2] += _BoostValue;


				
						
				if (hsv[1] > _MaxSat) hsv[1] = _MaxSat;
				if (hsv[1] < _MinSat) hsv[1] = _MinSat;

				//hsv[1] += (smoothRand(worldPos*0.1) - 0.5)*noiseS;
				float remapStrength = _ValueRemapStrength;
				float preRemapValue = (hsv[2])*_ValueRemapPremap / ((_MaxValue - _MinValue));
				//preRemapValue = hsv[2] / (_MaxValue - _MinValue);
				//preRemapValue = hsv[2] *2;
				hsv[2] = (preRemapValue * (_MaxValue - _MinValue) + _MinValue) * remapStrength + hsv[2]* (1-remapStrength);
				if (hsv[2] > _MaxValue) hsv[2] = _MaxValue;
				if (hsv[2] < _MinValue) hsv[2] = _MinValue;

				if (_maxHue < _minHue) {
					if (_maxHue > hsv[0]) {
						hsv[0] += 1;
					}
					_maxHue += 1;
				}

				bool which = true;
				if (hsv[0] > _maxHue || hsv[0] < _minHue) {
					float dis = abs(hsv[0] - _maxHue);
					if (dis < abs(hsv[0] + 1 - _maxHue)) {
						dis = abs(hsv[0] + 1 - _maxHue);
					}
					if (dis < abs(hsv[0] + 1 - _minHue)) {
						dis = abs(hsv[0] + 1 - _minHue);
						which = false;
					}
					if (dis < abs(hsv[0] - _minHue)) {
						which = false;
					}
					if (which) {
						hsv[0] = _maxHue;
					} else {
						hsv[0] = _minHue;
						
					}
				}
				if (hsv[0] > 1) hsv[0] -= 1;

				color.rgb = HSVtoRGB(hsv);
				
				//color.rgb = float3(0,0,0);
				
				//color.rgb = color.rgb;
				//color.rgb = color.rgb * (1 - noiseFinalValue);
				//color.rgb = _noiseColor.rgb  * noiseFinalValue;
				//color.rgb += (_noiseColor.rgb  * noiseFinalValue) + (color.rgb * (1-noiseFinalValue));
			}

			void vert(inout appdata_full v, out Input o) {
				UNITY_INITIALIZE_OUTPUT(Input, o);
				o.localPos = v.vertex.xyz;
			}

			void surf(Input IN, inout SurfaceOutput o)
			{
				//o.Albedo =_Indicator;
				o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
				//o.Albedo = _Indicator;
			}
			ENDCG
					 ////Pass: Fixed pipeline
					 //Pass
					 //{          
					 //	Tags{ "LightMode" = "Always" }
					 //	//AlphaTest Greater [_Cutoff]
					 //	//ZWrite Off
					 //	//ZTest Greater

					 //	SetTexture[_MainTex] {
					 //		
					 //	//constantColor [_Indicator]
					 //	//combine constant, texture
					 //	//combine constant* texture
					 //	//combine constant
					 //	//combine texture
					 //	}
					 //}
		}

			SubShader
				 {
					 Tags { "RenderType" = "Opaque" "PerformanceChecks" = "False" }
					 LOD 150

					 // ------------------------------------------------------------------
					 //  Base forward pass (directional light, emission, lightmaps, ...)
					 Pass
					 {
						 Name "FORWARD"
						 Tags { "LightMode" = "ForwardBase" }

						 Blend[_SrcBlend][_DstBlend]
						 ZWrite[_ZWrite]

						 CGPROGRAM
						 #pragma target 2.0

						 #pragma shader_feature _NORMALMAP
						 #pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
						 #pragma shader_feature _EMISSION 
						 #pragma shader_feature _METALLICGLOSSMAP 
						 #pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
						 #pragma shader_feature _ _SPECULARHIGHLIGHTS_OFF
						 #pragma shader_feature _ _GLOSSYREFLECTIONS_OFF
					 // SM2.0: NOT SUPPORTED shader_feature ___ _DETAIL_MULX2
					 // SM2.0: NOT SUPPORTED shader_feature _PARALLAXMAP

					 #pragma skip_variants SHADOWS_SOFT DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE

					 #pragma multi_compile_fwdbase
					 #pragma multi_compile_fog

					 #pragma vertex vertBase
					 #pragma fragment fragBase
					 #include "UnityStandardCoreForward.cginc"

					 ENDCG
				 }
					 // ------------------------------------------------------------------
					 //  Additive forward pass (one light per pass)
					 Pass
					 {
						 Name "FORWARD_DELTA"
						 Tags { "LightMode" = "ForwardAdd" }
						 Blend[_SrcBlend] One
						 Fog { Color(0,0,0,0) } // in additive pass fog should be black
						 ZWrite Off
						 ZTest LEqual

						 CGPROGRAM
						 #pragma target 2.0

						 #pragma shader_feature _NORMALMAP
						 #pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
						 #pragma shader_feature _METALLICGLOSSMAP
						 #pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
						 #pragma shader_feature _ _SPECULARHIGHLIGHTS_OFF
						 #pragma shader_feature ___ _DETAIL_MULX2
					 // SM2.0: NOT SUPPORTED shader_feature _PARALLAXMAP
					 #pragma skip_variants SHADOWS_SOFT

					 #pragma multi_compile_fwdadd_fullshadows
					 #pragma multi_compile_fog

					 #pragma vertex vertAdd
					 #pragma fragment fragAdd
					 #include "UnityStandardCoreForward.cginc"

					 ENDCG
				 }
					 // ------------------------------------------------------------------
					 //  Shadow rendering pass
					 Pass {
						 Name "ShadowCaster"
						 Tags { "LightMode" = "ShadowCaster" }

						 ZWrite On ZTest LEqual

						 CGPROGRAM
						 #pragma target 2.0

						 #pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
						 #pragma shader_feature _METALLICGLOSSMAP
						 #pragma skip_variants SHADOWS_SOFT
						 #pragma multi_compile_shadowcaster

						 #pragma vertex vertShadowCaster
						 #pragma fragment fragShadowCaster

						 #include "UnityStandardShadow.cginc"

						 ENDCG
					 }

					 // ------------------------------------------------------------------
					 // Extracts information for lightmapping, GI (emission, albedo, ...)
					 // This pass it not used during regular rendering.
					 Pass
					 {
						 Name "META"
						 Tags { "LightMode" = "Meta" }

						 Cull Off

						 CGPROGRAM
						 #pragma vertex vert_meta
						 #pragma fragment frag_meta

						 #pragma shader_feature _EMISSION
						 #pragma shader_feature _METALLICGLOSSMAP
						 #pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
						 #pragma shader_feature ___ _DETAIL_MULX2

						 #include "UnityStandardMeta.cginc"
						 ENDCG
					 }


				 }


					 FallBack "VertexLit"
					 //CustomEditor "StandardShaderGUI"
}

