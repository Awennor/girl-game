﻿Shader "Custom/CelShadingForward A" {
	Properties {
		_Color("Color", Color) = (1, 1, 1, 1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags {
			"RenderType" = "Opaque"
		}
		LOD 200

		//CGPROGRAM#pragma surface surf CelShadingForward #pragma target 3.0
		CGPROGRAM#pragma surface surf CelShadingForward #pragma target 3.0

		half4 LightingCelShadingForward(SurfaceOutput s, half3 lightDir, half atten) {
			

			half NdotL = dot(s.Normal, lightDir);
			//NdotL *= 2;

			//NdotL = 3;
			if (false) {
				
				int i = ((NdotL + 0.1) * 4);
				NdotL = i;
				NdotL = NdotL / 4 - 0.1;
				
			}
			
			
			//GREAT FUNCTION START
			if(false){
				if (NdotL <= 0) NdotL = 0;
				else
					if (NdotL <= 0.3) NdotL = 0;
					else
						if (NdotL <= 0.6) NdotL = 0.6;
						else
							if (NdotL <= 0.8) NdotL = 0.8;
				//else
				//	if(NdotL <= 0.6) NdotL = 0.6;
				//else
					//if(NdotL <= 0.9) NdotL = 0.65;
							else
								if (NdotL <= 1) NdotL = 1;
									
			}
			//GREAT FUNCTION END
			
			
			
			//NdotL = 1;
			
			//else if (NdotL <= 0.9 ) NdotL = 0.5;
				//else NdotL = 1;
			//if (NdotL <= 0.0) NdotL = 0;
			//if (NdotL >= 0.25) NdotL = 1;
			//NdotL = smoothstep(0, 0.25f, NdotL);
			//NdotL = smoothstep(0.25, 0.5f, NdotL);
			//if (NdotL == 0) NdotL = 1;
			
			half4 c;
			half3 lightColor = _LightColor0.rgb;
			
			c.rgb = s.Albedo * lightColor* (NdotL * atten * 2);
			
			if (NdotL == 0) {
				//c.rgb += 0.5*_LightColor0.rgb;
				//c.rgb += 0.05;
				half bright = s.Albedo.r + s.Albedo.g + s.Albedo.b;
				if (bright > 1) {
					c.r += 0.2;
					c.g += 0.3;
					c.b += 0.2;
				}
				lightColor.g += 0.5;
				lightColor.b += 0.5;
			}

			//c.rgb = s.Albedo;
			half albedopower = 0.3;
			half threeShould = -1;
			
			//if (c.r > threeShould && c.b > threeShould && c.g > threeShould)
			//if (_LightColor0.r < threeShould && _LightColor0.b < threeShould && _LightColor0.g < threeShould)
			
				//c.rgb = s.Albedo * albedopower + c.rgb*(1 - albedopower);
			//if(c.r < 32 && c.b1 < 32 && c.g < 32)
				//c.rgb = s.Albedo * albedopower + c.rgb*(1-albedopower);
			c.a = s.Alpha;
			return c;
		}

		sampler2D _MainTex;
		fixed4 _Color;

		struct Input {
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}