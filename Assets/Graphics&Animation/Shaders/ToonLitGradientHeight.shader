Shader "Toon/LitGradientHeight" {
	Properties {
		_Color ("Main Color", Color) = (0.5,0.5,0.5,1)
		_Color2 ("Main Color 2", Color) = (0.5,0.5,0.5,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Ramp ("Toon Ramp (RGB)", 2D) = "gray" {} 
		_HeightShown("Height Shown", Range(-9999, 9999)) = 0
		_HeightFade("Height Fade", Range(-9999, 9999)) = 0
	}

	SubShader {
		//Tags { "Queue" = "Transparent" "RenderType"="Transparent" }
			Tags{ "RenderType" = "Opaque" }
		LOD 200
		
CGPROGRAM
#pragma surface surf Lambert alpha:fade

		struct SurfaceOutputCustom {
			fixed3 Albedo;
			fixed3 Normal;
			fixed3 Emission;
			half Specular;
			fixed Gloss;
			fixed Alpha;
			float3 worldPos;
		};

sampler2D _Ramp;

// custom lighting function that uses a texture ramp based
// on angle between light direction and normal

float _HeightShown;
float _HeightFade;
float4 _Color2;

#pragma lighting ToonRamp exclude_path:prepass 
inline half4 LightingToonRamp (SurfaceOutputCustom s, half3 lightDir, half atten)
{
	#ifndef USING_DIRECTIONAL_LIGHT
	lightDir = normalize(lightDir);
	#endif
	
	half d = dot (s.Normal, lightDir)*0.5 + 0.5;
	half3 ramp = tex2D (_Ramp, float2(d,d)).rgb;

	float a = 0;
	if (s.worldPos.y > _HeightShown) {
		a = 1;
	}
	else {
		if (s.worldPos.y < _HeightFade) {
			a = 0;
		}
		else {
			a = (s.worldPos.y - _HeightFade) / (_HeightShown - _HeightFade);
		}
	}
	
	half4 c;
	c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
	c.a = 1;
	//c.r 
	//c.rgb = c.rgb * a + _Color2.rgb*(1-a);
	c.a = a;
	//c.rgb += s.Emission;
	//c.a = 0;

	//c.rgb = s.Emission;
	return c;
}


sampler2D _MainTex;
float4 _Color;


struct Input {
	float2 uv_MainTex : TEXCOORD0;
	float3 worldPos;
};



void surf (Input IN, inout SurfaceOutput o) {

	float a = 0;
	if (IN.worldPos.y > _HeightShown) {
		a = 1;
	}
	else {
		if (IN.worldPos.y < _HeightFade) {
			a = 0;
		}
		else {
			a = (IN.worldPos.y - _HeightFade) / (_HeightShown - _HeightFade);
		}
	}

	

	//float4 cC = _Color * a + _Color2 * (1 - a);
	float4 cC = _Color;

	half4 c = tex2D(_MainTex, IN.uv_MainTex) * cC;
	
	//c = c*a + _Color2*(1 - a);

	o.Albedo = c.rgb;

	//o.Alpha = c.a;
	o.Alpha = a;
	
	//o.worldPos = IN.worldPos;
	//o.Alpha = 1-a;
	
	//if (a == 0) o.Emission = _Color2;
}
ENDCG

	} 

	Fallback "Diffuse"
}
