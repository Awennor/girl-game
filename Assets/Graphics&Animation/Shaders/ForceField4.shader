﻿// Upgrade NOTE: replaced 'PositionFog()' with multiply of UNITY_MATRIX_MVP by position
// Upgrade NOTE: replaced 'V2F_POS_FOG' with 'float4 pos : SV_POSITION'


Shader "FX/Forest Force Field 2" {

	Properties{
		_Color("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (1,1,1,1)
		_Rate("Oscillation Rate", Range(5, 200)) = 50
		_Scale("Scale", Range(0.02, 0.5)) = 0.25
		_Distortion("Distortion", Range(0.1, 20)) = 1
	}

		SubShader{

		ZWrite Off
		Tags{ "Queue" = "Transparent" }
		//Blend DstColor Zero
		//Blend 


		Pass{
		Tags{ "LightMode" = "ForwardBase" }
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_fog_exp2
#include "UnityCG.cginc"

#pragma multi_compile_fwdbase
#include "AutoLight.cginc"

		float4 _Color;
	float4 _Color2;
	float _Rate;
	float _Scale;
	float _Distortion;

	struct v2f {
		float4 pos : SV_POSITION;
		float3 uv : TEXCOORD0;
		float3 vert : TEXCOORD1;
		LIGHTING_COORDS(2, 3)
	};

	v2f vert(appdata_base v)
	{
		v2f o;
		o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
		
		float s = 1 / _Scale;
		float t = _Time[0] * _Rate*_Scale;
		//o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		o.vert = sin((v.vertex.xyz + t) * s);
		o.uv = sin((v.texcoord.xyz + t) * s) * _Distortion;
		TRANSFER_VERTEX_TO_FRAGMENT(o);
		

		return o;
	}

	half4 frag(v2f i) : COLOR
	{
		float3 vert = i.vert;
		float3 uv = i.uv;
		float mix = 1 + sin((vert.x - uv.x) + (vert.y - uv.y) + (vert.z - uv.z));
		//float mix2 = 1 + sin((vert.x + uv.x) - (vert.y + uv.y) - (vert.z + uv.z)) / _Distortion;
		float mix2 = 3-mix;
		//float mix = ((vert.y + uv.y) % 0.3) / 0.3;
		//float mix2 = ((vert.y + uv.y) % 0.3 + 0.3) / 0.3;
		half4 c = half4((_Color * mix * 0.5) + (_Color2 * mix2 * 0.5));
		
		float attenuation = LIGHT_ATTENUATION(i);
		
		//c = c * attenuation;
		//c = c * attenuation *attenuation *attenuation;

		//c[1] = 0;
		//c[0] = 1;
		//c[1] = 1;

		//c[0] = 1;
		//c[1] = 0.5f;
		//c[2] = 0.5f;
		
		return c * attenuation;
	}
		ENDCG

	}
	}
		Fallback "Unlit/Color"
}