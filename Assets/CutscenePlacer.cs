﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CutscenePlacer : MonoBehaviour {

    [SerializeField]
    PositionKeyPair[] positionKeyPairs;

    [SerializeField]
    private string cutsceneName;

    public bool debugTestCutscene;
    private CutsceneComp cutscene;
    public UnityEvent OnCutsceneEnd;
    public UnityEvent OnCutsceneStartFail;
   

    public string CutsceneName {
        get {
            return cutsceneName;
        }

        set {
            cutsceneName = value;
        }
    }

    public void OnDrawGizmos() {
        Gizmos.DrawCube(transform.position, new Vector3(30, 0.1f, 30));
        //Gizmos.color = Color.red;
        //Gizmos.DrawCube(transform.position + transform.forward*14, new Vector3(2, 2, 2));
    }

    internal void LoadAndTryStartCutscene() {
        PlaceCutscene();
        StartCutscene();
    }

    public void StartCutscene() {
        if(cutscene.gameObject.activeSelf) {
            cutscene.cutsceneEnd.AddListener(CutsceneEnd_Callback);
            cutscene.CutsceneStartExternal();
        } else {
            OnCutsceneStartFail.Invoke();
        }
            
    }

    private void CutsceneEnd_Callback() {
        OnCutsceneEnd.Invoke();
    }


    // Use this for initialization
    void Start() {
        PlaceCutscene();

        if (debugTestCutscene) cutscene.CutsceneStartExternal();

        //cc.transform.rotation = this.transform.rotation;
    }

    private void PlaceCutscene() {
        if(cutsceneName.Length == 0) return;
        if(cutscene != null) {
            cutscene.transform.position = new Vector3(99999,99999, 99999);
        }
        
        cutscene = CentralResource.Instance.CutsceneHolder.GetCutscene(CutsceneName);
        cutscene.transform.position = this.transform.position;

        if (positionKeyPairs.Length > 0) {
            var v3e = cutscene.GetComponent<Vector3Events>();

            if (v3e != null) {
                for (int j = 0; j < positionKeyPairs.Length; j++) {
                    v3e.Invoke(positionKeyPairs[j].Position.position, positionKeyPairs[j].Key);
                }
            }
        }
    }

    // Update is called once per frame
    void Update() {

    }

    [Serializable]
    private class PositionKeyPair {
        [SerializeField]
        Transform position;
        [SerializeField]
        string key;

        public string Key {
            get {
                return key;
            }

            set {
                key = value;
            }
        }

        public Transform Position {
            get {
                return position;
            }

            set {
                position = value;
            }
        }
    }
}
