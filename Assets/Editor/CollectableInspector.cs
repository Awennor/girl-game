﻿using UnityEngine;
using System.Collections;
using UnityEditor;

using UnityEngine;
using System.Collections;
using UnityEditor;





[CustomEditor(typeof(Collectable))]
public class CollectableInspector : Editor {

    static private TreasureIdManager treasureIdManager = new TreasureIdManager();

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        string V = "New Fresh ID";
        Collectable myTarget = (Collectable)target;
        if (GUILayout.Button(V)) {
            int id = treasureIdManager.GetNewID();
            myTarget.UniqueId = id;
        }
        if (GUILayout.Button("Delete ID")) {
            int id = myTarget.UniqueId;
            treasureIdManager.RemoveID(id);
            myTarget.UniqueId = -1;
        }

    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
