﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

[CustomEditor(typeof(CutsceneComp))]
public class CutsceneCompEditor : Editor {

    MyBoolArray myBoolArray = new MyBoolArray();
    private SerializedProperty nodesProp;
    private SerializedProperty sceneCamera;
    public LocalizationDataDefine localForText;
    private SerializedProperty startCondition;
    private SerializedProperty triggerObject;
    private LocalizationDataDefine localForCharaName;
    bool showNormalInspector = false;
    private string dialogAutomatizationAux;

    public void OnEnable() {
        nodesProp = serializedObject.FindProperty("nodes");
        //nodesProp.FindPropertyRelative();
        sceneCamera = serializedObject.FindProperty("sceneCamera");
        startCondition = serializedObject.FindProperty("startCondition");
        triggerObject = serializedObject.FindProperty("trigger");
        if (localForText == null) {
            localForText = (LocalizationDataDefine)AssetDatabase.LoadAssetAtPath("Assets/Data/TextData/handtemplecutscenes.asset", typeof(LocalizationDataDefine));
            Debug.Log("Localization Data NOT SET");
        } else {
            Debug.Log("Localization Data already SET");
        }


        localForCharaName = (LocalizationDataDefine)AssetDatabase.LoadAssetAtPath("Assets/Data/TextData/LocalizationDataNew.asset", typeof(LocalizationDataDefine));
        //sceneCamera = serializedObject.FindProperty("localizationData");
    }

    public override void OnInspectorGUI() {
        //DrawDefaultInspector();
        var comp = target as CutsceneComp;

        var nodes = comp.Nodes;

        showNormalInspector = EditorGUILayout.Foldout(showNormalInspector, "Show normal inspector");
        if (showNormalInspector) {
            DrawDefaultInspector();
        }


        EditorGUILayout.PropertyField(startCondition);
        EditorGUILayout.PropertyField(sceneCamera);

        //serializedObject.ApplyModifiedProperties();

        if (comp.startCondition == CutsceneNodeStartCondition.Trigger) {
            EditorGUILayout.PropertyField(triggerObject);


        }

        //serializedObject.Update();
        //serializedObject.ApplyModifiedProperties();
        //Debug.Log(localizationData+"LOC DATA!!!");
        //localizationData = (LocalizationDataDefine) EditorGUILayout.ObjectField(localizationData, typeof(LocalizationDataDefine), true);


        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("+")) {

            var v = nodesProp.arraySize + 1;
            nodesProp.arraySize = v;

            //if(v < 0) v = 0;
            //nodesProp.InsertArrayElementAtIndex(v);
            //serializedObject.ApplyModifiedProperties();
        }
        if (GUILayout.Button("-")) {

            if (nodesProp.arraySize >= 0)
                nodesProp.arraySize--;

        }
        if (GUI.changed)
            serializedObject.ApplyModifiedProperties();
        EditorGUILayout.EndHorizontal();

        Event currentEvent = Event.current;

        for (int i = 0; i < nodes.Length; i++) {
            EditorGUILayout.Space();
            int nodeId = i;
            var n = nodes[i];
            var type = n.Type;
            Rect rect = EditorGUILayout.BeginHorizontal();
            Rect dropArea = GUILayoutUtility.GetRect(25.0f, 25.0f, GUILayout.ExpandWidth(false));
            GUI.Box(dropArea, "");
            EditorGUILayout.Space();
            myBoolArray[i] = EditorGUILayout.Foldout(myBoolArray[i], "");
            if (currentEvent.button == 0) {

                Vector2 mousePos = currentEvent.mousePosition;

                if (currentEvent.type == EventType.DragUpdated) {
                    //Debug.Log("Drag update ");
                    if (dropArea.Contains(mousePos)) {
                        //Debug.Log("Drag update link");
                        DragAndDrop.visualMode = DragAndDropVisualMode.Link;
                        currentEvent.Use();
                    }

                }

                if (currentEvent.type == EventType.DragPerform) {
                    DragAndDrop.AcceptDrag();
                    //Debug.Log("Drag perform...");
                    if (dropArea.Contains(mousePos)) {
                        //  Debug.Log("Drag perform 2...");
                        int? dragIndex = DragAndDrop.GetGenericData("drag index") as int?;
                        if (dragIndex.HasValue) {
                            //    Debug.Log("drag has value");
                            if (dragIndex.Value != i) {
                                Debug.Log(dragIndex.Value + " Drag and drop swap " + i);
                                MoveNode(dragIndex.Value, i);

                                serializedObject.Update();
                                currentEvent.Use();
                                return;
                            }

                        }
                    }

                }


                if (currentEvent.type == EventType.MouseDrag) {
                    //Debug.Log("MOUSE 0");

                    if (dropArea.Contains(mousePos)) {
                        Debug.Log("CLICK IN");
                        DragAndDrop.PrepareStartDrag();
                        DragAndDrop.StartDrag("dragnode");
                        DragAndDrop.SetGenericData("drag index", i);
                        currentEvent.Use();
                    }
                }
                if (currentEvent.type == EventType.MouseUp) {
                }
            }

            //itorGUILayout.LabelField("Node "+i);
            type = (CutsceneNodeType)EditorGUILayout.EnumPopup(type);




            if (currentEvent.type == EventType.ContextClick) {
                Vector2 mousePos = currentEvent.mousePosition;
                if (rect.Contains(mousePos)) {
                    // Now create the menu, add items and show it
                    GenericMenu menu = new GenericMenu();
                    menu.AddItem(new GUIContent("Insert node here"), false, (obj) => { InsertNode(nodeId); }, "item 1");
                    menu.AddItem(new GUIContent("Delete node"), false, (obj) => { DeleteNode(nodeId); }, "item 2");
                    if (nodeId > 0)
                        menu.AddItem(new GUIContent("Send Up"), false, (obj) => { MoveNode(nodeId, nodeId - 1); }, "item 3");
                    if (nodeId < nodes.Length - 1)
                        menu.AddItem(new GUIContent("Send Down"), false, (obj) => { MoveNode(nodeId, nodeId + 1); }, "item 4");
                    menu.AddItem(new GUIContent("Send to Start"), false, (obj) => { MoveNode(nodeId, 0); }, "item 5");
                    menu.AddItem(new GUIContent("Send to End"), false, (obj) => { MoveNode(nodeId, nodes.Length - 1); }, "item 6");

                    menu.ShowAsContext();
                    currentEvent.Use();
                }
            }





            n.Type = type;
            if (myBoolArray[i]) {
                EditorGUILayout.EndHorizontal();
                switch (type) {
                    case CutsceneNodeType.TextMessage:
                        string[] groupNames = localForText.GroupNames();
                        int groupIndex = 0;
                        for (int j = 0; j < groupNames.Length; j++) {
                            if (groupNames[j].Equals(n.Strings[0])) {
                                groupIndex = j;
                            }
                        }
                        EditorGUILayout.BeginHorizontal();
                        //EditorGUILayout.LabelField(n.Strings[0]);
                        int IndexNew = EditorGUILayout.Popup(groupIndex, groupNames);
                        if (IndexNew != -1) {
                            //Debug.Log("SET GROUP NAME"+groupNames[group]);
                            n.Strings[0] = groupNames[IndexNew];
                        }
                        string[] messages = localForText.MessageArrayFirstLanguage(IndexNew);
                        var group = localForText.groups[IndexNew];

                        var nodesInGroup = group.Nodes;
                        int messageIndex = 0;
                        for (int j = 0; j < nodesInGroup.Count; j++) {
                            if (nodesInGroup[j].key.Equals(n.Strings[1])) {
                                messageIndex = j;
                                break;
                            }
                        }

                        int messageIndexNew = EditorGUILayout.Popup(messageIndex, messages);
                        var locNode = group.Nodes[messageIndexNew];
                        n.Strings[1] = locNode.key;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        GameObjectField(n, 0);
                        int charaGroupIndex = localForCharaName.FindIndexOfGroupName("@SpeakerNames");
                        string[] names = localForCharaName.MessageArrayFirstLanguage(charaGroupIndex);
                        group = localForCharaName.groups[charaGroupIndex];
                        messages = names;
                        nodesInGroup = group.Nodes;
                        messageIndex = 0;
                        for (int j = 0; j < nodesInGroup.Count; j++) {
                            if (nodesInGroup[j].key.Equals(n.Strings[2])) {
                                messageIndex = j;
                                break;
                            }
                        }
                        int charaNameIndex = EditorGUILayout.Popup(messageIndex, messages);
                        n.Strings[2] = nodesInGroup[charaNameIndex].key;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Thinking");
                        BooleanField(n, 0);
                        EditorGUILayout.EndHorizontal();
                        //EditorGUILayout.BeginHorizontal();



                        //n.Ints[0] = group;
                        //n.Strings[0] = group;

                        break;
                    case CutsceneNodeType.AnimationTrigger:
                        EditorGUILayout.BeginHorizontal();
                        StringField(n, 0);
                        GameObjectFieldAsComponent<Animator>(n, 0);

                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.ShowEmail:
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Email Key");
                        StringField(n, 0);
                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.ShowArticle:
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Article Key");
                        StringField(n, 0);
                        EditorGUILayout.EndHorizontal();
                        break;

                    case CutsceneNodeType.MoveToFrom:
                        EditorGUILayout.BeginHorizontal();
                        FloatField(n, 0);
                        GameObjectField(n, 0);
                        GameObjectField(n, 1);
                        GameObjectField(n, 2);

                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.MoveTo:
                        EditorGUILayout.BeginHorizontal();
                        FloatField(n, 0);
                        GameObjectField(n, 0);
                        GameObjectField(n, 1);

                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.MoveAndRotateTo:
                        EditorGUILayout.BeginHorizontal();
                        FloatField(n, 0);
                        GameObjectField(n, 0);
                        GameObjectField(n, 1);

                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.MoveToAnimatedCharacter:

                        EditorGUILayout.BeginHorizontal();
                        //FloatField(n, 0);
                        {
                            //GameObjectField(n, 0);
                            GameObjectFieldAsComponent<MovementToGoalEnemy>(n, 0);
                            GameObjectField(n, 1);


                        }
                        //GameObjectField(n, 2);

                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.GameObjectActive:
                        EditorGUILayout.BeginHorizontal();
                        BooleanField(n, 0);
                        GameObjectField(n, 0);

                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.FaceTarget:
                        EditorGUILayout.BeginHorizontal();

                        GameObjectField(n, 0);
                        GameObjectField(n, 1);

                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.FadeIn:
                        EditorGUILayout.BeginHorizontal();
                        FloatField(n, 0);
                        ColorField(n, 0);
                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.FadeOut:
                        EditorGUILayout.BeginHorizontal();
                        FloatField(n, 0);
                        ColorField(n, 0);
                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.FadeTo:
                        EditorGUILayout.BeginHorizontal();

                        ColorField(n, 0);
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.BeginHorizontal();
                        EditorGUIUtility.labelWidth = 80;
                        EditorGUILayout.LabelField("Time");
                        FloatField(n, 0);

                        EditorGUILayout.LabelField("Fade To ");
                        FloatField(n, 1);
                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.Delay:
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Time");
                        FloatField(n, 0);
                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.RotateActorDefault: //default time
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Angle");
                        FloatField(n, 0);
                        GameObjectField(n, 0);
                        EditorGUILayout.EndHorizontal();
                        break;
                    case CutsceneNodeType.CallMethod:
                        EventField(nodeId);

                        break;
                    case CutsceneNodeType.ShowEffect:
                        StringField(n, 0);
                        GameObjectFieldAsComponent<Transform>(n, 0);
                        break;
                }

            } else {
                EditorGUILayout.EndHorizontal();
            }

            //EditorGUILayout.EndToggleGroup();
        }
        EditorGUILayout.BeginHorizontal();
        dialogAutomatizationAux = EditorGUILayout.TextField(dialogAutomatizationAux);
        if (GUILayout.Button("Create text messages for all dialogs")) {
            int arraySizeNow = nodesProp.arraySize;
            var ns = localForText.GetGroup(dialogAutomatizationAux).Nodes;
            nodesProp.arraySize += ns.Count;
            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();
            for (int i = 0; i < ns.Count; i++) {


                comp = (CutsceneComp)target;
                string key = ns[i].key;
                var cn = comp.Nodes[arraySizeNow + i];
                cn.Strings[0] = dialogAutomatizationAux;
                cn.Strings[1] = key;
                cn.Type = CutsceneNodeType.TextMessage;
            }
        }
        EditorGUILayout.EndHorizontal();
        if (GUILayout.Button("Text Message: get speaking GO from speaker name key")) {
            for (int i = 0; i < nodes.Length; i++) {

                var node = nodes[i];
                if (node.Type == CutsceneNodeType.TextMessage) {
                    var transform = comp.transform;

                    TextMessageFindingSpeakerObjInChildren(node, transform);
                }

            }

        }
        if (true) {
            //if (GUI.changed) {
            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();
        }



    }

    private static void TextMessageFindingSpeakerObjInChildren(CutsceneNode node, Transform transform) {
        int cc = transform.childCount;
        for (int j = 0; j < cc; j++) {
            var child = transform.GetChild(j);
            var speakerKey = node.Strings[2].ToLower();
            //Debug.Log("Speaker name " + speakerKey);
            if (child.gameObject.name.ToLower().Contains(speakerKey)) {
                node.GameObjects[0] = child.gameObject;
                return;
            }
            if (child.childCount > 0) {
                TextMessageFindingSpeakerObjInChildren(node, child);
            }

        }
    }

    private void EventField(int nodeId) {
        EditorGUIUtility.LookLikeControls();
        var eventProp = nodesProp.GetArrayElementAtIndex(nodeId).FindPropertyRelative("unityEvent");
        EditorGUILayout.PropertyField(eventProp);
    }


    private void MoveNode(int nodeId, int nodeId2) {
        //var p1 = nodesProp.GetArrayElementAtIndex(nodeId);
        //var p2 = nodesProp.GetArrayElementAtIndex(nodeId2);

        nodesProp.MoveArrayElement(nodeId, nodeId2);
        serializedObject.ApplyModifiedProperties();
    }

    private void DeleteNode(int nodeId) {
        nodesProp.DeleteArrayElementAtIndex(nodeId);
        serializedObject.ApplyModifiedProperties();
    }

    private void InsertNode(int nodeId) {
        nodesProp.InsertArrayElementAtIndex(nodeId);
        serializedObject.ApplyModifiedProperties();
    }

    private void BooleanField(CutsceneNode n, int v) {
        n.Bools[v] = EditorGUILayout.Toggle(n.Bools[v]);
    }

    private static void StringField(CutsceneNode n, int index) {
        n.SetString(EditorGUILayout.TextField("", n.GetString(index)), index);
    }

    private static void GameObjectField(CutsceneNode n, int index) {
        n.GameObjects[index] = (GameObject)EditorGUILayout.ObjectField(n.GameObjects[index], typeof(GameObject), true);
    }

    private static void GameObjectFieldAsComponent<T>(CutsceneNode n, int index) where T : Component {
        var gameObject = n.GameObjects[index];
        T comp = null;
        if (gameObject != null)
            comp = gameObject.GetComponent<T>();
        var compGotten = ((T)EditorGUILayout.ObjectField(comp, typeof(T), true));
        if (compGotten != null)
            n.GameObjects[index] = compGotten.gameObject;
    }

    private static void ColorField(CutsceneNode n, int index) {
        n.Colors[index] = EditorGUILayout.ColorField(n.Colors[index]);
    }

    private static void FloatField(CutsceneNode n, int index) {
        n.Floats[index] = EditorGUILayout.FloatField(n.Floats[index]);
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    protected void ExampleDragDropGUI(Rect dropArea, SerializedProperty property) {
        // Cache References:
        Event currentEvent = Event.current;
        EventType currentEventType = currentEvent.type;

        // The DragExited event does not have the same mouse position data as the other events,
        // so it must be checked now:
        if (currentEventType == EventType.DragExited) DragAndDrop.PrepareStartDrag();// Clear generic data when user pressed escape. (Unfortunately, DragExited is also called when the mouse leaves the drag area)

        if (!dropArea.Contains(currentEvent.mousePosition)) return;

        switch (currentEventType) {
            case EventType.MouseDown:
                DragAndDrop.PrepareStartDrag();// reset data
                //DragAndDrop.SetGenericData();
                break;
            case EventType.MouseDrag:
                // If drag was started here:

                break;
            case EventType.DragUpdated:

                break;
            case EventType.Repaint:
                if (
                DragAndDrop.visualMode == DragAndDropVisualMode.None ||
                DragAndDrop.visualMode == DragAndDropVisualMode.Rejected)
                    break;

                EditorGUI.DrawRect(dropArea, Color.grey);
                break;
            case EventType.DragPerform:

                break;
            case EventType.MouseUp:
                // Clean up, in case MouseDrag never occurred:
                DragAndDrop.PrepareStartDrag();
                break;
        }

    }

}
