﻿using UnityEngine;
using System.Collections;
using UnityEditor;

using UnityEngine;
using System.Collections;
using UnityEditor;
using CodeGeneration;
using Assets.Editor.T4stuff;
using System.IO;
using System;
using System.Text;
using System.Collections.Generic;

[CustomEditor(typeof(LocalizationDataDefine))]
public class LocalizationDataInspector : Editor {

    //static private TreasureIdManager treasureIdManager = new TreasureIdManager();
    string path = "Assets/translation new.csv";

    List<Folder> folderListAux = new List<Folder>();
    List<SerializedProperty> propListAux = new List<SerializedProperty>();
    private SerializedProperty groupsProp;
    bool rootFolderOpen;
    List<bool> folderFolded = new List<bool>();
    private LocalizationDataDefine myTarget;

    public void OnEnable() {
        groupsProp = serializedObject.FindProperty("groups");
        //nodesProp.FindPropertyRelative();

        //sceneCamera = serializedObject.FindProperty("localizationData");
    }

    public override void OnInspectorGUI() {

        serializedObject.Update();

        myTarget = (LocalizationDataDefine)target;

        //prepare variables for folder structure drawing
        //var folders = myTarget.folders;
        //folderListAux.Clear();
        //folderListAux.AddRange(folders);
        //while (folders.Count > folderFolded.Count) {
        //    folderFolded.Add(false);
        //}
        //string currentFolder = "";
        //propListAux.Clear();
        //for (int i = 0; i < groupsProp.arraySize; i++) {
        //    propListAux.Add(groupsProp.GetArrayElementAtIndex(i));
        //}
        //DrawFolder(currentFolder, -1);

        //remaining groups that are not on any folder
        //for (int i = 0; i < propListAux.Count; i++) {
        //EditorGUILayout.PropertyField(propListAux[i], true);
        //}



        serializedObject.ApplyModifiedProperties();

        DrawDefaultInspector();

        //string V = "New Fresh ID";

        if (GUILayout.Button("Correct New Lines")) {
            var gs = myTarget.groups;
            for (int i = 0; i < gs.Count; i++) {
                var ns = gs[i].Nodes;
                for (int j = 0; j < ns.Count; j++) {
                    var vs = ns[j].values;
                    for (int k = 0; k < vs.Length; k++) {
                        vs[k] = vs[k].Replace("\\n", "\n");
                    }
                }
            }
        }

        if (GUILayout.Button("Correct '")) {
            var gs = myTarget.groups;
            for (int i = 0; i < gs.Count; i++) {
                var ns = gs[i].Nodes;
                for (int j = 0; j < ns.Count; j++) {
                    var vs = ns[j].values;
                    for (int k = 0; k < vs.Length; k++) {
                        vs[k] = vs[k].Replace('’', '\'');
                    }
                }
            }
        }

        if (GUILayout.Button("Import OLD version CSV - has to write new function")) {
            var data = CSVReader.ReadStringAll(path);
            //myTarget.locUnits = new LocalizationUnit[data.Count];
            var lang = myTarget.languageCodes;
            //myTarget.groups;
            myTarget.groups.Clear();
            LocalizationGroup currentGroup = null;
            for (int i = 0; i < data.Count; i++) {
                var key = data[i]["key"];
                if (key[0] == '@') { //group sign
                    LocalizationGroup lG = new LocalizationGroup();
                    lG = new LocalizationGroup();
                    myTarget.groups.Add(lG);
                    lG.Name = key;
                    currentGroup = lG;
                } else { //node sign
                    var lU = new LocalizationNode();

                    lU.key = key;
                    lU.values = new string[lang.Length];
                    for (int j = 0; j < lang.Length; j++) {
                        lU.values[j] = data[i][lang[j]];
                    }
                    currentGroup.AddNode(lU);
                }

                //  myTarget.locUnits[i] = lU;

            }
        }

        if (GUILayout.Button("Export CSV")) {
            StringBuilder sb = new StringBuilder();



            var lang = myTarget.languageCodes;
            sb.Append("key");
            for (int i = 0; i < lang.Length; i++) {
                sb.Append(',').Append(lang[i]);
            }
            sb.AppendLine();

            var groups = myTarget.groups;
            for (int j = 0; j < groups.Count; j++) {

                var g = groups[j];
                sb.Append(g.Name);
                sb.Append(" "+g.FolderName);
                sb.AppendLine();

                var units = g.Nodes;
                for (int i = 0; i < units.Count; i++) {
                    sb.Append(units[i].key);
                    var values = units[i].values;
                    for (int k = 0; k < values.Length; k++) {
                        sb.Append(',').Append('"').Append(values[k]).Append('"');

                    }
                    sb.AppendLine();
                }
            }


            string str = sb.ToString();
            using (FileStream fs = new FileStream(path, FileMode.Create)) {
                using (StreamWriter writer = new StreamWriter(fs)) {
                    writer.Write(str);
                }
            }
            UnityEditor.AssetDatabase.Refresh();
            //*/
        }


    }

    private void DrawFolder(string currentFolder,
        int folderId
        ) {
        bool folderFolded;
        if (folderId == -1) { //root
            rootFolderOpen = EditorGUILayout.Foldout(rootFolderOpen, "Root");
            folderFolded = rootFolderOpen;
        } else {
            this.folderFolded[folderId] = EditorGUILayout.Foldout(this.folderFolded[folderId], currentFolder);
            folderFolded = this.folderFolded[folderId];
        }

        if (folderFolded) {
            for (int i = 0; i < folderListAux.Count; i++) {
                var folder = folderListAux[i];
                string ParentName = folder.parentName;
                if ((currentFolder.Length == 0 && ParentName.Length == 0) || (currentFolder.Equals(ParentName))) {
                    //this means the folder is inside the current folder, so it has to be drawn
                    string nameFolder = folder.name;
                    folderListAux.RemoveAt(i);

                    if (nameFolder.Length != 0) {

                        //Debug.Log("FOLDER NAME "+nameFolder+i);
                        DrawFolder(nameFolder, myTarget.folders.IndexOf(folder)); //recursive method
                    }
                    i--;
                }

            }
            for (int i = 0; i < propListAux.Count; i++) {
                string ParentName = propListAux[i].FindPropertyRelative("folderName").stringValue;
                if ((currentFolder.Length == 0 && ParentName.Length == 0) || (currentFolder.Equals(ParentName))) {

                    //GUILayout.
                    EditorGUILayout.PropertyField(propListAux[i], true);
                    propListAux.RemoveAt(i);
                    i--;
                    //propListAux[i].
                }
            }
        }
        EditorGUILayout.Space();

    }

    void Start() {

    }

    void Update() {

    }
}
