﻿using UnityEngine;
using System.Collections;
using UnityEditor;

using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor(typeof(MagicaVoxelBlockerUtil))]
public class BlockerGenerationMagicaInspector : Editor {
    private int type;

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        string V = "Generate Blockers - Takes time";

        if (GUILayout.Button(V)) {
            var utilComp = (BlockerCreationUtil)target;
            var mesh = utilComp.GetComponent<MeshFilter>().mesh;
            var vs = mesh.vertices;
            Vector3 lowestValues = new Vector3(99999,99999,99999);
            for (int i = 0; i < vs.Length; i++) {
                var p = vs[i];
                for (int j = 0; j < 3; j++) {
                    if(lowestValues[j] > p[j]) {
                        lowestValues[j] = p[j];
                    }
                }
            }
            Vector3 bounds = new Vector3(500,500,500);
            Vector3 startFrom = lowestValues + new Vector3(1,1,1)*0.5f;


            //var b = mesh[0];
            type = utilComp.type;
            Transform t = utilComp.transform;
            var prefab = utilComp.prefabBlock;
            GameObject blockHolder = new GameObject();
            blockHolder.name = "blockholder";
            
            int children = t.childCount;
            for (int i = 0; i < children; i++) {
            //for (int i = 0; i < 1; i++) {
                var tChild = t.GetChild(i);
                var p = tChild.position;
                p += new Vector3(0, 5);

                {

                    var offset = new Vector3(10, 0, 0);
                    TryCreatePrefab(prefab, tChild, p, offset, blockHolder);
                }
                {
                    var offset = new Vector3(0, 0, 10);
                    TryCreatePrefab(prefab, tChild, p, offset, blockHolder);
                }
                {
                    var offset = new Vector3(-10, 0, 0);
                    TryCreatePrefab(prefab, tChild, p, offset, blockHolder);
                }
                {
                    var offset = new Vector3(0, 0, -10);
                    TryCreatePrefab(prefab, tChild, p, offset, blockHolder);
                }
            }
            blockHolder.transform.SetParent(t);

        }


    }

    private  void TryCreatePrefab(GameObject prefab, Transform tChild, 
        Vector3 p, Vector3 offset, GameObject blockHolder) {
        var offsetSp = Quaternion.Inverse(tChild.rotation) * offset;
        Vector3 p2 = p + offsetSp;
        Debug.Log(offsetSp+"converted vector");
        var rotation = tChild.rotation;
        var summonPos = p2 + new Vector3(0, 5);
        if(type == 1) {

            summonPos = tChild.position  + new Vector3(0, 5);
            rotation *= Quaternion.LookRotation(offset, Vector3.up);
            
        }
        if (!Physics.CheckSphere(p2, 2)) {

            
            //var obj = (GameObject)Instantiate(prefab, summonPos, rotation);
            var obj = (GameObject) PrefabUtility.InstantiatePrefab(prefab);
            obj.transform.position = summonPos;
            obj.transform.rotation = rotation;
            
            obj.transform.SetParent(blockHolder.transform);
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
