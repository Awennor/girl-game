﻿using UnityEngine;
using System.Collections;
using UnityEditor;

using UnityEngine;
using System.Collections;
using UnityEditor;
using CodeGeneration;
using Assets.Editor.T4stuff;
using System.IO;
using System;
using System.Text;

[CustomEditor(typeof(LocalizationDataDefinition))]
public class LocalizationDataInspectorOld : Editor {

    //static private TreasureIdManager treasureIdManager = new TreasureIdManager();
    string path = "Assets/translation.csv";
    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        LocalizationDataDefinition myTarget = (LocalizationDataDefinition)target;
        //string V = "New Fresh ID";

        /*if (GUILayout.Button("Import Skill Names")) {
            var names = SkillInfoProvider.Instance.Names;
            var oldUnits = myTarget.locUnits;
            var newUnits = new LocalizationUnit[oldUnits.Length + names.Length];
            var lang = myTarget.languageCodes;
            for (int i = 0; i < newUnits.Length; i++) {
                if(i < oldUnits.Length) {
                    newUnits[i] = oldUnits[i];
                } else {
                    var locUnit = new LocalizationUnit();
                    var skillId = i - oldUnits.Length;
                    locUnit.key = "SKILLNAME"+ skillId;
                    locUnit.values = new string[lang.Length];
                    locUnit.values[0] = names[skillId];
                    for (int j = 1; j < locUnit.values.Length; j++) {
                        locUnit.values[j] = "";
                    }
                    newUnits[i] = locUnit;
                }
            }
            myTarget.locUnits = newUnits;
            
        }
        */

        if (GUILayout.Button("Import CSV")) {
            var data = CSVReader.ReadStringAll(path);
            myTarget.locUnits = new LocalizationUnit[data.Count];
            var lang = myTarget.languageCodes;
            for (int i = 0; i < data.Count; i++) {
                var lU = new LocalizationUnit();
                lU.key = data[i]["key"];
                lU.values = new string[lang.Length];
                for (int j = 0; j < lang.Length; j++) {
                    lU.values[j] = data[i][lang[j]];
                }
                myTarget.locUnits[i] = lU;

            }
        }

        if (GUILayout.Button("Export CSV")) {
            StringBuilder sb = new StringBuilder();
            
            var units = myTarget.locUnits;
            var lang = myTarget.languageCodes;
            sb.Append("key");
            for (int i = 0; i < lang.Length; i++) {
                sb.Append(',').Append(lang[i]);
            }
            sb.AppendLine();
            for (int i = 0; i < units.Length; i++) {
                sb.Append(units[i].key);
                var values = units[i].values;
                for (int j = 0; j < values.Length; j++) {
                    sb.Append(',').Append('"').Append(values[j]).Append('"');

                }
                sb.AppendLine();
            }

            string str = sb.ToString();
            using (FileStream fs = new FileStream(path, FileMode.Create)) {
                using (StreamWriter writer = new StreamWriter(fs)) {
                    writer.Write(str);
                }
            }
            UnityEditor.AssetDatabase.Refresh();
        }

        if (GUILayout.Button("Generate Enum")) {
            Debug.Log("Try generate");
            const string name = "LocalizationEnum";
            int length = myTarget.locUnits.Length;
            string[] sourceKeys = new string[length];
            for (int i = 0; i < length; i++) {
                sourceKeys[i] = myTarget.locUnits[i].key;
            }
            var generator = new EnumItemsGenerator(name, sourceKeys);

            var classDefintion = generator.TransformText();
            Debug.Log(classDefintion);

            var outputPath = Path.Combine(Application.dataPath + "/Scripts/Localization", name + ".cs");

            try {
                // Save new class to assets folder.
                File.WriteAllText(outputPath, classDefintion);
                Debug.Log("Generated!" + outputPath);

                // Refresh assets.
                AssetDatabase.Refresh();
            } catch (Exception e) {
                Debug.Log("An error occurred while saving file: " + e);
            }
        }

    }

    void Start() {

    }

    void Update() {

    }
}

