﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(LocalizationNode))]
public class LocalizationNodePropertyDrawer : PropertyDrawer {

    int selectedValue;

    public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
		//return Screen.width < 333 ? (16f + 18f) : 16f;
        return 64;
	}

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

        //      Rect contentPos = EditorGUI.PrefixLabel(position, label);
        //EditorGUI.PropertyField(contentPos, property.FindPropertyRelative("key"), GUIContent.none);

        var valuesProp = property.FindPropertyRelative("values");

        EditorGUI.indentLevel = 0;

        label = EditorGUI.BeginProperty(position, label, property);
        Rect contentPosition = position;
        float oldHeight = contentPosition.height;
        float oldWidth = contentPosition.width;

        int dropDownWidth = 30;
        int sizeFieldWidth = 30;
        int initialLabelWidth = 120;

        contentPosition.width = initialLabelWidth;
        EditorGUI.LabelField(contentPosition, "Key/size/index");

        contentPosition.x += contentPosition.width;
        contentPosition.width = oldWidth;
        contentPosition.width -= dropDownWidth;
        contentPosition.width -= sizeFieldWidth;
        contentPosition.width -= initialLabelWidth;
        contentPosition.height = 16;
        //EditorGUI.indentLevel = 0;
        EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("key"), GUIContent.none);

        contentPosition.x += contentPosition.width;
        contentPosition.width = sizeFieldWidth;
        valuesProp.arraySize = EditorGUI.IntField(contentPosition, valuesProp.arraySize);



        contentPosition.x += contentPosition.width;
        contentPosition.width = dropDownWidth;
        //selectedValue = EditorGUI.Popup(contentPosition, selectedValue, numberStrings.ToArray());
        selectedValue = EditorGUI.IntField(contentPosition, selectedValue);
        if (selectedValue >= valuesProp.arraySize) {
            selectedValue = valuesProp.arraySize-1;
            
        }
        //EditorGUI.sli
       

        var element0 = valuesProp.GetArrayElementAtIndex(selectedValue);

        var contentPosition2 = contentPosition;
        contentPosition2.x = position.x;
        contentPosition2.width = position.width;
        contentPosition2.y+= 16;
        contentPosition2.height = oldHeight - 16;
        element0.stringValue = EditorGUI.TextArea(contentPosition2,element0.stringValue);
        
		EditorGUI.EndProperty();

        //// Using BeginProperty / EndProperty on the parent property means that
        //// prefab override logic works on the entire property.
        //EditorGUI.BeginProperty(position, label, property);

        //// Draw label
        //position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        //// Don't make child fields be indented
        //var indent = EditorGUI.indentLevel;
        //EditorGUI.indentLevel = 0;

        //// Calculate rects
        ////var amountRect = new Rect(position.x, position.y, 30, position.height);
        ////var unitRect = new Rect(position.x + 35, position.y, 200, position.height);
        ////var nameRect = new Rect(position.x + 90, position.y, position.width - 90, position.height);

        //// Draw fields - passs GUIContent.none to each so they are drawn without labels
        ////EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("key"), GUIContent.none);
        ////EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("values"), GUIContent.none);
        ////EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);

        //// Set indent back to what it was
        //EditorGUI.indentLevel = indent;

        //EditorGUI.EndProperty();
    }
}