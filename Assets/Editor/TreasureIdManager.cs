﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

public class TreasureIdManager {
    private bool neverLoaded = true;
    List<int> treasureIds = new List<int>();
    string path =  "Assets/Resources/treasureids.txt";
    internal int GetNewID() {
        if (treasureIds.Count == 0 && neverLoaded) {
            LoadFile();
        }
        treasureIds.Sort();

        int freeId = 0;
        for (int i = 0; i < treasureIds.Count; i++) {
            if (treasureIds[i] == freeId) {
                freeId++;
            } else {
                break;
            }

        }
        treasureIds.Add(freeId);
        SaveFile();
        return freeId;
    }

    internal void RemoveID(int id) {
        treasureIds.Remove(id);
        SaveFile();
    }

    private void LoadFile() {
        neverLoaded = false;



        string content = null;
        using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate)) {
            using (StreamReader writer = new StreamReader(fs)) {
                content = writer.ReadToEnd();
            }
        }
        if(content.Length > 0) {
            var splitted = content.Split(',');
            for (int i = 0; i < splitted.Length; i++) {
                int treasureId = -1;
                if(int.TryParse(splitted[i], out treasureId)) {
                    treasureIds.Add(treasureId);
                }
                
            }
        }
        UnityEditor.AssetDatabase.Refresh();
    }

    private void SaveFile() {
        // You cannot add a subfolder, at least it does not work for me
        
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < treasureIds.Count; i++) {
            sb.Append(treasureIds[i]);
            if(i!= treasureIds.Count-1)
                sb.Append(',');
        }


        string str = sb.ToString();
        using (FileStream fs = new FileStream(path, FileMode.Create)) {
            using (StreamWriter writer = new StreamWriter(fs)) {
                writer.Write(str);
            }
        }
        UnityEditor.AssetDatabase.Refresh();
    }
}
