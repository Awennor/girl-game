﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(LocalizationKey))]
public class LocalizationKeyDrawer : PropertyDrawer {

   // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);
        //EditorGUI.LabelField(position, "TEST");
        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        
        // Don't make child fields be indented
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        Rect amountRect = new Rect(position.x, position.y, 50, position.height);
        Rect unitRect = new Rect(position.x + 55, position.y, 50, position.height);
        Rect nameRect = new Rect(position.x + 105, position.y, position.width - 90, position.height);

        

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        var serializedProperty = property.FindPropertyRelative("localizationData");
        EditorGUI.PropertyField(amountRect, serializedProperty, GUIContent.none);
        LocalizationDataDefine lDD = (LocalizationDataDefine)serializedProperty.objectReferenceValue;

        if (lDD != null) {
            var groupKeyProp = property.FindPropertyRelative("groupKey");
            var groupKey = groupKeyProp.stringValue;
            var entryKeyProp = property.FindPropertyRelative("entryKey");
            var entryKey = entryKeyProp.stringValue;

            var groupNames = lDD.GroupNames();
            int groupId = 0;
            for (int i = 0; i < groupNames.Length; i++) {
                if (groupNames[i].Equals(groupKey)) {
                    groupId = i;
                }
            }

            groupId = EditorGUI.Popup(unitRect, groupId, groupNames);
            var group = lDD.groups[groupId];
            groupKey = group.Name;
            groupKeyProp.stringValue = groupKey;
            var nodes = group.Nodes;
            int keyId = 0;
            for (int i = 0; i < nodes.Count; i++) {
                if (nodes[i].key.Equals(entryKey)) {
                    keyId = i;
                }
            }

            var messages = lDD.MessageArrayFirstLanguage(groupId);

            keyId = EditorGUI.Popup(nameRect, keyId, messages);
            entryKeyProp.stringValue = nodes[keyId].key;
        }
        //*/

        //EditorGUI.PropertyField(, property.FindPropertyRelative ("name"), GUIContent.none);

        // Set indent back to what it was
        //EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}