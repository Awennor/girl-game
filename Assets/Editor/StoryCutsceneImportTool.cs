﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System;

public class StoryCutsceneImportTool : EditorWindow {

    //LocalizationDataDefine localizationData;
    string groupname = "";
    string script = "";
    int selectedString = -1;
    private CutsceneComp cutsceneComp;
    LocalizationDataDefine localiData;
    List<string[]> aux = new List<string[]>();
    List<string> aux2 = new List<string>();

    [MenuItem("Tools/GirlUtils/Cutscene script importer")]
    static void Init() {
        StoryCutsceneImportTool window = (StoryCutsceneImportTool)EditorWindow.GetWindow(typeof(StoryCutsceneImportTool));
        window.Show();
        //ScriptableWizard.DisplayWizard<StoryCutsceneImportTool>("Cutscene Import Tool", "To Localization File", "To cutscene comp");

    }

    void OnGUI() {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        if (aux2.Count > 0) {
            selectedString = EditorGUILayout.Popup("Group for local. Aux",selectedString, aux2.ToArray());
            if(selectedString != -1) {
                groupname = aux2[selectedString];
                selectedString = -1;
            }
            

        }
        groupname = EditorGUILayout.TextField("Group for local.", groupname);
        

        EditorGUILayout.LabelField("script");
        script = EditorGUILayout.TextArea(script);
        localiData = (LocalizationDataDefine)EditorGUILayout.ObjectField("Local. Data", localiData, typeof(LocalizationDataDefine), true);
        if(localiData != null) {
            aux2.Clear();
            aux2.AddRange(localiData.GroupNames());
        }
        cutsceneComp = (CutsceneComp)EditorGUILayout.ObjectField("CutsceneComp", cutsceneComp, typeof(CutsceneComp), true);
        if (GUILayout.Button("Extract to localization data")) {
            LocalizationGroup lg = new LocalizationGroup();
            lg.Name = groupname;
            localiData.groups.Add(lg);

            ProcessScriptInPieces(script);
            var lines = script.Split('\n');
            foreach (string[] pieces in aux) {
                string dialogPiece = pieces[1];
                LocalizationNode ln = new LocalizationNode();
                lg.Nodes.Add(ln);
                ln.key = "d" + lg.Nodes.Count;
                ln.values = new string[localiData.languageCodes.Length];
                ln.values[0] = dialogPiece;
            }
            EditorUtility.SetDirty(localiData);
        }
        if (GUILayout.Button("Extract to cutscene comp")) {

            ProcessScriptInPieces(script);


            for (int i = 0; i < aux.Count; i++) {
                string speaker = aux[i][0];
                CutsceneNode cn = new CutsceneNode();
                cn.Strings = new MyStringArray();
                cn.SetString(groupname, 0);
                cn.SetString("d" + (i + 1), 1);
                cn.SetString(speaker.ToUpper(), 2);
                cn.Type = CutsceneNodeType.TextMessage;
                cutsceneComp.AddNode(cn);
            }
        }

        if (GUILayout.Button("Replace existing dialogs in cutscene comp")) {

            ProcessScriptInPieces(script);

            int node_index = 0;
            for (int i = 0; i < aux.Count; i++) {
                var nodes = cutsceneComp.Nodes;
                CutsceneNode cn = null;
                for (; node_index < nodes.Length; node_index++) {
                    if (nodes[node_index].Type == CutsceneNodeType.TextMessage) {
                        cn = nodes[node_index];
                        node_index++; //so it does not repeat
                        break;
                    }
                }
                if (cn == null) { //if no dialog nodes in current scene, add node
                    cn = new CutsceneNode();
                    cutsceneComp.AddNode(cn);
                }
                string speaker = aux[i][0];

                cn.Strings = new MyStringArray();
                cn.SetString(groupname, 0);
                cn.SetString("d" + (i + 1), 1);
                cn.SetString(speaker.ToUpper(), 2);
                cn.Type = CutsceneNodeType.TextMessage;

            }
        }
    }



    private void ProcessScriptInPieces(string script) {
        aux.Clear();
        var lines = script.Split('\n');


        for (int i = 0; i < lines.Length; i++) {

            var line = lines[i];
            if (line.Length >= 1 && line[0] != '#') {
                var pieces = line.Split(new string[] { ": " }, StringSplitOptions.None);
                //line.Split("a");
                if (pieces.Length <= 1) {
                    continue;
                } else {

                    aux.Add(pieces);

                }
            }

        }
    }
}
