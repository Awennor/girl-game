﻿using UnityEngine;
using System.Collections;
using UnityEditor;

using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor(typeof(UVChanger))]
public class UVChangerInspector : Editor {
    private string meshname = "name of the mesh";

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        string V = "Do UV Change and Create New Mesh";

        meshname= GUILayout.TextArea(meshname);
        
        if (GUILayout.Button(V)) {
            var utilComp = (UVChanger)target;
            var filters = utilComp.MeshFilters;
            for (int i = 0; i < filters.Count; i++) {
                Mesh m = filters[i].sharedMesh;

                //Debug.Log(AssetDatabase.GetAssetPath(m));
                var mesh = CopyMesh(m, meshname);
                filters[i].sharedMesh = mesh;
                utilComp.ChangeThisMesh(mesh);
            }
            


        }


    }

    [MenuItem("Assets/CopyMesh")]
    static Mesh CopyMesh(Mesh m, string meshname)
    {
        Mesh mesh = m;
        Mesh newmesh = new Mesh();
        newmesh.vertices = mesh.vertices;
        newmesh.triangles = mesh.triangles;
        newmesh.uv = mesh.uv;
        newmesh.normals = mesh.normals;
        newmesh.colors = mesh.colors;
        newmesh.tangents = mesh.tangents;
        AssetDatabase.CreateAsset(newmesh, "Assets/Graphics&Animation/MeshData/" + meshname+ ".asset");
        return newmesh;
    }
 

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
