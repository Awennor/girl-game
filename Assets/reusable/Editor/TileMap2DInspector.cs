﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;



[CustomEditor(typeof(TileMap2DRenderer))]
public class TileMap2DRendererInspector : Editor {
    public override void OnInspectorGUI() {

        DrawDefaultInspector();
        if (GUILayout.Button("Generate Objects using prefabs")) {
            (target as TileMap2DRenderer).InstantiateObjects(InstantiateWithPrefab);
        }
        
    }

    public GameObject InstantiateWithPrefab(GameObject obj) {
        if(ValidateCreatePrefab(obj)) {
            return (GameObject) PrefabUtility.InstantiatePrefab(obj);
        } else {
            return (target as TileMap2DRenderer).DefaultInstantiate(obj);
        }
    }

    public bool ValidateCreatePrefab(GameObject go) {
        if (go == null)
            return false;
        return PrefabUtility.GetPrefabType(go) == PrefabType.Prefab || PrefabUtility.GetPrefabType(go) == PrefabType.ModelPrefab;
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
