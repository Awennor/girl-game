﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class FontReplaceWindow : EditorWindow {
    private GameObject selection;

    public Font fontToUse;

    [MenuItem("Window/Font Replace Window")]

    public static void ShowWindow() {
        EditorWindow.GetWindow(typeof(FontReplaceWindow));
    }



    void OnGUI() {
        fontToUse = (Font)EditorGUILayout.ObjectField("CutsceneComp", fontToUse, typeof(Font), true);
        if (GUILayout.Button("Change all fonts in selected Text Components")) {
            var objs = Selection.gameObjects;
            foreach(var o in objs) {
                var text = o.GetComponent<Text>();
                if(text != null) {
                    text.font = fontToUse;
                }
            }
        }

        if (GUILayout.Button("Change all fonts in all findable Text Components")) {
            
            //var objs = GameObject.FindObjectsOfType<Text>();
            var objs = Resources.FindObjectsOfTypeAll<Text>();
            Debug.Log("number of objects "+objs.Length);
            foreach(var o in objs) {
                //var text = o.GetComponent<Text>();
                var text = o;
                if(text != null) {
                    text.font = fontToUse;
                }
            }
        }

    }

}
