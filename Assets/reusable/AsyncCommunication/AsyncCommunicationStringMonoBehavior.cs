﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsyncCommunicationStringMonoBehavior : MonoBehaviour {

    public AsyncCommunicationStringAsset commuAsset;
    
    public string user;

    public EventStateMachine callbackHandler;

    void Awake() {
        commuAsset.OnMessageReceive += CommuAsset_OnMessageReceive;
    }

    private void CommuAsset_OnMessageReceive(string arg1, string arg2) {
        if(!arg1.Equals(user)) { //not message from self
            if(callbackHandler != null) {
                callbackHandler.ChangeStateByName(arg2);
            }
        }
    }

    public void Send(string m) {
        commuAsset.SendMessage(user, m);
    }

}
