﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "String Communication", menuName = "Async Communication/String Communication", order = 3)]
public class AsyncCommunicationStringAsset : ScriptableObject {

    private Action<string, string> OnMessageReceiveInternal;

    public event Action<string, string> OnMessageReceive{
        add {
            OnMessageReceiveInternal -= value;
            OnMessageReceiveInternal += value;
        }
        remove {
            OnMessageReceiveInternal -= value;
        }
    }

    public void SendMessage(string sender, string message) {
        if (OnMessageReceiveInternal != null)
            OnMessageReceiveInternal(sender, message);
    }



}
