﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMap2DHolder : MonoBehaviour {
    [SerializeField]
    TwoDArrayInt tiles;

    public TwoDArrayInt Tiles {
        get {
            return tiles;
        }

        set {
            tiles = value;
        }
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
