﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EditorGenerateAuxiliar : MonoBehaviour {

    public GameObject matrixObj;
    public bool generateNow_Editor;
    [SerializeField]
    private GameObject generatedParent;
    [SerializeField]
    private Transform[] positions;

    List<GameObject> generatedObjects = new List<GameObject>();
    public MyGameObjectListEvent OnObjectsGenerate;

    // Use this for initialization
    void Start() {
        generatedParent = transform.GetOrAddChildWithName("Automatically_Generated");
    }

    // Update is called once per frame
    void Update() {
        if (generateNow_Editor) {
            generatedObjects.Clear();
            generateNow_Editor = false;
            generatedParent.transform.DestroyAllChildren();
            for (int j = 0; j < positions.Length; j++) {
                for (int i = 0; i < positions[j].childCount; i++) {
                    var obj = Instantiate(matrixObj, generatedParent.transform);
                    obj.transform.position = positions[j].GetChild(i).position;
                    generatedObjects.Add(obj);

                }
            }

            OnObjectsGenerate.Invoke(generatedObjects);
        }
    }
}
