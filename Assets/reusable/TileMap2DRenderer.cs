﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMap2DRenderer : MonoBehaviour {

    [SerializeField]
    TwoDArrayInt tiles;
    [SerializeField]
    float scale = 1;
    [SerializeField]
    MyDicIntGameObject renderData = new MyDicIntGameObject();
    private GameObject parent;
    public event Action<GameObject, PointInt2D> tileInstantiated;

    public void InstantiateObjects(Func<GameObject, GameObject> instantiateMethod) {
        if (parent == null)
            parent = gameObject.transform.GetOrAddChildWithName("AutoGen");
        parent.transform.DestroyAllChildren();
        for (int i = 0; i < tiles.Rows; i++) {
            for (int j = 0; j < tiles.Columns; j++) {
                var tile = tiles[i, j];
                if (renderData.ContainsKey(tile)) {
                    var matrix = renderData[tile];
                    var go = instantiateMethod(matrix.gameObject);
                    go.transform.SetParent(parent.transform);
                    go.transform.localPosition = new Vector3(i*scale, 0, j * scale);
                    if(tileInstantiated != null) {
                        tileInstantiated(go, new PointInt2D(i,j));
                    }
                }

            }
        }

    }

    public void InstantiateObjectsWithDoneCallBack(Action done) {
        InstantiateObjects(DefaultInstantiate);
        if(done != null) {
            done();
        }
    }

    public void AddRenderData(int element, GameObject matrix) {
        renderData[element] = matrix;
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void ClearRenderData() {
        renderData.Clear();
    }

    public GameObject DefaultInstantiate(GameObject obj) {
        return Instantiate(obj);
    }

    public TwoDArrayInt Tiles {
        get {
            return tiles;
        }

        set {
            tiles = value;
        }
    }
}
