﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Persistence Asset", menuName = "Data/Persistence Asset", order = 2)]
public class PersistenceScriptableObject : ScriptableObject {
    [SerializeField]
    AutoSerializeDictionary slotMaster = new AutoSerializeDictionary();
    [SerializeField]
    AutoSerializeDictionary slotCurrent = new AutoSerializeDictionary();
    bool loaded;

    

    private void TryLoad() {
        if (!loaded) {
            loaded = true;
            slotMaster.LoadMethodExternal = LoadPlayerPrefs;
            slotMaster.SaveMethodExternal = SavePlayerPrefs;
            slotMaster.LoadData();
            slotCurrent.LoadMethodExternal = slotMaster.GetData;
            slotCurrent.SaveMethodExternal = slotMaster.AddDataAndSave;
        }

    }

    private string LoadPlayerPrefs(string key) {
        string content = PlayerPrefs.GetString(key, null);
        return content;
    }

    private void SavePlayerPrefs(string key, string content) {
        PlayerPrefs.SetString(key, content);
    }

    [ContextMenu("Force save current slot")]
    void ForceSave() {
        slotCurrent.SaveSelf();
    }

    [ContextMenu("Init Slot 0")]
    void InitSlot0AndLoadMaster() {
        //PlayerPrefs.DeleteKey(slotMaster.Key);
        TryLoad();
        SlotChosen(0);
        slotCurrent.SaveSelf();
    }

    public void SlotChosen(int slot) {
        TryLoad();
        slotCurrent.Key = "" + slot;
        slotCurrent.LoadData();
    }

    public string GetDataFromCurrentSlot(string arg1) {
        return slotCurrent.GetData(arg1);
    }

    internal void SaveDataToCurrentSlot(string arg1, string arg2) {
        TryLoad();
        slotCurrent.AddDataAndSave(arg1, arg2);
    }
}
