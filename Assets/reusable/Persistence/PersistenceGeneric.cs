﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class MyPersistenceTwoDArrayInt : PersistenceGeneric<TwoDArrayInt> {}
[Serializable]
public class PersistenceGeneric<T>{
    [SerializeField]
    string key;

    XMLSerializationHelper<T> xmlHelper = new XMLSerializationHelper<T>();
    public PersistenceComponent persistMaster;
    public void Save(T data) {
        string xml = xmlHelper.SerializeToStringWithEncode(data);
        persistMaster.AddData(key, xml);
    }

    public T Load() {
        string xml = persistMaster.GetData(key);
        if(xml == null || xml.Length <= 0) {
            return default(T);
        }
        return xmlHelper.DeserializeStringWithDecode(xml);
    }

}
