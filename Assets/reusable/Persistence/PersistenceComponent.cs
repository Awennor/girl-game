﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class PersistenceComponent : MonoBehaviour {
    [SerializeField]
    AutoSerializeDictionary persistDict;
    public PersistenceScriptableObject persistMaster;
    bool bound;

    public void Awake() {
        Bind();
    }

    private void Bind() {
        //Debug.Log("TRY BIND "+bound);
        //if (!bound) {
        bound = true;
        persistDict.LoadMethodExternal = persistMaster.GetDataFromCurrentSlot;
        persistDict.SaveMethodExternal = persistMaster.SaveDataToCurrentSlot;
        //Debug.Log(persistDict.SaveMethodExternal);
        //}

    }

    internal void AddData(string key, string data) {
        Bind();
        persistDict.AddDataAndSave(key, data);
    }

    internal string GetData(string key) {
        Bind();
        return persistDict.GetData(key);

    }
}

[Serializable]
public class AutoSerializeDictionary {
    [SerializeField]
    MyDicStringString data = new MyDicStringString();
    private XMLSerializationHelper<MyDicStringString> serializer = new XMLSerializationHelper<MyDicStringString>();
    private Action<string, string> saveMethodExternal;
    private Func<string, string> loadMethodExternal;
    [SerializeField]
    private string key;

    public Func<string, string> LoadMethodExternal {
        get {
            return loadMethodExternal;
        }

        set {
            loadMethodExternal = value;
        }
    }

    public string Key {
        get {
            return key;
        }

        set {
            key = value;
        }
    }

    public Action<string, string> SaveMethodExternal {
        get {
            return saveMethodExternal;
        }

        set {
            saveMethodExternal = value;
        }
    }

    public void AddDataAndSave(string key, string dataToSave) {
        data[key] = dataToSave;
        //Debug.Log(saveMethodExternal);
    }

    public void SaveSelf() {
        Debug.Log(saveMethodExternal);
        data.OnBeforeSerialize();
        var stringData = serializer.SerializeToStringWithEncode(data);

        if (saveMethodExternal == null) {
            Debug.Log("NO SAVE METHOD");
        } else {
            //Debug.Log("SAVE OKAY");
        }
        saveMethodExternal(this.Key, stringData);
    }

    public string GetData(string key) {
        if (data.ContainsKey(key))
            return data[key];
        else
            return null;
    }

    public void LoadData() {
        string load = loadMethodExternal(Key);
        if (load != null && load.Length > 0) {

            data = serializer.DeserializeStringWithDecode(load);
            data.OnAfterDeserialize();
        } else {
            data = new MyDicStringString();
        }
    }


}
