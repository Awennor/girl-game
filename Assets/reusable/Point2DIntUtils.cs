﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Point2DIntUtils {


    internal static void FillWithRectangleCenteredOn(PointInt2D center, PointInt2D size, List<PointInt2D> aux) {
        //calculation of left down corner
        PointInt2D middleOfRectangle =  size.MultiplyFloor(0.5f);
        PointInt2D ldCorner = center - middleOfRectangle;
        //fill up the rectangle
        for (int i = 0; i < size.x; i++) {
            for (int j = 0; j < size.y; j++) {
                aux.Add(ldCorner + new PointInt2D(i,j));
            }
        }
    }

}
