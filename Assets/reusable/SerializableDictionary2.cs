﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace com.spacepuppy.Collections
{

    public abstract class DrawableDictionary
    {

    }

    [System.Serializable()]
    public class SerializableDictionaryBase<TKey, TValue> : DrawableDictionary, UnityEngine.ISerializationCallbackReceiver
    {

        #region Fields

        [System.NonSerialized(), XmlIgnore]
        private Dictionary<TKey, TValue> _dict;

        #endregion

        #region IDictionary Interface

        [XmlIgnore]
        public int Count
        {
            get { return (_dict != null) ? _dict.Count : 0; }
        }


        public void Add(TKey key, TValue value)
        {
            if (_dict == null) _dict = new Dictionary<TKey, TValue>();
            _dict.Add(key, value);
        }

        public bool ContainsKey(TKey key)
        {
            if (_dict == null) return false;
            return _dict.ContainsKey(key);
        }

        [XmlIgnore]
        public ICollection<TKey> Keys
        {
            get
            {
                if (_dict == null) _dict = new Dictionary<TKey, TValue>();
                return _dict.Keys;
            }
        }

        public bool Remove(TKey key)
        {
            if (_dict == null) return false;
            return _dict.Remove(key);
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            if(_dict == null)
            {
                value = default(TValue);
                return false;
            }
            return _dict.TryGetValue(key, out value);
        }

        [XmlIgnore]
        public ICollection<TValue> Values
        {
            get
            {
                if (_dict == null) _dict = new Dictionary<TKey, TValue>();
                return _dict.Values;
            }
        }

        [XmlIgnore]
        public TValue this[TKey key]
        {
            get
            {
                if (_dict == null) throw new KeyNotFoundException();
                return _dict[key];
            }
            set
            {
                if (_dict == null) _dict = new Dictionary<TKey, TValue>();
                _dict[key] = value;
            }
        }

        public void Clear()
        {
            if (_dict != null) _dict.Clear();
        }

        void Add(KeyValuePair<TKey, TValue> item)
        {
            if (_dict == null) _dict = new Dictionary<TKey, TValue>();
            (_dict as ICollection<KeyValuePair<TKey, TValue>>).Add(item);
        }

        bool Contains(KeyValuePair<TKey, TValue> item)
        {
            if (_dict == null) return false;
            return (_dict as ICollection<KeyValuePair<TKey, TValue>>).Contains(item);
        }

        void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            if (_dict == null) return;
            (_dict as ICollection<KeyValuePair<TKey, TValue>>).CopyTo(array, arrayIndex);
        }

        bool Remove(KeyValuePair<TKey, TValue> item)
        {
            if (_dict == null) return false;
            return (_dict as ICollection<KeyValuePair<TKey, TValue>>).Remove(item);
        }

        bool IsReadOnly
        {
            get { return false; }
        }

        public Dictionary<TKey, TValue>.Enumerator GetEnumerator()
        {
            if (_dict == null) return default(Dictionary<TKey, TValue>.Enumerator);
            return _dict.GetEnumerator();
        }

        //System.Collections.IEnumerator GetEnumerator()
        //{
        //    if (_dict == null) return Enumerable.Empty<KeyValuePair<TKey, TValue>>().GetEnumerator();
        //    return _dict.GetEnumerator();
        //}

        //IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        //{
        //    if (_dict == null) return Enumerable.Empty<KeyValuePair<TKey, TValue>>().GetEnumerator();
        //    return _dict.GetEnumerator();
        //}

        #endregion

        #region ISerializationCallbackReceiver

        [UnityEngine.SerializeField()]
        public TKey[] _keysinner;
        [UnityEngine.SerializeField()]
        public TValue[] _valuesinner;

        public void OnAfterDeserialize()
        {
            if(_keysinner != null && _valuesinner != null)
            {
                if (_dict == null) _dict = new Dictionary<TKey, TValue>(_keysinner.Length);
                else _dict.Clear();
                for(int i = 0; i < _keysinner.Length; i++)
                {
                    if (i < _valuesinner.Length)
                        _dict[_keysinner[i]] = _valuesinner[i];
                    else
                        _dict[_keysinner[i]] = default(TValue);
                }
            }

            //_keysinner = null;
            //_valuesinner = null;
        }

        public void OnBeforeSerialize()
        {
            if(_dict == null || _dict.Count == 0)
            {
                _keysinner = null;
                _valuesinner = null;
            }
            else
            {
                int cnt = _dict.Count;
                _keysinner = new TKey[cnt];
                _valuesinner = new TValue[cnt];
                int i = 0;
                var e = _dict.GetEnumerator();
                while (e.MoveNext())
                {
                    _keysinner[i] = e.Current.Key;
                    _valuesinner[i] = e.Current.Value;
                    i++;
                }
            }
        }

        #endregion

    }
}