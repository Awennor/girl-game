﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

[Serializable]
public class TwoDArrayInt : TwoDArray<int> {


    public TwoDArrayInt(int c, int r) : base(c, r) { }
    public TwoDArrayInt() : base() { }

    
}

[Serializable]
public class TwoDArray<T> {

    [SerializeField]
    T[] content;

    [SerializeField]
    int columns;
    [SerializeField]
    int rows;

    public TwoDArray() {

    }

    public TwoDArray(int rows, int columns) {
        Allocate(rows, columns);
    }

    internal void IterateAction(Action<T, int, int> action) {
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < columns; c++) {
                action(content[this.i(r,c)],r, c);
            }
        }
    }

    public void Allocate(int rows, int columns) {
        this.columns = columns;
        this.rows = rows;
        Content = new T[rows * columns];
    }

    public void Allocate(PointInt2D size) {
        this.columns = size.x;
        this.rows = size.y;


        Content = new T[rows * columns];
    }

    internal void ChangeEveryElementTo(T v) {
        for (int i = 0; i < content.Length; i++) {
            content[i] = v;
        }
    }

    public T this[int index] {
        get {
            return Content[index];
        }

        set {
            Content[index] = value;
        }
    }

    [XmlIgnore]
    public PointInt2D Size {
        get {
            return new PointInt2D(columns, rows);
        }
    }

    [XmlIgnore]
    public T this[PointInt2D p] {
        get {
            return this[p.y, p.x];
        }

        set {
            this[p.y, p.x] = value;
        }
    }

    [XmlIgnore]
    public T this[int r, int c] {
        get {
            return Content[i(r, c)];
        }

        set {
            Content[i(r, c)] = value;
        }
    }

    internal bool SafeSet(int r, int c, T e) {
        var index = i(r, c);
        if (index >= 0 && index < content.Length) {
            content[index] = e;
            return true;
        }
        return false;

    }

    internal bool ValidIndex(PointInt2D pos) {
        var index = i(pos);
        if (index >= 0 && index < content.Length) {
            return true;
        } else {
            return false;
        }
         
    }

    private int i(PointInt2D pos) {
        return pos.Row * columns + pos.Column;
    }

    private int i(int r, int c) {
        return r * columns + c;
    }

    public int Columns {
        get {
            return columns;
        }
        set {
            columns = value;
        }
    }

    public int Rows {
        get {
            return rows;
        }
        set {
            rows = value;
        }
    }

    public T[] Content {
        get {
            return content;
        }

        set {
            content = value;
        }
    }

    public void Add() {
    }

}
