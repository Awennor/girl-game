﻿using com.spacepuppy.Collections;
using System;
using UnityEngine;


[Serializable]
public class MyDicIntGameObject : SerializableDictionaryBase<int, GameObject> { };
[Serializable]
public class MyDicStringString : SerializableDictionaryBase<string, string> { };
