﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataConstantMaster", menuName = "Data/Data Constants/Master Asset", order = 2)]
public class DataConstantMasterAsset : ScriptableObject {

    [SerializeField]
    public DataConstantDefinitionAssetPair[] assetPairs;

    [Serializable]
    public class DataConstantDefinitionAssetPair{
        [SerializeField]
        int assetIdentifier;
        [SerializeField]
        DataConstantDefinitionAsset asset;

        public DataConstantDefinitionAsset Asset{
            get {
                return asset;
            }

            set {
                asset = value;
            }
        }

        public int AssetIdentifier {
            get {
                return assetIdentifier;
            }

            set {
                assetIdentifier = value;
            }
        }
    }

    public string[] AssetNames() {
        string[] s =  new String[assetPairs.Length];
        for (int i = 0; i < assetPairs.Length; i++) {
            s[i] = assetPairs[i].Asset.name;
        }
        return s;
    }

    public int AssetIdentificationOfIndex(int index) {
        if(index <0 || index > assetPairs.Length) return -1;
        return assetPairs[index].AssetIdentifier;
    }

    public DataConstantDefinitionAsset GetAsset(int assetIdentifier) {
        for (int i = 0; i < assetPairs.Length; i++) {
            if(assetPairs[i].AssetIdentifier == assetIdentifier) {
                return assetPairs[i].Asset;
            }
        }
        return null;
    }

    public int IndexOf(int assetIdentifier) {
        for (int i = 0; i < assetPairs.Length; i++) {
            if(assetPairs[i].AssetIdentifier == assetIdentifier) {
                return i;
            }
        }
        return -1;
    }
}
