﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataConstantDefinition", menuName = "Data/Data Constants/Definition Asset", order = 3)]
public class DataConstantDefinitionAsset : ScriptableObject {

    [SerializeField]
    DataConstantDefinitionPair[] constants;

    [Serializable]
    public class DataConstantDefinitionPair {
        [SerializeField]
        string label;
        [SerializeField]
        int value;

        public string Label {
            get {
                return label;
            }

            set {
                label = value;
            }
        }

        public int Value {
            get {
                return value;
            }

            set {
                this.value = value;
            }
        }
    }

    public int IndexOf(int assetIdentifier) {
        for (int i = 0; i < constants.Length; i++) {
            if (constants[i].Value == assetIdentifier) {
                return i;
            }
        }
        return -1;
    }

 
    public string[] ConstantLabels() {
        string[] s =  new String[constants.Length];
        for (int i = 0; i < constants.Length; i++) {
            s[i] = constants[i].Label;
        }
        return s;
    }

    public int ValueOfIndex(int v) {
        if(v < 0 || v >= constants.Length) return -1;
        return constants[v].Value;
    }
}
