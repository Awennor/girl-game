﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct DataConstantValue : IEquatable<DataConstantValue> {


    public static implicit operator int (DataConstantValue d) {
        return d.Value;
    }

    [SerializeField]
    int constantType;
    [SerializeField]
    int value;

    public DataConstantValue(int type, int value) {
        constantType = type;

        this.value = value;
    }

    public DataConstantValue(int value) {
        constantType = 0;
        this.value = value;
    }

    public int Value {
        get {
            return value;
        }
    }

    public int ConstantType {
        get {
            return constantType;
        }

        set {
            constantType = value;
        }
    }

    bool IEquatable<DataConstantValue>.Equals(DataConstantValue other) {
        return other == this;
    }

    public override bool Equals(System.Object obj) {
        return obj is DataConstantValue && this == (DataConstantValue)obj;
    }

    public static bool operator ==(DataConstantValue x, DataConstantValue y) {
        return x.value == y.value;
    }

    public static bool operator !=(DataConstantValue x, DataConstantValue y) {
        return x.value != y.value;
    }
}
