﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataConstantObjectActivator : MonoBehaviour {
    [SerializeField]
    SwitchUnit[] switches;

    public void IntValueReceive(int i) {
        //Debug.Log("VALUE RECEIVE "+i);
        foreach (var s in switches) {
            bool active = false;
            foreach (var v in s.values) {
                if (v == i) {
                    active = true;
                    break;
                }
            }
            foreach (var obj in s.objs) {
                obj.SetActive(active);
            }
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    [Serializable]
    private class SwitchUnit  {
        public DataConstantValue[] values;
        public GameObject[] objs;


    }
}
