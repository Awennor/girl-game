﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataConstantSwitchEvent : MonoBehaviour {
    [SerializeField]
    SwitchUnit[] switches;

    public void IntValueReceive(int i) {
        //Debug.Log("VALUE RECEIVE "+i);
        foreach (var s in switches) {
            if(s.value == i) {
                s.uEvent.Invoke();
                //Debug.Log("VALUE RECEIVE EVENT"+i);
            }
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [Serializable]
    private class SwitchUnit{
        public DataConstantValue value;
        public UnityEventSequence uEvent;


    }
}
