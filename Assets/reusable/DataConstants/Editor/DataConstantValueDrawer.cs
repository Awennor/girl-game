﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(DataConstantValue))]
public class DataConstantValueDrawer : PropertyDrawer {

    static DataConstantMasterAsset dataConstantMasterAsset;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        if (dataConstantMasterAsset == null) {
            const string path = "Assets/DataConstants/DataConstantMaster.asset";
            dataConstantMasterAsset = AssetDatabase.LoadAssetAtPath<DataConstantMasterAsset>(path);
            if (dataConstantMasterAsset == null) {
                EditorGUI.LabelField(position, "Asset not found at " + path);
            }
        }
        if (dataConstantMasterAsset != null) {
            var type = property.FindPropertyRelative("constantType");
            var value = property.FindPropertyRelative("value");

            position.width /=2;
            type.intValue = 
                dataConstantMasterAsset.AssetIdentificationOfIndex((EditorGUI.Popup(position, dataConstantMasterAsset.IndexOf(type.intValue), dataConstantMasterAsset.AssetNames())));
            var dataConstantDefinitionAsset = dataConstantMasterAsset.GetAsset(type.intValue);
            
            position.x += position.width;
            if(dataConstantDefinitionAsset == null) {
                EditorGUI.LabelField(position, "Contant Type not found");
                return;
            }
            var indexOfValue = EditorGUI.Popup(position, dataConstantDefinitionAsset.IndexOf(value.intValue), dataConstantDefinitionAsset.ConstantLabels());
            value.intValue =
                dataConstantDefinitionAsset.ValueOfIndex(indexOfValue);

        }


        //EditorGUI.Popup(position);
    }
}
