﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineUtils : MonoBehaviour {

    public GameObject go, go2;
    public LineRenderer lineRender;

    [ExposeMethodInEditor]
    public void ConnectTwoObjects() {
        lineRender.SetPosition(0, go.transform.position);
        lineRender.SetPosition(1, go2.transform.position);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
