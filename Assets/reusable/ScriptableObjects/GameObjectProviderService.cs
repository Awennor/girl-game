﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameObjectProviderService", menuName = "ScriptableAssets/GameObjectProviderService", order = 1)]
public class GameObjectProviderService : ScriptableObject {

    private Func<int,GameObject> provider;

    public Func<int, GameObject> Provider {
        get {
            return provider;
        }

        set {
            provider = value;
        }
    }
}
