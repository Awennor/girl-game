﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Integer Asset", menuName = "General Scriptable Objects/Integer Asset", order = 3)]
public class IntegerAsset : ScriptableObject {
    [SerializeField]
    int value;

    public void Add(int amount) {
        value += amount;
    }

    public int Value {
        get {
            return value;
        }

        set {
            this.value = value;
        }
    }
}
