﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector3Events : MonoBehaviour {

    public Vector3EventKeyed[] events;

    public void Invoke(Vector3 v, string key) {
        for (int i = 0; i < events.Length; i++) {
            if(events[i].Key.Equals(key)) {
                events[i].vectorEvent.Invoke(v);
            }
        }
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
    [Serializable]
    public class Vector3EventKeyed {
        public MyVector3Event vectorEvent;
        [SerializeField]
        string key;

        public string Key {
            get {
                return key;
            }

            set {
                key = value;
            }
        }
    }
}
