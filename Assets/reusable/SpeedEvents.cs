﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpeedEvents : MonoBehaviour {
    private Vector3 oldAccel;
    private Vector3 oldPosition = Vector3.one;
    private Vector3 oldSpeed;
    [SerializeField]
    private float triggerSpeed;
    public UnityEvent OnSpeedAboveTrigger;
    public UnityEvent OnSpeedBelowTrigger;
    public MyFloatEvent OnSpeedRotationYAngleUpdate;
    float oldYAngle;
    public MyVector3Event OnUpdateSpeed;

    // Use this for initialization
    void Start() {
        //oldPosition = transform.position;
        //Debug.Log("Old pos INIT A");

    }

    // Update is called once per frame
    void Update() {
        if (Time.deltaTime != 0) {
            var newPosition = transform.position;
            if (oldPosition == Vector3.one || oldPosition == Vector3.zero) {
                oldPosition = newPosition;

            } else {

            }

            var distanceMoved = newPosition - oldPosition;
            var speedNow = distanceMoved / Time.deltaTime;
            var triggerSquared = triggerSpeed * triggerSpeed;
            if (oldSpeed.sqrMagnitude < triggerSquared &&
                speedNow.sqrMagnitude >= triggerSquared) {
                OnSpeedAboveTrigger.Invoke();
            }
            if (oldSpeed.sqrMagnitude >= triggerSquared &&
                speedNow.sqrMagnitude < triggerSquared) {
                OnSpeedBelowTrigger.Invoke();
            }

            OnUpdateSpeed.Invoke(speedNow);
            
            var angleRads = Mathf.Atan(distanceMoved.x / distanceMoved.z);
            float angleDeg = angleRads * Mathf.Rad2Deg;
            if (float.IsNaN(angleRads)) {
                if (distanceMoved.x < 0) {
                    angleDeg = 270;
                    Debug.Log("NAN 270");
                } else {
                    angleDeg = 90;
                    //Debug.Log("NAN 90");
                }
            } else {
                if (distanceMoved.z < 0) {

                    angleDeg += 180;
                    //Debug.Log("+180");
                } else {
                    if (distanceMoved.x < 0) {
                        angleDeg -= 180;
                    }
                }
            }

            //Debug.Log("angle Deg"+angleDeg);

            //if(distanceMoved.x < 0)
            //Debug.Log(angleDeg +"_"+oldYAngle);

            if (
                oldYAngle != angleDeg && !
                (float.IsNaN(oldYAngle) && float.IsNaN(angleDeg)
                )) {
                OnSpeedRotationYAngleUpdate.Invoke(angleDeg);
            }

            oldYAngle = angleDeg;

            var speedChange = speedNow - oldSpeed;
            var accelNow = speedChange / Time.deltaTime;
            oldAccel = accelNow;
            oldSpeed = speedNow;
            oldPosition = newPosition;
        }


    }
}
