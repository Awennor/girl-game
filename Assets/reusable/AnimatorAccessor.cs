﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorAccessor : MonoBehaviour {

    public Animator animator;

    public void BoolTrue(string boolName) {
        animator.SetBool(boolName, true);
    }

    public void BoolFalse(string boolName) {
        animator.SetBool(boolName, false);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
