﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class GameObjectSwitched_Event {
    [SerializeField]
    GameObjectSwitched_EventUnit[] callbacks;

    public void CallEvent(GameObject caller) {
        foreach (var c in callbacks) {
            if (c.targetGO == caller) {
                c.eventSeq.Invoke();
            }
        }

    }


}

[Serializable]
public class GameObjectSwitched_EventUnit {
    [SerializeField]
    public GameObject targetGO;
    [SerializeField]
    public UnityEvent eventSeq;
}
