﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

class XMLEncoder{
    private XmlElement node;

    public XMLEncoder(){
        var doc = new XmlDocument();
        node = doc.CreateElement("root");
    }

    

    public string Escape(string unescaped) {

        
        node.InnerText = unescaped;
        return node.InnerXml;
    }

    public string Unescape(string escaped) {

        node.InnerXml = escaped;
        return node.InnerText;
    }
}
