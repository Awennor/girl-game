﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public struct PointInt2D {
    public int x;
    public int y;

    public int Row {
        get {
            return y;
        }
    }

    public int Column {
        get {
            return x;
        }
    }

    internal PointInt2D MultiplyCeiling(float f) {
        //float x = this.x;
        //float y = this.y;
        return new PointInt2D(Mathf.CeilToInt(x*f),Mathf.CeilToInt(y*f));
    }

     internal PointInt2D MultiplyFloor(float f) {
        //float x = this.x;
        //float y = this.y;
        return new PointInt2D(Mathf.FloorToInt(x*f),Mathf.FloorToInt(y*f));
    }

    public PointInt2D(int x, int y) : this() {
        this.x = x;
        this.y = y;
    }

    public static PointInt2D operator +(PointInt2D a, PointInt2D b) {
        a.x += b.x;
        a.y += b.y;
        return a;
    }
    public static PointInt2D operator -(PointInt2D a) {
        a.x *= -1;
        a.y *= -1;
        return a;
    }
    public static PointInt2D operator -(PointInt2D a, PointInt2D b) {
        return a + (-b);
    }
    public static PointInt2D operator *(float d, PointInt2D a) {
        return a;
    }

    public static PointInt2D operator /(PointInt2D a, float d) {
        a.x = (int)(a.x / d);
        a.y = (int)(a.y / d);
        return a;
    }

    internal static PointInt2D AdjacentPoint(PointInt2D p, int i) {
        switch (i) {
            case 0:
                return p + new PointInt2D(1, 0);
            case 1:
                return p + new PointInt2D(0, 1);
            case 2:
                return p + new PointInt2D(-1, 0);
            case 3:
                return p + new PointInt2D(0, -1);
            default:
                break;
        }
        return p;
    }
}