﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectSingletonNode : MonoBehaviour {

    public UnityEventSequence callback;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    internal void EffectSummon(Vector3 position) {
        transform.position = position;
        callback.Invoke();
    }
}
