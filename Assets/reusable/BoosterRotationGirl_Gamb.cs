﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoosterRotationGirl_Gamb : MonoBehaviour {

    public float maxDev = 10;
    public float speedFactor = 1;
    public float normalFactor = 1;

    public void SetSpeed(Vector3 t) {
        transform.LookAt(
            transform.position 
            - t.normalized * speedFactor
            - transform.parent.forward*normalFactor);
    }

    public void SetYRotation(float yrot) {
        if (!enabled) return;
        var rotation = transform.rotation;
        var euler = rotation.eulerAngles;
        float yparent = transform.parent.rotation.eulerAngles.y;
        //Debug.Log(euler);
        euler.x = 180;
        euler.z = 0;
        if (float.IsNaN(yrot))
            yrot = 90;
        
        if(yrot < yparent - maxDev) {
            yrot = yparent - maxDev;
        }
        if(yrot > yparent + maxDev) {
            yrot = yparent + maxDev;
        }
        
        euler.y = yrot;
        Debug.Log("euler" + euler + yparent);
        transform.rotation = Quaternion.Euler(euler);
        //transform.localRotation = Quaternion.Euler(euler);
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
