using System;
using UnityEngine;


namespace UnityStandardAssets.Utility
{
    public class FollowTarget : MonoBehaviour
    {
        public Transform target;
        public Vector3 offset = new Vector3(0f, 7.5f, 0f);


        private void LateUpdate()
        {

            var vector3 = target.position + offset;
            if(vector3 != transform.position)
                transform.position = vector3;
        }
    }
}
