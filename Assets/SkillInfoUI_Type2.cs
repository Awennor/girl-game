﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SkillInfoUI_Type2 : MonoBehaviour, SkillInfoUI_Observer {
    private SkillInfoProvider.SkillId Skill;
    private SkillIconHolder skillIconHolder;
    private SkillInfoUI skillInfoUI;


    public MyStringEvent
        OnChangeSkill_Name,
        OnChangeSkill_Damage,
        OnChangeSkill_Cost,
        OnChangeSkill_EffectText;
    public MySpriteEvent OnSpriteChange;
    public MyIntEvent OnEnergyCostChange;

    public UnityEvent
        OnBattleCard, OnPhysicalBattleCard, OnMindBattleCard, OnUtilityCard, OnBuffCard, 
        OnClickEffectShow, OnNormalColor, OnDisabledColor;


    public void OnChangeSkill(SkillInfoProvider.SkillId skill) {

        this.Skill = skill;
        Debug.Log("skillid CHANGE SKILL" + skill);
        //skillNeverSet = false;
        var skillInfoProvider = SkillInfoProvider.Instance;
        string name = skillInfoProvider.GetName(skill);


        if (skillIconHolder == null) {
            skillInfoUI = GetComponent<SkillInfoUI>();
            skillIconHolder = skillInfoUI.SkillIconHolder;
        }
        var sprite = skillIconHolder.Sprite[(int)skill];

        //skillIcon.color = skillIconHolder.Colors[(int)skill];
        var energyCost = skillInfoProvider.GetEnergyCost(skill);
        EnergyCostChange(energyCost);

        OnChangeSkill_Name.Invoke(name);
        OnSpriteChange.Invoke(sprite);

        var battleSkill = skillInfoProvider.IsBattleSkill(skill);
        if (battleSkill) {
            int damage = skillInfoProvider.GetBaseDamage(skill);
            int mindDamage = skillInfoProvider.GetMindDamage(skill);
            int repetition = skillInfoProvider.GetRepetition(skill);
            int finalDamage;
            bool physical = damage >= mindDamage || damage < 0;
            if (physical) {
                finalDamage = damage;
                OnPhysicalBattleCard.Invoke();
            } else {
                finalDamage = mindDamage;
                OnMindBattleCard.Invoke();
            }
            //finalDamage *= repetition;

            string damageText = null;
            if(repetition > 1)
                damageText= finalDamage + "X"+repetition;
            else
                damageText= finalDamage + "";
            OnChangeSkill_Damage.Invoke(damageText);
        }

        string effectText = "";

        var utilSkill = skillInfoProvider.IsUtilSkill(skill);
        if (utilSkill) {
            OnUtilityCard.Invoke();
            SkillUtilDataUnit utilU = skillInfoProvider.GetSkillUtilData(skill);

            if (utilU.DrawAmount > 0)
                effectText += "d" + utilU.DrawAmount;
            if (utilU.EnergyAmount > 0)
                effectText += "e" + utilU.EnergyAmount;

        }
        var buff = skillInfoProvider.GetSkillBuffData((int)skill);
        if (buff != null) {
            OnBuffCard.Invoke();
            var atks = buff.Buff.AttackMultipliers;
            if (atks.Length >= 1 && atks[0] > 0) {
                effectText += "A+" + atks[0] * 100 + "%";
            }

            if (buff.Buff.DefenseMultiplier > 0) {
                effectText += "D+" + buff.Buff.DefenseMultiplier * 100 + "%";
                
            }
        }
        if (effectText.Length > 0) {
            OnChangeSkill_EffectText.Invoke(effectText);
        }

    }

    private void EnergyCostChange(int energyCost) {
        OnEnergyCostChange.Invoke(energyCost);
        skillInfoUI.EnergyChange(energyCost);
        OnChangeSkill_Cost.Invoke(energyCost + "");
    }

    public void OnClickEffect() {
        OnClickEffectShow.Invoke();
    }

    public void OnDisableColor() {
        OnDisabledColor.Invoke();
    }

    public void OnEffect1Request() {

    }

    public void OnEnabledColor() {
        OnNormalColor.Invoke();
    }

    public void OnMightUseEnergies() {

    }

    public void OnNegativeEnergies() {

    }

    public void OnNormalColorBullets(int amount) {

    }

    public void OnNoSkill() {

    }

    public void OnPositiveEnergies() {

    }

    public void OnSpecialColorBullets() {

    }

    public void OnStopMightUseEnergies() {

    }

    // Use this for initialization
    void Awake() {
        skillInfoUI = GetComponent<SkillInfoUI>();
        skillInfoUI.DisableDefaultFuncionality();
        skillInfoUI.AddObservable(this);
    }

    void Start() {
        skillIconHolder = skillInfoUI.SkillIconHolder;
    }

    // Update is called once per frame
    void Update() {

    }
}
