﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CentralResourceAccess : MonoBehaviour {

    public void TimeScale_SuperSlow(float timeToNormal) {
        const float superSlowScale = 0.02f;
        Time.timeScale = superSlowScale;
        DOVirtual.DelayedCall(timeToNormal*superSlowScale,NormalTimeScale, false);
        Debug.Log("SUPERSLOW");
    }

    private void NormalTimeScale() {
        Time.timeScale = 1f;
        Debug.Log("NORMAL");
    }

    public void AnimatorTriggerOnHero(string trigger) {
        CentralResource.Instance.Hero.GetComponentInChildren<Animator>().SetTrigger(trigger);
    }

    public void OverworldInputStatus(bool status) {
        CentralResource.Instance.OverworldInput.InputActive = status;
    }

    public void ShowEffectOnHero(string effectName) {
        EffectSingleton.Instance.ShowEffectHere(effectName, CentralResource.Instance.Hero.transform);
    }

    public void DarkFadeOutScreen(float time) {
        CentralResource.Instance.TransitionOverlay.DarkFadeout(time);
    }

    public void FadeInScreen(float time) {
        CentralResource.Instance.TransitionOverlay.FadeinScreen(time);
    }

    public void MoveHeroTo(Transform t) {
        CentralResource.Instance.Hero.transform.position = t.position;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
