﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoCube : MonoBehaviour {

    public Color drawColor;

    public void OnDrawGizmos() {
        Gizmos.color = drawColor;
        Gizmos.DrawCube(transform.position, new Vector3(3,3,3));
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
