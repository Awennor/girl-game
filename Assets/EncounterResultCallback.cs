﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterResultCallback : MonoBehaviour {

	// Use this for initialization
	void Start () {
        CentralResource.Instance.BattleFlowSys.OnBattleEnd += BattleFlowSys_OnBattleEnd;
	}

    private void BattleFlowSys_OnBattleEnd() {
        if(CentralResource.Instance.BattleFlowSys.LatestBattleWon) {

        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
