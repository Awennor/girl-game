﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFactory : MonoBehaviour {

    [SerializeField]
    DataConstantAndGameObject[] individualObjs;
    
    [SerializeField]
    List<DataConstantValue> humanoidEnemies;
    public HumanoidBattlerFactory humanoidFactory;

    public GameObjectProviderService service;

    public void Awake() {
        service.Provider = Instantiate;
    }

    public GameObject Instantiate(int v) {

        foreach (var obj in individualObjs) {
            if(obj.Value == v) {
                return Instantiate(obj.Obj);
            }
        }

        var dataConstantValue = new DataConstantValue(individualObjs[0].Value.ConstantType, v);
        if (humanoidEnemies.Contains(dataConstantValue)) {
            return humanoidFactory.Generate(dataConstantValue);
        }
        return null;
    }

    [Serializable]
    private class DataConstantAndGameObject {
        [SerializeField]
        DataConstantValue value;
        [SerializeField]
        GameObject obj;

        public DataConstantValue Value {
            get {
                return value;
            }

            set {
                this.value = value;
            }
        }

        public GameObject Obj {
            get {
                return obj;
            }

            set {
                obj = value;
            }
        }
    }


}
