﻿using com.spacepuppy.Collections;
using FlowCanvas;
using NodeCanvas.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MyDicStringSkillId : SerializableDictionaryBase<string, SkillInfoProvider.SkillId> { };

public class HumanoidBattlerFactory : MonoBehaviour {

    public GameObject heroOriginal;
    [SerializeField]
    List<HumanoidBattlerConstructionData> constructionData;
    public string modelObjName;

    public GameObject Generate(DataConstantValue e) {
        var obj = Instantiate(heroOriginal);
        obj.AddComponent<EnemyCharacterBattleInit>();
        obj.GetComponent<HeroCharaInit>().enabled = false;
        var actorSetupDefault = obj.GetComponent<ActorSetupDefault>();
        actorSetupDefault.maxMindHP = 10;
        actorSetupDefault.maxHP = 10;
        //actorSetupDefault.OnSetupDone.AddListener(AfterSetupDone);
        actorSetupDefault.hero = false;
        //obj.AddComponent<HumanoidBattlerAI>();
        obj.GetComponent<MindControlTemp>().enabled = false;
        obj.GetComponent<CounterAttackSystem>().enabled = false;
        obj.GetComponent<PlayerMovement>().AutoDisable = true;
        obj.GetComponent<OverworldAttack>().enabled = false;
        obj.GetComponent<TempHeroRevive>().enabled = false;
        obj.AddComponent<EnemyCharacterBattleInit>().ATBchargeTime = 999;
        obj.AddComponent<BuffShow>().enabled = false;

        HumanoidBattlerConstructionData d = GetData(e);
        actorSetupDefault.maxHP = d.MaxHP;
        actorSetupDefault.maxMindHP = d.MaxMindHP;
        SkinnedMeshRenderer mesh = actorSetupDefault.mainRenderer as SkinnedMeshRenderer;
        mesh.sharedMesh = d.MainMesh;
        var meshObj = obj.transform.FindChild(modelObjName);
        meshObj.localScale = meshObj.localScale * d.ModelScale;



        actorSetupDefault.Initialize();


        //after initialization code


        var controllableObject = obj.GetComponent<ControllableObject>();
        if (d.FlowScript != null) {
            var flow = d.FlowScript;
            var flowComp = obj.AddComponent<FlowScriptController>();
            flowComp.behaviour = flow;
            var blackboard = obj.AddComponent<Blackboard>();
            var skillVariables = d.SkillVariables;
            var keys = skillVariables.Keys;

            foreach (var item in skillVariables) {
                blackboard.AddVariable(item.Key, item.Value);
            }

            controllableObject.AddAntiInputScript(flowComp);
        }

        controllableObject.Controlling(false);
        obj.GetComponent<AttackInput>().enabled = true;
        obj.GetComponent<CollisionProj>().EnemySideSet();



        return obj;

    }

    private HumanoidBattlerConstructionData GetData(DataConstantValue data) {
        for (int i = 0; i < constructionData.Count; i++) {
            if (constructionData[i].Enemy == data) {
                return constructionData[i];
            }
        }
        return null;
    }


    // Use this for initialization
    void Awake() {
        //Generate();
    }

    // Update is called once per frame
    void Update() {

    }

    [Serializable]
    private class HumanoidBattlerConstructionData {
        [SerializeField]
        DataConstantValue enemy;
        [SerializeField]
        int maxHP, maxMindHP;
        [SerializeField]
        float modelScale;
        [SerializeField]
        Mesh mainMesh;
        [SerializeField]
        FlowScript flowScript;
        [SerializeField]
        MyDicStringSkillId skillVariables;


        public float ModelScale {
            get {
                return modelScale;
            }

            set {
                modelScale = value;
            }
        }

        public Mesh MainMesh {
            get {
                return mainMesh;
            }

            set {
                mainMesh = value;
            }
        }

        public DataConstantValue Enemy {
            get {
                return enemy;
            }

            set {
                enemy = value;
            }
        }

        public int MaxHP {
            get {
                return maxHP;
            }

            set {
                maxHP = value;
            }
        }

        public int MaxMindHP {
            get {
                return maxMindHP;
            }

            set {
                maxMindHP = value;
            }
        }

        public FlowScript FlowScript {
            get {
                return flowScript;
            }

            set {
                flowScript = value;
            }
        }

        public MyDicStringSkillId SkillVariables {
            get {
                return skillVariables;
            }

            set {
                skillVariables = value;
            }
        }
    }


}
