﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupActivator : MonoBehaviour {

    [SerializeField]
    List<GameObject> objects;

    public void AmountActive(int amount) {
        for (int i = 0; i < objects.Count; i++) {
            objects[i].SetActive(i < amount);
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
