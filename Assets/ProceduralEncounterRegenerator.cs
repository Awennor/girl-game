﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralEncounterRegenerator : MonoBehaviour {

    public IntegerAsset progress;

    public EncounterProcedural lastEncounter;
    public EncounterProcedural[] normalEncounters;
    public List<int> normalEncounterDifficultyScale_Priority;
    public float[] normalEncounterDifficultyScale_Value;

    public void Regenerate() {
        int p = progress.Value;
        int mainD = p * 3 + 5;
        lastEncounter.Generate(mainD);
        for (int i = 0; i < normalEncounters.Length; i++) {
            int diffScaleI = RandomSpecial.PriorityArray(normalEncounterDifficultyScale_Priority);
            int difficultyE = (int) (mainD*normalEncounterDifficultyScale_Value[diffScaleI]);
            if(difficultyE <= 0) {
                difficultyE = 1;
            }
            normalEncounters[i].Generate(difficultyE);
        }

    }


}
