﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProgressCutsceneBinder : MonoBehaviour {

    public LinearProgressionSystem progression;
    public string cutscenePlace;
    public string cutscenePlaceKey;
    public string cutsceneNameKey;
    public CutscenePlacer cutscenePlacer;
    public bool TryLoadCutsceneOnStart;
    public UnityEvent OnTryExecuteCutsceneFail;

    public void TryLoadCutscene() {
        if(!gameObject.activeSelf)
            return;
        MyDicStringString data = 
            progression.GetCurrentEntryThatContainsPair(cutscenePlaceKey, cutscenePlace);
        bool fail = true;
        if (data != null) {
            if (data[cutscenePlaceKey].Equals(cutscenePlace)) {
                string cutsceneName = data[cutsceneNameKey];
                if (cutsceneName != null) {
                    cutscenePlacer.CutsceneName = cutsceneName;
                    cutscenePlacer.LoadAndTryStartCutscene();
                    fail = false;
                    Debug.Log("CUTSCENE NAME IS WORKS...?");
                } else
                    Debug.Log("CUTSCENE NAME IS NULL");

            }

        }

        if(fail) {
            OnTryExecuteCutsceneFail.Invoke();
        }

    }

    // Use this for initialization
    void Start() {
        if (TryLoadCutsceneOnStart) {
            TryLoadCutscene();
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
