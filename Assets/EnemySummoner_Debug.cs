﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySummoner_Debug : MonoBehaviour {

    public GameObjectProviderService enemyProvider;
    public DataConstantValue enemyId;

	// Use this for initialization
	void Start () {
		var enemy = enemyProvider.Provider(enemyId);
        enemy.transform.position = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
