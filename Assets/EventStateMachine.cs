﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventStateMachine : MonoBehaviour {

    [SerializeField]
    List<State> states = new List<State>();
    private State currentState;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void FirstState() {
        NewState(states[0]);
    }

    public void NextState() {
        int stateIndex = 0;
        if(currentState != null) {
            stateIndex = states.IndexOf(currentState);
        }
        stateIndex++;
        if(states.Count <= stateIndex) {
            stateIndex = 0;
        }
        NewState(states[stateIndex]);
    }

    public void ChangeStateByName(string state) {
        for (int i = 0; i < states.Count; i++) {
            if(states[i].StateName.Equals(state)) {
                NewState(states[i]);
            }
        }
    }



    private void NewState(State state) {
        currentState =state;
        state.StateBegin.Invoke();
    }

    [Serializable]
    public class State {
        [SerializeField]
        string stateName;
        [SerializeField]
        UnityEventSequence stateBegin;

        public string StateName {
            get {
                return stateName;
            }

            set {
                stateName = value;
            }
        }

        public UnityEventSequence StateBegin {
            get {
                return stateBegin;
            }

            set {
                stateBegin = value;
            }
        }
    }

}


