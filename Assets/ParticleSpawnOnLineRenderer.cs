﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpawnOnLineRenderer : MonoBehaviour {

    public LineRenderer lineRenderer;
    private ParticleSystem particle;
    public float step = 0.1f;
    public int particlesPerStep = 1;
    public float spawnProb = 0.1f;
    public float spawnPeriod = 0.1f;
    float counter = 0;

    // Use this for initialization
    void Start() {
        particle = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update() {
        counter += Time.deltaTime;
        if(counter > spawnPeriod) {
            Spawn();
            counter = 0;
        }
            
    }

    private void Spawn() {
        if (step == 0) step = 0.1f;
        for (int i = 1; i < lineRenderer.numPositions; i++) {
            var pos1 = lineRenderer.GetPosition(i - 1);
            var pos2 = lineRenderer.GetPosition(i);
            var dis = pos2 - pos1;
            var disDir = dis.normalized;
            if (disDir == Vector3.zero) {
                continue;
            }
            var disWalker = Vector3.zero; ;
            while (disWalker.sqrMagnitude < dis.sqrMagnitude) {
                disWalker += disDir * step;
                if (RandomSpecial.RandomBool(spawnProb)) {
                    var particlePosition = pos1 + disWalker;

                    if (!lineRenderer.useWorldSpace) {
                        particlePosition += lineRenderer.transform.position;
                        //particlePosition = lineRenderer.transform.localToWorldMatrix * particlePosition ;
                    }
                    particle.transform.position = particlePosition;
                    particle.Emit(particlesPerStep);
                }



            }

        }
    }
}
