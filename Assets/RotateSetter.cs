﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSetter : MonoBehaviour {

    public Transform[] referenceTransforms;
    
    public void RotateToReference(int refId) {
        if(enabled)
            this.transform.rotation = referenceTransforms[refId].rotation;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
