﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleShapeParticle : MonoBehaviour {

    public Mesh mesh;
    public float baseT;
    public float height;
    public ParticleSystem[] emissionShapes;
    public ParticleSystemRenderer[] particleShape;

    // Use this for initialization
    void Start() {
        if (mesh == null) {
            RecreateMesh();
        }
    }

    [ExposeMethodInEditor]
    public void RecreateMesh() {
        mesh = new Mesh();

        Vector3[] vertices = new Vector3[3];

        vertices[0] = new Vector3(baseT / 2, 0, 0);
        vertices[1] = new Vector3(-baseT / 2, 0, 0);
        vertices[2] = new Vector3(0, height, 0);

        mesh.vertices = vertices;

        int[] tris = new int[3];
        for (int i = 0; i < tris.Length; i++) {
            tris[i] = i;
        }

        mesh.triangles = tris;

        Vector3[] normals = new Vector3[3];

        normals[0] = -Vector3.forward;
        normals[1] = -Vector3.forward;
        normals[2] = -Vector3.forward;

        mesh.normals = normals;

        Vector2[] uv = new Vector2[3];

        uv[0] = new Vector2(1, 0);
        uv[1] = new Vector2(0, 0);
        uv[2] = new Vector2(0, 1);

        mesh.uv = uv;

        for (int i = 0; i < emissionShapes.Length; i++) {
            var shape = emissionShapes[i].shape;
            shape.mesh = mesh;
        }
        for (int i = 0; i < particleShape.Length; i++) {
            particleShape[i].mesh = mesh;

        }


    }

    // Update is called once per frame
    void Update() {

    }
}
