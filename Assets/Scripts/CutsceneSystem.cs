﻿
using System;

public class CutsceneSystem {

    public event Action<CutsceneComp> CutsceneStartCallback;
    public event Action<CutsceneComp> OnCutsceneConditionalStart;

    public CutsceneSystem() {

    }

    public void CutsceneStart(CutsceneComp comp) {
        if(CutsceneStartCallback != null)
            CutsceneStartCallback(comp);
    }

    internal void ConditionalCutsceneStartTry(CutsceneComp cutsceneComp) {
        if(OnCutsceneConditionalStart != null) {
            OnCutsceneConditionalStart(cutsceneComp);
        }
    }
}