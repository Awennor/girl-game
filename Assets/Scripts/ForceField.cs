﻿using UnityEngine;
using System.Collections;

public class ForceField : MonoBehaviour {
    private CollisionProj collisionProj;
    [SerializeField]
    GameObject fieldObj;
    private ScaleAppearDisappear fieldSummoner;

    // Use this for initialization
    void Start () {
        collisionProj = GetComponent<CollisionProj>();
        if(fieldObj != null)
            fieldSummoner = fieldObj.GetComponent<ScaleAppearDisappear>();
	}

    public void FieldState(bool b) {
        collisionProj.IgnoreCollision = b;
        
        fieldSummoner.Show(b);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
