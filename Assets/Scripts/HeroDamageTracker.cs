﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public  class HeroDamageTracker{

    public  HeroDamageTracker(CentralResource cr) {
        CollisionProj.damageHappen += DamageHappen;
        cr.BattleFlowSys.OnBattleStart += BattleStart;
    }

    private void BattleStart() {
        Damage = 0;
        timesHit = 0;
    }

    private void DamageHappen(CollisionProj arg1, float arg2) {
        if(arg1.gameObject == hero) {
            if(arg2 > 0) {
                Damage += arg2;
                timesHit++;
            }
        }

    }

    private GameObject hero;
    private float damage;
    private int timesHit;

    public GameObject Hero {
        get {
            return hero;
        }

        set {
            hero = value;
        }
    }

    public float Damage {
        get {
            return damage;
        }

        set {
            damage = value;
        }
    }
}
