﻿using UnityEngine;
using System.Collections;
using System;

public class BuffShow : MonoBehaviour {
    private ActorStats actorStats;
    [SerializeField]
    string effectName;
    [SerializeField]
    string effectNameSummonTime;
    GameObject effect;
    private int lastCount;


    // Use this for initialization
    void Start () {
	    actorStats = GetComponent<SkillHelper>().ActorStats;
        actorStats.battleBuffsChange += battleBuffChange;

	}

    private void battleBuffChange(ActorStats obj) {
        var battleBufs = obj.BattleTempBufs;
        bool summon = false;
        bool increase = lastCount < battleBufs.Count;
        
        lastCount = battleBufs.Count;
        if (lastCount > 0) {
            if(effect != null) {
                if(!effect.activeSelf) {
                    effect.SetActive(true);
                    summon = true;
                }

            } else {
                effect = EffectSingleton.Instance.ShowAndAddEffect(effectName, transform);
                summon = true;
            }
        } else {
            if(effect != null) {
                effect.SetActive(false);

            }
        }
        if(increase) {
            EffectSingleton.Instance.ShowEffectHere(effectNameSummonTime, transform);
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
