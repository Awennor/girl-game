﻿using UnityEngine;
using System.Collections;

public class CloserToHeroParticleChanger : MonoBehaviour {

    new public ParticleSystem particleSystem;
    GameObject target;
    float maxDistance = 50f;
    //float minDistance = 0f;
    int minEmission = 1;
    int maxEmission = 50;

    int minEmission2 = 300;
    int maxEmission2 = 500;
    float maxDistance2 = 19f;

    int minEmission3 = 4000;
    int maxEmission3 = 5000;
    float maxDistance3 = 14f;
    

    // Use this for initialization
    void Start() {
        target = CentralResource.Instance.Hero;
    }

    // Update is called once per frame
    void Update() {
        var tP = target.transform.position;
        tP -= transform.position;
        var disSQ = tP.sqrMagnitude;
        int emission = minEmission;
        var maxDSQ = maxDistance * maxDistance;
        if (disSQ < maxDSQ) {
            float ratio = 1f - (disSQ / maxDSQ);
            emission = (int)(maxEmission * ratio) + minEmission;
            var maxSQ2 = maxDistance2 * maxDistance2;
            if (disSQ < maxSQ2) {
                ratio = 1f - (disSQ / maxSQ2);
                emission = (int)(maxEmission2 * ratio) + minEmission2;
            }
            var maxSQ3 = maxDistance3 * maxDistance3;
            if (disSQ < maxSQ3) {
                ratio = 1f - (disSQ / maxSQ3);
                emission = (int)(maxEmission3 * ratio) + minEmission3;
            }

        }
        particleSystem.emissionRate = emission;
    }
}
