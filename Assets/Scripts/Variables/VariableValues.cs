﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class VariableValues{

    public static float DEFAULTVALUE = -1;

    //[System.NonSerialized]
    

	public List<float> values = new List<float>();

    internal float GetValue(int variableName) {
        if(values.Count <= variableName) {
            return DEFAULTVALUE;
        }
        return values[variableName];
    }

    internal void SetValue(int variableName, float v) {
        while(values.Count <= variableName) {
            values.Add(DEFAULTVALUE);
        }
        values[variableName] = v;

    }
}
