﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ProjectilContinuousDamage : MonoBehaviour {

    List<CollisionProj> colliding = new List<CollisionProj>();
    float collisionRepeatPeriod = -1;
    float currentTime;
    private ProjectilData projectilData;

    public float CollisionRepeatPeriod {
        get {
            return collisionRepeatPeriod;
        }

        set {
            collisionRepeatPeriod = value;
            currentTime = collisionRepeatPeriod;
        }
    }

    void OnCollisionExit(Collision col) {
        for (int i = 0; i < colliding.Count; i++) {
            if (colliding[i] == null || colliding[i].gameObject == col.collider.gameObject) {
                colliding.Remove(colliding[i]);
            }
        }

    }

    // Use this for initialization
    void Start() {
        projectilData =GetComponent<ProjectilData>();
    }

    // Update is called once per frame
    void Update() {
        
        if (CollisionRepeatPeriod > 0) {
            currentTime -= Time.deltaTime;
            if (currentTime <= 0) {

                Debug.Log("Update! COLLIDE AGAIN");
                ColliderAgain();
                currentTime = CollisionRepeatPeriod;
            }
        }

    }

    private void ColliderAgain() {
        for (int i = 0; i < colliding.Count; i++) {
            colliding[i].CollideProjectilData(projectilData);
        }
    }

    internal void CollideWith(CollisionProj collisionProj) {
        if(!colliding.Contains(collisionProj))
            colliding.Add(collisionProj);
    }
}
