﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts.Logic;
using System;

public class MindHPSystem : MonoBehaviour {

	RangedValue mindHp = new RangedValue();
    public Action zeroMind;
    public Action<MindHPSystem> changeMind;


	// Use this for initialization
	void Start () {
        mindHp.ValueChanged += valueChange;
	}

    private void valueChange(RangedValue obj) {
        if(changeMind != null)
            changeMind(this);
        if(obj.isZero()) {
            if(zeroMind != null)
                zeroMind();
        }
    }

    internal void SetMaxHPFullHeal(int v)
    {
        mindHp.Max = v;
        mindHp.Value = v;
    }

    internal float GetMindHP()
    {
        return mindHp.Value;
    }

    internal float GetMaxMindHP()
    {
        return mindHp.Max;
    }

    // Update is called once per frame
    void Update () {
	
	}

    internal void DealDamage(float damage)
    {
        mindHp.Value -= damage;
    }

    internal bool canMindControl() {
        return mindHp.Value == 0;
    }

    internal void GoToRatio(float v) {
        mindHp.Value = mindHp.Max*v;
    }

    internal void Recover(int v) {
        mindHp.Value += v;
    }
}
