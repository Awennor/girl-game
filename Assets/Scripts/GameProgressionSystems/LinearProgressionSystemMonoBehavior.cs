﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearProgressionSystemMonoBehavior : MonoBehaviour {
    public bool ResetProgressOnAwake = false;
    public LinearProgressionSystem progress;

    public void Advance() {
        if (gameObject.activeSelf)
            progress.Advance();
    }

    // Use this for initialization
    void Start() {

    }

    void Awake() {
        if (ResetProgressOnAwake) {
            progress.ZeroProgress();
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
