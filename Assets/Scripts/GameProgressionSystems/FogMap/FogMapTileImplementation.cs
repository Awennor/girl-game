﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class FogMapTileImplementation : MonoBehaviour {

    public UnityEvent OnUnlockState, OnLockState, OnClearedState, 
        OnUnlockTargetted_true, OnUnlockTargetted_false, OnUnlockGraphicalTransition;
    public MyIntEvent OnTileIdentifierSet;
    public MyMonoBehaviourEvent OnCleared;
    public MyMonoBehaviourBoolEvent OnUnlockPotentialShowStatus;
    [SerializeField]
    PointInt2D position_map;

    public int unlockSize_temp;

    public void SetUnlockSize_TEMP(int v) {
        unlockSize_temp = v;
    }

    public PointInt2D Position_map {
        get {
            return position_map;
        }

        set {
            position_map = value;
        }
    }

    public void UnlockPotentialShowStatus(bool show) {
        OnUnlockPotentialShowStatus.Invoke(this, show);
    }

    public void Cleared() {
        OnCleared.Invoke(this);
    }

    public void UpdateGraphic_UnlockState() {
        OnUnlockState.Invoke();
    }

    public void UpdateGraphic_LockState() {
        OnLockState.Invoke();
    }

    public void UpdateGraphic_ClearedState() {
        OnClearedState.Invoke();
    }

    internal void UnlockTarget(bool v) {
        if(v) {
            OnUnlockTargetted_true.Invoke();
        } else {
            OnUnlockTargetted_false.Invoke();
        }
    }

    public void SetTileIdentifier(int tile) {
        OnTileIdentifierSet.Invoke(tile);
    }

    internal void GraphicTransition_ToUnlock() {
        OnUnlockGraphicalTransition.Invoke();
    }
}
