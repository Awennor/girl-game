﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FogMapGenerator : MonoBehaviour {
    [SerializeField]
    private FogMap fogMap;
    [SerializeField]
    int width, height;

    [SerializeField]
    private List<ElementProbability> elements = new List<ElementProbability>();
    int safeAreaElement = 1;

    internal void ClearElements() {
        elements.Clear();
    }

    internal void SetSize(PointInt2D mapSize) {
        width = mapSize.x;
        height = mapSize.y;
    }

    int safeAreaLength = 3;

    public List<ElementProbability> Elements {
        get {
            return elements;
        }

        set {
            elements = value;
        }
    }

    public FogMap FogMap {
        get {
            return fogMap;
        }

        set {
            fogMap = value;
        }
    }

    internal void AddElement(int i, int weight) {
        elements.Add(new ElementProbability(i, weight));
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    [ExposeMethodInEditor]
    public void Generate() {
        FogMap = new FogMap(width, height);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int e = RandomElement();
                FogMap[i, j] = e;
            }
        }
        FogMap.Rectangle(safeAreaElement, new PointInt2D(width / 2, height / 2), new PointInt2D(safeAreaLength, safeAreaLength));

    }
    List<int> auxList = new List<int>();
    private int RandomElement() {
        
        auxList.Clear();
        foreach (var e in Elements) {
            for (int i = 0; i < e.Weight; i++) {
                auxList.Add(e.Number);
            }
        }
        return auxList.RandomElementSpecial();
    }

    [Serializable]
    public struct ElementProbability {
        [SerializeField]
        int number;
        [SerializeField]
        int weight;

        public ElementProbability(int i, int weight) : this() {
            this.number = i;
            this.weight = weight;
        }


        public int Number {
            get {
                return number;
            }

            set {
                number = value;
            }
        }

        public int Weight {
            get {
                return weight;
            }

            set {
                weight = value;
            }
        }
    }
}
