﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogMapUnlockHelper : MonoBehaviour {

    [SerializeField]
    TwoDFogmapTileArray implementations_map;

    public FogMapProgress progress;

    List<PointInt2D> aux = new List<PointInt2D>();
    private FogMapTileImplementation latestTilePotential;

    public TwoDFogmapTileArray Implementations_map {
        get {
            return implementations_map;
        }

        set {
            implementations_map = value;
        }
    }

    public void TileIMplementation_OnUnlockPotentialShow(MonoBehaviour mb, bool status) {
        
        if (status) {
            StopAllUnlockPotentialShow();
            var tile = mb as FogMapTileImplementation;
            latestTilePotential = tile;
            var posMap = tile.Position_map;
            int unlockSize = tile.unlockSize_temp;

            aux.Clear();
            Point2DIntUtils.FillWithRectangleCenteredOn(posMap, new PointInt2D(unlockSize, unlockSize), aux);
            for (int i = 0; i < aux.Count; i++) {
                int p = progress.ProgressionMap[aux[i]];
                if (p == FogMapProgress.LOCKED) {
                    var fogMapTileImplementation = implementations_map[aux[i]];
                    if (fogMapTileImplementation) {
                        fogMapTileImplementation.UnlockTarget(true);
                    }

                }
            }
        } else {
            if(latestTilePotential == mb) {
                StopAllUnlockPotentialShow();
            }
        }

    }

    public void TileCleared(MonoBehaviour arg0) {
        var tile = (arg0 as FogMapTileImplementation);
        var clearedPos = tile.Position_map;
        var fogMapProgress = progress;
        fogMapProgress.ProgressionMap[clearedPos] = FogMapProgress.CLEARED;
        tile.UpdateGraphic_ClearedState();

        var posMap = tile.Position_map;
        int unlockSize = tile.unlockSize_temp;

        aux.Clear();
        Point2DIntUtils.FillWithRectangleCenteredOn(posMap, new PointInt2D(unlockSize, unlockSize), aux);
        for (int i = 0; i < aux.Count; i++) {
            int p = progress.ProgressionMap[aux[i]];
            if (p == FogMapProgress.LOCKED) {
                var fogMapTileImplementation = implementations_map[aux[i]];
                if (fogMapTileImplementation) {
                    //fogMapTileImplementation.UpdateGraphic_UnlockState();
                    fogMapTileImplementation.GraphicTransition_ToUnlock();
                }
                fogMapProgress.ProgressionMap[aux[i]] = FogMapProgress.UNLOCKED;

            }
        }
        
    }

    private void StopAllUnlockPotentialShow() {
        //Debug.Log("POTENTIAL DIE");
        implementations_map.IterateAction(StopUnlockPotentialShow);
    }

    public void StopUnlockPotentialShow(FogMapTileImplementation arg1, int arg2, int arg3) {
        if (arg1 != null)
            arg1.UnlockTarget(false);
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
