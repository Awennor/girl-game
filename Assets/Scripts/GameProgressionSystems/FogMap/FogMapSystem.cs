﻿using com.spacepuppy.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MyDicIntFogmapTile : SerializableDictionaryBase<int, FogMapTileImplementation> { };
[Serializable]
public class TwoDFogmapTileArray : TwoDArray<FogMapTileImplementation> { };
public class FogMapSystem : MonoBehaviour {

    public FogMapGenerator fogMapGenerator;
    public TileMap2DRenderer fogMapRenderer;
    public FogMapProgress fogMapProgress;
    public FogMapUnlockHelper fogMapUnlocker;

    [SerializeField]
    List<FogMapSystemUnit> units;
    [SerializeField]
    PointInt2D mapSize;
    [SerializeField]
    TwoDFogmapTileArray implementations_map;

    public void Start() {
        LoadProgress();
    }

    [ExposeMethodInEditor]
    public void GenerateTiles() {
        fogMapRenderer.tileInstantiated -= FogMapRenderer_tileInstantiated;
        fogMapRenderer.tileInstantiated += FogMapRenderer_tileInstantiated;
        fogMapGenerator.ClearElements();
        fogMapGenerator.SetSize(mapSize);
        fogMapRenderer.ClearRenderData();
        for (int i = 0; i < units.Count; i++) {
            int weight = units[i].Weight;
            var elementId = units[i].Tile;
            fogMapGenerator.AddElement(elementId, weight);
            if (units[i].TileImplementation != null)
                fogMapRenderer.AddRenderData(elementId, units[i].TileImplementation.gameObject);
        }
        fogMapGenerator.Generate();
        implementations_map.Allocate(fogMapGenerator.FogMap.Tiles.Size);
        fogMapRenderer.Tiles = fogMapGenerator.FogMap.Tiles;
        fogMapProgress.MapTiles = fogMapGenerator.FogMap.Tiles;
        fogMapRenderer.InstantiateObjectsWithDoneCallBack(FogMapRendener_InstantiateDone);
    }

    private void FogMapRendener_InstantiateDone() {
        fogMapUnlocker.Implementations_map = this.implementations_map;
    }

    private void FogMapRenderer_tileInstantiated(GameObject arg1, PointInt2D arg2) {
        var implementation = arg1.GetComponent<FogMapTileImplementation>();
        implementation.Position_map = arg2;
        implementations_map[arg2] = implementation;
        implementation.SetTileIdentifier(fogMapGenerator.FogMap.Tiles[arg2]);
        //implementation.OnCleared.AddListener(TileCleared);
        fogMapUnlocker.Implementations_map = this.implementations_map;

    }

    public void TileCleared(MonoBehaviour arg0) {
        var clearedPos = (arg0 as FogMapTileImplementation).Position_map;
        fogMapProgress.ProgressionMap[clearedPos] = FogMapProgress.CLEARED;
        //TEMPORARY CODE
        for (int i = 0; i < 4; i++) {
            var adjPos = PointInt2D.AdjacentPoint(clearedPos, i);
            if (fogMapProgress.ProgressionMap.ValidIndex(adjPos)) {
                int progressAdj = fogMapProgress.ProgressionMap[adjPos];
                if (progressAdj == FogMapProgress.LOCKED) {
                    fogMapProgress.ProgressionMap[adjPos] = FogMapProgress.UNLOCKED;
                }
            }

        }
        fogMapProgress.ProgressionMap.IterateAction(InitializeProgressState); // SHOULD NOT BE CALLING THIS HERE
    }

    [ExposeMethodInEditor]
    public void LoadProgress() {
        fogMapProgress.Load();
        fogMapProgress.ProgressionMap.IterateAction(InitializeProgressState);
    }

    private void InitializeProgressState(int progress, int row, int column) {
        var tile = implementations_map[row, column];
        if (tile == null) return;
        if (progress == FogMapProgress.UNLOCKED) {
            //Debug.Log("TILE UNLOCKED!!!");
            tile.UpdateGraphic_UnlockState();
        }
        if (progress == FogMapProgress.LOCKED) {
            tile.UpdateGraphic_LockState();
        }
        if (progress == FogMapProgress.CLEARED) {
            tile.UpdateGraphic_ClearedState();
        }
    }

    private static int ElementIdCalc(int i) {
        return i + 2;
    }

    [Serializable]
    private class FogMapSystemUnit {
        [SerializeField]
        int weight;
        [SerializeField]
        private FogMapTileImplementation tileImplementation;
        [SerializeField]
        private DataConstantValue tile;

        public int Weight {
            get {
                return weight;
            }

            set {
                weight = value;
            }
        }
        public DataConstantValue Tile {
            get {
                return tile;
            }

            set {
                tile = value;
            }
        }

        public FogMapTileImplementation TileImplementation {
            get {
                return tileImplementation;
            }

            set {
                tileImplementation = value;
            }
        }
    }
}
