﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FogMap { 
    
    [SerializeField]
    TwoDArrayInt tiles;

    public int randomField;

    public TwoDArrayInt Tiles {
        get {
            return tiles;
        }

        set {
            tiles = value;
        }
    }

    public FogMap(int r, int c) {
        Tiles = new TwoDArrayInt(r,c);
    }

    public int this[int i, int j]{
        get{
            return Tiles[i,j];
        }
        set{
            Tiles[i,j] = value;
        }
    }

    internal void Rectangle(int safeAreaElement, PointInt2D leftDownCorner, PointInt2D rectSize) {
        for (int i = 0; i < rectSize.x; i++) {
            for (int j = 0; j < rectSize.y; j++) {
                Tiles[leftDownCorner + new PointInt2D(i,j)] = safeAreaElement;
            }
        }
    }
}
