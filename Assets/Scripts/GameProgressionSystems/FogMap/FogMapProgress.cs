﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogMapProgress : MonoBehaviour {

    [SerializeField]
    TwoDArrayInt progressionMap;
    public const int LOCKED = 0;
    public const int UNLOCKED = 1;
    public const int CLEARED = 2;
    public MyPersistenceTwoDArrayInt progressionPersistence;
    public bool LoadOnAwake;

    [SerializeField]
    NewProgressMapGenerationRules[] newMapGenerationRules;

    public TwoDArrayInt ProgressionMap {
        get {
            return progressionMap;
        }
    }

    public TwoDArrayInt MapTiles {
        get {
            return mapTiles;
        }

        set {
            mapTiles = value;
        }
    }

    [SerializeField]
    TwoDArrayInt mapTiles;

    [ExposeMethodInEditor]
    public void Save() {
        progressionPersistence.Save(progressionMap);
    }

    [ExposeMethodInEditor]
    public void Load() {
        progressionMap = progressionPersistence.Load();
        if (progressionMap == null ||
            progressionMap.Content == null || progressionMap.Columns == 0) {
            CreateNewProgressionMap();
        }
    }

    [ExposeMethodInEditor]
    public void CreateNewProgressionMap() {
        progressionMap = new TwoDArrayInt();
        progressionMap.Allocate(mapTiles.Rows, mapTiles.Columns);

        foreach (var r in newMapGenerationRules) {
            switch (r.Rule) {
                case RuleTypes.LockAll:
                    progressionMap.ChangeEveryElementTo(LOCKED);
                    break;
                case RuleTypes.UnlockAll:
                    progressionMap.ChangeEveryElementTo(UNLOCKED);
                    break;
                case RuleTypes.UnlockAllAdjascentToTile:
                    for (int i = 0; i < mapTiles.Rows; i++) {
                        for (int j = 0; j < mapTiles.Columns; j++) {
                            if(mapTiles[i,j] == r.tile_relevant) {
                                UnlockAllAdjacent(i, j);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void UnlockAllAdjacent(int row, int column) {
        progressionMap.SafeSet(row + 1, column, UNLOCKED);
        progressionMap.SafeSet(row - 1, column, UNLOCKED);
        progressionMap.SafeSet(row, column + 1, UNLOCKED);
        progressionMap.SafeSet(row, column - 1, UNLOCKED);
    }



    [ExposeMethodInEditor]
    public void DiscardProgressionMap() {
        progressionMap = null;
    }


    // Use this for initialization
    void Start() {

    }

    void Awake() {
        if (LoadOnAwake)
            Load();
    }

    // Update is called once per frame
    void Update() {

    }

    [Serializable]
    private class NewProgressMapGenerationRules {
        [SerializeField]
        RuleTypes rule;
        public DataConstantValue tile_relevant;

        public RuleTypes Rule {
            get {
                return rule;
            }

            set {
                rule = value;
            }
        }
    }

    [Serializable]
    private enum RuleTypes {
        LockAll, UnlockAll,
        UnlockAllAdjascentToTile,
        //UnlockAllOnTile,  not implemented yet
    }

}
