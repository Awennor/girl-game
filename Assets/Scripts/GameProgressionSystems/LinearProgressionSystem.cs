﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Linear Progression System", menuName = "Data/Game Progression/Linear Progression System", order = 3)]
public class LinearProgressionSystem : ScriptableObject {

    int currentProgress;

    [SerializeField]
    List<Unit> units;

    public void Awake() {
        currentProgress = 0;
        Debug.Log(currentProgress + "PROGRESS ");
    }

    internal void ZeroProgress() {
        currentProgress = 0;
    }

    internal MyDicStringString GetCurrentEntryThatContainsKey(string cutscenePlaceKey) {
        return GetCurrentEntryThatContainsPair(cutscenePlaceKey, null); //same as only requiring key
    }

    internal MyDicStringString GetCurrentEntryThatContainsPair(string key, string value) {
        if (units.Count <= currentProgress) {
            return null;
        }
        var sD = units[currentProgress].StringData;
        foreach (var dic in sD) {
            if (dic.ContainsKey(key) && (value == null || dic[key].Equals(value))) {
                return dic;
            }
        }
        return null;
    }

    internal void Advance() {
        currentProgress++;
    }

    //public List<MyDicStringString>

    [Serializable]
    private class Unit {
        [SerializeField]
        List<MyDicStringString> stringData;

        public List<MyDicStringString> StringData {
            get {
                return stringData;
            }

            set {
                stringData = value;
            }
        }
    }
}
