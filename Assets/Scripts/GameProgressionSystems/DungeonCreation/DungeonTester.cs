﻿using FeatureMapGenerator.BaseGridGenerator2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonTester : MonoBehaviour {



    // Use this for initialization
    void Start() {


    }

    [ExposeMethodInEditor]
    public void TestDungeon() {


        var stage2DConfig = new WalkableGridConfig();
        stage2DConfig.Size = new PointInt2D(40, 40);
        stage2DConfig.Seed = 982131290;
        stage2DConfig.RoomMaxSize = new PointInt2D(6, 6);
        stage2DConfig.RoomMinSize = new PointInt2D(3, 3);
        stage2DConfig.CorridorMinSizeNorth = new PointInt2D(1, 4);
        stage2DConfig.CorridorMaxSizeNorth = new PointInt2D(1, 7);
        var walkableGridGenerator1 = new WalkableGridGenerator();
        walkableGridGenerator1.Generate(stage2DConfig);
        var grid = walkableGridGenerator1.Grid2D;

        //FeatureMapGenerator
        

        GetComponent<WalkableGridRenderer>().SummonObjects(grid);
    }

    // Update is called once per frame
    void Update() {

    }
}
