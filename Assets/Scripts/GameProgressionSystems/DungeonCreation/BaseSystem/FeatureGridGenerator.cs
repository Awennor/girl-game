﻿namespace FeatureMapGenerator {
    using BaseGridGenerator2D;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    class FeatureGridGenerator {
        WalkableGrid walkableGrid;
        FeatureGrid featureGrid;

        public Func<int, PointInt2D> featureSizeProvider;

        public void Generate(FeatureMapConfig config, WalkableGrid walkableGrid) {
            featureGrid = new FeatureGrid();
            featureGrid.Config = config;
            var features = config.FeatureList;
            for (int i = 0; i < features.Count; i++) {
                var f = features[i];
                int q = f.Quantity;
                for (int j = 0; j < q; j++) {

                }
            }
        }
    }

    public class FeatureGrid {

        FeatureMapConfig config;

        public FeatureMapConfig Config {
            get {
                return config;
            }

            set {
                config = value;
            }
        }
    }

    [Serializable]
    public class FeatureMapConfig {

        [SerializeField]
        int seed;

        List<FeatureInfo> featureInfo;

        public List<FeatureInfo> FeatureList {
            get {
                return featureInfo;
            }

            set {
                featureInfo = value;
            }
        }
    }

    public class FeatureInfo {
        int[] dataNumber;
        int quantity;

        public int[] DataNumber {
            get {
                return dataNumber;
            }

            set {
                dataNumber = value;
            }
        }

        public int Quantity {
            get {
                return quantity;
            }

            set {
                quantity = value;
            }
        }
    }

}