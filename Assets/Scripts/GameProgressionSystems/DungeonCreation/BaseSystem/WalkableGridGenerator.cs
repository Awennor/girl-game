﻿namespace FeatureMapGenerator.BaseGridGenerator2D {

    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using System;
    using System.Text;

    public class WalkableGridGenerator {
        private WalkableGridConfig config;
        private WalkableGrid grid2D;
        List<PointInt2D> aux = new List<PointInt2D>();

        public WalkableGrid Grid2D {
            get {
                return grid2D;
            }

            set {
                grid2D = value;
            }
        }

        public void Generate(WalkableGridConfig config) {
            this.config = config;
            Grid2D = new WalkableGrid();
            Grid2D.Size = config.Size;
            Grid2D.EmptyGrid();
            RandomSpecial.Seed = config.Seed;
            GenerateRoom(config.Size / 2, RandomDirection()); // starts up with room on the middle
            for (int i = 0; i < 1000; i++) {
                Chunk c;
                if (RandomSpecial.RandomBool() && Grid2D.HasCorridor) {
                    c = Grid2D.RandomCorridor();
                } else {
                    c = Grid2D.RandomRoom();
                }
                c.frontierPoints(aux); //fills up aux with the frontier points of the room
                var randomFrontier = aux.RandomElementSpecial();
                Direction reachableFrom;
                //Debug.Log(randomFrontier+"RANDOM FRONTIER");
                if (Grid2D.ReachableWall(randomFrontier.x, randomFrontier.y, out reachableFrom)) {
                    bool success = false;
                    if (RandomSpecial.RandomBool()) {
                        success = GenerateRoom(randomFrontier, reachableFrom.Opposite());
                    } else {
                        success = GenerateCorridor(randomFrontier, reachableFrom.Opposite());
                    }

                    if (success) {
                        Grid2D.SetTile(Tile.WALKABLE, randomFrontier);
                        Grid2D.SetTile(Tile.WALKABLE, randomFrontier + reachableFrom.Opposite().UnitVector());
                    }
                    //stage2D.SetTile();
                }


            }
            Grid2D.Print();
        }

        private void GenerateRoom(PointInt2D randomFrontier, object p) {
            throw new NotImplementedException();
        }

        private Direction RandomDirection() {
            return (Direction)RandomSpecial.Range(0, 3);
        }

        private bool GenerateRoom(PointInt2D pos, Direction d) {

            Debug.Log("Room creation point" + pos);

            var chunkMinSize = config.RoomMinSize;
            var chunkMaxSize = config.RoomMaxSize;
            Chunk c;
            bool created;
            GenerateChunkRandomSize(pos, d, chunkMinSize, chunkMaxSize, out c, out created);
            if (created) {
                Grid2D.AddRoom(c);
            }
            return created;

        }

        private bool GenerateCorridor(PointInt2D pos, Direction d) {

            Debug.Log("Room creation point" + pos);

            var chunkMinSize = config.CorridorMinSizeNorth;
            var chunkMaxSize = config.CorridorMaxSizeNorth;
            if (d == Direction.EAST || d == Direction.WEST) {
                chunkMinSize = InvertXYVector(chunkMinSize);
                chunkMaxSize = InvertXYVector(chunkMaxSize);
            }
            //ADAPT THIS CODE TO CHANGE CHUNKMINSIZE AND MAX SIZE IN ACCORDANCE TO DIRECTION
            //    BASICALLY INVERT VALUES IN CASE OF EAST OR WEST I GUESS
            Chunk c;
            bool created;
            GenerateChunkRandomSize(pos, d, chunkMinSize, chunkMaxSize, out c, out created);
            if (created) {
                Grid2D.AddCorridor(c);
            }
            return created;

        }

        private static PointInt2D InvertXYVector(PointInt2D chunkMinSize) {
            int y = chunkMinSize.x;
            chunkMinSize.x = chunkMinSize.y;
            chunkMinSize.y = y;
            return chunkMinSize;
        }

        private void GenerateChunkRandomSize(PointInt2D pos, Direction d, PointInt2D chunkMinSize, PointInt2D chunkMaxSize, out Chunk c, out bool created) {
            var chunkDimensions = RandomBetween2Points(chunkMinSize, chunkMaxSize) + new PointInt2D(2, 2);
            chunkDimensions.x = chunkDimensions.x;
            chunkDimensions.y = chunkDimensions.y;
            c = null;
            created = GenerateChunk(pos, chunkDimensions, d, out c);
        }

        public static PointInt2D RandomBetween2Points(PointInt2D v1, PointInt2D v2) {
            PointInt2D v3 = new PointInt2D(UnityEngine.Random.Range(v1.x, v2.x), UnityEngine.Random.Range(v1.y, v2.y));
            return v3;
        }

        private bool GenerateChunk(PointInt2D pos, PointInt2D size, Direction d, out Chunk c) {
            PointInt2D leftCorner = new PointInt2D();
            c = null;
            int rw = size.x; // room width
            int rh = size.y; // room height
            //int sw = (int) size.x;
            //int sh = (int) size.y;
            if (d == Direction.NORTH) {
                leftCorner = pos + new PointInt2D(-rw / 2, 1);
            }
            if (d == Direction.SOUTH) {
                leftCorner = pos + new PointInt2D(-rw / 2, -rh);
            }
            if (d == Direction.WEST) {
                leftCorner = pos + new PointInt2D(-rw, -rh / 2);
            }
            if (d == Direction.EAST) {
                leftCorner = pos + new PointInt2D(1, -rh / 2);
            }

            if (
                leftCorner.x < 0 || leftCorner.y < 0
                || leftCorner.x + size.x > Grid2D.Size.x - 1
                || leftCorner.y + size.y > Grid2D.Size.y - 1) {

                return false;
            }

            for (int i = 0; i < rw; i++) {
                for (int j = 0; j < rh; j++) {
                    var cannotCreate =
                        Get(leftCorner + new PointInt2D(i, j)) != Tile.UNUSED //can build on top of UNUSED
                        && Get(leftCorner + new PointInt2D(i, j)) != Tile.WALL;  //can build on top WALL, make this configurable
                    if (cannotCreate) {

                        return false;
                    }
                }
            }

            for (int i = 0; i < rw; i++) {
                for (int j = 0; j < rh; j++) {
                    bool wall = i == 0 || i == rw - 1 || j == 0 || j == rh - 1;
                    if (wall)
                        Set(Tile.WALL, leftCorner + new PointInt2D(i, j));
                    else
                        Set(Tile.WALKABLE, leftCorner + new PointInt2D(i, j));
                }
            }
            c = new Chunk();
            c.BottomLeft = leftCorner;
            c.Size = size;
            return true;
        }

        private Tile Get(PointInt2D PointInt2D) {
            return Grid2D.GetTile(PointInt2D.x, PointInt2D.y);
        }

        private void Set(Tile t, PointInt2D PointInt2D) {
            Grid2D.SetTile(t, PointInt2D.x, PointInt2D.y);
        }


    }

    [Serializable]
    public class WalkableGridConfig {
        [SerializeField]
        PointInt2D size;
        [SerializeField]
        PointInt2D roomMinSize;
        [SerializeField]
        PointInt2D roomMaxSize;
        [SerializeField]
        PointInt2D corridorMinSize;
        [SerializeField]
        PointInt2D corridorMaxSize;
        [SerializeField]
        int seed;

        public PointInt2D RoomMinSize {
            get {
                return roomMinSize;
            }

            set {
                roomMinSize = value;
            }
        }

        public PointInt2D RoomMaxSize {
            get {
                return roomMaxSize;
            }

            set {
                roomMaxSize = value;
            }
        }

        public PointInt2D Size {
            get {
                return size;
            }

            set {
                size = value;
            }
        }

        public int Seed {
            get {
                return seed;
            }

            set {
                seed = value;
            }
        }

        public PointInt2D CorridorMinSizeNorth {
            get {
                return corridorMinSize;
            }

            set {
                corridorMinSize = value;
            }
        }

        public PointInt2D CorridorMaxSizeNorth {
            get {
                return corridorMaxSize;
            }

            set {
                corridorMaxSize = value;
            }
        }
    }

    [Serializable]
    public class WalkableGrid {
        [SerializeField]
        int w;
        [SerializeField]
        int h;
        [SerializeField]
        List<Tile> tiles = new List<Tile>();
        List<Chunk> rooms = new List<Chunk>();


        List<Chunk> corridors = new List<Chunk>();

        public PointInt2D Size {
            get {
                return new PointInt2D(w, h);
            }

            set {

                w = value.x;
                h = value.y;
                Debug.Log(w + "wh" + h);
            }
        }

        public bool HasCorridor {
            get {
                return corridors.Count != 0;
            }
        }

        internal void Print() {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < w; i++) {
                for (int j = 0; j < h; j++) {
                    char c = (char)((GetTile(i, j)) + '0');

                    if (c == '2')
                        c = '_';
                    if (c == '1')
                        c = '1';
                    if (c == '0')
                        c = '0';
                    s.Append(c);
                }

                s.AppendLine();
            }
            Debug.Log(s.ToString());
        }

        internal Tile GetTile(int x, int y) {

            return tiles[y * w + x];
        }

        internal void SetTile(Tile t, int x, int y) {

            var v = y * w + x;
            //Debug.Log("Index"+v+"_"+tiles.Count);
            tiles[v] = t;
        }

        internal void SetTile(Tile wALKABLE, PointInt2D randomFrontier) {
            SetTile(wALKABLE, randomFrontier.x, randomFrontier.y);
        }

        internal void EmptyGrid() {

            while (tiles.Count < w * h) {
                tiles.Add(Tile.UNUSED);
            }
            for (int i = 0; i < tiles.Count; i++) {
                tiles[i] = Tile.UNUSED;
            }
        }

        internal void AddRoom(Chunk c) {
            rooms.Add(c);

        }

        internal void AddCorridor(Chunk c) {
            corridors.Add(c);
        }

        internal Chunk RandomRoom() {

            return rooms.RandomElementSpecial();
        }

        internal Chunk RandomCorridor() {

            return corridors.RandomElementSpecial();
        }

        internal bool ReachableWall(int x, int y, out Direction reachableFrom) {
            if (GetTile(x, y) == Tile.WALL) {
                if (GetTile(x + 1, y) == Tile.WALKABLE) {
                    reachableFrom = Direction.EAST;
                    return true;
                }
                if (GetTile(x - 1, y) == Tile.WALKABLE) {
                    reachableFrom = Direction.WEST;
                    return true;
                }
                if (GetTile(x, y + 1) == Tile.WALKABLE) {
                    reachableFrom = Direction.NORTH;
                    return true;
                }
                if (GetTile(x, y - 1) == Tile.WALKABLE) {
                    reachableFrom = Direction.NORTH;
                    return true;
                }
            }
            reachableFrom = Direction.NORTH; //does not matter, not reachable
            return false;
        }


    }

    public enum Tile {
        UNUSED, WALKABLE, WALL,
    }

    public enum Direction {
        NORTH, SOUTH, EAST, WEST

    }

    

    public class Chunk {
        PointInt2D size;
        PointInt2D bottomLeft;

        public PointInt2D BottomLeft {
            get {
                return bottomLeft;
            }

            set {
                bottomLeft = value;
            }
        }

        public PointInt2D Size {
            get {
                return size;
            }

            set {
                size = value;
            }
        }

        internal void frontierPoints(List<PointInt2D> aux) {
            aux.Clear();
            int w = size.x;
            int h = size.y;
            int x0 = bottomLeft.x;
            int y0 = bottomLeft.y;
            for (int i = 0; i < w; i++) {
                aux.Add(new PointInt2D(i + x0, y0));
                aux.Add(new PointInt2D(i + x0, y0 + h - 1));
            }
            for (int i = 0; i < h; i++) {
                aux.Add(new PointInt2D(x0, y0 + i));
                aux.Add(new PointInt2D(x0 + w - 1, y0 + i));
            }
        }
    }

    public static class DirectionMethod {
        public static Direction Opposite(this Direction s1) {
            switch (s1) {
                case Direction.NORTH:
                    return Direction.SOUTH;
                    break;
                case Direction.SOUTH:
                    return Direction.NORTH;
                    break;
                case Direction.EAST:
                    return Direction.WEST;
                    break;
                case Direction.WEST:
                    return Direction.EAST;
                    break;
                default:
                    return Direction.NORTH;
                    break;
            }

        }

        public static PointInt2D UnitVector(this Direction s1) {
            switch (s1) {
                case Direction.NORTH:
                    return new PointInt2D(0, 1);
                    break;
                case Direction.SOUTH:
                    return new PointInt2D(0, -1);
                    break;
                case Direction.EAST:
                    return new PointInt2D(1, 0);
                    break;
                case Direction.WEST:
                    return new PointInt2D(-1, 0);
                    break;
                default:
                    return new PointInt2D(0, 0);
                    break;
            }

        }



    }
}
