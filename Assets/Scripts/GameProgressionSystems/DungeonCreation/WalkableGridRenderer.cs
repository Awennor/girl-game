﻿using FeatureMapGenerator.BaseGridGenerator2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkableGridRenderer : MonoBehaviour {

    public GameObject parent;
    public GameObject prefab;
    public float gridWorldScale;

    public void SummonObjects(WalkableGrid grid) {
        if(parent == null) parent = transform.GetOrAddChildWithName("Generated_Automatic");
        var size = grid.Size;
        for (int i = 0; i < size.x; i++) {
            for (int j = 0; j < size.y; j++) {
                var tile = grid.GetTile(i,j);
                if(tile == Tile.WALKABLE) {
                    var obj = Instantiate(prefab);
                    obj.transform.position = new Vector3(i*gridWorldScale, 0, j*gridWorldScale);
                    obj.transform.SetParent(parent.transform);
                }
            }
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
