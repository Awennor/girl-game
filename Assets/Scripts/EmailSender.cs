﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.IO;
using System.Net.Mime;

public class EmailSender : MonoBehaviour {

    string mailName = "datacollectionxone@gmail.com";

    void Start() {


        //string body = "This is for testing SMTP mail from GMAIL";
        //string subject = "TestBody";
        //SendEmail(body, subject);

    }

    public void SendEmail(string subject, string body) {
        Debug.Log("" + subject + "XXX" + body);
        MailMessage mail = new MailMessage();
        mail.From = new MailAddress(mailName);
        //mail.To.Add(mailName);
        mail.To.Add("gabrielpff2@gmail.com");
        mail.Subject = subject;
        mail.Body = body;
        SendMail(mail);
    }

    private void SendMail(MailMessage mail) {
        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        smtpServer.Port = 587;
        smtpServer.Credentials = new System.Net.NetworkCredential(mailName, "senhapaia") as ICredentialsByHost;
        smtpServer.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
        smtpServer.Send(mail);
        Debug.Log("success");
    }

    public void SendMailWithCsv(string subject, string fileName, string fileBody) {
        using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(fileBody))) {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(mailName);
            mail.To.Add("gabrielpff2@gmail.com");
            mail.Subject = subject;
            mail.Body = "";
            Attachment attachment = new Attachment(stream, new ContentType("text/csv"));
            attachment.Name = fileName;
            mail.Attachments.Add(attachment);

            SendMail(mail);
        }
    }


}