﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class InfoConfirmationUI : MonoBehaviour {

    public Button button;
    public GameObject graphic;
    public SkillInfoUI skillInfo;
    public Text text;
    private InfoConfirmAccess infoConfirmAccess;

    void Awake() {
        button.onClick.AddListener(ButtonPress);
        infoConfirmAccess = CentralResource.Instance.InfoConfirmAccess;
        infoConfirmAccess.RequestShow += RequestShow;
    }

    private void RequestShow() {
        graphic.SetActive(true);
        text.text = infoConfirmAccess.Text;
        skillInfo.gameObject.SetActive(infoConfirmAccess.ShowSkill);
        skillInfo.ChangeSkill(infoConfirmAccess.SkillId);
        Time.timeScale = 0;
        button.Select();
    }

    // Use this for initialization
    void Start () {

	    //button.onClick.AddListener(ButtonPress);
	}

    private void ButtonPress() {
        graphic.SetActive(false);
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
