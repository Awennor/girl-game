﻿using UnityEngine;
using System.Collections;

public class BNoseAttack1 : SkillBehaviour {

	// Use this for initialization
	void Start () {
	    base.skillHelperInit();
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.NoseShoot1, StartAttack);
        
	}

    

    public void StartAttack() {
        skillHelper.AnimationTrigger("charge1");
        SoundSingleton.Instance.PlaySound("charge2a");
        const float delayToAttack = 1f;
        skillHelper.DelayedMethod(delayToAttack, DoAttack);
        skillHelper.FaceTarget(delayToAttack+0.1f);
        skillHelper.AttackState(delayToAttack+0.3f);
        skillHelper.SuperArmor(delayToAttack+0.3f);
        //DoAttack();
    }

    private void DoAttack() {
        SoundSingleton.Instance.PlaySound("enemyattack");
        skillHelper.AnimationTrigger("battlepose", 0.2f);
        for (int i = 0; i < 3; i++) {
            GameObject obj = skillHelper.GetProj(ProjectilHolder.Projs.EnemyBall1, SkillInfoProvider.SkillId.Bomb, 15f);
            obj.transform.localPosition += new Vector3(0,3.5f+1.2f,0);
            var r = this.transform.rotation;
            obj.transform.rotation = r * Quaternion.Euler(0, (i - 1) * 24, 0);
            skillHelper.SpeedForward(obj, 20);
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
