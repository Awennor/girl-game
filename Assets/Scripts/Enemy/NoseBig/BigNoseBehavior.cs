﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;

public class BigNoseBehavior : EnemyBehaviourBase {
    private BNoseAttack1 bNoseAttack1;
    private BNoseAttack2 bNoseAttack2;
    

    private void Attack() {
        if(isActiveAndEnabled) {
            if(RandomSpecial.RandomBool())
                bNoseAttack1.StartAttack();
            else
                bNoseAttack2.StartAttack();
            base.delayedMethodScaleByEnemyNumber(Attack);
            
        }
    }
    internal override void OnEnableSub() {
        
        base.delayedMethodScaleByEnemyNumberFullRandom(Attack);
    }

    internal override void AwakeSub() {
        baseDelay = 1.4f;
        delayPerEnemy = 2f;
        bNoseAttack1 = gameObject.AddComponent<BNoseAttack1>();
        bNoseAttack2 = gameObject.AddComponent<BNoseAttack2>();
        base.delayedMethodScaleByEnemyNumberFullRandom(Attack);

    }
}
