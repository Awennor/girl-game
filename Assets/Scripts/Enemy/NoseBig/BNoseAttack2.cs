﻿using UnityEngine;
using System.Collections;

public class BNoseAttack2 : SkillBehaviour {

	// Use this for initialization
	void Start () {
	    base.skillHelperInit();
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.NoseShoot2, StartAttack);
	}

    

    public void StartAttack() {
        skillHelper.AnimationTrigger("charge2");
        SoundSingleton.Instance.PlaySound("charge2");
        const float delayToAttack = 1.7f;
        skillHelper.DelayedMethod(delayToAttack, DoAttack);
        skillHelper.DelayedMethod(delayToAttack+0.4f, DoAttack2);
        skillHelper.FaceTarget(delayToAttack+0.1f);
        skillHelper.AttackState(delayToAttack+0.3f);
        skillHelper.SuperArmor(delayToAttack+0.3f);
        
    }

    private void DoAttack() {
        SoundSingleton.Instance.PlaySound("enemyattack");
        skillHelper.AnimationTrigger("battlepose", 0.2f);
        for (int i = 0; i < 2; i++) {
            GameObject obj = skillHelper.GetProj(ProjectilHolder.Projs.EnemyBall1, SkillInfoProvider.SkillId.NoseShoot1, 3f);
            obj.transform.localPosition += new Vector3(0,3.5f+1.2f,0)+this.transform.rotation* new Vector3(i*14-7f, 0);
            
            var r = this.transform.rotation;
            obj.transform.rotation = r;
            skillHelper.SpeedForward(obj, 120);
        }
    }

    private void DoAttack2() {
        SoundSingleton.Instance.PlaySound("enemyattack");
        skillHelper.AnimationTrigger("battlepose", 0.2f);
        for (int i = 0; i < 1; i++) {
            GameObject obj = skillHelper.GetProj(ProjectilHolder.Projs.EnemyBall1, SkillInfoProvider.SkillId.NoseShoot1, 3f);
            obj.transform.localPosition += new Vector3(0,4.5f,0);
            
            var r = this.transform.rotation;
            obj.transform.rotation = r;
            skillHelper.SpeedForward(obj, 120);
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
