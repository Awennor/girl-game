﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;

public class MovementToGoalEnemy : MonoBehaviour {
    public float speed = 90f;           

    Vector3 movement;                   
    public bool autoDisable = false;
    
    [SerializeField]
    float wantedDistance;
    [SerializeField]
    float wantedDistanceMax;
    [SerializeField]
    bool advanceOnly;
    private Invoker invoker;
    bool waiting;
    public Action reachGoodDistance;
    private MovementAnimatedActor moveAnimAct;
    Transform target;
    bool disableOnReach = false;

    public float WantedDistance
    {
        get
        {
            return wantedDistance;
        }

        set
        {
            wantedDistance = value;
        }
    }

    public Transform Target {
        get {
            return target;
        }

        set {
            target = value;
        }
    }

    public MovementAnimatedActor MoveAnimAct {
        get {
            return moveAnimAct;
        }

        set {
            moveAnimAct = value;
        }
    }

    public bool DisableOnReach {
        get {
            return disableOnReach;
        }

        set {
            disableOnReach = value;
        }
    }

    void Start()
    {
        var control = GetComponent<ControllableObject>();
        if(control != null)control.AddAntiInputScript(this);
        this.enabled = !autoDisable;
        invoker = GetComponent<Invoker>();
        MoveAnimAct = GetComponent<MovementAnimatedActor>();
    }

    void OnEnable() {
        waiting = false;
    }

    void FixedUpdate()
    {

        var chosenTarget = target;
        if (chosenTarget == null) return;
        MoveAnimAct.Speed = speed;
        Vector3 targetPosition = chosenTarget.transform.position;
        Vector3 selfPos = this.transform.position;
        targetPosition -= selfPos;
        float sqrMag = targetPosition.sqrMagnitude;
        bool noMovement = false;
        float wantedSq = wantedDistance * wantedDistance;
        //Debug.Log(sqrMag +"dis dis2-"+wantedSq);
        if (wantedDistanceMax * wantedDistanceMax < sqrMag) {
            waiting = false;
        }

        if (waiting) {
            noMovement = true;
            
        }
        bool goodDis = false;
        if (sqrMag < wantedSq  && advanceOnly) {
            noMovement = true;
            goodDis = true;
            
        }
        if (Mathf.Abs(sqrMag - wantedSq) < 3) {
            noMovement = true;
            goodDis = true;
        }
        if (goodDis && !waiting) {
            if (reachGoodDistance != null) {
                reachGoodDistance();
            }
            if(disableOnReach) {
                MoveAnimAct.H = 0;
                MoveAnimAct.V = 0;
                enabled = false;
                return;
            }
        }
        if (noMovement) {
            waiting = true;
            targetPosition = Vector3.zero;
        }
        targetPosition.Normalize();
        float h = targetPosition.x;
        float v = targetPosition.z;
        MoveAnimAct.H = h;
        MoveAnimAct.V = v;

    }

    internal void StopMove() {
        MoveAnimAct.H = 0;
        MoveAnimAct.V = 0;

    }

    private void nowait()
    {
        waiting = false;
    }






}
