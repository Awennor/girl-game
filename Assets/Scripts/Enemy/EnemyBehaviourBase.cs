﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;
using System.Collections.Generic;

public abstract class EnemyBehaviourBase : MonoBehaviour {
    private BattleFlowSystem battleFlow;
    private Invoker invoker;
    protected float baseDelay=1f;
    protected float delayPerEnemy=1f;
    protected float maxDelay = 9999f;
    protected float variance;
    protected float minDelay = 0.1f;
    protected SkillHelper skillHelper;
    protected List<Action> auxActionList = new List<Action>();
    int actionListIndex = 0;

    public Action GetActionFromAuxListAndAdvance () {
        if(actionListIndex >= auxActionList.Count) {
            actionListIndex = 0;
        }
        actionListIndex++;
        return auxActionList[actionListIndex-1];
        
        

    }

    internal void Awake() {
        invoker = GetComponent<Invoker>();
        GetComponent<ControllableObject>().AddAntiInputScript(this);
        battleFlow = CentralResource.Instance.BattleFlowSys;
        skillHelper = GetComponent<SkillHelper>();
        AwakeSub();
    }

    internal void delayedMethodScaleByEnemyNumber(Action attack) {
        delayedMethodScaleByEnemyNumber(attack, baseDelay, delayPerEnemy);
    }

     internal void delayedMethodScaleByEnemyNumberFullRandom(Action attack) {
        delayedMethodScaleByEnemyNumberRandom(attack, baseDelay, delayPerEnemy);
    }

    void delayedMethodScaleByEnemyNumber(Action attack, float baseDelay, float delayByEnemy) {
        var delay = baseDelay + delayByEnemy * battleFlow.NEnemies - 1;
        
        ActualAction(attack, delay);
    }

    private void ActualAction(Action attack, float delay) {
        if(delay > maxDelay) delay = maxDelay;
        delay *= 1+RandomSpecial.randomFloatOnZero(variance);
        if(delay < minDelay ) delay = minDelay;
        invoker.AddActionUnique(attack, delay);
    }

    void delayedMethodScaleByEnemyNumberRandom(Action attack, float baseDelayMax, float delayByEnemyMax) {
        baseDelayMax = baseDelayMax* RandomSpecial.randomFloat(0.9f)+0.1f;
        delayByEnemyMax = delayByEnemyMax* RandomSpecial.randomFloat(0.9f)+0.1f;
        var resultDelay = baseDelayMax + delayByEnemyMax * (battleFlow.NEnemies - 1);
        //Debug.Log("Scaled delay of attack "+resultDelay);
        ActualAction(attack, resultDelay);
        //invoker.AddActionUnique(attack, resultDelay);
    }

    internal void OnEnable() {
        OnEnableSub();
    }

    abstract internal void AwakeSub();
    abstract internal void OnEnableSub();

    internal void ResetAuxIndexToOne() {
        actionListIndex = 0;
    }
}
