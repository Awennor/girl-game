﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;

public class MovementToTargetEnemy2 : MonoBehaviour {
    public float speed = 90f;            // The speed that the player will move at.

    Vector3 movement;                   // The vector to store the direction of the player's movement.
    public Animator anim;                      // Reference to the animator component.
    Rigidbody playerRigidbody;          // Reference to the player's rigidbody.
    public bool autoDisable = false;
    private CharacterControllerWrapper CharacterControllerWrapper;
    private BattleStateMachine battleState;
    private ClosestTarget closeTarget;
    [SerializeField]
    float wantedDistance;
    [SerializeField]
    float wantedDistanceMax;
    [SerializeField]
    bool advanceOnly;
    private Invoker invoker;
    bool waiting;
    public Action reachGoodDistance;

    public float WantedDistance
    {
        get
        {
            return wantedDistance;
        }

        set
        {
            wantedDistance = value;
        }
    }

    void Start()
    {
        var control = GetComponent<ControllableObject>();
        control.AddAntiInputScript(this);
        this.enabled = !autoDisable;
        playerRigidbody = GetComponent<Rigidbody>();
        CharacterControllerWrapper = GetComponent<CharacterControllerWrapper>();
        battleState = GetComponent<BattleStateMachine>();
        battleState.stateChanged += (bsm) =>
        {
            if (anim != null)
                anim.SetBool("walking", false);
        };
        closeTarget = GetComponent<ClosestTarget>();
        invoker = GetComponent<Invoker>();
    }


    void FixedUpdate()
    {
        if (!battleState.CanMove())
        {
            return;
        }
        Vector3 targetPosition = closeTarget.ChosenTarget.transform.position;
        Vector3 selfPos = this.transform.position;
        targetPosition -= selfPos;
        float sqrMag = targetPosition.sqrMagnitude;
        bool noMovement = false;
        float wantedSq = wantedDistance * wantedDistance;
        //Debug.Log(sqrMag +"dis dis2-"+wantedSq);
        if (wantedDistanceMax * wantedDistanceMax < sqrMag) {
            waiting = false;
        }

        if (waiting) {
            noMovement = true;
            
        }
        bool goodDis = false;
        if (sqrMag < wantedSq  && advanceOnly) {
            noMovement = true;
            goodDis = true;
            
        }
        if (Mathf.Abs(sqrMag - wantedSq) < 3) {
            noMovement = true;
            goodDis = true;
        }
        if (goodDis && !waiting) {
            if (reachGoodDistance != null) {
                reachGoodDistance();
            }
        }
        if (noMovement) {
            waiting = true;
            targetPosition = Vector3.zero;
        }
        targetPosition.Normalize();
        float h = targetPosition.x;
        float v = targetPosition.z;
        // Move the player around the scene.
        Move(h, v);
        Debug.Log(v+"Movement rocking around" + h);

        // Turn the player to face the mouse cursor.
        Turning(h, v);

        // Animate the player.
        Animating(h, v);
    }

    private void nowait()
    {
        waiting = false;
    }

    void Move(float h, float v)
    {
        movement.Set(h, 0, v);
        movement = movement.normalized * speed;
        CharacterControllerWrapper.SetSpeed(movement);
    }

    void Turning(float h, float v)
    {
        movement.x = h;
        movement.z = v;
        if (movement.sqrMagnitude != 0)
        {
            Quaternion newRotation = Quaternion.LookRotation(movement);
            playerRigidbody.MoveRotation(newRotation);
        }
    }

    void Animating(float h, float v)
    {
        // Create a boolean that is true if either of the input axes is non-zero.
        bool walking = h != 0f || v != 0f;

        // Tell the animator whether or not the player is walking.
        if (anim != null)
            anim.SetBool("walking", walking);

    }
}
