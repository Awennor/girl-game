﻿using UnityEngine;
using System.Collections;
using System;

public class DodgeBehavior : MonoBehaviour {
    private SkillHelper skillHelper;
    [SerializeField]
    float dodgeSpeed;
    [SerializeField]
    float dodgeDuration;
    [SerializeField]
    float dodgePeriod;
    [SerializeField]
    bool dodgePerpendicularTarget = true;
    [SerializeField]
    bool wantToDodge;

    public bool WantToDodge {
        get {
            return wantToDodge;
        }

        set {
            wantToDodge = value;
            if(wantToDodge && enabled)
                Dodge();
        }
    }

    // Use this for initialization
    void Start () {
        skillHelper = GetComponent<SkillHelper>();
        GetComponent<BattleStateMachine>().stateChanged += stateChange;
        var ctrl = GetComponent<ControllableObject>();
        ctrl.AddAntiInputScript(this);
	}

    private void stateChange(BattleStateMachine obj)
    {
        if (obj.CanMove() && enabled) {
            skillHelper.DelayedMethodUnique(Dodge, dodgePeriod);
        }
    }

    void OnEnable() {
        if(wantToDodge)
            Dodge();
    }

    void OnDisable() {
        wantToDodge = false;
    }

    void Dodge() {
        if (skillHelper == null) {
            return;
        }
        if (enabled && skillHelper.CanMoveState() && wantToDodge) {
            
            skillHelper.AttackState(dodgeDuration);
            //skillHelper.Move(Vector3Special.vectorXZRandomDirection() * dodgeSpeed, dodgeDuration);
            skillHelper.FaceTarget(1);
            Vector3 direction = transform.right;
            direction += direction + transform.forward * RandomSpecial.randomFloatOnZero(0.1f);
            if (RandomSpecial.RandomBool()) {
                direction *= -1;
            }
            direction.y = 0;
            var speedMove = direction.normalized * dodgeSpeed;
            speedMove.y += -50;
            skillHelper.Move(speedMove, dodgeDuration);
            skillHelper.DelayedMethodUnique(Dodge, dodgePeriod);
            
            
        }
    }

    internal void WantToDodgeOn() {
        
        
        wantToDodge = true;
        if(wantToDodge) Dodge();
    }

    // Update is called once per frame
    void Update () {
	
	}
}
