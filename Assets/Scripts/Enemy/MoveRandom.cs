﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;

public class MoveRandom : MonoBehaviour {

    float angularAccel;
    private Invoker invoker;
    Vector3 direction;
    private MovementAnimatedActor move;
    [SerializeField]
    float speed = -1;
    public bool noResting;
    public float minRefreshTime;
    public float maxRefreshTime;

    // Use this for initialization
    void Start () {
        invoker = GetComponent<Invoker>();
        direction = Vector3.forward;
        move = GetComponent<MovementAnimatedActor>();
        Refresh();
        GetComponent<MovementConstraint>().constrainedMove+=MoveContrain ;
	}

    private void MoveContrain(bool arg1, bool arg2)
    {
        Refresh();
    }

    void OnEnable() {
        if(invoker != null)
            Refresh();
    }

    void Refresh() {
        //UnityEngine.Random.Range();
        if (UnityEngine.Random.Range(0f, 1f) > 0.5f){
            angularAccel = UnityEngine.Random.Range(-10, 10);
        }
        else{
            angularAccel = 0;
        }
        angularAccel = UnityEngine.Random.Range(-20, 20);
        if (UnityEngine.Random.Range(0f, 1f) > 0.5f && !noResting)
        {
            direction = Vector3.zero;
        }
        else
        {
            direction = Vector3.forward;
            var rot = Quaternion.AngleAxis(UnityEngine.Random.Range(0, 360), Vector3.up);
            direction = rot * direction;
        }
        invoker.AddAction(Refresh, UnityEngine.Random.Range(minRefreshTime, maxRefreshTime
            ));
    }

    internal void EnableMethod() {
        enabled = true;
    }

    // Update is called once per frame
    void FixedUpdate () {
        if(speed != -1)
            move.Speed = speed;
        move.H = direction.x;
        move.V = direction.z;

        if (angularAccel != 0) {
            var rot = Quaternion.AngleAxis(angularAccel * Time.deltaTime, Vector3.up);
            direction = rot * direction;
        }
        

    }


}
