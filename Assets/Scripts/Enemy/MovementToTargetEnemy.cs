﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;

public class MovementToTargetEnemy : MonoBehaviour {
    public float speed = 90f;            // The speed that the player will move at.

    Vector3 movement;                   // The vector to store the direction of the player's movement.
    public bool autoDisable = false;
    private ClosestTarget closeTarget;
    [SerializeField]
    float wantedDistance;
    [SerializeField]
    float wantedDistanceMax;
    [SerializeField]
    bool advanceOnly;
    private Invoker invoker;
    bool waiting;
    public Action reachGoodDistance;
    private MovementAnimatedActor moveAnimAct;

    public float WantedDistance
    {
        get
        {
            return wantedDistance;
        }

        set
        {
            wantedDistance = value;
        }
    }

    void Start()
    {
        var control = GetComponent<ControllableObject>();
        control.AddAntiInputScript(this);
        this.enabled = !autoDisable;
        closeTarget = GetComponent<ClosestTarget>();
        invoker = GetComponent<Invoker>();
        moveAnimAct = GetComponent<MovementAnimatedActor>();
    }

    void OnEnable() {
        waiting = false;
    }

    void FixedUpdate()
    {

        var chosenTarget = closeTarget.ChosenTarget;
        if (chosenTarget == null) return;
        //Debug.Log("Updating");
        moveAnimAct.Speed = speed;
        Vector3 targetPosition = chosenTarget.transform.position;
        Vector3 selfPos = this.transform.position;
        targetPosition -= selfPos;
        float sqrMag = targetPosition.sqrMagnitude;
        bool noMovement = false;
        float wantedSq = wantedDistance * wantedDistance;
        //Debug.Log(sqrMag +"dis dis2-"+wantedSq);
        if (wantedDistanceMax * wantedDistanceMax < sqrMag) {
            waiting = false;
        }

        if (waiting) {
            noMovement = true;
            
        }
        bool goodDis = false;
        if (sqrMag < wantedSq  && advanceOnly) {
            noMovement = true;
            goodDis = true;
            
        }
        if (Mathf.Abs(sqrMag - wantedSq) < 3) {
            noMovement = true;
            goodDis = true;
        }
        if (goodDis && !waiting) {
            if (reachGoodDistance != null) {
                reachGoodDistance();
            }
        }
        if (noMovement) {
            waiting = true;
            targetPosition = Vector3.zero;
        }
        targetPosition.Normalize();
        float h = targetPosition.x;
        float v = targetPosition.z;
        moveAnimAct.H = h;
        moveAnimAct.V = v;

    }

    private void nowait()
    {
        waiting = false;
    }






}
