﻿using UnityEngine;
using System.Collections;
using System;

public class HairBAttack2 : SkillBehaviour {
    GameObject source;
    private Transform handBone;
    private GameObject energyFocused;


    public GameObject Source {
        get {
            return source;
        }

        set {
            source = value;
        }
    }

    public Transform HandBone {
        get {
            return handBone;
        }

        set {
            handBone = value;
        }
    }



    // Use this for initialization
    void Start() {
        base.skillHelperInit();
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.HairSuicide, Attack1Start);
    }

    public void DelayedAttack(float v, Action a) {
        skillHelper.DelayedMethod(a, v);
    }

    private void Attack1() {
        //skillHelper.ScreenShakeType2(0.1f);

        //skillHelper.AnimationTrigger("battlepose", 0.1f);
        SoundSingleton.Instance.PlaySound("enemysetup2");

        Vector3 projSpawnPosition = Source.transform.position;
        //skillHelper.MoveTowardsDirectionXZ(-60, 0, 0.1f);
        GameObject obj = skillHelper.GetProj(ProjectilHolder.Projs.EnemyBall1, SkillInfoProvider.SkillId.HairSuicide, 15f);
        //obj.transform.localPosition += new Vector3(0, 2f, 0);
        obj.transform.position = source.transform.position;
        var r = this.transform.rotation;
        obj.transform.rotation = r;
        skillHelper.SpeedForward(obj, 50);
        var position = transform.position;
        position.y = source.transform.position.y;
        const float timeToReturn = 0.5f;
        skillHelper.ProjMoveTo(obj, position, 80f, timeToReturn);
        skillHelper.ReverseTagProj(timeToReturn, obj);
        skillHelper.ProjCollideWithGround(obj);
        

    }

    internal void HandAuraOff() {
        energyFocused.SetActive(false);
    }

    internal void HandAura() {
        if (energyFocused != null) {
            energyFocused.SetActive(true);
        } else
            energyFocused = EffectSingleton.Instance.ShowAndAddEffect("energyfocused", handBone.transform);
    }


    public void Attack1Start() {
        const float timeToAttack = 0.43f;
        string animationTriggerBefore = "attack1before";
        string animationTriggerAfter = "attack1";
        Action attack = Attack1;
        DelayedAttack2(timeToAttack, animationTriggerBefore, animationTriggerAfter, attack);
    }


    private void DelayedAttack2(float timeToAttack, string animationTriggerBefore, string animationTriggerAfter, Action attack) {
        //skillHelper.DelayedMethod(timeToAttack, attack);
        for (int i = 0; i < 5; i++) {
            skillHelper.DelayedMethod(timeToAttack+i*0.1f, attack);
        }
        skillHelper.FaceTarget(timeToAttack + 0.1f);
        skillHelper.AttackState(timeToAttack + 0.1f);
        skillHelper.SuperArmor(timeToAttack + 0.1f);
        skillHelper.AnimationTrigger(animationTriggerBefore);
        skillHelper.AnimationTrigger(animationTriggerAfter, timeToAttack - 0.1f);

    }




    // Update is called once per frame
    void Update() {

    }
}
