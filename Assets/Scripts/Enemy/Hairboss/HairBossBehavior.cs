﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;
using System.Collections.Generic;

public class HairBossBehavior : EnemyBehaviourBase {
    //private NoseAttacks noseAttack1;
    public Transform sourceAtk1;
    public Transform sourceAtk3;
    public Transform handBone;
    private HairBAttack1 hairAttack1;
    private Dictionary<string, Transform> positionDictionary;
    private MovementToGoalEnemy moveGoal;
    Action moveAction;
    private HairBHoldattack holdAttack;
    private HPSystem hpSystem;
    private DodgeBehavior dodgeBehav;
    [SerializeField]
    private bool dodgeAndShootPhase;
    [SerializeField]
    private bool shootMove;
    [SerializeField]
    private int bombAttackLevel = 1;
    [SerializeField]
    private int shootAttackLevel = 1;
    [SerializeField]
    private int walkAttackLevel = 1;


    private void AttackNew() {
        if (isActiveAndEnabled) {
            GetActionFromAuxListAndAdvance()();

        }
    }

    private void Attack1() {
        if (isActiveAndEnabled) {
            //hairAttack1.HandAura();
            //hairAttack1.DelayedAttack(0f, holdAttack.StartAttack);
            //return;
            hairAttack1.HandAura();
            hairAttack1.DelayedAttack(0f, hairAttack1.Attack1Start);
            hairAttack1.DelayedAttack(0.53f, hairAttack1.Attack1Start);
            
            hairAttack1.DelayedAttack(0.53f * 2, hairAttack1.Attack3Start);
            //hairAttack1.DelayedAttack(0.53f * 0, hairAttack1.Attack3Start);
            hairAttack1.DelayedAttack(0.53f * 2 + 0.64f, hairAttack1.HandAuraOff);
            const float moveTime = 0.53f * 7;
            hairAttack1.DelayedAttack(moveTime, moveAction);
            const float backToBattlePose = 0.53f * 2 + 0.8f;
            skillHelper.AnimationTrigger("battlepose", backToBattlePose);

            //base.delayedMethodScaleByEnemyNumber(AttackNew);

        }
    }

    private void Attack2() {
        if (isActiveAndEnabled) {
            hairAttack1.BombAttackStart();
            //skillHelper.AnimationTrigger("battlepose", 0.53f * 2 + 0.8f);
            base.delayedMethodScaleByEnemyNumber(AttackNew);

        }
    }

    internal override void OnEnableSub() {

        base.delayedMethodScaleByEnemyNumberFullRandom(AttackNew);
    }

    internal override void AwakeSub() {

        //auxActionList.Add(Attack1);
        //auxActionList.Add(move1);
        //auxActionList.Add(Attack1);
        auxActionList.Add(move2);
        auxActionList.Add(Attack1);
        auxActionList.Add(Attack2);

        baseDelay = 5f;
        delayPerEnemy = 1.8f;
        moveAction = move1;
        maxDelay = 5f;
        variance = 0.2f;
        moveGoal = GetComponent<MovementToGoalEnemy>();
        moveGoal.WantedDistance = 12f;
        moveGoal.reachGoodDistance = MoveReach;
        var encounter = EncounterSingleton.Instance.EncounterManager.LastEncounter;
        positionDictionary = encounter.RelevantPointDictionary;
        hairAttack1 = gameObject.AddComponent<HairBAttack1>();
        holdAttack = gameObject.AddComponent<HairBHoldattack>();
        hairAttack1.BombLevel = bombAttackLevel;
        hairAttack1.LineLevel = shootAttackLevel;
        hairAttack1.Attack4Level = walkAttackLevel;
        var suicideAttack = gameObject.AddComponent<HairBAttack2>();
        suicideAttack.Source = sourceAtk1.gameObject;
        suicideAttack.HandBone = handBone;
        holdAttack.EndAttack += () => {
            //base.delayedMethodScaleByEnemyNumber(AttackNew);
            AttackNew();
        };
        holdAttack.Source = handBone.gameObject;

        hairAttack1.Source = sourceAtk1.gameObject;
        hairAttack1.SourceAtk3 = sourceAtk3.gameObject;
        hairAttack1.HandBone = handBone;
        base.delayedMethodScaleByEnemyNumberFullRandom(AttackNew);
        hpSystem = GetComponent<HPSystem>();
        hpSystem.HPChange += hpChange;
        dodgeBehav = GetComponent<DodgeBehavior>();
    }

    private void hpChange(HPSystem obj) {
        if (obj.RatioGoneBelow(0.65f)) {
            auxActionList.Clear();
            base.ResetAuxIndexToOne();
            auxActionList.Add(AttackHold);
            for (int i = 0; i < 2; i++) {
                auxActionList.Add(Attack1);
                auxActionList.Add(Attack1);
                auxActionList.Add(Attack1);
                auxActionList.Add(Attack2);
            }
        }
        if (obj.RatioGoneBelow(0.45f) && dodgeAndShootPhase) {
            auxActionList.Clear();
            base.ResetAuxIndexToOne();
            auxActionList.Add(Phase2);
            for (int i = 0; i < 35; i++) {
                auxActionList.Add(AttackShootSimple);
            }

            auxActionList.Add(Phase1);
            for (int i = 0; i < 2; i++) {
                auxActionList.Add(Attack1);
                auxActionList.Add(Attack1);
                auxActionList.Add(Attack1);
                auxActionList.Add(Attack2);
            }

            auxActionList.Add(AttackHold);

        }
    }

    private void AttackShootSimple() {
        var delay = 0.3f + RandomSpecial.randomFloat(1.5f);
        if (RandomSpecial.RandomBool(0.3f)) {
            delay = 0.29f;
        }
        skillHelper.DelayedMethod(delay, AttackNew);
        hairAttack1.AttackQuickShotStart();
    }

    private void Phase1() {
        AttackNew();
        dodgeBehav.WantToDodge = false;
    }

    private void Phase2() {
        AttackNew();
        dodgeBehav.WantToDodge = true;
    }

    private void AttackHold() {
        holdAttack.StartAttack();
    }

    private void MoveReach() {
        moveGoal.Target = null;
        skillHelper.SuperArmor(0f);
        moveGoal.StopMove();
        //skillHelper.AnimationTrigger("battlepose");
        AttackNew();
    }

    private void move1() {
        DoShootMove();
        moveGoal.Target = positionDictionary["pos1"];
        skillHelper.SuperArmor(10f);
        if (RandomSpecial.RandomBool()) {
            moveAction = move2;
        } else {
            moveAction = move3;
        }
    }

    private void DoShootMove() {
        if(!shootMove) return;
        if(walkAttackLevel > 1) skillHelper.DelayedMethod(hairAttack1.Attack4, 1f);
        skillHelper.DelayedMethod(hairAttack1.Attack4, 2f);
        
    }

    private void move2() {
        DoShootMove();
        skillHelper.SuperArmor(10f);
        moveGoal.Target = positionDictionary["pos2"];
        if (RandomSpecial.RandomBool()) {
            moveAction = move1;
        } else {
            moveAction = move3;
        }
    }

    private void move3() {
        DoShootMove();
        skillHelper.SuperArmor(10f);
        moveGoal.Target = positionDictionary["pos3"];
        if (RandomSpecial.RandomBool()) {
            moveAction = move2;
        } else {
            moveAction = move1;
        }
    }
}
