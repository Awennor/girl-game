﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class HairBHoldattack : SkillBehaviour {
    GameObject source;
    private GameObject energyFocused;
    private GameObject target;
    private Vector3 oldTargetPosition;
    private Transform oldTargetParent;
    const float animationEndTime = 6.958f;
    [SerializeField]
    private const int GoUpSpeed = 140;
    public float HoldBlastAfterImpact = 0.12f;
    public float TimeMovingForwardCamera = 0.17f;
    private SkillHelper skillHelperTarget;
    private bool holdingTarget;
    public float OffsetDuringZoom = 25;
    public Vector3 localPositionHoldTargetOffset = new Vector3(0, 0.0368f, 0.0205f);
    public Action EndAttack = ()=> { };

    public GameObject Source {
        get {
            return source;
        }

        set {
            source = value;
        }
    }

    public void StartAttack() {
        skillHelper.AnimationTrigger("attack2before");
        skillHelper.DelayedMethod(ThrowProj, 3f);
        skillHelper.AnimationTrigger("attack2", 2.9f);
        SoundSingleton.Instance.PlaySound("meatyloop");
        SoundSingleton.Instance.StopSoundDelay("meatyloop", 3f);
    }

    private void ThrowProj() {
        const int timeProjDisappear = 10;
        var proj = skillHelper.ProjAttackHomingFollow(ProjectilHolder.Projs.DarkBall1, SkillInfoProvider.SkillId.Bomb, timeProjDisappear, 28, source.transform.position);
        if(proj == null) return;
        SoundSingleton.Instance.PlaySound("projattack1");
        skillHelper.AnimationTrigger("battlepose", 0.3f);
        
        
        skillHelper.DelayedMethodUnique(AttackEnd, timeProjDisappear+4f);
        skillHelper.SpecialCollisionCase(proj, Collided);
    }

    void Collided(ProjectilData proj, GameObject collidedTarget) {
        if(this == null) return;
        if(collidedTarget == CentralResource.Instance.Hero && collidedTarget != CharacterControlledSystem.Instance.Controlled) {
            return;
        }
        EffectSingleton.Instance.ShowEffectHere("darkappear", collidedTarget.transform);
        EffectSingleton.Instance.ShowEffectHere("darkappear", source.transform);
        SoundSingleton.Instance.PlaySound("darkappear2");
        holdingTarget = true;
        
        skillHelper.FaceTargetNow();
        const float TimeToZoom3 = 5.1f;
        skillHelper.Zoom(0.8f, 2f, 1f);
        skillHelper.Zoom(0.6f, 1f, 4f);
        skillHelper.Zoom(0.4f, 1f, TimeToZoom3);
        //skillHelper.Zoom(0.4f, 0.2f, animationEndTime - 0.2f);
        float explosionTime = animationEndTime + HoldBlastAfterImpact;
        skillHelper.Zoom(1.2f, 0.1f, explosionTime);
        skillHelper.Zoom(1f, 2f, explosionTime+1f);
        skillHelper.DelayedMethod(HeroLockIn, TimeToZoom3);
        skillHelper.DelayedMethod(LockCamera, animationEndTime);
        skillHelper.DelayedMethod(CameraShake, animationEndTime);
        skillHelper.DelayedMethod(SlowDown, animationEndTime-0.0785f);
        skillHelper.DelayedMethod(normalTime, animationEndTime-0.0781f);
                   
        //skillHelper.DelayedMethod(MoveCameraABit, TimeToZoom3);
        //skillHelper.DelayedMethod(MoveCameraWithMove, animationEndTime - TimeMovingForwardCamera);

        if (proj != null) Destroy(proj.gameObject);
        target = collidedTarget;
        skillHelperTarget = target.GetComponent<SkillHelper>();
        skillHelperTarget.IgnoreCollisions();
        skillHelperTarget.AnimationTrigger("trappedup", animationEndTime-0.22f);
        oldTargetPosition = target.transform.position;
        //Debug.Log("OLD POS"+oldTargetPosition);
        oldTargetParent = target.transform.parent;
        collidedTarget.GetComponent<BattleStateMachine>().ChangeState(BattleStateMachine.States.Damage);
        collidedTarget.GetComponent<BattleStateMachine>().RemoveChangesToNormal();
        collidedTarget.transform.SetParent(source.transform);
        skillHelper.AnimationTrigger("holdattack");
        skillHelper.DelayedMethod(SummonProj, 2f);
        target.GetComponentInChildren<Animator>().SetTrigger("trapped");

        //skillHelper.DelayedMethod(LockCamera, 1.5f);
        const float holdDark = 0.5f;
        //CentralResource.Instance.FadeHolder.FadeOutAndFadeIn(animationEndTime, 0.1f, holdDark, 1f);
        skillHelper.DelayedMethod(ThrowUp, explosionTime);
        const float comeDownTime = animationEndTime + holdDark;
        //skillHelper.DelayedMethod(ComeDown, comeDownTime);
        skillHelper.DelayedMethod(returnCamera, explosionTime+1f);
        skillHelper.DelayedMethod(DealDamageToTarget, explosionTime + 2f);
        skillHelper.DelayedMethod(Impact, explosionTime);
        skillHelper.DelayedMethodUnique(AttackEnd, explosionTime+6.2f);
        target.transform.localRotation = Quaternion.identity;

        //collidedTarget.transform.localPosition = new Vector3(0,0,0);
    }

    private void normalTime() {
        Time.timeScale = 1;
    }

    private void SlowDown() {
        Time.timeScale = 0.005f;
    }

    private void CameraShake() {
        CentralResource.Instance.CameraControl.Shake(HoldBlastAfterImpact+1, new Vector3(5,5,0), 30, 50);
    }

    private void HeroLockIn() {
        //Time.timeScale = 0.24f;
        CentralResource.Instance.CameraControl.PauseFollowingTarget(false);
        CentralResource.Instance.CameraControl.SmoothFollow.AutomaticFollow = true;;
    }

    private void MoveCameraABit() {
        //Time.timeScale = 0.24f;
        CentralResource.Instance.CameraControl.Camera.transform.DOMove(Camera.main.transform.position-transform.forward*OffsetDuringZoom, 0.9f);
    }

    private void MoveCameraWithMove() {
        CentralResource.Instance.CameraControl.Camera.transform.DOMove(Camera.main.transform.position+transform.forward*OffsetDuringZoom*1.6f, TimeMovingForwardCamera);
    }

    private void returnCamera() {
        //Time.timeScale = 1f;
        CentralResource.Instance.CameraControl.PauseFollowingTarget(false);
        //CentralResource.Instance.CameraControl.BackToNormalTarget();
    }

    private void LockCamera() {
        CentralResource.Instance.CameraControl.PauseFollowingTarget(true);
        CentralResource.Instance.CameraControl.SmoothFollow.AutomaticFollow = false;;
        //CentralResource.Instance.CameraControl.ChangeTargetTemp(gameObject);
    }


    private void ThrowUp() {
        //target = null;
        holdingTarget = false;
        target.transform.SetParent(oldTargetParent);
        target.transform.rotation = Quaternion.identity;
        target.GetComponent<CinematicFall>().StartFall(new Vector3(0.1f, GoUpSpeed, 0.1f));
        //target.GetComponentInChildren<Animator>().SetTrigger("fallafter");
        //skillHelperTarget.MoveTowardsDirectionXZ(0, GoUpSpeed, 5f);
    }

    private void Impact() {
        SoundSingleton.Instance.PlaySound("explosion");
        SoundSingleton.Instance.PlaySound("hurthero");
        EffectSingleton.Instance.ShowEffectHere("bigblast", target.transform.position);
        skillHelper.AnimationTrigger("laugh", 1f);
        skillHelper.AnimationTrigger("battlepose", 4f);
        
    }

    private void AttackEnd() {
        EndAttack();
    }

    private void DealDamageToTarget() {
        skillHelper.DealDirectDamage(target, 20);

    }

    private void ReturnControl() {

        target.transform.position = oldTargetPosition + new Vector3(0, 5, 0); ;
        target.transform.SetParent(oldTargetParent);
        //target.transform.position = oldTargetPosition;
        target.GetComponent<BattleStateMachine>().ToNormal();
        target.transform.rotation = Quaternion.identity;
        target = null;
    }

    private void SummonProj() {

        SoundSingleton.Instance.PlaySound("summon1");
        var summonProjPos = transform.forward * 35.5f + transform.position + new Vector3(0, 4+18, 0);
        EffectSingleton.Instance.ShowEffectHere("darkappear", summonProjPos);
        
        var ef = EffectSingleton.Instance.ShowEffectHere("bigenergyball",summonProjPos);
        Destroy(ef, animationEndTime - 2f+HoldBlastAfterImpact);
        //obj.transform.position = summonProjPos;

    }

    // Use this for initialization
    void Start() {
        base.skillHelperInit();

    }

    // Update is called once per frame
    void Update() {
        if (target != null && holdingTarget) {
            target.transform.localRotation = Quaternion.identity;
            target.transform.localPosition = Vector3.zero + localPositionHoldTargetOffset;
        }
        if (Input.GetKeyDown(KeyCode.J)) {
            Collided(null, CentralResource.Instance.Hero);
        }
    }
}
