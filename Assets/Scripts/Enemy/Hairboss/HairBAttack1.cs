﻿using UnityEngine;
using System.Collections;
using System;

public class HairBAttack1 : SkillBehaviour {
    GameObject source;
    GameObject sourceAtk3;
    private Transform handBone;
    private GameObject energyFocused;


    public GameObject Source {
        get {
            return source;
        }

        set {
            source = value;
        }
    }


    public int BombLevel { get; set; }
    public int LineLevel { get; set; }
    public int Attack4Level { get; set; }

    public GameObject SourceAtk3 {
        get {
            return sourceAtk3;
        }

        set {
            sourceAtk3 = value;
        }
    }

    public Transform HandBone {
        get {
            return handBone;
        }

        set {
            handBone = value;
        }
    }



    // Use this for initialization
    void Start() {
        base.skillHelperInit();
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.HairShoot1, Attack1Start);
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.HairShoot2, Attack3Start);
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.HairBomb, BombAttackStart);
    }

    public void DelayedAttack(float v, Action a) {
        skillHelper.DelayedMethod(a, v);
    }

    private void Attack1() {
        //skillHelper.ScreenShakeType2(0.1f);

        //skillHelper.AnimationTrigger("battlepose", 0.1f);
        SoundSingleton.Instance.PlaySound("enemysetup2");

        Vector3 projSpawnPosition = Source.transform.position;
        //skillHelper.MoveTowardsDirectionXZ(-60, 0, 0.1f);
        var obj = skillHelper.ProjAttackHoming(
            ProjectilHolder.Projs.EnemyBallCool,
            SkillInfoProvider.SkillId.HairShoot1,
            15f,
            70f,
            projSpawnPosition
            );
        skillHelper.ImpactSound(obj, "impact4");
    }

    internal void HandAuraOff() {
        energyFocused.SetActive(false);
    }

    internal void HandAura() {
        if (energyFocused != null) {
            energyFocused.SetActive(true);
        } else
            energyFocused = EffectSingleton.Instance.ShowAndAddEffect("energyfocused", handBone.transform);
    }

    public void BombAttackStart() {

        //skillHelper.DelayedMethod(1f, BombAttackDo);
        DelayedAttack2(1f, "attack2before", "attack2", BombAttackDo);
        skillHelper.AnimationTrigger("battlepose", 1.5f);
        SoundSingleton.Instance.PlaySound("meatyloop");
        SoundSingleton.Instance.StopSoundDelay("meatyloop", 1f);
    }

    private void BombAttackDo() {
        var selfPosition = transform.position;
        EffectSingleton.Instance.ShowEffectHere("spell", handBone.transform);
        for (int i = 0; i < 3; i++) {
            int j = i;
            skillHelper.DelayedMethod(() => {
                SoundSingleton.Instance.PlaySound("summon1");
                Vector3 dis1 = new Vector3(45, 0);
                dis1 = Quaternion.Euler(0, j * 120, 0) * dis1;
                dis1.y = 5;
                float timeToExplode = 3f;
                var obj = skillHelper.GetProj(
                    ProjectilHolder.Projs.EnemyBall3,
                    SkillInfoProvider.SkillId.HairBomb,
                    timeToExplode);
                obj.transform.position = dis1 + selfPosition;
                var position = dis1 + selfPosition;
                EffectSingleton.Instance.ShowEffectHere("darkappear", position);
                skillHelper.DelayedMethod(() => {
                    SoundSingleton.Instance.PlaySound("summon2");

                    EffectSingleton.Instance.ShowEffectHere("spell", position);
                    int attacks = 20;
                    if (BombLevel == 1) {
                        attacks = 8;
                    }
                    for (int l = 0; l < attacks; l++) {
                        var obj2 = skillHelper.GetProj(
                            ProjectilHolder.Projs.EnemyBall2,
                            SkillInfoProvider.SkillId.HairBomb,
                            25f);
                        obj2.transform.position = position;
                        float angleVar = 360 / attacks;
                        skillHelper.ProjMoveTowardsTargetXZ
                            (obj2, 5, 0, -angleVar * 2 + l * angleVar);
                    }

                }, timeToExplode - 0.05f);

            }
            , i * 0.6f + 0.2f);


        }
    }

    public void Attack3Start() {
        DelayedAttack2(0.63f, "attack1before", "attack3", Attack3);

    }

    public void Attack4() {
        var target = skillHelper.ClosestTarget();
        if (target == null) return;
        Vector3 fwd = target.transform.forward;
        fwd.y = 0;
        SoundSingleton.Instance.PlaySound("projattack2");
        for (int i = 0; i < 3; i++) {
            var proj = skillHelper.GetProj(
                ProjectilHolder.Projs.EnemyBall1,
                SkillInfoProvider.SkillId.HairShoot1,
                10f
            );
            var distance = Quaternion.Euler(0, i * 130, 0) * fwd.normalized * 24;
            proj.transform.position = target.transform.position + distance + new Vector3(0, 5, 0);
            EffectSingleton.Instance.ShowEffectHere("redappear", proj.transform);
            float speed = 80;
            if (Attack4Level == 1) {
                speed *= 0.8f;
            }

            const float timeToHighSpeed = 0.9f;
            //skillHelper.ProjSpeed(-distance.normalized * speed*0.22f, proj);
            //skillHelper.ProjSpeed(-distance.normalized * speed, proj, 0, 10f);
            //skillHelper.ProjSpeed(-distance.normalized * speed, proj, 0.3f, 0.3f, DG.Tweening.Ease.InCubic);
            skillHelper.ProjSpeed(distance.normalized * speed * 0.3f, proj, 0.05f, timeToHighSpeed);
            skillHelper.ProjSpeed(-distance.normalized * speed, proj, timeToHighSpeed, 10f);
        }


    }

    public void Attack1Start() {
        const float timeToAttack = 0.43f;
        string animationTriggerBefore = "attack1before";
        string animationTriggerAfter = "attack1";
        Action attack = Attack1;
        DelayedAttack2(timeToAttack, animationTriggerBefore, animationTriggerAfter, attack);
    }

    public void AttackQuickShotStart() {
        //skillHelper.ScreenShakeType2(0.1f);
        DelayedAttack2(0.11f, "battlepose", "attack3", AttackQuickShot);

    }

    private void AttackQuickShot() {
        skillHelper.AnimationTrigger("battlepose", 0.1f);
        skillHelper.AnimationTrigger("attack3");
        SoundSingleton.Instance.PlaySound("enemysetup2");

        Vector3 projSpawnPosition = sourceAtk3.transform.position;
        //skillHelper.MoveTowardsDirectionXZ(-60, 0, 0.1f);
        skillHelper.VelocityYZeroAndFixRotation(skillHelper.ProjAttackHoming(
            ProjectilHolder.Projs.EnemyBallCool,
            SkillInfoProvider.SkillId.HairShoot1,
            15f,
            80f,
            projSpawnPosition
            ));
    }

    private void DelayedAttack2(float timeToAttack, string animationTriggerBefore, string animationTriggerAfter, Action attack) {
        skillHelper.DelayedMethod(timeToAttack, attack);
        skillHelper.FaceTarget(timeToAttack + 0.1f);
        skillHelper.AttackState(timeToAttack + 0.1f);
        skillHelper.SuperArmor(timeToAttack + 0.1f);
        skillHelper.AnimationTrigger(animationTriggerBefore);
        skillHelper.AnimationTrigger(animationTriggerAfter, timeToAttack - 0.1f);

    }


    private void Attack3() {
        //skillHelper.ScreenShakeType2(0.2f);
        for (int i = 0; i < 3; i++) {
            SoundSingleton.Instance.PlaySound("enemysetup2", 0.1f * i);
        }

        int shootLine = 10;
        if (LineLevel == 1) {
            shootLine = 4;
        }
        for (int i = 0; i < shootLine; i++) {

            Vector3 projSpawnPosition = sourceAtk3.transform.position;
            var obj = skillHelper.ProjAttackHoming(
                ProjectilHolder.Projs.EnemyBallCool,
                SkillInfoProvider.SkillId.HairShoot2,
                15f,
                (10 - i) * 7f + 7f,
                projSpawnPosition
                );
            skillHelper.VelocityYZeroAndFixRotation(obj);
            skillHelper.ImpactSound(obj, "impact4");
        }


    }



    // Update is called once per frame
    void Update() {

    }
}
