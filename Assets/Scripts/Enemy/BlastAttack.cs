﻿using UnityEngine;
using System.Collections;

public class BlastAttack : MonoBehaviour {
    private SkillHelper skill;

    // Use this for initialization
    void Start () {
        skill = GetComponent<SkillHelper>();
        GetComponent<AttackInput>().SetAttackMethod((int)SkillInfoProvider.SkillId.Explosion, 
            Blast);
        
    }

    public void Blast() {
        SoundSingleton.Instance.PlaySound("explosion");
        SoundSingleton.Instance.PlaySound("hurt1");
        skill.GetProj(ProjectilHolder.Projs.EnemyBlast, SkillInfoProvider.SkillId.Explosion, 0.2f).transform.position = transform.position;
        Destroy(gameObject);
    }
	
	// Update is called once per frame
	void Update () {
	    
	}
}
