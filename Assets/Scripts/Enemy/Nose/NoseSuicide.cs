﻿using UnityEngine;
using System.Collections;
using System;

public class NoseSuicide : SkillBehaviour {
    private SkillInfoProvider.SkillId skill;



    // Use this for initialization
    void Start() {
        base.skillHelperInit();
        //attackInput.SetAttackMethod(SkillInfoProvider.SkillId.SuicideNose, StartAttack);
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.SuicideBigNose, StartAttack);
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.SuicideNose, DoAttack);
        //attackInput.SetAttackMethod(SkillInfoProvider.SkillId.SuicideBigNose, DoAttack);
    }



    public void StartAttack() {
        skill = (SkillInfoProvider.SkillId) skillHelper.CurrentSkill;

        SoundSingleton.Instance.PlaySound("charge2.2s");
        string effectName = "charge2.2s";
        if(skill == SkillInfoProvider.SkillId.SuicideBigNose)
            effectName = "chargebig2.2s";
        EffectSingleton.Instance.ShowEffectHere(effectName, transform, new Vector3(0, 2, 0));
        skillHelper.DelayedMethod(2.2f, DoAttack);
        //skillHelper.DelayedMethod(DamageMethod, 2.25f);
        //skillHelper.DelayedMethod(1.3f,DoAttack);

        skillHelper.AttackState(2.6f);
        skillHelper.SuperArmor(2.6f);
        //DoAttack();
    }



    private void DoAttack() {
        skill = (SkillInfoProvider.SkillId) skillHelper.CurrentSkill;
        EffectSingleton.Instance.ShowEffectHere("hitparticle", transform, new Vector3(0, 2, 0));
        SoundSingleton.Instance.PlaySound("blast");
        skillHelper.AnimationTrigger("battlepose", 0.2f);
        if (skill == SkillInfoProvider.SkillId.SuicideNose) {
            int ProjNumbers = 7;
            for (int i = 0; i < ProjNumbers; i++) {
                
                GameObject obj = skillHelper.GetProj(ProjectilHolder.Projs.EnemyBall2, skill, 15f);
                obj.transform.localPosition += new Vector3(0, 2f, 0);
                var r = this.transform.rotation;
                obj.transform.rotation = r * Quaternion.Euler(0, (i) * 30 - 90, 0);
                skillHelper.SpeedForward(obj, 60);

                skillHelper.ProjCollideWithGround(obj);
            }
        }
        if (skill == SkillInfoProvider.SkillId.SuicideBigNose) {
            int ProjNumbers = 20;
            for (int i = 0; i < ProjNumbers; i++) {
                
                GameObject obj = skillHelper.GetProj(ProjectilHolder.Projs.EnemyBall1, skill, 15f);
                obj.transform.localPosition += new Vector3(0, 4f, 0);
                var r = this.transform.rotation;
                int i2 = i % ProjNumbers/2;
                obj.transform.rotation = r * Quaternion.Euler(0, (i2) * 36 - 180, 0);
                skillHelper.SpeedForward(obj, 30+i*10);

                skillHelper.ProjCollideWithGround(obj);
            }
        }
        skillHelper.DamageSelf(10f);

    }

    // Update is called once per frame
    void Update() {

    }
}
