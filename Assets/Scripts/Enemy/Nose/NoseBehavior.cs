﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;

public class NoseBehavior : EnemyBehaviourBase {
    private NoseAttacks noseAttack1;
    public float baseDelayScale = 1;
    public float projSpeedScale = 1;
    

    private void Attack() {
        if(isActiveAndEnabled) {
            noseAttack1.StartAttack();
            base.delayedMethodScaleByEnemyNumber(Attack);
            
        }
    }
    internal override void OnEnableSub() {
        
        base.delayedMethodScaleByEnemyNumberFullRandom(Attack);
    }

    internal override void AwakeSub() {
        baseDelay = 1.5f*baseDelayScale;
        delayPerEnemy = 1.8f;
        maxDelay = 5f;
        variance = 0.2f;
        noseAttack1 = gameObject.AddComponent<NoseAttacks>();
        noseAttack1.SpeedScale = projSpeedScale;

        base.delayedMethodScaleByEnemyNumberFullRandom(Attack);

    }
}
