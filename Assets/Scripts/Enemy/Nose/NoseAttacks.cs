﻿using UnityEngine;
using System.Collections;

public class NoseAttacks : SkillBehaviour {

	// Use this for initialization
	void Start () {
	    base.skillHelperInit();
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.NoseShootMini, StartAttack);
	}

    float speedScale = 1;

    public float SpeedScale {
        get {
            return speedScale;
        }

        set {
            speedScale = value;
        }
    }

    public void StartAttack() {
        SoundSingleton.Instance.PlaySound("charge1");
        skillHelper.AnimationTrigger("charge1");
        skillHelper.DelayedMethod(1f,DoAttack);
        skillHelper.DelayedMethod(1.3f,DoAttack);
        skillHelper.FaceTarget(1.1f);
        skillHelper.AttackState(1.3f);
        skillHelper.SuperArmor(1.3f);
        //DoAttack();
    }

    private void DoAttack() {
        SoundSingleton.Instance.PlaySound("enemysetup");
        skillHelper.AnimationTrigger("battlepose", 0.2f);
        for (int i = 0; i < 2; i++) {
            GameObject obj = skillHelper.GetProj(ProjectilHolder.Projs.EnemyBall2, SkillInfoProvider.SkillId.NoseShootMini, 15f);
            obj.transform.localPosition += new Vector3(0,2f,0);
            var r = this.transform.rotation;
            obj.transform.rotation = r * Quaternion.Euler(0, (i) * 8 - 4, 0);
            skillHelper.SpeedForward(obj, 32*speedScale);
            
            skillHelper.ProjCollideWithGround(obj);
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
