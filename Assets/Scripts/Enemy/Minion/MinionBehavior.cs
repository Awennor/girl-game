﻿using UnityEngine;
using System.Collections;

public class MinionBehavior : MonoBehaviour {
    private MinionClawAttack minionClawAttack;
    public Transform rightClaw;
    private MovementToTargetEnemy movement;
    bool wantToSlash;
    private MoveRandom moveRandom;
    private DodgeBehavior dodgeBehavior;
    private SkillHelper skillHelper;
    public float clawAttackTravelTimeMultiplicator = 1;
    public float clawAttackSensorScaleMultiplicator = 1;
    public float clawAttackTimeToAttackMultiplicator = 1;
    float timeToAttackRecharge = 5;


    // Use this for initialization
    void Start() {
        GetComponent<ControllableObject>().AddAntiInputScript(this);
        GetComponent<CharacterBattleHolder>().CharaCallbackOnce(SetupChara);
        moveRandom = GetComponent<MoveRandom>();
        dodgeBehavior = GetComponent<DodgeBehavior>();
        skillHelper = GetComponent<SkillHelper>();

        //SetupChara(chara);
    }

    private void SetupChara(CharacterBattle chara) {
        var atb = chara.Atb;
        minionClawAttack = gameObject.AddComponent<MinionClawAttack>();
        minionClawAttack.ProjScaleMultiplicator = clawAttackSensorScaleMultiplicator;
        minionClawAttack.TravelTime *= clawAttackTravelTimeMultiplicator;
        minionClawAttack.TimeToAttackMultiplicator = clawAttackTimeToAttackMultiplicator;
        minionClawAttack.AttackHolder = rightClaw;

        GetComponent<AvailableAttackListTempOld>().AddAttack(minionClawAttack.StartAttackCpu);
        GetComponent<FaceGameObject>().RotationSpeed = 7;
        movement = GetComponent<MovementToTargetEnemy>();
        movement.reachGoodDistance += () => {
            if (wantToSlash) {
                movement.enabled = false;
                minionClawAttack.StartAttackCpu();
                //atb.Empty();
                //moveRandom.enabled = true;
                skillHelper.DelayedMethodCancellable(dodgeBehavior.WantToDodgeOn, 2.5f);
                skillHelper.DelayedMethod(timeToAttackRecharge, AttackRecharge);
                

            }

        };
        skillHelper = GetComponent<SkillHelper>();
        skillHelper.DelayedMethod(timeToAttackRecharge/2, AttackRecharge);



    }

    void AttackRecharge() {
        if (this == null) return;
        if (!enabled) return;
        moveRandom.enabled = false;
        movement.enabled = true;
        wantToSlash = true;

        dodgeBehavior.WantToDodge = false;
    }


    // Update is called once per frame
    void Update() {

    }
}
