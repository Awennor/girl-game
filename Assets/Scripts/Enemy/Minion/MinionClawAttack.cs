﻿using UnityEngine;
using System.Collections;
using System;

public class MinionClawAttack : MonoBehaviour {
    private SkillHelper skillHelper;
    Transform attackHolder;
    private bool faceTargetOn = true;
    [SerializeField]
    float travelTime = 0.2f;
    [SerializeField]
    float projScaleMultiplicator = 1f;

    public Transform AttackHolder
    {
        get
        {
            return attackHolder;
        }

        set
        {
            attackHolder = value;
        }
    }

    public float ProjScaleMultiplicator {
        get {
            return projScaleMultiplicator;
        }

        set {
            projScaleMultiplicator = value;
        }
    }

    public float TravelTime {
        get {
            return travelTime;
        }

        set {
            travelTime = value;
        }
    }

    public float TimeToAttackMultiplicator {
        get {
            return timeToAttackMultiplicator;
        }

        set {
            timeToAttackMultiplicator = value;
        }
    }

    float timeToAttackMultiplicator = 1f;

    public void StartAttackCpu() {
        faceTargetOn =true;
        StartAttack();
    }

    private void StartAttack() {
        SoundSingleton.Instance.PlaySound("enemysetup");
        float timeToAtttack = 0.4f * TimeToAttackMultiplicator;
        skillHelper.SuperArmor(timeToAtttack + 0.3f);
        skillHelper.AttackState(timeToAtttack + 0.3f);
        skillHelper.BlockAttacking(timeToAtttack + 0.4f);
        
        if (faceTargetOn)
            skillHelper.FaceTarget(0.1f);
        skillHelper.AnimationTrigger("chargeslash");
        skillHelper.DelayedMethodCancellable(AttackPartA, timeToAtttack);
        skillHelper.StopMovement();
    }

    private void AttackPartA()
    {
        SoundSingleton.Instance.PlaySound("clawattack");
        skillHelper.FaceTarget(false);
        //int damage = SkillInfoProvider.Instance.GetBaseDamage(SkillInfoProvider.SkillId.Clawstrike);
        float timeToDisappear = TravelTime;
        GameObject obj = skillHelper.AttachSensor(SkillInfoProvider.SkillId.Clawstrike, attackHolder, timeToDisappear, ProjScaleMultiplicator*8);
        //obj.GetComponent<ProjectilData>();
        skillHelper.AnimationTrigger("attackslash");
        skillHelper.WeaponTrail(timeToDisappear);
        const int chaseSpeed = 160;
        const int speedY = 6;
        skillHelper.MoveTowardsDirectionXZ(chaseSpeed, speedY, TravelTime);
        //skillHelper.ZeroSpeed(timeToDisappear);
        skillHelper.AnimationTrigger("battlepose", 0.4f);
        
    }

    // Use this for initialization
    void Start () {
        skillHelper = GetComponent<SkillHelper>();
        var attackInput = GetComponent<AttackInput>();

        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.Clawstrike, StartAttackPlayer);
        /*for (int i = 0; i < 4; i++)
        {
            attackInput.SetAttackMethod(i, StartAttack);
        }
        */
    }

    private void StartAttackPlayer() {
        faceTargetOn = false; 
        StartAttack();
    }

    // Update is called once per frame
    void Update () {
	
	}
}
