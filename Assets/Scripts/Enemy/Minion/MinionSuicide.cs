﻿using UnityEngine;
using System.Collections;
using System;

public class MinionSuicide : MonoBehaviour {
    private SkillHelper skillHelper;
    Transform attackHolder;
    

    public Transform AttackHolder
    {
        get
        {
            return attackHolder;
        }

        set
        {
            attackHolder = value;
        }
    }

    

    public void StartAttackCpu() {
        StartAttack();
    }

    private void StartAttack() {
        float timeToAtttack = 1.5f;
        skillHelper.SuperArmor(2f);
        skillHelper.AttackState(2f);
        skillHelper.BlockAttacking(2f);
        
        skillHelper.AnimationTrigger("suicide");
        skillHelper.DelayedMethod(AttackPartA, timeToAtttack);
        skillHelper.StopMovement();
    }

    private void AttackPartA()
    {
        EffectSingleton.Instance.ShowEffectHere("hitparticle", transform, new Vector3(0, 2, 0));
        skillHelper.DamageSelf();
        
    }

    // Use this for initialization
    void Start () {
        skillHelper = GetComponent<SkillHelper>();
        var attackInput = GetComponent<AttackInput>();

        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.SuicideMinion, StartAttackPlayer);
        /*for (int i = 0; i < 4; i++)
        {
            attackInput.SetAttackMethod(i, StartAttack);
        }
        */
    }

    private void StartAttackPlayer() {
        StartAttack();
    }

    // Update is called once per frame
    void Update () {
	
	}
}
