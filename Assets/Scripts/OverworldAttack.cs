﻿using System;
using UnityEngine;

internal class OverworldAttack : MonoBehaviour {
    private SkillHelper skillHelper;

    void Start() {
        skillHelper = GetComponent<SkillHelper>();
        var attackI = GetComponent<AttackInput>();
        attackI.overworldAttackTry += overWorldDo;
    }


    private void overWorldDo() {
        if (!enabled) return;

        skillHelper.BlockAttacking(0.4f);
        skillHelper.AnimationTrigger("shootrelax");
        skillHelper.DelayedMethod(ProjAppear, 0.1f);
        skillHelper.AttackState(0.5f);
        skillHelper.ZeroSpeed();

    }

    private void ProjAppear() {
        SoundSingleton.Instance.PlaySoundOnPause("analyse");
        skillHelper.AnimationTrigger("normalslow", 0.2f);
        var proj = skillHelper.GetProj(ProjectilHolder.Projs.HeroExamine,
            SkillInfoProvider.SkillId.Vermishoot, 2.3f);
        proj.transform.rotation = this.transform.rotation;
        proj.transform.position += new Vector3(0, 4.9f, 0) + transform.forward * 0.2f;
        skillHelper.DamagesDirect(proj, 0, 0);

    }
}