﻿using UnityEngine;
using System.Collections;
using System;

public class TutoHelpSingleton {

    public Action<bool> ShowHeroUI = (b)=> { };
    public Action<bool> HPHelp = (b)=> { };
    public Action<bool> MovementShow = (b)=> { };
    public Action<bool> ATBHelp = (b)=> {};
    public Action<bool> CellHelp = (b)=> {};
    public Action<bool> MasterHelpOption = (b)=> {};
    public Action<bool> ExamineHelp = (b)=> {};
    public Action<bool> BattleHelpBeginner = (b)=> {};
    public Action<bool> OptionsHelp = (b)=> {};
    public Action<bool> DeckEdit = (b)=> {};

    internal void ExamineTipActive(bool v) {
        if(ExamineTip != null) {
            ExamineTip(v);
        }
    }

    public Action<bool> MasterHelpOptionAvailable = (b)=> {};
    public Action<bool> ExamineTip = (b)=> { };
    
    //public Action<bool> ShowHeroUI;

    static TutoHelpSingleton instance = new TutoHelpSingleton();

    static public TutoHelpSingleton Instance {
        get {
            return instance;
        }

        set {
            instance = value;
        }
    }
}
