﻿using UnityEngine;
using System.Collections;

public class TutorialTriggers : MonoBehaviour {

    public enum TutoTriggers {
        MovementShow, HPShow, HPHide, MoveHide, None,
        ActivateATBHelp, HideATBHelp, CellphoneHelp,
        MasterOn, MasterOff, MasterOptionOn, AttackHelp,
        MindControlUsable, HPHelpHide, MindControlTargetHelp,
        ExamineTip,
        MasterOptionOff, ExamineHelpOn, ExamineHelpOff,
        DeckEditOn, DeckEditOff, BattleHelpBeginnerOn,
        BattleHelpBeginnerOff, OptionHelp, 
    };

    public TutoTriggers[] triggers;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

     void OnTriggerExit(Collider other) {
        for (int i = 0; i < triggers.Length; i++) {
            switch (triggers[i]) {
                case TutoTriggers.OptionHelp:
                    TutoHelpSingleton.Instance.OptionsHelp(false);
                    break;
                default:
                    break;
            }
        }
    }

    void OnTriggerEnter(Collider other) {
        for (int i = 0; i < triggers.Length; i++) {
            switch (triggers[i]) {
                case TutoTriggers.OptionHelp:
                    TutoHelpSingleton.Instance.OptionsHelp(true);
                    break;
                case TutoTriggers.MovementShow:
                    TutoHelpSingleton.Instance.MovementShow(true);
                    break;
                case TutoTriggers.HPShow:
                    TutoHelpSingleton.Instance.ShowHeroUI(true);
                    triggers[i] = TutoTriggers.None;
                    break;
                case TutoTriggers.HPHide:
                    TutoHelpSingleton.Instance.ShowHeroUI(false);
                    triggers[i] = TutoTriggers.None;
                    break;
                case TutoTriggers.HPHelpHide:
                    TutoHelpSingleton.Instance.HPHelp(false);
                    triggers[i] = TutoTriggers.None;
                    break;
                case TutoTriggers.MoveHide:
                    TutoHelpSingleton.Instance.MovementShow(false);
                    triggers[i] = TutoTriggers.None;
                    break;
                case TutoTriggers.ActivateATBHelp:
                    TutoHelpSingleton.Instance.ATBHelp(true);
                    break;
                case TutoTriggers.DeckEditOn:
                    TutoHelpSingleton.Instance.DeckEdit(true);
                    break;
                case TutoTriggers.DeckEditOff:
                    TutoHelpSingleton.Instance.DeckEdit(false);
                    break;
                case TutoTriggers.HideATBHelp:
                    TutoHelpSingleton.Instance.ATBHelp(false);
                    break;
                case TutoTriggers.MasterOn:
                    TutoHelpSingleton.Instance.MasterHelpOptionAvailable(true);
                    break;
                case TutoTriggers.MasterOff:
                    TutoHelpSingleton.Instance.MasterHelpOptionAvailable(false);
                    triggers[i] = TutoTriggers.None;
                    break;
                case TutoTriggers.MasterOptionOn:
                    TutoHelpSingleton.Instance.MasterHelpOption(true);
                    break;
                case TutoTriggers.MasterOptionOff:
                    TutoHelpSingleton.Instance.MasterHelpOption(false);
                    break;
                case TutoTriggers.ExamineHelpOn:
                    TutoHelpSingleton.Instance.ExamineHelp(true);
                    break;
                case TutoTriggers.ExamineHelpOff:
                    TutoHelpSingleton.Instance.ExamineHelp(false);
                    break;
                case TutoTriggers.BattleHelpBeginnerOn:
                    TutoHelpSingleton.Instance.BattleHelpBeginner(true);
                    break;
                case TutoTriggers.BattleHelpBeginnerOff:
                    TutoHelpSingleton.Instance.BattleHelpBeginner(false);
                    break;
                default:
                    break;
            }
        }
    }
}
