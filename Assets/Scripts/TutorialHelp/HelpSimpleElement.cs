﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class HelpSimpleElement : MonoBehaviour {
    public TutorialTriggers.TutoTriggers tutoTriggers;
    public GameObject graphic;
    private bool atbWanted;
    private bool cellHelpWanted;
    private bool cellphoneUIVisible;
    private bool hideOnCellPhone = true;
    private bool visible;
    private bool masterVisible = false;
    [Tooltip("Weather or not to hide when all help is hidden")]
    public bool NotHelp =false;
    private Animator animator;
    bool firstAppearance = true;
    public bool alwaysAnimate = false;
    private bool fromCellphone;
    private bool lastVisible;
    private bool canMindControl;
    private bool HideOnMindControlPossible;
    private bool targettingSystemOn;
    private bool hideOnTargettingSystem = true;
    private bool onlyOnTargettingSystem = false;
    public float delayFirstAppearAnimation = -1;
    private bool delayedAppearance;
    private bool delayDone;

    void Start() {
        CentralResource.Instance.TargetChoosingSystem.targetOn += (b)=> {
            targettingSystemOn = b;
            refreshVisible();
        };
    }

    // Use this for initialization
    void Awake() {
        animator = GetComponent<Animator>();
        TutoHelpSingleton.Instance.MasterHelpOption += MasterSwitch;
        BattleSkillUI.Visible += cellphoneUI;
        MindControlTemp.CanMindControl += (b, mC)=> {  
            canMindControl = b;
            refreshVisible();
        };
        
        switch (tutoTriggers) {
            case TutorialTriggers.TutoTriggers.MovementShow:
            case TutorialTriggers.TutoTriggers.MoveHide:
                TutoHelpSingleton.Instance.MovementShow += Visible;
                break;
            case TutorialTriggers.TutoTriggers.HPShow:
            case TutorialTriggers.TutoTriggers.HPHide:
                TutoHelpSingleton.Instance.ShowHeroUI += Visible;
                TutoHelpSingleton.Instance.HPHelp += Visible;
                break;
            case TutorialTriggers.TutoTriggers.AttackHelp:
                CurrentSkillDisplay.skillDisplayShow += (id)=> {
                    Visible(id >= 0);
                };
                HideOnMindControlPossible = true;
                break;
            case TutorialTriggers.TutoTriggers.HideATBHelp:
            case TutorialTriggers.TutoTriggers.ActivateATBHelp:
                TutoHelpSingleton.Instance.ShowHeroUI += ATBHelpWant;
                CentralResource.Instance.BattleFlowSys.OnBattleStart += ()=>{
                    if(atbWanted) {
                        Visible(true);
                    }
                };
                CentralResource.Instance.BattleFlowSys.OnBattleEnd += ()=>{
                    Visible(false);
                };
                break;
            case TutorialTriggers.TutoTriggers.None:
                break;
            case TutorialTriggers.TutoTriggers.CellphoneHelp:
                hideOnCellPhone = false;
                TutoHelpSingleton.Instance.ShowHeroUI += CellhelpWant;
                BattleSkillUI.Visible += (b)=> {
                    Visible(b&& cellHelpWanted);
                };
                
                break;
            case TutorialTriggers.TutoTriggers.MasterOff:
            case TutorialTriggers.TutoTriggers.MasterOn:
                TutoHelpSingleton.Instance.MasterHelpOptionAvailable += Visible;
                break;
            case TutorialTriggers.TutoTriggers.MindControlUsable:
                MindControlTemp.CanMindControl += (b, mC)=> {
                    
                    Visible(b);
                };
                break;
            case TutorialTriggers.TutoTriggers.MindControlTargetHelp: {
                    onlyOnTargettingSystem = true;
                    hideOnTargettingSystem = false;
                    Visible(true);
                }
                break;
            case TutorialTriggers.TutoTriggers.ExamineTip: {
                    TutoHelpSingleton.Instance.ExamineTip += Visible;
                }
                break;
            default:
                break;
        }
    }

    private void MasterSwitch(bool obj) {
        masterVisible = obj;
        refreshVisible();
    }

    private void cellphoneUI(bool obj) {
        fromCellphone = true;
        cellphoneUIVisible = obj;
        refreshVisible();
    }

    private void CellhelpWant(bool obj) {
        cellHelpWanted = obj;
    }

    private void ATBHelpWant(bool obj) {
        atbWanted = obj;
    }

    private void Visible(bool b) {
        visible = b;
        refreshVisible();

    }

    private void refreshVisible() {
        if(firstAppearance && delayFirstAppearAnimation > 0 && !delayDone) {
            delayedAppearance = true;
        }

        bool visibleReal = this.visible;
        if (cellphoneUIVisible && hideOnCellPhone) {
            visibleReal = false;
        }

        if (onlyOnTargettingSystem && !targettingSystemOn) {
            visibleReal = false;
        }

        var visibleLocal = visibleReal && (masterVisible || NotHelp) 
            && (! canMindControl || !HideOnMindControlPossible) && 
            (!targettingSystemOn || !hideOnTargettingSystem);
        if(visibleLocal && delayedAppearance) {
            visibleLocal = false;
            DOVirtual.DelayedCall(delayFirstAppearAnimation, delayFirstAppearEnd);
        }
        if(visibleLocal && animator!= null && (firstAppearance || alwaysAnimate) &&!fromCellphone
            &&lastVisible==false) {
            animator.SetTrigger("appearanim");
            firstAppearance = false;
        }
        graphic.SetActive(visibleLocal);
        lastVisible = visibleLocal;
        fromCellphone = false;
        
    }

    private void delayFirstAppearEnd() {
        delayedAppearance = false;
        delayDone = true;
        refreshVisible();
    }

    // Update is called once per frame
    void Update() {

    }
}
