﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using FeatureMapGenerator.BaseGridGenerator2D;

public class CentralResourceFeeder : MonoBehaviour {

    public GameObject projectImpactGraphics;
    public CameraControl cameraControl;
    bool awaken;
    public GameObject hero;
    public LocalizationDataDefinition localizationData;
    public LocalizationDataDefine[] localizationDataNew;
    public Font[] fontLanguage;
    public TransitionOverlay overlay;


    void Awake() {
        //Debug.Log("AWAKENING OF RESOURCE");
        if (!awaken) {


            //remove this later
            /*var stage2DConfig = new WalkableGridConfig();
            stage2DConfig.Size = new PointInt2D(40, 40);
            stage2DConfig.Seed = 982131290;
            stage2DConfig.RoomMaxSize = new PointInt2D(6, 6);
            stage2DConfig.RoomMinSize = new PointInt2D(3, 3);
            stage2DConfig.CorridorMinSizeNorth = new PointInt2D(1, 4);
            stage2DConfig.CorridorMaxSizeNorth = new PointInt2D(1, 7);
            new WalkableGridGenerator().Generate(stage2DConfig);
            */
            //var dungeon = new Dungeon();
            awaken = true;
            //CentralResource.Instance.ProjectilImpactGraphics = projectImpactGraphics;
            CentralResource.Instance.SensorProjProvider = GetComponent<SensorProjProvider>();
            CentralResource.Instance.ProjectilHolder = GetComponent<ProjectilHolder>();
            CentralResource.Instance.CameraControl = cameraControl;
            CentralResource.Instance.UpdateSupport = GetComponent<UpdateSupport>();
            CentralResource.Instance.Hero = hero;
            CentralResource.Instance.FeedLocalizationDataOld(localizationData);
            CentralResource.Instance.FeedLocalizationData(localizationDataNew);
            CentralResource.Instance.LocalizationInfo.Fonts = fontLanguage;
            CentralResource.Instance.TransitionOverlay = overlay;
        }
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
