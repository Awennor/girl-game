﻿using UnityEngine;
using System.Collections;

public class AutomaticArray<T> {

    [SerializeField]
    T[] elements;
    public T this[int index]    // Indexer declaration
    {
        get {
            CheckSize(index+1);
            return elements[index];
        }

        set
        {
            CheckSize(index+1);
            elements[index] = value;
        }

        
    }

    private void CheckSize(int wantedSize) {
        if(elements == null) {
            elements = new T[wantedSize + 4];
            return;
        }
        if (elements.Length < wantedSize) {
            var temp = new T[wantedSize + 4];
            for (int i = 0; i < elements.Length; i++) {
                temp[i] = elements[i];
            }
            elements = temp;
        }
    }
}
