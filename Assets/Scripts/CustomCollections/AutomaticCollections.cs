﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MyIntArray : AutomaticArray<int>{}
[System.Serializable]
public class MyFloatArray : AutomaticArray<float>{}
[System.Serializable]
public class MyStringArray : AutomaticArray<string>{}
[System.Serializable]
public class MyGameObjectArray : AutomaticArray<GameObject>{}

[System.Serializable]
public class MyColorObjectArray : AutomaticArray<Color>{}
[System.Serializable]
public class MyBoolArray : AutomaticArray<bool>{}
