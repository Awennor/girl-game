﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class RobotMascot : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    public UnityEvent OnAnalyzeOn, OnAnalyzeOff;

    public void Appear() {
        EffectSingleton.Instance.ShowEffectHere("appeargood", transform);
        gameObject.SetActive(true);
    }

    public void Disappear() {
        EffectSingleton.Instance.ShowEffectHere("appeargood", transform);
        gameObject.SetActive(false);
    }

    public void AnalyzeState(bool state) {
        if(state) OnAnalyzeOn.Invoke();
        else OnAnalyzeOff.Invoke();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
