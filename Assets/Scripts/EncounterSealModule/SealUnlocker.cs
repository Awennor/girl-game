﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SealUnlocker : MonoBehaviour {

    public MyGameObjectEvent OnBindToSeal;
    public MyBoolEvent LineVisible;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    internal void BindSeal(Seal seal) {
        OnBindToSeal.Invoke(seal.gameObject);
    }

    internal void UnbindSeal(Seal seal) {
        OnBindToSeal.Invoke(null);
    }

    internal void VisibleConnection(Seal seal, bool visible) {
        LineVisible.Invoke(visible);
    }
}
