﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SealToEncounterManager : MonoBehaviour {

    public List<Seal> seals;
    public List<EncounterNameTrigger> encounters;

    public SealToEncounterManager() {

    }

    [ExposeMethodInEditor]
    private void AssignEncountersToSeal() {
        foreach (var seal in seals) {
            if (seal == null) continue;
            seal.ClearUnlockers();
            var sealPos = seal.transform.position;
            foreach (var encounter in encounters) {
                var enc_pos = encounter.transform.position;
                if ((sealPos - enc_pos).sqrMagnitude < seal.RadiusForUnlockers * seal.RadiusForUnlockers) {
                    seal.AddUnlocker(encounter.gameObject);
                }
            }
        }
    }

    // Use this for initialization
    void Start() {
        for (int i = 0; i < encounters.Count; i++) {
            var encounterNameTrigger = encounters[i];
            encounters[i].battleComplete.AddListener(() => {

                EncounterWin(encounterNameTrigger);
            });
        }
    }

    // Update is called once per frame
    void Update() {

    }

    public void AddEncounterTriggers(List<GameObject> list) {
        encounters.Clear();
        for (int i = 0; i < list.Count; i++) {
            var encounterNameTrigger = list[i].GetComponentInChildren<EncounterNameTrigger>();
            encounters.Add(encounterNameTrigger);
        }
        AssignEncountersToSeal();
    }

    private void EncounterWin(EncounterNameTrigger encounterNameTrigger) {
        for (int i = 0; i < seals.Count; i++) {
            if (seals[i].WaitingForUnlocking) {
                if (seals[i].IsUnlocker(encounterNameTrigger.gameObject)) {
                    seals[i].UnlockerUnlocking(encounterNameTrigger.gameObject);
                }
            }

        }
    }
}

