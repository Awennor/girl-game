﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seal : MonoBehaviour {

    [SerializeField]
    private float radiusForUnlockers;
    [SerializeField]
    List<SealUnlocker> currentUnlockers = new List<SealUnlocker>();

    bool unlocked;

    public UnityEventSequenceBehaviour OnUnlockSeal;

    public void OnDrawGizmosSelected() {
        Gizmos.color = new Color(0,1,1, 0.4f);
        Gizmos.DrawSphere(transform.position, RadiusForUnlockers);
    }

    public float RadiusForUnlockers {
        get {
            return radiusForUnlockers;
        }

        set {
            radiusForUnlockers = value;
        }
    }


    public bool WaitingForUnlocking { get {
            return !unlocked;
        } }

    internal void ClearUnlockers() {
        for (int i = 0; i < currentUnlockers.Count; i++) {
            if(currentUnlockers[i] != null)
                currentUnlockers[i].UnbindSeal(this);
        }
        currentUnlockers.Clear();
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    internal void AddUnlocker(GameObject gameObject) {
        var sealUnlocker = gameObject.GetComponent<SealUnlocker>();
        sealUnlocker.BindSeal(this);
        sealUnlocker.VisibleConnection(this, true);
        currentUnlockers.Add(sealUnlocker);
        
    }

    internal bool IsUnlocker(GameObject obj) {
        for (int i = 0; i < currentUnlockers.Count; i++) {
            if(obj == currentUnlockers[i].gameObject) {
                return true;
            }
        }
        return false;
    }

    internal void UnlockerUnlocking(GameObject obj) {
        this.gameObject.SetActive(false);
        OnUnlockSeal.Invoke();
        for (int i = 0; i < currentUnlockers.Count; i++) {
            if(currentUnlockers[i] != null)
                currentUnlockers[i].VisibleConnection(this, false);
        }
        unlocked = true;
    }
}
