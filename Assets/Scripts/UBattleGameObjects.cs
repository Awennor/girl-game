﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UBattleGameObjects {

    List<GameObject> enemies = new List<GameObject>();
    List<GameObject> heroes = new List<GameObject>();

    public List<GameObject> Enemies
    {
        get
        {
            return enemies;
        }

        set
        {
            enemies = value;
        }
    }

    public List<GameObject> Heroes
    {
        get
        {
            return heroes;
        }

        set
        {
            heroes = value;
        }
    }

    internal void AddHero(GameObject gameObject)
    {
        Heroes.Add(gameObject);
    }

    internal void AddEnemy(GameObject gameObject)
    {
        Enemies.Add(gameObject);
        //Debug.Log("ENEMY ADDED "+enemies.Count);
    }
}
