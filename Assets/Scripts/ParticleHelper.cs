﻿using UnityEngine;
using System.Collections;

public class ParticleHelper {

    public static GameObject SummonParticle(GameObject particlePrefab, GameObject target) {
        GameObject clone = (GameObject) Object.Instantiate(particlePrefab, target.transform.position, Quaternion.identity);
        clone.transform.SetParent(target.transform);

        return clone;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
