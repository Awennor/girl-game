﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class CurrentSkillDisplay : MonoBehaviour {

    public SkillButton skillButton;
    public SkillButtonsControl skillButtonControl;
    public Text textUse;
    private GameObject OldControlled;
    private ChosenSkillHolder chosenSkillHolder;
    public static Action<int> skillDisplayShow;
    public static Action<int, ChosenSkillHolder> skillDisplayShow2;
    public SkillInfoUI skillInfo;
    public CurrentSkillViewManager currentSkillView;

    void Awake() {
        CharacterControlledSystem.Instance.controlledCharaChange += (obj) =>
        {

            if (chosenSkillHolder != null) {
                chosenSkillHolder.skillChange -= SkillChange;
                chosenSkillHolder.usesChange -= usesChange;
            }
            
            chosenSkillHolder = obj.GetComponent<ChosenSkillHolder>();
            
            chosenSkillHolder.skillChange += SkillChange;
            chosenSkillHolder.usesChange += usesChange;
            
            if(skillButton != null)
                skillButton.GetComponent<UIFollowObj>().Target = obj;
            SkillChange(chosenSkillHolder);
            usesChange(chosenSkillHolder);
        };
    }

	// Use this for initialization
	void Start () {

        
	}

    private void usesChange(ChosenSkillHolder obj)
    {

        int use = obj.CurrentUses;
        skillInfo.SpecialColorBullets();
        int repetition = SkillInfoProvider.Instance.GetRepetition(skillInfo.Skill);
        //skillInfo.
        //Debug.Log(use+"USE");
        if(repetition > use && use >= 0) {
            currentSkillView.UseAnimation();
        }
        if (use <= 0)
            textUse.gameObject.SetActive(false);
        else {
            skillInfo.NormalColorBullets(use);
            textUse.gameObject.SetActive(true);
            textUse.text = "" + use;
        }
        
    }

    private void SkillChange(ChosenSkillHolder obj)
    {
        
        int s = obj.CurrentSkill;
        currentSkillView.ChangeSkillId(s);
        
        skillButton.SkillId = s;
        
        skillButtonControl.updateButtonIcon(skillButton);
        if(skillDisplayShow!= null) skillDisplayShow(s);
        if(skillDisplayShow2!= null) skillDisplayShow2(s, obj);

    }

    // Update is called once per frame
    void Update () {
	
	}
}
