﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class BridgeController : MonoBehaviour {

    int index = 0;
    private ParticleSystem bridgeAppear;
    private ParticleSystem bridgeDisappear;
    bool hiding;
    bool showing;
    private bool hidden;
    private bool showed = false;

    public void BridgeShow() {
        if(showed || showing) return;
        SoundSingleton.Instance.PlaySound("appear1");
        hidden =false;
        hiding = false;
        index = 0;
        gameObject.SetActive(true);
        bridgeAppear = EffectSingleton.Instance.ShowEffectHere("bridgeappear", transform).GetComponent<ParticleSystem>();
        int children = gameObject.transform.childCount;
        for (int i = 0; i < children; i++) {
            gameObject.transform.GetChild(i).gameObject.SetActive(false);
        }
        ShowOneChildCallNext();
        showing= true;
    }

    private void ShowOneChildCallNext() {
        if (index < gameObject.transform.childCount) {
            var childT = gameObject.transform.GetChild(index);
            childT.gameObject.SetActive(true);

            bridgeAppear.transform.position = childT.position + new Vector3(0, 5, 0);
            bridgeAppear.Emit(100);
            DOVirtual.DelayedCall(0.2f, ShowOneChildCallNext);
            index++;
        } else {
            showing = false;
            showed = true;
        }
    }

    private void HideOneChildCallNext() {
        if (index < gameObject.transform.childCount) {
            var childT = gameObject.transform.GetChild(index);
            

            bridgeDisappear.transform.position = childT.position + new Vector3(0, 5, 0);
            bridgeDisappear.Emit(100);
            childT.gameObject.SetActive(false);
            DOVirtual.DelayedCall(0f, HideOneChildCallNext);
            index++;
        } else {
            hiding = false;
            hidden = true;
        }
    }

    public void BridgeHide() {
        if(hiding || hidden) return;
        showed = false;
        showing = false;
        index = 0;
        //gameObject.SetActive(true);
        bridgeDisappear = EffectSingleton.Instance.ShowEffectHere("bridgedisappear", transform).GetComponent<ParticleSystem>();
        int children = gameObject.transform.childCount;
        
        HideOneChildCallNext();
        //gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
