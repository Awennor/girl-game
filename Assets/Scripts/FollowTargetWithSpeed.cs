﻿using UnityEngine;
using System.Collections;

public class FollowTargetWithSpeed : MonoBehaviour {

    [SerializeField]
    GameObject target;
    public float speedMax = 24f;
    new public Rigidbody rigidbody;
    public Vector3 offset;
    private Vector3 distance;
    public bool noTargetZeroSpeed = true;

    public GameObject Target {
        get {
            return target;
        }

        set {
            target = value;
        }
    }

    public float DistanceSquare {
        get {
            if (target != null)
                CalculateDistance();
            return distance.sqrMagnitude;
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (Target != null) {
            CalculateDistance();

            Vector3 speed = distance.normalized * speedMax;
            rigidbody.velocity = speed;
        } else {
            if (noTargetZeroSpeed)
                rigidbody.velocity = new Vector3(0, 0);
        }

    }

    private void CalculateDistance() {
        Vector3 pos = Target.transform.position + offset;
        distance = pos - transform.position;
    }
}
