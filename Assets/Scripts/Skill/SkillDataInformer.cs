﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillDataInformer : MonoBehaviour {

    
    [SerializeField]
    SkillAttackList[] skillAttackLists;
    //[SerializeField]
    //SkillDataUnit[] skillDataUnitHero1;
    [SerializeField]
    SkillInfoProvider.SkillId[] skillIdsHeroAccessible;
    [SerializeField]
    SkillUtilDataUnit[] skillUtilDataUnit;
    
    [SerializeField]
    SkillBuffDataUnit[] skillBuffDataUnit;

    List<SkillDataUnit> aux = new List<SkillDataUnit>();

    // Use this for initialization
    void Awake () {
        for (int i = 0; i < skillAttackLists.Length; i++) {
            var skills = skillAttackLists[i].Skills;
            aux.AddRange(skills);
        }
        SkillInfoProvider.Instance.Skills = aux.ToArray();
        SkillInfoProvider.Instance.SkillsUtil = skillUtilDataUnit;
        SkillInfoProvider.Instance.SkillIdHeroAccessible = skillIdsHeroAccessible;
        SkillInfoProvider.Instance.SkillsBuff = skillBuffDataUnit;
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

[System.Serializable]
class SkillAttackList {
    [SerializeField]
    string listName;
    [SerializeField]
    SkillDataUnit[] skills;

    internal SkillDataUnit[] Skills {
        get {
            return skills;
        }

        set {
            skills = value;
        }
    }
}
