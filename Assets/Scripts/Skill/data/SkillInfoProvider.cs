﻿using UnityEngine;
using System.Collections;
using System;

public class SkillInfoProvider  {

    SkillDataUnit[] skills;
    SkillUtilDataUnit[] skillsUtil;
    SkillBuffDataUnit[] skillsBuff;
    SkillId[] skillIdHeroAccessible;

    static SkillInfoProvider instance;
    public enum SkillId {
        FocusSphere, Slash, Braincontrol, Lightning, Heal,
        Clawstrike, Vermishoot, Explosion, Bomb, FocusSphereB,
        NoseShoot1, NoseShoot2, NoseShootMini, Laser, AttackRaise1,
        Energy1, Heal2, MindSphere, SuicideMinion, SuicideNose,
        SuicideBigNose, MindSlash, MindBomb, Shotgun, MindShotgun, 
        Multishoot, Multishootmind, HeroExplosion, MindHExplosion, BombB,
        LightningB, DefenseUp1, DefenseUp2, AttackUp1, AttackUp2,
        DashSlash, DeckReset, HairShoot1, HairShoot2, HairBomb,
        HairSuicide, FocusSphereZero, SpearAttack1, SpearAttack2,
        SpearAttack3, SpearProj,
        
    };




    internal int GetRepetition(int currentSkill)
    {
        var skillDataUnit = GetSkillData(currentSkill);
        if(skillDataUnit ==null) return 1;
        return skillDataUnit.Repetitions;
    }

    internal bool IsBattleSkill(SkillId skill) {
        return GetSkillData(skill)!= null;
    }

    internal bool IsUtilSkill(SkillId skill) {
        return GetSkillUtilData(skill)!= null;
    }

    

    public string GetName(SkillId skillid) {
        
        var skillid1 = (int)skillid;
        var text = CentralResource.Instance.LocalizationInfo.GetText((int)LocalizationEnum.SKILLNAME0 + skillid1);
        if(text == null) {
            return "NONAME";
        }
        return text;
    }

    public int GetRepetition(SkillId skillid)
    {

        var skillDataUnit = GetSkillData(skillid);
        if(skillDataUnit == null) return -1;
        return skillDataUnit.Repetitions;
        
    }

    public int GetBaseDamage(SkillId skillid)
    {
        return GetSkillData(skillid).BaseDamage;
    }

    private SkillDataUnit GetSkillData(SkillId skillid) {
        for (int i = 0; i < skills.Length; i++) {
            if(skills[i].EnumV == skillid) {
                return skills[i];
            }
        }
        return null;
    }

    public SkillUtilDataUnit GetSkillUtilData(SkillId skillid) {
        for (int i = 0; i < SkillsUtil.Length; i++) {
            if(SkillsUtil[i].EnumV == skillid) {
                return SkillsUtil[i];
            }
        }
        return null;
    }

    public SkillBuffDataUnit GetSkillBuffData(int skillid) {
        for (int i = 0; i < skillsBuff.Length; i++) {
            if(skillid == (int) skillsBuff[i].EnumV) {
                return skillsBuff[i];
            }
        }
        return null;
    }

    private SkillDataUnit GetSkillData(int skillInt) {
        return GetSkillData((SkillId) skillInt);
    }

     public SkillUtilDataUnit GetSkillUtilData(int skillInt) {
        return GetSkillUtilData((SkillId) skillInt);
    }

    public int GetEnergyCost(SkillId skillid)
    {
        var skillDataUnit = GetSkillData(skillid);
        if(skillDataUnit!=null)
            return skillDataUnit.Cost;
        var skillDataUnitUtil = GetSkillUtilData(skillid);
        if(skillDataUnitUtil!=null) {
            return skillDataUnitUtil.Cost;
        }
        var sBuff = GetSkillBuffData((int)skillid);
        if(sBuff != null)
            return sBuff.Cost;
        return -1;
    }

    public int GetEnergyCost(int skillid)
    {
        var skillDataUnit = GetSkillData(skillid);
        if(skillDataUnit!=null)
            return skillDataUnit.Cost;
        var skillDataUnitUtil = GetSkillUtilData(skillid);
        if(skillDataUnitUtil!=null) {
            return skillDataUnitUtil.Cost;
        }
        var buff = GetSkillBuffData(skillid);
        if(buff != null) {
            return buff.Cost;
        }
        return -10;
    }

    public int GetMindDamage(SkillId skillid)
    {
        return GetSkillData(skillid).MindDamage;
    }

    static public SkillInfoProvider Instance
    {
        get
        {
            if (instance == null) instance = new SkillInfoProvider();
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    internal SkillDataUnit[] Skills
    {

        set
        {
            skills = value;
        }
    }



    public int NumberOfSkills {
        get {
            return skills.Length;
        }
    }

    public int NumberOfSkillsHeroThisBuild {
        get {
            return SkillIdHeroAccessible.Length;
        }
    }

    public SkillId[] SkillIdHeroAccessible {
        get {
            return skillIdHeroAccessible;
        }

        set {
            skillIdHeroAccessible = value;
        }
    }

    public SkillUtilDataUnit[] SkillsUtil {
        get {
            return skillsUtil;
        }

        set {
            skillsUtil = value;
        }
    }

    public SkillBuffDataUnit[] SkillsBuff {
        get {
            return skillsBuff;
        }

        set {
            skillsBuff = value;
        }
    }





    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
