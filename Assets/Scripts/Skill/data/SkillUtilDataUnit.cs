﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class SkillUtilDataUnit : IComparable
{
    [SerializeField]
    private SkillInfoProvider.SkillId enumV;

    [SerializeField]
    private int cost;

    [SerializeField]
    private int drawAmount;
    [SerializeField]
    private int energyAmount;
    [SerializeField]
    bool deckRefresh;

    public int Cost
    {
        get
        {
            return cost;
        }

        set
        {
            cost = value;
        }
    }

    public SkillInfoProvider.SkillId EnumV
    {
        get
        {
            return enumV;
        }

        set
        {
            enumV = value;
        }
    }

    public int DrawAmount {
        get {
            return drawAmount;
        }

        set {
            drawAmount = value;
        }
    }

    public int EnergyAmount {
        get {
            return energyAmount;
        }

        set {
            energyAmount = value;
        }
    }

    public bool DeckRefresh {
        get {
            return deckRefresh;
        }

        set {
            deckRefresh = value;
        }
    }

    public int CompareTo(object obj) {
        SkillUtilDataUnit sdI = (SkillUtilDataUnit) obj;
        if (sdI.enumV > this.enumV) {
            return 1;
        }
        if (sdI.enumV > this.enumV)
        {
            return -1;
        }
        return 0;
    }
}

