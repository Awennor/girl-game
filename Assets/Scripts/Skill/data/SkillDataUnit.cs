﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
class SkillDataUnit : IComparable
{
    [SerializeField]
    private SkillInfoProvider.SkillId enumV;
    [SerializeField]
    private int baseDamage;
    [SerializeField]
    private int mindDamage;
    [SerializeField]
    private int cost;
    [SerializeField]
    private int repetitions;

    public int BaseDamage
    {
        get
        {
            return baseDamage;
        }

        set
        {
            baseDamage = value;
        }
    }

    public int MindDamage
    {
        get
        {
            return mindDamage;
        }

        set
        {
            mindDamage = value;
        }
    }

    public int Cost
    {
        get
        {
            return cost;
        }

        set
        {
            cost = value;
        }
    }

    public int Repetitions
    {
        get
        {
            return repetitions;
        }

        set
        {
            repetitions = value;
        }
    }

    public SkillInfoProvider.SkillId EnumV
    {
        get
        {
            return enumV;
        }

        set
        {
            enumV = value;
        }
    }

    public int CompareTo(object obj)
    {
        SkillDataUnit sdI = (SkillDataUnit) obj;
        if (sdI.enumV > this.enumV) {
            return 1;
        }
        if (sdI.enumV > this.enumV)
        {
            return -1;
        }
        return 0;
    }
}

