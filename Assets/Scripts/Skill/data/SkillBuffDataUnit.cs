﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class SkillBuffDataUnit : IComparable
{
    [SerializeField]
    private SkillInfoProvider.SkillId enumV;

    [SerializeField]
    private int cost;

    [SerializeField]
    ActorStats.Buff buff;

    int durationTurn = -1;
    

    public int Cost
    {
        get
        {
            return cost;
        }

        set
        {
            cost = value;
        }
    }

    public SkillInfoProvider.SkillId EnumV
    {
        get
        {
            return enumV;
        }

        set
        {
            enumV = value;
        }
    }

    public ActorStats.Buff Buff {
        get {
            return buff;
        }

        set {
            buff = value;
        }
    }

    public int CompareTo(object obj) {
        SkillBuffDataUnit sdI = (SkillBuffDataUnit) obj;
        if (sdI.enumV > this.enumV) {
            return 1;
        }
        if (sdI.enumV > this.enumV)
        {
            return -1;
        }
        return 0;
    }
}

