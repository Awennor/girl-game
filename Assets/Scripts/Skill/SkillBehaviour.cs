﻿using UnityEngine;
using System.Collections;
using System;

public class SkillBehaviour : MonoBehaviour {
    protected SkillHelper skillHelper;
    protected AttackInput attackInput;


    internal void skillHelperInit() {
        skillHelper = GetComponent<SkillHelper>();
        attackInput = GetComponent<AttackInput>();
    }

}
