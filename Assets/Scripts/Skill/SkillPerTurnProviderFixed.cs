﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Assets.Scripts.LogicScript;

public class SkillPerTurnProviderFixed : MonoBehaviour {

    public List<SkillInfoProvider.SkillId> everyTurnSkills = new List<SkillInfoProvider.SkillId>();
    public List<SkillInfoProvider.SkillId> everyTurnSkillsSP = new List<SkillInfoProvider.SkillId> {
        //SkillInfoProvider.SkillId.Braincontrol
    };
    public SkillInfoProvider.SkillId subSkill;
    public bool subSkillHas = true;
    private ATBManager atb;
    private ChosenSkillHolder chosenSkills;

    // Use this for initialization
    void Start () {
	    GetComponent<CharacterBattleHolder>().CharaCallbackOnce(CharaSetup);
        chosenSkills = GetComponent<ChosenSkillHolder>();
	}

    private void CharaSetup(CharacterBattle obj) {
        atb = obj.Atb;
        atb.ATBFull += ATBFull;
    }

    private void ATBFull() {
        if(this == null) return;
        
        //Debug.Log("NO CONTROL");
        if(CharacterControlledSystem.Instance.Controlled != gameObject)
            return;
        atb.Empty();
        //Debug.Log("ATB FULL SKILL ADDED");
        chosenSkills.Clear();
        for (int i = 0; i < everyTurnSkills.Count; i++) {
            chosenSkills.AddSkill((int)everyTurnSkills[i]);
        }
        if(subSkillHas) chosenSkills.SubSkill = (int) subSkill;
        //Debug.Log("SUBSKILL "+subSkill);
        
    }

    // Update is called once per frame
    void Update () {
	
	}
}
