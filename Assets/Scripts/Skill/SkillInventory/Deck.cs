﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[System.Serializable]
public class Deck {



    List<int> skillIds = new List<int>();


    public List<int> SkillIds {
        get {
            return skillIds;
        }

        set {
            skillIds = value;
        }
    }

    public void AddCard(int s) {
        SkillIds.Add(s);
    }

    public void InsertCard(int sid, int pos) {
        SkillIds.Insert(pos, sid);
    }

    internal void RemovePosition(int gId) {
        SkillIds.RemoveAt(gId);
    }

    internal void EmptyDeck() {
        skillIds.Clear();
    }

    internal int AmountOf(int skillId) {
        int amount = 0;
        for (int i = 0; i < skillIds.Count; i++) {
            if(skillIds[i]==skillId) {
                amount++;
            }
        }
        return amount;
    }

    internal void CopyDeck(Deck currentDeck) {
        this.skillIds.Clear();
        this.skillIds.AddRange(currentDeck.skillIds);
    }

    internal void FillSkillIdsVisible(List<int> skillIdsVisibleDeck) {
        skillIdsVisibleDeck.Clear();
        for (int i = 0; i < SkillIds.Count; i++) {
            if(!skillIdsVisibleDeck.Contains(skillIds[i])) {
                skillIdsVisibleDeck.Add(skillIds[i]);
            }
        }
    }

    internal void RemoveSkillIdFromEnd(int skillId) {
        for (int i = skillIds.Count-1; i >= 0; i--) {
            if(skillIds[i] == skillId) {
                skillIds.RemoveAt(i);
                return;
            }
        }
    }
}
