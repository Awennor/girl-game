﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SkillInventoryDeckManagement {

    SkillInvenDeckData skillInvenDeckData;
    public static int MAXCARDSDECK = 30;
    public static int MAXSINGLECARDDECK = 5;
    Deck deckTemp = new Deck();

    bool canEditDeck = true;

    internal void AddToInventory(SkillInfoProvider.SkillId skillId) {
        skillInvenDeckData.SkillInventory.AddAmount((int)skillId, 1);
    }

    internal void ClearCurrentDeck() {
        skillInvenDeckData.CurrentDeck.EmptyDeck();
    }

    public Action <SkillInventoryDeckManagement> DeckChanged;

    public SkillInventoryDeckManagement() {
        //InitDeckAndIventory();
        //*/
        //AddIvenAndDeck(SkillInfoProvider.SkillId.AttackUp1, 1);
        //AddIvenAndDeck(SkillInfoProvider.SkillId.FocusSphere, 1);
        //AddIvenAndDeck(SkillInfoProvider.SkillId.Energy1, 1);
        //skillInventory.AddAmount((int)SkillInfoProvider.SkillId.Energy1, 4);

    }

    private void InitDeckAndIventory() {
        var skillInventory = SkillInvenDeckData.SkillInventory;

        var decks = skillInvenDeckData.GetDecks();
        for (int i = 0; i < decks.Length; i++) {
            decks[i] = new Deck();
        }
        //var deck = skillInvenDeckData.CurrentDeck;
        ///*
        AddStarterInvenAndDeck();
    }

    public void AddStarterInvenAndDeck() {
        skillInvenDeckData.CurrentDeck.EmptyDeck();
        AddIvenAndDeck(SkillInfoProvider.SkillId.FocusSphere, 3);
        AddIvenAndDeck(SkillInfoProvider.SkillId.Slash, 3);
        AddIvenAndDeck(SkillInfoProvider.SkillId.Heal, 5);
        //AddIvenAndDeck(SkillInfoProvider.SkillId.Heal2, 2);
        AddIvenAndDeck(SkillInfoProvider.SkillId.Lightning, 2);
        AddIvenAndDeck(SkillInfoProvider.SkillId.Bomb, 2);
        AddIvenAndDeck(SkillInfoProvider.SkillId.FocusSphereB, 3);
        AddIvenAndDeck(SkillInfoProvider.SkillId.Laser, 1);
        AddIvenAndDeck(SkillInfoProvider.SkillId.Energy1, 1);
        AddIvenAndDeck(SkillInfoProvider.SkillId.MindSphere, 4);
        AddIvenAndDeck(SkillInfoProvider.SkillId.MindSlash, 3);
        AddIvenAndDeck(SkillInfoProvider.SkillId.MindBomb, 1);
        AddIvenAndDeck(SkillInfoProvider.SkillId.AttackRaise1, 2);
    }

    internal void FillSkillIdsVisibleOnTempDeck(List<int> skillIdsVisibleDeck) {
        deckTemp.FillSkillIdsVisible(skillIdsVisibleDeck);
    }

    internal void CopyTempToCurrent() {
        skillInvenDeckData.CurrentDeck.CopyDeck(deckTemp);
        AlertDeckChanged();
    }

    public void AlertDeckChanged() {
        if(DeckChanged!= null) {
            DeckChanged(this);
        }
    }

    internal void DeckTempBecomeCurrentContent() {
        deckTemp.CopyDeck(skillInvenDeckData.CurrentDeck);
    }

    private void AddIvenAndDeck(SkillInfoProvider.SkillId sId, int amount) {
        var sId1 = (int)sId;
        skillInvenDeckData.SkillInventory.AddAmount(sId1, amount);
        for (int i = 0; i < amount; i++) {
            AddToCurrentDeck(sId1);
        }
    }

    internal SkillInvenDeckData SkillInvenDeckData {
        get {
            return skillInvenDeckData;
        }

        set {
            skillInvenDeckData = value;
            if(skillInvenDeckData.IsEmpty()) {
                InitDeckAndIventory();
            }
        }
    }

    public bool CanEditDeck {
        get {
            return canEditDeck;
        }

        set {
            canEditDeck = value;
        }
    }

    internal void AddToCurrentDeck(int skillId) {
        skillInvenDeckData.CurrentDeck.AddCard(skillId);
    }

    internal void AddToTempDeck(int skillId) {
        deckTemp.AddCard(skillId);
    }

    internal void InsertOnTempDeck(int skillId, int pos) {
        deckTemp.InsertCard(skillId, pos);
    }

    internal List<int> GetDeckTempContent() {
        return deckTemp.SkillIds;
    }

    internal List<int> GetCurrentDeckContent() {
        return skillInvenDeckData.CurrentDeck.SkillIds;
    }



    internal void RemoveSkillDeckTempPosition(int gId) {
        deckTemp.RemovePosition(gId);
    }

    internal void RemoveSkillIdDeckTempFromEnd(int skillId) {
        deckTemp.RemoveSkillIdFromEnd(skillId);
    }

    internal int AmountInTempDeck(int skillId) {
        return deckTemp.AmountOf(skillId);
    }

    internal bool DeckTempFull() {
        return deckTemp.SkillIds.Count >= MAXCARDSDECK;

    }

    internal int DeckTempAmount() {
        return deckTemp.SkillIds.Count;

    }
}
