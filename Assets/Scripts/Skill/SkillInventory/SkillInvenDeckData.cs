﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

[System.Serializable]
public class SkillInvenDeckData{

    //this class has to be persisted

    public Deck[] decks = new Deck[10];
    public SkillInventory skillInventory = new SkillInventory();
    int currentDeck;

    [XmlIgnore]
    public Deck CurrentDeck {
        get {
            return decks[currentDeck];
        }
    }



    internal SkillInventory SkillInventory {
        get {
            return skillInventory;
        }

        set {
            skillInventory = value;
        }
    }

    internal bool IsEmpty() {
        return skillInventory.IsEmpty();
    }

    internal Deck[] GetDecks() {
        return decks;
    }
}
