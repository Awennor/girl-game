﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class SkillInventory {
    List<int> skillQuantities = new List<int>();

    public List<int> SkillQuantities {
        get {
            return skillQuantities;
        }

        set {
            skillQuantities = value;
        }
    }

    internal void SetAmount(int skillId, int quantity) {
        CheckLimit(skillId);
        SkillQuantities[skillId] = quantity;
    }

    private void CheckLimit(int skillId) {
        while(SkillQuantities.Count <= skillId) {
            SkillQuantities.Add(0);
        }
    }

    internal int AmountOf(int skillId) {
        if(skillId < SkillQuantities.Count) {
            return SkillQuantities[skillId];
        } else {
            return 0;
        }
    }

    internal void AddAmount(int sId, int amount) {
        CheckLimit(sId);
        SkillQuantities[sId] += amount;
    }

    internal bool IsEmpty() {
        for (int i = 0; i < SkillQuantities.Count; i++) {
            if(SkillQuantities[i]>0) return false;
        }
        return true;
    }
}
