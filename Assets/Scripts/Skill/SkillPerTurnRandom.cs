﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SkillPerTurnRandom : SkillPerTurnProvider {

    public List<SkillInfoProvider.SkillId> everyTurnSkills = new List<SkillInfoProvider.SkillId>();
    public int amountPerTurn;

    protected override void FillUpSkillListNewTurn() {
        skillList.Clear();
        for (int i = 0; i < amountPerTurn; i++) {
            skillList.Add((int)everyTurnSkills[UnityEngine.Random.Range(0, everyTurnSkills.Count)]);
            
        }
        
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
