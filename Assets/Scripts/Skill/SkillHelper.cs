﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;
using DG.Tweening;

public class SkillHelper : MonoBehaviour {
    private Animator animator;
    Invoker invoker;
    private ClosestTarget closestTarget;
    private WeaponTrailManager weaponTrailManager;


    String heroProjTag = "HeroProj";
    String enemyProjTag = "EnemyProj";
    String usedTag = "EnemyProj";
    private FaceGameObject faceObj;
    ActorStats actorStats;

    internal void SuperArmor(float v) {
        superArmor.Activate(v);
    }

    internal void SpecialCollisionCase(GameObject proj, Action<ProjectilData, GameObject> collided) {
        proj.GetComponent<ProjectilData>().SpecialCollisionCase = collided;
    }

    internal void SpeedForward(GameObject obj, float v) {
        obj.GetComponent<Rigidbody>().velocity = obj.transform.rotation * Vector3.forward * v;
    }

    internal void ApplyBuffBattle(int skillId) {
        var buff = SkillInfoProvider.Instance.GetSkillBuffData(skillId);
        actorStats.NewBattleBuff(buff.Buff);
    }

    internal GameObject ProjAttackHoming(
        ProjectilHolder.Projs projId,
        SkillInfoProvider.SkillId skillId,
        float timeToDisappear,
        float speed,
        Vector3 projSpawnPosition
        ) {
        var closeTarget = ClosestTarget();
        Vector3 tPos = new Vector3();
        if (closeTarget != null) tPos = closeTarget.transform.position;
        GameObject proj = GetProj(projId, skillId, timeToDisappear);

        proj.transform.position = projSpawnPosition;


        var vectorDirection = (tPos - proj.transform.position).normalized;
        //FaceTarget
        proj.GetComponent<Rigidbody>().velocity =
            vectorDirection * speed;
        proj.transform.rotation = Quaternion.LookRotation(vectorDirection);
        return proj;
        //proj.transform.rotation = rotation;
        //SpeedForward(proj, speed);

    }

    internal void ReverseTagProj(float delay, GameObject proj) {
        DOVirtual.DelayedCall(delay, () => {
            if (sideHero) {

                proj.layer = LayersNumber.PROJENEMYSIMPLE;
                proj.tag = enemyProjTag;
            } else {
                proj.layer = LayersNumber.PROJHEROSIMPLE;
                proj.tag = heroProjTag;
            }
        });
    }

    internal void ProjMoveTo(GameObject obj, Vector3 position, float speed, float delay) {
        obj.GetComponent<Rigidbody>().DOMove(position, speed).SetSpeedBased(true).SetDelay(delay);
    }

    internal GameObject ProjAttackHomingFollow(
    ProjectilHolder.Projs projId,
    SkillInfoProvider.SkillId skillId,
    float timeToDisappear,
    float speed,
    Vector3 projSpawnPosition
    ) {
        var closestTarget = ClosestTarget();
        if (closestTarget == null) return null;
        GameObject proj = GetProj(projId, skillId, timeToDisappear);
        proj.transform.position = projSpawnPosition;


        Vector3 tPos = closestTarget.transform.position;
        proj.GetComponent<Rigidbody>().velocity =
            (tPos - proj.transform.position).normalized * speed;
        var followTargetWithSpeed = proj.GetComponent<FollowTargetWithSpeed>();
        followTargetWithSpeed.Target = closestTarget;
        followTargetWithSpeed.speedMax = speed;
        followTargetWithSpeed.enabled = true;
        return proj;
        //proj.transform.rotation = rotation;
        //SpeedForward(proj, speed);

    }



    internal void DealDamage(GameObject target, SkillInfoProvider.SkillId skillId) {
        var damage = SkillInfoProvider.Instance.GetBaseDamage(GetCurrentSkill());
        target.GetComponent<HPSystem>().DealDamage(damage);
        DamageActions.DamageHappens(damage, target);

    }

    internal void DealDirectDamage(GameObject target, float damageBase) {
        var damage = damageBase;
        damage *= actorStats.GetAttackMultiplier();
        target.GetComponent<HPSystem>().DealDamage(damage);
        DamageActions.DamageHappens(damage, target);

    }

    internal void AddImpactProj(GameObject obj, GameObject obj2) {
        obj.GetComponent<ProjectilData>().ActivateOnImpact.Add(obj2);
    }

    private Rigidbody rigidBody;

    internal void HealSelf() {
        HealSelf(GetCurrentSkill());
    }

    internal void VelocityYZeroAndFixRotation(GameObject gameObject) {
        var rB = gameObject.GetComponent<Rigidbody>();

        var v = rB.velocity;
        v.y = 0;
        rB.velocity = v;
        gameObject.transform.LookAt(v);
    }

    internal void IgnoreCollisions() {
        GetComponent<CollisionProj>().IgnoreCollision = true;
    }

    internal void DamageSelf(float multiplier = 1) {
        var damage = SkillInfoProvider.Instance.GetBaseDamage(GetCurrentSkill());
        hp.DealDamage(damage * multiplier);
        DamageActions.DamageHappens(damage * multiplier, gameObject);
    }

    internal void HealSelf(SkillInfoProvider.SkillId heal) {

        var healedAmount = SkillInfoProvider.Instance.GetBaseDamage(heal);

        hp.DealDamage(healedAmount);
        DamageActions.DamageHappens(healedAmount, gameObject);
    }

    public SkillInfoProvider.SkillId GetCurrentSkill() {
        return (SkillInfoProvider.SkillId)currentSkill;

    }


    internal void BlockAttacking(float v) {
        battleStateMachine.BlockAttacks(v);
    }



    private MovementLinearCtrl movementLinear;

    internal GameObject InstantiateEffect(GameObject prefab, Vector3 position) {
        GameObject obj = Instantiate(prefab);
        obj.transform.position = position;
        return obj;
    }

    internal void ProjSpeed(Vector3 vector3, GameObject proj) {
        proj.GetComponent<Rigidbody>().velocity = vector3;
    }

    internal void ProjSpeed(Vector3 speed, GameObject proj, float delay, float time) {

        proj.GetComponent<Rigidbody>().DOMove(proj.transform.position + speed * time, time).SetDelay(delay);
    }

    internal void ProjSpeed(Vector3 speed, GameObject proj, float delay, float time, Ease ease) {

        proj.GetComponent<Rigidbody>().DOMove(speed * time, time).SetDelay(delay).SetEase(ease);
    }

    internal void DamagesDirect(GameObject obj, int damage, int mindDamage) {
        var pd = obj.GetComponent<ProjectilData>();
        pd.Damage = damage;
        pd.DamageMind = mindDamage;
    }

    internal void Zoom(float ratio, float timeZooming, float delay = 0) {
        CentralResource.Instance.CameraControl.Zoom(ratio, timeZooming, delay);
    }

    internal void ScreenShakeType1(float duration) {
        CentralResource.Instance.CameraControl.Shake(
            duration,
            new Vector3(2, 2),
            20,//(int)(duration*20), 
            UnityEngine.Random.Range(20, 80));
    }

    internal void ScreenShakeType2(float duration) {

        CentralResource.Instance.CameraControl.Shake(
            duration,
            new Vector3(0.5f, 0.5f),
            15,
            UnityEngine.Random.Range(20, 80));
    }

    internal void InstantiateEffectAndParentIt(GameObject prefab, Vector3 position) {

        InstantiateEffect(prefab, position).transform.parent = this.transform;
    }



    private CharacterControllerWrapper characterControllerWrapper;

    internal void ImpactSound(GameObject obj, string v) {
        obj.GetComponent<ProjectilData>().ImpactSound = v;
    }

    private BattleStateMachine battleStateMachine;
    private SuperArmor superArmor;
    private bool sideHero;
    private HPSystem hp;
    private LineDrawManager lineDrawManager;
    private ControllableObject controllableObject;
    private AttackInput attackInput;

    private bool skillIdLocked;
    int currentSkill;

    public LineDrawManager LineDrawManager {
        get {
            return lineDrawManager;
        }

        set {
            lineDrawManager = value;
        }
    }

    public int CurrentSkill {
        get {
            return currentSkill;
        }

        set {
            currentSkill = value;
        }
    }

    public ActorStats ActorStats {
        get {
            return actorStats;
        }

        set {
            actorStats = value;
        }
    }

    public AttackInput AttackInput {
        get {
            return attackInput;
        }

        set {
            attackInput = value;
        }
    }

    internal void StopMovement() {
        characterControllerWrapper.SetSpeed(new Vector3(0, 0, 0));
    }

    internal void ProjCollideWithGround(GameObject obj) {
        if (sideHero) {
            obj.layer = LayersNumber.PROJHEROGROUND;
        } else {
            obj.layer = LayersNumber.PROJENEMYGROUND;
        }
        obj.GetComponent<CollisionProjSelfManaged>().enabled = true;
    }

    internal void projKnockback(GameObject proj, Vector3 speed, float knockbacktime) {
        var pD = proj.GetComponent<ProjectilData>();
        pD.KnockbackNormal = speed;
        pD.KnockbackMoveDuration = knockbacktime;
    }

    public bool CanMoveState() {
        return battleStateMachine.CanMove();
    }

    public void FaceTargetNow() {
        faceObj.Target = closestTarget.ChosenTarget;
        faceObj.FaceNow();

    }

    public void FaceTarget(float duration) {

        FaceTarget(true);
        invoker.AddActionUniqueReplaceHigherDelay(FaceTargetEnd, duration);

    }

    private void FaceTargetEnd() {
        FaceTarget(false);
    }

    internal void FaceTarget(bool v) {

        if (v) {
            if (!controllableObject.ControllingFlag) {
                faceObj.Target = closestTarget.ChosenTarget;
                faceObj.enabled = true;
            }

        } else {
            faceObj.enabled = false;
            faceObj.Target = null;
        }
    }

    internal void Damages(ProjectilData pd, SkillInfoProvider.SkillId sId) {
        pd.Damage = SkillInfoProvider.Instance.GetBaseDamage(sId)
            * actorStats.GetAttackMultiplier();
        pd.DamageMind = SkillInfoProvider.Instance.GetMindDamage(sId)
            * actorStats.GetAttackMultiplier();
    }

    internal void Damages(ProjectilData pd, int sId) {
        Damages(pd, (SkillInfoProvider.SkillId)sId);
    }

    void Awake() {
        AttackInput = GetComponent<AttackInput>();
        invoker = GetComponent<Invoker>();
    }


    // Use this for initialization
    void Start() {

        controllableObject = GetComponent<ControllableObject>();
        LineDrawManager = GetComponent<LineDrawManager>();
        animator = GetComponentInChildren<Animator>();
        
        hp = GetComponent<HPSystem>();
        closestTarget = GetComponent<ClosestTarget>();
        weaponTrailManager = GetComponent<WeaponTrailManager>();
        faceObj = GetComponent<FaceGameObject>();
        GetComponent<CharacterBattleHolder>().CharaCallbackOnce((cb) => {
        });
        rigidBody = GetComponent<Rigidbody>();
        movementLinear = GetComponent<MovementLinearCtrl>();
        characterControllerWrapper = GetComponent<CharacterControllerWrapper>();
        battleStateMachine = GetComponent<BattleStateMachine>();
        superArmor = GetComponent<SuperArmor>();
    }

    internal void AttackState(float backToNormalTime) {
        battleStateMachine.ChangeState(
                        BattleStateMachine.States.Attacking,
                        backToNormalTime);
    }

    internal void AttackStateWithMove(float backToNormalTime) {
        AttackState(backToNormalTime);
        battleStateMachine.AllowAttackAndMove(backToNormalTime);
    }

    internal void AnimationTrigger(string v1, float v2) {
        invoker.AddAction(() => {
            AnimationTrigger(v1);
        }, v2);
    }



    internal void ZeroSpeed(float v) {
        invoker.AddAction(ZeroSpeed, v);
    }
    internal void ZeroSpeed() {
        //rigidBody.velocity = Vector3.zero;
        characterControllerWrapper.ZeroSpeed();
        movementLinear.Velocity = Vector3.zero;
        movementLinear.enabled = false;


    }

    internal void MoveTowardsTargetXZ(float chaseSpeed, float speedY, float endTime) {
        var targetPosition = ClosestTarget().transform.position;
        var selfPos = transform.position;
        targetPosition = targetPosition - selfPos;
        targetPosition.y = 0;
        targetPosition = targetPosition.normalized * chaseSpeed;
        targetPosition.y = speedY;
        movementLinear.enabled = true;
        movementLinear.Velocity = targetPosition;
        movementLinear.Disable(endTime);
    }

    internal void ProjMoveTowardsTargetXZ(GameObject proj, float chaseSpeed, float speedY, float angleY = 0) {

        var gameObject1 = ClosestTarget();
        var targetPosition = new Vector3();
        if (gameObject1 != null)
            targetPosition = gameObject1.transform.position;
        var selfPos = proj.transform.position;
        targetPosition = targetPosition - selfPos;
        targetPosition.y = 0;
        targetPosition = targetPosition.normalized * chaseSpeed;
        targetPosition.y = speedY;
        targetPosition = Quaternion.Euler(0, angleY, 0) * targetPosition;
        proj.GetComponent<Rigidbody>().velocity = targetPosition;
    }

    internal void Move(Vector3 speed, float endTime) {

        movementLinear.enabled = true;
        movementLinear.Velocity = speed;
        movementLinear.Disable(endTime);
    }

    internal Vector3 MoveTowardsDirectionXZ(float chaseSpeed, float speedY, float endTime) {
        var targetPosition = transform.forward;
        //targetPosition = targetPosition - selfPos;
        targetPosition.y = 0;
        targetPosition = targetPosition.normalized * chaseSpeed;
        targetPosition.y = speedY;
        movementLinear.enabled = true;
        movementLinear.Velocity = targetPosition;
        movementLinear.Disable(endTime);
        return targetPosition;
        //rigidBody.velocity = targetPosition;

    }

    public GameObject ClosestTarget() {
        return closestTarget.ChosenTarget;
    }

    internal GameObject AttachSensor(SkillInfoProvider.SkillId skillId, Transform attackHolder, float timeToDisappear, float scale = 1f) {
        //attackHolder = gameObject.transform;
        var gameObject1 = CentralResource.Instance.SensorProjProvider.GetProj(scale);
        gameObject1.transform.localPosition = Vector3.zero;
        var p = gameObject1.transform.parent;
        //Debug.Log(p + "parent A");
        //Debug.Log(attackHolder + " BONE HOLDER IS...");
        gameObject1.transform.SetParent(attackHolder);
        gameObject1.transform.parent = attackHolder;
        Destroy(gameObject1, timeToDisappear);
        //gameObject1.GetComponent<ProjectilData>().Damage = damage;
        var projectilData = gameObject1.GetComponent<ProjectilData>();
        projectilData.Owner = gameObject;
        Damages(projectilData, skillId);

        p = gameObject1.transform.parent;
        Debug.Log(p + "parent B");
        DelayedMethod(() => {
            gameObject1.transform.localPosition = Vector3.zero;
        }, 0.01f);
        gameObject1.tag = usedTag;
        if (sideHero) {
            gameObject1.layer = LayersNumber.PROJHEROSIMPLE;
        } else {
            gameObject1.layer = LayersNumber.PROJENEMYSIMPLE;
        }
        return gameObject1;
    }

    internal GameObject AttachProj(ProjectilHolder.Projs proj, SkillInfoProvider.SkillId skillId,
        Transform attackHolder,
        float timeToDisappear, Quaternion q = new Quaternion()) {
        GameObject projObj = GetProj(proj, skillId, timeToDisappear);

        projObj.transform.localPosition = Vector3.zero;
        projObj.transform.position = attackHolder.transform.position;
        DelayedMethod(() => {
            if (projObj != null) {
                projObj.transform.localPosition = Vector3.zero;
                projObj.transform.localRotation = q;
            }

        }, 0.01f);


        projObj.transform.SetParent(attackHolder);
        //projObj.transform.parent = attackHolder;

        return projObj;
    }



    public GameObject GetProj(ProjectilHolder.Projs proj, SkillInfoProvider.SkillId skillId, float timeToDisappear) {
        GameObject gameObject1 = CentralResource.Instance.ProjectilHolder.GetProjectil(proj);
        gameObject1.transform.SetParent(CentralResource.Instance.ProjectilHolder.ProjectilParent);
        //gameObject1.transform.localPosition = Vector3.zero;
        gameObject1.transform.position = this.transform.position;

        //Destroy(gameObject1, timeToDisappear);

        var projectilData = gameObject1.GetComponent<ProjectilData>();

        PrepareProj(skillId, projectilData);
        //gameObject1.GetComponent<ProjectilData>().Damage = damage;

        projectilData.DelayedDestroy(timeToDisappear);
        return gameObject1;
    }

    public void PrepareProj(SkillInfoProvider.SkillId skillId,ProjectilData projectilData) {
        projectilData.Owner = gameObject;
        Damages(projectilData, skillId);
        projectilData.gameObject.tag = usedTag;
    }


    internal void WeaponTrail(float timeToDisappear) {
        weaponTrailManager.ActivateAll();
        invoker.AddAction(weaponTrailManager.deactivateAll, timeToDisappear);
    }

    public void AnimationTrigger(string trigger) {
        animator.SetTrigger(trigger);
    }

    public void DelayedMethod(Action action, float delay) {
        invoker.AddAction(action, delay);
    }

    public void DelayedMethodCancellable(Action action, float delay) {
        invoker.AddAction(action, delay, 1);
    }

    public void DelayedMethod(float delay, Action action) {
        invoker.AddAction(action, delay);
    }

    public void DelayedMethodCancellable(float delay, Action action) {
        invoker.AddAction(action, delay, 1);
    }

    public void DelayedMethodUnique(Action action, float delay) {
        invoker.removeAllActionsLikeThis(action);
        invoker.AddAction(action, delay);
    }

    // Update is called once per frame
    void Update() {

    }

    internal void HeroSide() {
        usedTag = heroProjTag;
        sideHero = true;
    }

    internal void EnemySide() {
        usedTag = enemyProjTag;
        sideHero = false;
    }
}
