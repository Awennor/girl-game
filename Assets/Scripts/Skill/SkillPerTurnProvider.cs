﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class SkillPerTurnProvider : MonoBehaviour {

    protected List<int> skillList = new List<int>();
    protected List<int> skillList2 = new List<int>();
    protected List<int> recentlyAdded = new List<int>();
    int energy;

    public int Energy {
        get {
            return energy;
        }

        set {
            energy = value;
        }
    }

    public List<int> RecentlyAdded {
        get {
            return recentlyAdded;
        }

        set {
            recentlyAdded = value;
        }
    }

    public List<int> GetTurnSkillIds() {
        
        return skillList;
    }

    public List<int> GetTurnSkillIds2()
    {
        
        return skillList2;
    }

    public void NewTurn() {
        FillUpSkillListNewTurn();
        FillUpSkillList2();
    }

    protected abstract void FillUpSkillListNewTurn();
    protected virtual void FillUpSkillList2() {
    }
    internal virtual void UsedUpSkillInPosition(int v) {   
    }

    

    public void EndTurn() {
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void BattleStart() {
        energy  = 0;
        BattleStartChildClass();
    }

    virtual internal void BattleStartChildClass() {
        energy  = 0;
    }

    virtual internal void DeckRefreshUsed() {
        
    }
}
