﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AutoLocalizeText : MonoBehaviour {

    public LocalizationEnum localizationenum;

	// Use this for initialization
	void Start () {
	    GetComponent<Text>().text = CentralResource.Instance.LocalizationInfo.GetText(localizationenum);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
