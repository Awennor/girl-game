﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

class XMLSerializationHelper<T> {

    XmlSerializer xmlSerializer;
    XMLEncoder encoder = new XMLEncoder();

    internal XMLEncoder Encoder {
        get {
            return encoder;
        }

        set {
            encoder = value;
        }
    }

    public XMLSerializationHelper() {
        xmlSerializer = new XmlSerializer(typeof(T));
        
    }

    public string SerializeToStringWithEncode(T toSerialize) {

        string xmlString = SerializeToString(toSerialize);
        return encoder.Escape(xmlString);
    }

    public T DeserializeStringWithDecode(string xmlString) {
        xmlString = encoder.Unescape(xmlString);
        return DeserializeString(xmlString);
    }

    public string SerializeToString(T toSerialize) {

        string xmlString;
        using (StringWriter textWriter = new StringWriter()) {
            xmlSerializer.Serialize(textWriter, toSerialize);
            xmlString = textWriter.ToString();
            
            return xmlString;
        }
        
    }

    public T DeserializeString(string xmlString) {
        
        using (TextReader reader = new StringReader(xmlString)) {
            var result = (T)xmlSerializer.Deserialize(reader);
            return result;
        }
    }
}

