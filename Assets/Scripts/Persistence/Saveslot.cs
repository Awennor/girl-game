﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class Saveslot {

    
    public List<int> collectableIdsGotten = new List<int>();

    public VariableValues variableValues = new VariableValues();

    public SkillInvenDeckData skillInvenDeckData = new SkillInvenDeckData();
    public SpawnPositionSave spawnPositionSave = new SpawnPositionSave();

    public SettingsPersistent settingsPersistent = new SettingsPersistent();
    public int randomId = UnityEngine.Random.Range(0,2147483647);



    

    internal bool AlreadyGottenCollectable(int uniqueId) {
        return collectableIdsGotten.Contains(uniqueId);
    }

    internal void AddCollectableGotten(int uniqueId) {
        collectableIdsGotten.Add(uniqueId);
    }

    internal void SetVariableValue(VariableEnum variableEnum, float newValue) {
        variableValues.SetValue((int)variableEnum, newValue);
    }

    internal float GetVariableValue(VariableEnum variableName) {
        return variableValues.GetValue((int)variableName);
    }
}

[System.Serializable]
public class Saveslots {

    [SerializeField]
    public List<Saveslot> saveSlots =new List<Saveslot>();

    internal void AddSlot(Saveslot s) {
        saveSlots.Add(s);
    }

    internal Saveslot GetSlot(int slot) {
        return saveSlots[slot];
    }
}
