﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class SettingsPersistent{

    //public bool MouseAiming { get; set; }
    public AimingMode aimingMode;
    public SettingsPersistent() {
        aimingMode = AimingMode.AUTOMATIC;
        //MouseAiming = true;
    }
}
