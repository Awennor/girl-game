﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PersistenceSaveSlots{
    private const string PREFKEY = "saveslotspre";
    Saveslots saveSlots;
    XMLSerializationHelper<Saveslots> saverXML = new XMLSerializationHelper<Saveslots>();
    public event Action dataLoad;
    int slot = 0;

    public PersistenceSaveSlots() {
        LoadData();
    }

    public Saveslot CurrentSlot {
        get {
            
            return SaveSlots.GetSlot(slot);
        }
    }

    public Saveslots SaveSlots {
        get {
            return saveSlots;
        }

        set {
            saveSlots = value;
        }
    }

    public void LoadData() {
        PlayerPrefs.DeleteKey(PREFKEY); //debug to delete save
        if (PlayerPrefs.HasKey(PREFKEY)) {
            var xmlString = PlayerPrefs.GetString(PREFKEY);
            Debug.Log(xmlString);
            SaveSlots = saverXML.DeserializeStringWithDecode(xmlString);
        } else {
            SaveSlots = new Saveslots();
            var s = new Saveslot();
            SaveSlots.AddSlot(s);

            SaveData();
        }
        if(dataLoad != null) {
            dataLoad();
        }
    }



    public void SaveData() {
        var xmlString = saverXML.SerializeToStringWithEncode(SaveSlots);
        PlayerPrefs.SetString(PREFKEY, xmlString);
        PlayerPrefs.Save();
        Debug.Log(""+ xmlString);
    }
}
