﻿using System;

[System.Serializable]
public class SpawnPositionSave {

    public string sceneName = "";
    public int spawnNumber = -1;

    internal void SaveData(string sceneName, int spawnPoint) {
        this.sceneName = sceneName;
        spawnNumber = spawnPoint;
    }
}