﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTreasure : MonoBehaviour {

    public Countdown countdown;
    public Collectable collect;

    public List<SkillInfoProvider.SkillId> possibleSkills;

    bool collectVisible;

    // Use this for initialization
    void Start() {
        countdown.timeUpEvent.AddListener(Countdown_Timeup);
        collectVisible = RandomSpecial.RandomBool();
        UpdateVisible();
        UpdateCountDown();
        collect.SkillId = possibleSkills.RandomElementSpecial();

    }

    private void UpdateVisible() {
        collect.gameObject.SetActive(collectVisible);
    }

    private void Countdown_Timeup() {
        collectVisible = !collectVisible;
        UpdateVisible();
        UpdateCountDown();
    }

    private void UpdateCountDown() {
        countdown.StartCount(RandomSpecial.Range(120f, 360f));
    }

    public void PrizeGet() {
        collectVisible = false;
        collect.SkillId = possibleSkills.RandomElementSpecial();
        UpdateVisible();
        UpdateCountDown();
    }

    // Update is called once per frame
    void Update() {

    }
}
