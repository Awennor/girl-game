﻿using UnityEngine;
using System.Collections;

public class HeroFeetPositioner : MonoBehaviour {
    private Vector3 oldPos;

    // Use this for initialization
    void Start() {


    }

   

    // Update is called once per frame
    void Update() {
        Refresh();

    }

    private void Refresh() {
        if (CentralResource.Instance.Hero != null) {
            float heroY = CentralResource.Instance.Hero.transform.position.y;
            
            Vector3 pos = transform.position;
            Vector3 dis = -oldPos + pos;
            oldPos = pos;

            pos.y = heroY;
            transform.position = pos;
            //Vector3 eu = transform.rotation.eulerAngles;
            
            Vector3 eu;
            if(dis == Vector3.zero) {
                eu = transform.rotation.eulerAngles;
            } else
                eu = Quaternion.LookRotation(dis, Vector3.forward).eulerAngles;
            
            transform.rotation = Quaternion.Euler(90, eu.y, 0);
            /*Vector3 scale = new Vector3(5,5,1);
            Debug.Log("parent scale "+transform.parent.localScale);
            scale.x *= transform.parent.localScale.x;
            scale.y *= transform.parent.localScale.y;
            scale.z *= transform.parent.localScale.z;

            transform.localScale = scale;*/
        }
    }
}
