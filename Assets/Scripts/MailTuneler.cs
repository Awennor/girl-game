﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System;
using UnityEngine.Events;

public class MailTuneler : MonoBehaviour {

    public Text source;
    public string subjectLine;
    public EmailSender emailSender;
    public UnityEvent EmailSendComplete;

	public void SendMail() {
        DOVirtual.DelayedCall(0.1f, SendMailImplementation);

    }

    private void SendMailImplementation() {
        emailSender.SendEmail(
            subjectLine+ " "+CentralResource.Instance.SaveSlots.CurrentSlot.randomId, source.text);
        DOVirtual.DelayedCall(0.1f, EmailSendComplete.Invoke);
    }

    
}
