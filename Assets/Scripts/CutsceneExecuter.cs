﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;
using UnityEngine.UI;

public class CutsceneExecuter : MonoBehaviour {

    public List<GameObject> hideDuringCutscene;
    private CutsceneComp cutscene;
    public Image overlay;

    public Camera mainCamera;
    Camera sceneCamera;
    public DialogSystem dialog;
    public MyStringEvent emailShow;
    public MyStringEvent OnArticleShow;
    public bool enableOverworldInputOnEnd = true;
    public event Action<string> emailShowAction;


    public Canvas canvas;


    int index = 0;
    private bool DelayedAdvancing;
    private float rotateActorDefaultSpeed = 360;
    private bool waitingForDialog;


    public void Start() {
        CentralResource.Instance.CutsceneSystem.CutsceneStartCallback += CutsceneStart;
        dialog.wannaClose.AddListener(AdvanceFromDialog);
    }

    private void AdvanceFromDialog() {
        if (waitingForDialog) {
            waitingForDialog = false;
            Advance();
        }

    }

    public void CutsceneStart(CutsceneComp cutscene) {
        this.cutscene = cutscene;
        HideObjectsNotFromCutscene(true);
        CentralResource.Instance.OverworldInput.InputActive = false;
        index = 0;

        Debug.Log("Cutscene Start");

        sceneCamera = cutscene.SceneCamera;
        dialog.Camera = sceneCamera;

        int cc = cutscene.transform.childCount;
        for (int i = 0; i < cc; i++) {
            var c = cutscene.transform.GetChild(i);
            c.gameObject.SetActive(true);
        }

        if (sceneCamera != null) {
            //canvas.worldCamera = sceneCamera;
            mainCamera.enabled = false;
        }


        Execute();
    }

    private void Execute() {
        Debug.Log("Node execute");
        if (cutscene.Nodes.Length > index) {
            var node = cutscene.Nodes[index];

            switch (node.Type) {
                case CutsceneNodeType.Delay:
                    DelayedAdvance(node.Floats[0]);
                    break;
                case CutsceneNodeType.SimultaneousExecution:
                    Advance();
                    break;
                case CutsceneNodeType.RotateActorDefault:
                    {

                        var target = node.GameObjects[0];
                        var lR = target.transform.localRotation;
                        var euler = lR.eulerAngles;
                        var angle = node.Floats[0];
                        float time = angle / rotateActorDefaultSpeed;
                        if (time < 0) time = -time;
                        euler.y += angle;
                        target.transform.DOLocalRotate(euler, time);
                        var faceGameObject = target.GetComponent<FaceGameObject>();
                        if (faceGameObject != null)
                            faceGameObject.Target = null;
                        DelayedAdvance(time);
                    }
                    break;
                case CutsceneNodeType.FaceTarget:
                    {
                        var target = node.GameObjects[0];
                        target.GetComponent<FaceGameObject>().Target = node.GameObjects[1];
                        Advance();
                    }

                    break;
                case CutsceneNodeType.ShowEffect:
                    {
                        EffectSingleton.Instance.ShowEffectHere(node.Strings[0], node.GameObjects[0].transform);
                        Advance();
                    }
                    break;
                case CutsceneNodeType.AnimationTrigger:
                    {
                        var target = node.GameObjects[0];
                        target.GetComponent<Animator>().SetTrigger(node.Strings[0]);
                        Advance();
                    }

                    break;
                case CutsceneNodeType.TextMessage:
                    {
                        string groupName = node.Strings[0];
                        string messageKey = node.Strings[1];
                        string charaKey = node.Strings[2];
                        string mainText = CentralResource.Instance.LocalizationHolder.GetText(groupName, messageKey);
                        string charaText = CentralResource.Instance.LocalizationHolder.GetSpeakerName(charaKey);
                        //Debug.Log(mainText);
                        dialog.ShowMessage(mainText, charaText);
                        dialog.CenterOn(node.GameObjects[0]);
                        dialog.NormalColor();
                        if (node.Bools[0]) { //bool for thinking, may be replaced with a better style system
                            dialog.GrayColor();
                        }

                        waitingForDialog = true;
                        //Advance();
                    }
                    break;
                case CutsceneNodeType.GameObjectActive:
                    {
                        var target = node.GameObjects[0];
                        target.SetActive(node.Bools[0]);
                        Advance();
                    }

                    break;
                case CutsceneNodeType.FadeTo:
                    {
                        Color c = node.Colors[0];
                        FadeTo(node.Floats[0], node.Floats[1], c);
                        DelayedAdvance(node.Floats[0]);
                    }
                    //DelayedAdvance(node.Floats[0]);
                    break;
                case CutsceneNodeType.FadeIn:
                    {
                        Color c = node.Colors[0];
                        c.a = 1;
                        FadeTo(node.Floats[0], 0, c);
                        DelayedAdvance(node.Floats[0]);
                    }
                    //DelayedAdvance(node.Floats[0]);
                    break;
                case CutsceneNodeType.FadeOut:
                    {
                        Color c = node.Colors[0];
                        c.a = 0;
                        FadeTo(node.Floats[0], 1, c);
                        DelayedAdvance(node.Floats[0]);
                    }

                    //DelayedAdvance(node.Floats[0]);
                    break;
                case CutsceneNodeType.MoveToFrom:
                    {
                        var target = node.GameObjects[0];
                        target.transform.position = node.GameObjects[1].transform.position;
                        target.transform.DOMove(node.GameObjects[2].transform.position, node.Floats[0]);
                        DelayedAdvance(node.Floats[0]);
                    }

                    //DelayedAdvance(node.Floats[0]);
                    break;
                case CutsceneNodeType.MoveTo:
                    {
                        var target = node.GameObjects[0];
                        target.transform.DOMove(node.GameObjects[1].transform.position, node.Floats[0]);
                        DelayedAdvance(node.Floats[0]);
                    }

                    //DelayedAdvance(node.Floats[0]);
                    break;
                case CutsceneNodeType.MoveAndRotateTo:
                    {
                        var target = node.GameObjects[0];
                        target.transform.DOMove(node.GameObjects[1].transform.position, node.Floats[0]);
                        target.transform.DORotate(node.GameObjects[1].transform.rotation.eulerAngles, node.Floats[0]);
                        DelayedAdvance(node.Floats[0]);
                    }

                    //DelayedAdvance(node.Floats[0]);
                    break;
                case CutsceneNodeType.MoveToAnimatedCharacter:
                    {
                        var target = node.GameObjects[0];
                        var moveToGoal = target.GetComponent<MovementToGoalEnemy>();
                        moveToGoal.enabled = true;
                        moveToGoal.DisableOnReach = true;
                        moveToGoal.WantedDistance = 4;
                        moveToGoal.Target = node.GameObjects[1].transform;

                        moveToGoal.reachGoodDistance -= Advance;
                        moveToGoal.reachGoodDistance += Advance;
                        //target.transform.position = node.GameObjects[1].transform.position;
                        //target.transform.DOMove(node.GameObjects[2].transform.position, node.Floats[0]);
                        //DelayedAdvance(node.Floats[0]);
                    }

                    //DelayedAdvance(node.Floats[0]);
                    break;
                case CutsceneNodeType.ShowEmail:
                    emailShow.Invoke(node.Strings[0]);
                    if (emailShowAction != null) {
                        emailShowAction(node.Strings[0]);
                    }
                    break;
                case CutsceneNodeType.ShowArticle:
                    OnArticleShow.Invoke(node.Strings[0]);
                    
                    break;
                case CutsceneNodeType.CallMethod:
                    {
                        node.unityEvent.Invoke();
                        Advance();
                        //target.transform.position = node.GameObjects[1].transform.position;
                        //target.transform.DOMove(node.GameObjects[2].transform.position, node.Floats[0]);
                        //DelayedAdvance(node.Floats[0]);
                    }

                    //DelayedAdvance(node.Floats[0]);
                    break;

            }
        } else {
            FadeTo(1, 1, new Color(0, 0, 0, 0));
            DOVirtual.DelayedCall(1, CutsceneEnd);
            //DOVirtual.DelayedCall(1,AfterCutsc);
        }

    }

    private void DelayedAdvance(float v) {
        if (cutscene.Nodes.Length > index + 1 && cutscene.Nodes[index + 1].Type
            == CutsceneNodeType.SimultaneousExecution) {
            Advance();
            return;
        }
        DelayedAdvancing = true;
        DOVirtual.DelayedCall(v, Advance).target = this;
    }

    private void FadeTo(float time, float finalAlpha, Color initialColor) {
        //overlay.gameObject.SetActive(true);
        overlay.enabled = true;

        if (time == 0) {
            initialColor.a = finalAlpha;
            overlay.color = initialColor;
        } else {
            overlay.color = initialColor;
            overlay.DOFade(finalAlpha, time);
        }

    }

    public void ExternalAdvance() {
        Advance();
    }

    private void Advance() {
        //Debug.Log("Advance Happens...");
        DelayedAdvancing = false;
        index++;
        Execute();
    }

    public void CutsceneEnd() {
        var e = cutscene.GetComponent<CutsceneVariableAntiRepetition>().VariableEnum;
        CentralResource.Instance.VariableSystem.SetVariableValue(e, 1);
        HideObjectsNotFromCutscene(false);
        FadeTo(1, 0, Color.black);
        int cc = cutscene.transform.childCount;
        if (enableOverworldInputOnEnd) {
            CentralResource.Instance.OverworldInput.InputActive = true;
        }
        for (int i = 0; i < cc; i++) {
            var c = cutscene.transform.GetChild(i);
            c.gameObject.SetActive(false);
        }
        if (sceneCamera != null) {
            //canvas.worldCamera = mainCamera;
            mainCamera.enabled = true;
        }
        cutscene.CutsceneEnd();
    }

    private void HideObjectsNotFromCutscene(bool hide) {
        for (int i = 0; i < hideDuringCutscene.Count; i++) {
            hideDuringCutscene[i].SetActive(!hide);
        }
    }
}
