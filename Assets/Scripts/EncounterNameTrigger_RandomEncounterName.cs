﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterNameTrigger_RandomEncounterName : MonoBehaviour {

    public List<string> encounterNames;
    private EncounterNameTrigger encounterNameTrigger;

    // Use this for initialization
    void Start () {

        encounterNameTrigger = GetComponent<EncounterNameTrigger>();
        encounterNameTrigger.battleComplete.AddListener(()=> {
            UpdateName();
        });
        UpdateName();

    }

    private void UpdateName() {
        encounterNameTrigger.EncounterName = encounterNames.RandomElementSpecial();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
