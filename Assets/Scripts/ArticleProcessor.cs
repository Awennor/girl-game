﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ArticleProcessor : MonoBehaviour {

    public ArticleData articleData;
    public MyStringEvent date, title, body;
    public UnityEvent show;

    public void ShowArticle(string articleId) {
        show.Invoke();
        gameObject.SetActive(true);
        SingleArticle sM = articleData.GetArticle(articleId);
        date.Invoke(string.Format("{0}/{1:00}/{2:00}", sM.Year, sM.Month, sM.Day));

        var mailBody = CentralResource.Instance.LocalizationHolder.ArticleBody(sM.Id);
        body.Invoke(mailBody);
        title.Invoke(CentralResource.Instance.LocalizationHolder.ArticleTitle(sM.Id));
    }

	// Use this for initialization
	void Start () {
		//ShowEmail("silva1");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
