﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using System;
using UnityEngine.Events;

public class EnemySpawnEffect : MonoBehaviour {

    public Material material;
    float initialHeight;
    float currentHeight;
    float finalHeight;
    public float time;
    public float offsetHeights;
    private TweenerCore<float, float, FloatOptions> tween;
    public UnityEventSequence OnEffectDone;

    public void EnemyInstantiatedBattle(GameObject obj) {
        var mC = obj.GetComponent<MaterialChanger>();
        mC.ChangeMaterialShared(material);
        mC.revertMaterial(time, true);

        var rendererMain = mC.RendererMain;
        var meshR = rendererMain.GetComponent<SkinnedMeshRenderer>();
        var mesh = meshR.sharedMesh;
        var vs = mesh.vertices;
        float smallestZ = 9999;
        float highestZ = -9999;
        for (int i = 0; i < vs.Length; i++) {
            float z = vs[i].z;
            z *= meshR.transform.lossyScale.z;
            if(z < smallestZ) smallestZ = z;
            if(z > highestZ) highestZ = z;
        }
        var offsetComp = obj.GetComponent<HeightOffsetForAppearShader>();
        float minOffset = 0;
        float maxOffset = 0;
        if(offsetComp != null) {
            minOffset = offsetComp.minOffset;
            maxOffset = offsetComp.maxOffset;
        }
        if(initialHeight < highestZ) initialHeight = highestZ+ maxOffset+offsetHeights;
        if(finalHeight > smallestZ) finalHeight = smallestZ + minOffset - offsetHeights;
        //Debug.Log(initialHeight + "_HEIGHT_"+finalHeight);
        currentHeight = initialHeight;
        if(tween != null) {
            tween.Kill();
        }
        tween = DOTween.To(()=> currentHeight, x=> currentHeight=x, finalHeight, time)
            .SetUpdate(true).OnComplete(EffectDone);
            //.SetUpdate(false)
            //.SetDelay(0.1f);
            ;
        UpdateHeightMaterial();
        

    }

    private void EffectDone() {
        OnEffectDone.Invoke();
    }

    // Use this for initialization
    void Start () {
        CentralResource.Instance.BattleFlowSys.OnBattleStart += ()=> {
            initialHeight = -9999;
            finalHeight = 9999;
        };
	    //currentHeight = finalHeight;
	}
	
	// Update is called once per frame
	void Update () {
	    //if(initialHeight != finalHeight) {
            UpdateHeightMaterial();
        //}
    }

    private void UpdateHeightMaterial() {
        material.SetFloat("_HeightShown", currentHeight);
    }
}
