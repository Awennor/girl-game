﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class RepeatingMovingProp : MonoBehaviour {

    public FollowTargetWithSpeed followTargetWithSpeed;

    bool moving = true;
    private Vector3 initialPos;

    // Use this for initialization
    void Start () {
	    initialPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	    if(moving ) {
            if (followTargetWithSpeed.DistanceSquare < 3*3 ) {
                //Debug.Log("reached!");
                moving = false;
                //followTargetWithSpeed.enabled = false;
                DOVirtual.DelayedCall(0.3f, MoveAgain);
                gameObject.SetActive(false);
                
            }
        }
	}

    private void MoveAgain() {
        //Debug.Log("Move again!");
        gameObject.SetActive(true);
        moving = true;
        gameObject.transform.position = initialPos;
    }
}
