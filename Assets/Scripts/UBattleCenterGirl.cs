﻿using UnityEngine;
using System.Collections;

public class UBattleCenterGirl {

    UBattleGameObjects uBattleGameObjects = new UBattleGameObjects();

    private static UBattleCenterGirl instance;
    public static UBattleCenterGirl Instance
    {
        get
        {
            if (instance == null) {
                instance = new UBattleCenterGirl();
            }
                
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public UBattleGameObjects UBattleGameObjects
    {
        get
        {
            return uBattleGameObjects;
        }

        set
        {
            uBattleGameObjects = value;
        }
    }
}
