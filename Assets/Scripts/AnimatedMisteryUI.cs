﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class AnimatedMisteryUI : MonoBehaviour {

    public Graphic animatedObj;
    public Graphic animatedObj2;
    public DelayedEvent animationEnd;


    public void Reset(){
        animatedObj.transform.localScale = new Vector3(1,1,1);
        animatedObj2.color = Color.black;
        gameObject.SetActive(true);
    }

	// Use this for initialization
	public void Animate () {
        DOTween.Sequence()
            .Append(animatedObj.DOFade(0f, 0.05f))
            .AppendInterval(0.06f)
            .Append(animatedObj.DOFade(1f, 0.05f))
            .AppendInterval(0.06f)
            .Append(animatedObj.DOFade(0f, 0.05f))
            .AppendInterval(0.06f)
            .Append(animatedObj.DOFade(1f, 0.05f))
            .AppendInterval(0.06f)
            .Append(animatedObj.DOFade(0f, 0.05f))
            .AppendInterval(0.06f)
            .Append(animatedObj.DOFade(1f, 0.05f))
            .AppendInterval(0.06f)
            .AppendCallback(()=> {
                animatedObj2.DOFade(0f, 0.3f);
            })
            .Append(animatedObj.transform.DOScale(10, 0.1f))
            .AppendCallback(()=> {
                animationEnd.Call();
                gameObject.SetActive(false);
                SoundSingleton.Instance.PlaySoundOnPause("uigood3");
            })
            .SetUpdate(true);
            ;
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
