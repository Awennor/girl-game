﻿using System;
using DG.Tweening;

internal class OverworldRandomEncounter {
    private bool immuneToEncounterState;
    float immunityTime = 5f;

    public OverworldRandomEncounter() {

    }

    public bool ImmuneToEncounterState {
        get {
            return immuneToEncounterState;
        }
    }

    public void ImmunityActivate() {
        immuneToEncounterState = true;
        DOVirtual.DelayedCall(immunityTime,ImmunityOff, false);
    }

    private void ImmunityOff() {
        immuneToEncounterState = false;
    }
}