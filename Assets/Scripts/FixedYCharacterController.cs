﻿using UnityEngine;
using System.Collections;

public class FixedYCharacterController : MonoBehaviour {
    private CharacterController characterCtrl;
    private float y;

    void Awake() {
        characterCtrl = GetComponent<CharacterController>();
    }

    void Start() {
        y = characterCtrl.transform.position.y;
    }

    // Update is called once per frame
    void Update() {
        var p = transform.position;
        if (p.y != y) {
            p.y = y;
            transform.position = p;
        }


    }
}
