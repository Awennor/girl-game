﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ChosenSkillHolder : MonoBehaviour {

    List<int> skillIds = new List<int>();
    int currentSkill = -1;
    int currentUses;
    public Action<ChosenSkillHolder> skillChange;
    public Action<ChosenSkillHolder> usesChange;
    SkillInfoProvider skillInfo = SkillInfoProvider.Instance;
    int subSkill = -1;

    public int CurrentSkill
    {
        get
        {
            return currentSkill;
        }

        set
        {
            currentSkill = value;
        }
    }

    public int CurrentUses
    {
        get
        {
            return currentUses;
        }

        set
        {
            currentUses = value;
        }
    }

    public int SubSkill {
        get {
            return subSkill;
        }

        set {
            subSkill = value;
        }
    }

    public void UseUp() {

        CurrentUses--;

        if (usesChange != null)
            usesChange(this);
        if (CurrentUses <= 0) {
            currentSkill = -1;
            if (skillIds.Count > 0)
            {
                skillIds.RemoveAt(0);
                updateCurrentSkill();
            }
        }

    }

    internal void Clear()
    {
        skillIds.Clear();
        currentSkill = -1;
        CurrentUses = -1;
        if (skillChange != null) {
            skillChange(this);
        }
        if (usesChange != null)
            usesChange(this);
    }

    internal void Reset() {
        skillIds.Clear();
        currentUses = 0;
        updateCurrentSkill();

    }

    private void updateCurrentSkill()
    {
        if (skillIds.Count <= 0)
        {
            currentSkill = -1;
        }
        else {
            CurrentSkill = skillIds[0];
        }
        
        if (skillChange != null) skillChange(this);
        if(currentSkill >= 0)
            CurrentUses = skillInfo.GetRepetition(CurrentSkill);
        else
        {
            currentUses = 0;
        }
        if (usesChange != null)
            usesChange(this);
    }

    internal void AddSkill(int skillId)
    {
        skillIds.Add(skillId);
        if (skillIds.Count == 1) {
            updateCurrentSkill();
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void DebugSkillList() {
        //Debug.Log("Number of skills chosen "+skillIds.Count);
    }
}
