﻿using UnityEngine;
using System.Collections;
using System;
using FlowCanvas.Nodes;

public class CollisionProjSelfManaged : MonoBehaviour {

    ProjectilData projectilData;
    

    public ProjectilData ProjectilData {
        
        get {
            return projectilData;
        }

        set {
            projectilData = value;
        }
    }
    public static Action<CollisionProjSelfManaged> collidedWithScenarioStatic;

    // Use this for initialization
    void Start() {

    }

    // UprojectilDataate is called once per frame
    void UprojectilDataate() {

    }

    void OnTriggerEnter(Collider col) {
        var other = col.gameObject;

        if (other.layer == LayersNumber.SCENARIO) {
            if (projectilData.DestroyOnImpact) {
                //SoundSingleton.Instance.PlaySound("bullethit");
                //SoundSingleton.Instance.PlaySound("projground");
                /*CentralResource.Instance.CameraControl.Shake(
                    0.1f,
                    new Vector3(0.5f, 0.5f),
                    15,//(int)(duration*20), 
                    UnityEngine.Random.Range(20, 80));
                */
                Destroy(gameObject);
            }
            var soundName = projectilData.ImpactSound;
            if (soundName != null) {
                SoundSingleton.Instance.PlaySound(soundName);
            }

            var objs = projectilData.ActivateOnImpact;

            for (int i = 0; i < objs.Count; i++) {
                objs[i].SetActive(true);
                projectilData.activateOnImpactCallback(projectilData.gameObject, objs[i]);
                objs[i].transform.SetParent(null);
            }
            if (collidedWithScenarioStatic != null) {
                collidedWithScenarioStatic(this);
            }
        }

    }
}
