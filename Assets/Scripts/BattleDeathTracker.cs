﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BattleDeathTracker {
    int destructed;
    int suicided;
    int fratricided;

    List<string> enemyObjectNames = new List<string>();

    public List<string> EnemyObjectNames {
        get {
            return enemyObjectNames;
        }

        set {
            enemyObjectNames = value;
        }
    }

    public BattleDeathTracker(CentralResource cr) {
        cr.BattleFlowSys.OnBattleStart += BattleStart;
    }

    public void Death(GameObject enemy) {
        string objectName = enemy.name;
        enemyObjectNames.Add(objectName);
        if (enemy == CharacterControlledSystem.Instance.Controlled) {
            suicided++;
        } else {
            if (CentralResource.Instance.Hero == enemy.GetComponent<CollisionProj>().LastAttackingProjOwner) {
                destructed++;
            } else {
                fratricided++;

            }
           
        }
        /*        
        if(CentralResource.Instance.Hero == enemy.GetComponent<ProjectilData>()) {
            destructed++;
        } else {
            if(enemy == CharacterControlledSystem.Instance.Controlled) {
                suicided++;
            } else {
                fratricided++;
            }
        }*/
    }

    private void BattleStart() {
        suicided = 0;
        destructed = 0;
        fratricided = 0;
        enemyObjectNames.Clear();
    }

    public float DestructedRatio() {
        ShowInfoDebug();
        float r = destructed;
        //Debug.Log("r1"+r);
        r = r / (float)(suicided + fratricided + destructed);
        //Debug.Log("r2"+r);
        return r;
    }

    private void ShowInfoDebug() {
        Debug.Log("DEBUG BATTLEDEATHTRACKER\nsuicided" + suicided + "\ndestructed" + destructed + "\nfratricided" + fratricided);
    }
}
