﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu(fileName = "EnemyPrizeData", menuName = "Data/Enemy Prizes", order = 2)]
public class EnemyPrizeDataDefinition : ScriptableObject {

    public EnemyPrizeUnit[] enemyPrizeUnits;

    internal EnemyPrizeUnit GetPrizeOfEnemy(string v) {
        for (int i = 0; i < enemyPrizeUnits.Length; i++) {
            if(enemyPrizeUnits[i].prefabName.Equals(v)) {
                return enemyPrizeUnits[i];
            }
        }
        return null;
    }
}

[System.Serializable]
public class EnemyPrizeUnit {
    public string prefabName;
    public Prize[] prizes;

}

[System.Serializable]
public class Prize {
    public SkillInfoProvider.SkillId skillId;
    public int excellency;
    public VictoryStyle victoryStyle;
}

