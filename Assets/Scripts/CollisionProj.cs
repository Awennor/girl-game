﻿using UnityEngine;
using System.Collections;
using System;
using Assets.ReusablePidroh.Scripts;
using System.Collections.Generic;

public class CollisionProj : MonoBehaviour {
    bool enemySide;
    private HPSystem hpSystem;
    public Action collisionProjAfter;

    ActorStats actorStats;

    ProjectilData lastCollidedProjData;
    bool ignoreCollision;
    DamageReaction damageReaction;
    private MovementLinearCtrl movementLinearCtrl;
    private SuperArmor superArmor;
    public static Action<CollisionProj, float> damageHappen;
    public static Action<CollisionProj, float> damageMindHappen;
    public static Action<CollisionProj> collisionProjAfterStatic;
    private MindHPSystem mindHPSystem;
    CollisionProj collisionTransferObject;
    private Invoker invoker;
    private string hurtSoundDefault = "hurt1";
    private string hurtSoundMindDefault = "hurt2";
    public List<Func<CollisionProj, bool>> CollisionInterruptors = new List<Func<CollisionProj, bool>>();
    

    public ProjectilData LastCollidedProjData {
        get {
            return lastCollidedProjData;
        }

        set {
            lastCollidedProjData = value;
        }
    }

    public bool IgnoreCollision {
        get {
            return ignoreCollision;
        }

        set {
            ignoreCollision = value;
        }
    }

    public bool EnemySide {
        get {
            return enemySide;
        }

        set {
            enemySide = value;
        }
    }

    public bool DuringDamageProtection { get; internal set; }

    public string HurtSoundDefault {
        get {
            return hurtSoundDefault;
        }

        set {
            hurtSoundDefault = value;
        }
    }

    public ActorStats ActorStats {
        get {
            return actorStats;
        }

        set {
            actorStats = value;
        }
    }

    public GameObject LastAttackingProjOwner { get; private set; }

    public string HurtSoundMindDefault {
        get {
            return hurtSoundMindDefault;
        }

        set {
            hurtSoundMindDefault = value;
        }
    }





    // Use this for initialization
    void Start() {
        damageReaction = GetComponent<DamageReaction>();
        hpSystem = GetComponent<HPSystem>();
        mindHPSystem = GetComponent<MindHPSystem>();
        movementLinearCtrl = GetComponent<MovementLinearCtrl>();
        superArmor = GetComponent<SuperArmor>();
        invoker = GetComponent<Invoker>();
    }

    // Update is called once per frame
    void Update() {

    }

    private void OnTriggerEnter(Collider col) {
        OnTriggerEnter2(col);
    }

    public void OnTriggerEnter2(Collider col) {
        
        if ((col.gameObject.tag.Equals("HeroProj") && EnemySide) ||
            (col.gameObject.tag.Equals("EnemyProj") && !EnemySide)
            ) {
            ProjectilData pD = col.GetComponent<ProjectilData>();
            CollideProjectilData(pD);
        }
    }

    public void CollideProjectilData(ProjectilData pD) {

        if (this == null) {
            return;
        }
        if (IgnoreCollision) return;

        if (collisionTransferObject != null) {
            collisionTransferObject.CollideProjectilData(pD);
            return;
        }


        if (pD != null) {
            
            if (pD.IgnoreCollision)
                return;
            if (pD.CollidedWith.Contains(gameObject) && !pD.MultiHit) {
                return;
            }
            if(pD.SpecialCollisionCase != null) {
                pD.SpecialCollisionCase(pD, gameObject);
                return;
            }
            if (damageReaction.isDamageProtectionTime() && DuringDamageProtection) {
                return;
            }

            foreach(var bifurquer in CollisionInterruptors) {
                bool interrupt = bifurquer(this);
                if(interrupt)
                    return;
            }
            
            

            LastAttackingProjOwner = pD.Owner;
             
            var soundName = pD.ImpactSound;
            if (soundName != null) {
                SoundSingleton.Instance.PlaySound(soundName);
            }
            //SoundSingleton.Instance.PlaySound("hurt1");

            var pcd = pD.gameObject.GetComponent<ProjectilContinuousDamage>();
            if (pcd != null)
                pcd.CollideWith(this);

            pD.CollidedWith.Add(gameObject);

            lastCollidedProjData = pD;
            //life--;

            var damage = pD.Damage;
            var damageMind = pD.DamageMind;
            //Canvas c;
            //c.worldCamera
            if(damage >= damageMind)
                SoundSingleton.Instance.PlaySound(HurtSoundDefault);
            else
                SoundSingleton.Instance.PlaySound(HurtSoundMindDefault);
            CentralResource.Instance.CameraControl.Shake(
                0.1f,
                new Vector3(0.5f, 0.5f),
                15,//(int)(duration*20), 
                UnityEngine.Random.Range(20, 80));
            damage *= 1/actorStats.GetDefenseMultiplier();
            damageMind *= 1/actorStats.GetDefenseMultiplier();

            var objs = pD.ActivateOnImpact;
            for (int i = 0; i < objs.Count; i++) {
                objs[i].SetActive(true);
                pD.activateOnImpactCallback(pD.gameObject, objs[i]);
                objs[i].transform.SetParent(null);
            }

            hpSystem.DealDamage(damage);
            if (mindHPSystem != null)
                mindHPSystem.DealDamage(damageMind);

            if (damageMindHappen != null) {
                damageMindHappen(this, damageMind);
            }
            if (damageHappen != null) {
                damageHappen(this, damage);
            }
            bool interruptActive = !superArmor.SuperArmorActive;
            bool knockbackActive = !superArmor.SuperArmorActive;

            CentralResource.Instance.TimeScaleController.ChangeTime(0.2f, 0.0001f);

            if (interruptActive) {
                damageReaction.DefaultDamage();
            }

            if (knockbackActive) {
                Vector3 knockback = pD.KnockbackNormal;
                if (pD.KnockbackOnDistanceDirection != 0) {
                    //var body = pD.GetComponent<Rigidbody>();
                    //knockback = body.velocity.normalized * pD.KnockbackOnSpeed;
                    var magnitude = pD.KnockbackOnDistanceDirection;
                    var dis = transform.position - pD.transform.position;
                    dis.y = 0;
                    knockback += dis.normalized * magnitude;
                    //knockback.y = 0;
                }
                if (pD.KnockbackOnSpeed != 0) {
                    var body = pD.GetComponent<Rigidbody>();
                    var velocity = body.velocity;
                    velocity.y = 0;
                    knockback += velocity.normalized * pD.KnockbackOnSpeed;
                    //knockback.y = 0;
                }

                float moveTime = damageReaction.CurrentMoveTime();

                if (pD.KnockbackMoveDuration > 0) {
                    moveTime = pD.KnockbackMoveDuration;
                }

                movementLinearCtrl.Velocity = knockback;
                if (knockback.sqrMagnitude != 0) {
                    Quaternion newRotation = Quaternion.LookRotation(-knockback);
                    this.transform.rotation = newRotation;
                }
                movementLinearCtrl.Disable(moveTime);
                movementLinearCtrl.enabled = true;
            }



            if (pD.DestroyOnImpact)
                Destroy(pD.gameObject);
            if (collisionProjAfter != null)
                collisionProjAfter();
            if (collisionProjAfterStatic != null)
                collisionProjAfterStatic(this);

        }

    }

    internal void StopCollisionsForATime(float v) {
        ignoreCollision = true;
        
        invoker.AddAction(ignoreCollisionDisable, v);
    }

    internal void ignoreCollisionDisable() {
        ignoreCollision = false;
    }

    internal void ChangeSide() {
        EnemySide = !EnemySide;
    }

    internal void HeroSide() {
        EnemySide = false;
        gameObject.layer = LayersNumber.HERO;
    }

    internal void EnemySideSet() {
        EnemySide = true;
        gameObject.layer = LayersNumber.ENEMY;
    }

    internal void SetCollisionTransferObject(GameObject judas) {

        if (judas == null) collisionTransferObject = null;
        else
            collisionTransferObject = judas.GetComponent<CollisionProj>();
    }
}
