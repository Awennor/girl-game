﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CutsceneStartIfVariable{

    VariableEnum variableEnum;

    public CutsceneStartIfVariable(CutsceneSystem cutsceneSystem) {
        cutsceneSystem.OnCutsceneConditionalStart += CutsceneSystem_OnCutsceneConditionalStart;
    }

    internal void RequestCutscene(VariableEnum cutsceneVariable) {
        variableEnum = cutsceneVariable;
    }

    private void CutsceneSystem_OnCutsceneConditionalStart(CutsceneComp obj) {
        if(obj.GetComponent<CutsceneVariableAntiRepetition>().VariableEnum == variableEnum) {
            obj.CutsceneStartExternal();

        }
    }

    public VariableEnum VariableEnum {
        get {
            return variableEnum;
        }

        set {
            variableEnum = value;
        }
    }
}
