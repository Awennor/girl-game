﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EncounterManager_EnemySummoner : MonoBehaviour {
    private EncounterManager encounterManager;

    public MyGameObjectEvent EnemyInstantiatedBattle;
    public UnityEventSequence OnSummonFinish;
    private int spawnIndex_Regular;
    private int spawnIndex_OnWave;
    private int waveId;

    public GameObjectProviderService enemyService;


    // Use this for initialization
    void Start() {
        encounterManager = GetComponent<EncounterManager>();

    }

    public void SummonEnemiesStart() {
        spawnIndex_Regular = 0;
        spawnIndex_OnWave = 0;
        waveId = 0;
        SummonOneEnemy();
    }

    public void SummonNextWave() {
        waveId++;
        spawnIndex_OnWave = 0;
        var encounterSingle = encounterManager.LastEncounter;
        if (encounterSingle.NormalWaves.Count > waveId) {
            encounterManager.SummoningEnemies_External();
            SummonOneEnemy();
        } else {
            encounterManager.NoMoreEnemiesToSummon_External();
        }

    }

    public void SummonEnemyContinue() {
        SummonOneEnemy();
    }

    private void SummonOneEnemy() {

        var encounterSingle = encounterManager.LastEncounter;
        var spawns = encounterSingle.SpawnPoints;
        int i = spawnIndex_Regular;
        if (i >= spawns.Count) {
            SummonOneEnemy_Wave();

            return;
        }

        var group = encounterManager.LastGroup;
        var enemiesEncounter = encounterSingle.EnemiesRandom;

        var fixedEncounters = encounterSingle.FixedEncounters;
        bool fixedEncounter;



        SoundSingleton.Instance.PlaySound("darktouch2");
        fixedEncounter = false;

        for (int j = 0; j < fixedEncounters.Length; j++) {

            if (fixedEncounters[j].SpawnPoint == spawns[i]) {

                int enemyId = fixedEncounters[j].EnemyWhich;

                //Debug.Log(t.localPosition);
                //int enemyId = enemiesEncounter[fixedEncounters[j].EnemyWhich];

                Enemysummon(group, spawns, i, enemyId);

                fixedEncounter = true;
                //CentralResource.Instance.BattleFlowSys.AddEnemy();
                break;
            }

        }
        if (!fixedEncounter) {

            int enemyId = enemiesEncounter[UnityEngine.Random.Range(0, enemiesEncounter.Length)];

            Enemysummon(group, spawns, i, enemyId);
        }
        spawnIndex_Regular++;

    }

    private void SummonOneEnemy_Wave() {
        var encounterSingle = encounterManager.LastEncounter;
        var group = encounterManager.LastGroup;
        if (encounterSingle.NormalWaves.Count > waveId) {
            var wave = encounterSingle.NormalWaves[waveId];
            var fixedEs = wave.Fixedenemies;
            if (fixedEs.Count > spawnIndex_OnWave) {
                var fixedE = fixedEs[spawnIndex_OnWave];
                EnemySummon(group, fixedE.EnemyWhich, fixedE.EncounterPosition.transform);
                spawnIndex_OnWave++;
                SoundSingleton.Instance.PlaySound("darktouch2");
            } else {
                OnSummonFinish.Invoke();
            }
        } else {

            return;
        }

    }

    private void Enemysummon(EncounterGroup group, System.Collections.Generic.List<GameObject> spawns, int i, int enemyId) {
        Transform t = spawns[i].transform;
        EnemySummon(group, enemyId, t);
    }

    private void EnemySummon(EncounterGroup group, int enemyId, Transform t) {
        //var prefab = group.Enemies[enemyId];
        var enemy = enemyService.Provider(enemyId);
        enemy.transform.SetParent(encounterManager.EnemyParent.transform);
        enemy.transform.position = t.position;
        EnemyInstantiatedBattle.Invoke(enemy);
    }

    // Update is called once per frame
    void Update() {

    }
}
