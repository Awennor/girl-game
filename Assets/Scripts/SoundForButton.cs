﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundForButton : MonoBehaviour {

    public string soundName;

	// Use this for initialization
	void Start () {
	    GetComponent<Button>().onClick.AddListener(()=> {
            SoundSingleton.Instance.PlaySoundOnPause(soundName);
        });
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
