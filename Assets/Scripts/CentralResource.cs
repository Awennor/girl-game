﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class CentralResource{

    static CentralResource instance;
    GameObject projectilImpactGraphics;
    SensorProjProvider sensorProjProvider;
    ProjectilHolder projectilHolder;
    List<GameObject> waitingForImpactGraphics = new List<GameObject>();
    TargetChoosingSystem targetChoosingSystem;
    BattleFlowSystem battleFlowSys = new BattleFlowSystem();
    CameraControl cameraControl;
    SkillInventoryDeckManagement skillInventoryDeckManagement = new SkillInventoryDeckManagement();
    InputPlayerStatus_Movement inputPlayer = new InputPlayerStatus_Movement();
    UpdateSupport updateSupport;
    GameObject hero;
    LevelChangeSystem levelChangeSystem = new LevelChangeSystem();
    InfoConfirmAccess infoConfirmAccess = new InfoConfirmAccess();
    PersistenceSaveSlots saveSlots = new PersistenceSaveSlots();
    TransitionOverlay transitionOverlay;
    VariableSystem variableSystem = new VariableSystem();
    CutsceneSystem cutsceneSystem = new CutsceneSystem();
    CutsceneStartIfVariable cutsceneVar;
    LocalizationHolder localizationHolder;
    ScriptedEncounterIdStarter scriptedEncounIDStart = new ScriptedEncounterIdStarter();
    OverworldRandomEncounter overworldRandomEncounter = new OverworldRandomEncounter();
    TimeScaleController timeScaleController;
    

    internal void FeedLocalizationDataOld(LocalizationDataDefinition localizationData) {
        localizationInfo = new LocalizationInfo(localizationData);

    }

    internal void FeedLocalizationData(LocalizationDataDefine[] localizationDatas) {
        LocalizationHolder = new LocalizationHolder(localizationDatas);
    }

    HeroDamageTracker heroDamageTracker;
    BattleDeathTracker battleDeathTracker;
    AimingInfoHolder aimingInfo = new AimingInfoHolder();
    LocalizationInfo localizationInfo;
    private CutsceneHolder cutsceneHolder;

    public CentralResource() {
        CutsceneVar = new CutsceneStartIfVariable(cutsceneSystem);
        HeroDamageTracker = new HeroDamageTracker(this);
        BattleDeathTracker = new BattleDeathTracker(this);
        saveSlots.dataLoad += DataLoaded;
        var skillInvent = saveSlots.SaveSlots.GetSlot(0).skillInvenDeckData;
        skillInventoryDeckManagement.SkillInvenDeckData = skillInvent;
        //Debug.Log("CR create");
    }

    private void DataLoaded() {
        //Debug.Log("Data loaded");
        var skillInvent = saveSlots.SaveSlots.GetSlot(0).skillInvenDeckData;
        skillInventoryDeckManagement.SkillInvenDeckData = skillInvent;
    }

    public static CentralResource Instance
    {
        get
        {

            if (instance == null) instance = new CentralResource();
            return instance;
        }

        set
        {
            instance = value;
        }
    }


    public SensorProjProvider SensorProjProvider
    {
        get
        {
            return sensorProjProvider;
        }

        set
        {
            sensorProjProvider = value;
        }
    }

    public ProjectilHolder ProjectilHolder
    {
        get
        {
            return projectilHolder;
        }

        set
        {
            projectilHolder = value;
        }
    }

    public TargetChoosingSystem TargetChoosingSystem {
        get {
            return targetChoosingSystem;
        }

        set {
            targetChoosingSystem = value;
        }
    }

    public BattleFlowSystem BattleFlowSys {
        get {
            return battleFlowSys;
        }

        set {
            battleFlowSys = value;
        }
    }

    public CameraControl CameraControl {
        get {
            return cameraControl;
        }

        set {
            cameraControl = value;
        }
    }

    public SkillInventoryDeckManagement SkillInventoryDeckManagement {
        get {
            return skillInventoryDeckManagement;
        }

        set {
            skillInventoryDeckManagement = value;
        }
    }

    public InputPlayerStatus_Movement InputPlayerMovement {
        get {
            return inputPlayer;
        }

        set {
            inputPlayer = value;
        }
    }

    internal UpdateSupport UpdateSupport {
        get {
            return updateSupport;
        }

        set {
            updateSupport = value;
            updateSupport.Updates += inputPlayer.Update;
        }
    }

    public GameObject Hero {
        get {
            return hero;
        }

        set {
            hero = value;
            heroDamageTracker.Hero = hero;
        }
    }

    public LevelChangeSystem LevelChangeSystem {
        get {
            return levelChangeSystem;
        }

        set {
            levelChangeSystem = value;
        }
    }

    internal InfoConfirmAccess InfoConfirmAccess {
        get {
            return infoConfirmAccess;
        }

        set {
            infoConfirmAccess = value;
        }
    }

    public PersistenceSaveSlots SaveSlots {
        get {
            return saveSlots;
        }

        set {
            saveSlots = value;
        }
    }

    public FadeHolder FadeHolder { get; internal set; }

    public HeroDamageTracker HeroDamageTracker {
        get {
            return heroDamageTracker;
        }

        set {
            heroDamageTracker = value;
        }
    }

    public BattleDeathTracker BattleDeathTracker {
        get {
            return battleDeathTracker;
        }

        set {
            battleDeathTracker = value;
        }
    }

    public AimingInfoHolder AimingInfo {
        get {
            return aimingInfo;
        }

        set {
            aimingInfo = value;
        }
    }

    public LocalizationInfo LocalizationInfo {
        get {
            return localizationInfo;
        }

        set {
            localizationInfo = value;
        }
    }

    public TransitionOverlay TransitionOverlay {
        get {
            return transitionOverlay;
        }

        set {
            transitionOverlay = value;
        }
    }

    internal VariableSystem VariableSystem {
        get {
            return variableSystem;
        }

        set {
            variableSystem = value;
        }
    }

    internal CutsceneSystem CutsceneSystem {
        get {
            return cutsceneSystem;
        }

        set {
            cutsceneSystem = value;
        }
    }

    public LocalizationHolder LocalizationHolder {
        get {
            return localizationHolder;
        }

        set {
            localizationHolder = value;
        }
    }

    internal ScriptedEncounterIdStarter ScriptedEncounterIdStarter {
        get {
            return scriptedEncounIDStart;
        }

        set {
            scriptedEncounIDStart = value;
        }
    }

    public CutsceneStartIfVariable CutsceneVar {
        get {
            return cutsceneVar;
        }

        set {
            cutsceneVar = value;
        }
    }

    internal OverworldRandomEncounter OverworldRandomEncounter {
        get {
            return overworldRandomEncounter;
        }

        set {
            overworldRandomEncounter = value;
        }
    }

    public OverworldInput OverworldInput { get; internal set; }

    internal CutsceneHolder CutsceneHolder {
        get {
            return cutsceneHolder;
        }

        set {
            cutsceneHolder = value;
        }
    }

    public TimeScaleController TimeScaleController {
        get {
            return timeScaleController;
        }

        set {
            timeScaleController = value;
        }
    }


    /*
public HeroDamageTracker HeroDamageTracker {
get {
return heroDamageTracker;
}

set {
heroDamageTracker = value;
}
}
*/
}
