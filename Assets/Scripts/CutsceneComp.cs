﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

public class CutsceneComp : MonoBehaviour {

    public CutsceneNodeStartCondition startCondition;
    public GameObject trigger;
    [SerializeField]
    Camera sceneCamera;
    [SerializeField]
    private CutsceneNode[] nodes;
    Boolean cutsceneStarted = false;
    public UnityEvent cutsceneEnd;
    public Transform heroPosEnd;

    public void OnDrawGizmos() {
        Gizmos.DrawCube(transform.position, new Vector3(30, 0.1f, 30));
    }

    public CutsceneNode[] Nodes {
        get {
            return nodes;
        }

        set {
            nodes = value;
        }
    }


    public Camera SceneCamera {
        get {
            return sceneCamera;
        }

        set {
            sceneCamera = value;
        }
    }


    // Use this for initialization
    void Start() {
        if (trigger != null && startCondition == CutsceneNodeStartCondition.Trigger) {
            trigger.AddComponent<EventTriggerCollider>().u2.AddListener(CutsceneStart);
        }
        if (startCondition == CutsceneNodeStartCondition.StartMethod) {
            //Debug.Log("Cutscene Start...");
            CutsceneStart();
        }
        if (startCondition == CutsceneNodeStartCondition.ConditionalStartMethod) {
            CentralResource.Instance.CutsceneSystem.ConditionalCutsceneStartTry(this);
        }
    }

    public void CutsceneStartExternal() {
        CutsceneStart();
    }

    private void CutsceneStart() {
        if(!cutsceneStarted) {
            cutsceneStarted = true;
            CentralResource.Instance.CutsceneSystem.CutsceneStart(this);
        }
            
    }

    // Update is called once per frame
    void Update() {

    }

    internal void CutsceneEnd() {
        if(heroPosEnd) {
            CentralResource.Instance.Hero.transform.position = heroPosEnd.position;
        }
        cutsceneEnd.Invoke();
        
    }

    public void AddNode(CutsceneNode cn) {
        var ns = nodes;
        nodes = new CutsceneNode[nodes.Length+1];
        Array.Copy(ns, nodes, ns.Length);
        nodes[nodes.Length-1] = cn;
    }
}

[Serializable]
public class CutsceneNode {
    [SerializeField]
    CutsceneNodeType type;
    [SerializeField]
    MyStringArray strings;
    [SerializeField]
    MyIntArray ints;
    [SerializeField]
    MyFloatArray floats;
    [SerializeField]
    MyGameObjectArray gameObjects;
    [SerializeField]
    MyColorObjectArray colors;
    [SerializeField]
    MyBoolArray bools;

    public UnityEvent unityEvent;

    public void SetString(string v1, int v2) {
        Strings[v2] = v1;
    }

    public string GetString(int v) {

        return Strings[v];
    }

    public CutsceneNodeType Type {
        get {
            return type;
        }

        set {
            type = value;
        }
    }




    public MyGameObjectArray GameObjects {
        get {
            return gameObjects;
        }

        set {
            gameObjects = value;
        }
    }

    public MyIntArray Ints {
        get {
            return ints;
        }

        set {
            ints = value;
        }
    }

    public MyFloatArray Floats {
        get {
            return floats;
        }

        set {
            floats = value;
        }
    }

    public MyColorObjectArray Colors {
        get {
            return colors;
        }

        set {
            colors = value;
        }
    }

    public MyStringArray Strings {
        get {
            return strings;
        }

        set {
            strings = value;
        }
    }

    public MyBoolArray Bools {
        get {
            return bools;
        }

        set {
            bools = value;
        }
    }
}

public enum CutsceneNodeType {
    TextMessage, AnimationTrigger, FadeIn, FadeOut, FadeTo, Delay,
    GameObjectActive, MoveToFrom, MoveToAnimatedCharacter, SimultaneousExecution, CallMethod,
    RotateActorDefault,
    FaceTarget, ShowEmail, ShowEffect, MoveTo, MoveAndRotateTo, ShowArticle,
}
public enum CutsceneNodeStartCondition {
    StartMethod, Trigger, Method, ConditionalStartMethod,
}