﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class LevelChangeSystem {
    GameObject currentLevel;
    int spawnPoint;

    public GameObject CurrentLevel {
        get {
            return currentLevel;
        }

        set {
            if(currentLevel != null)
                GameObject.Destroy(currentLevel);
            currentLevel = value;
        }
    }

    public int SpawnPoint {
        get {
            return spawnPoint;
        }

        set {
            spawnPoint = value;
        }
    }
}
