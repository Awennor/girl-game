﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class BuffSkillsExecuter {
    GameObject obj;
    private SkillHelper skillHelp;

    public BuffSkillsExecuter(GameObject obj) {
        this.obj = obj;
        skillHelp = obj.GetComponent<SkillHelper>();
        var aInput = skillHelp.AttackInput;
        var skillsBuff = SkillInfoProvider.Instance.SkillsBuff;
        for (int i = 0; i < skillsBuff.Length; i++) {
            var skillId = skillsBuff[i].EnumV;
            aInput.SetAttackMethod(skillId, StartSkill);
        }
        
    }

    private void StartSkill() {
        skillHelp.AttackState(0.5f);
        skillHelp.AnimationTrigger("spellcharge");
        DOVirtual.DelayedCall(0.5f, DoBuff);
        DOVirtual.DelayedCall(0.2f, chargeEffects);
        skillHelp.StopMovement();
        
        skillHelp.BlockAttacking(0.52f);
        
    }

    private void chargeEffects() {
        EffectSingleton.Instance.ShowEffectHere("charge0.3s", obj.transform);
        SoundSingleton.Instance.PlaySound("charge0.3s");
    }

    private void DoBuff() {
        skillHelp.AnimationTrigger("spelluse");
        skillHelp.AnimationTrigger("normalslow", 0.1f);
        SoundSingleton.Instance.PlaySound("chargerelease1");
        EffectSingleton.Instance.ShowEffectHere("chargerelease", obj.transform, new Vector3(0,5,0));
        //Debug.Log("Doing buff");
        var skill = skillHelp.CurrentSkill;
        skillHelp.ApplyBuffBattle(skill);
    }

    public GameObject Obj {
        get {
            return obj;
        }

        set {
            obj = value;
        }
    }
}
