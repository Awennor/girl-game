﻿using UnityEngine;
using System.Collections;
using System;

public class Heal : MonoBehaviour {

    public GameObject healEffectPrefab;
    private SkillHelper skillHelper;
    private HPSystem hp;

    // Use this for initialization
    void Start () {
        var attackInput = GetComponent<AttackInput>();
        skillHelper = GetComponent<SkillHelper>();

        hp = GetComponent<HPSystem>();
        attackInput.SetAttackMethod(
            (int)SkillInfoProvider.SkillId.Heal, doHeal);
        attackInput.SetAttackMethod(
            (int)SkillInfoProvider.SkillId.Heal2, doHeal);
    }


    private void doHeal()
    {
        
        skillHelper.HealSelf();
        skillHelper.InstantiateEffectAndParentIt(healEffectPrefab, 
            this.transform.position);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
