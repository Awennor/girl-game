﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using Assets.Scripts.LogicScript;
using System;

public class playershooting : MonoBehaviour {

    public GameObject bullet;
    public Transform target;
    public Animator anim;                      // Reference to the animator component.
    Rigidbody rigidBody;
    Invoker invoker;
    ATBManager atb;
    int bullets = 0;
    private ClosestTarget closestTarget;
    [SerializeField]
    GameObject shootingSource;
    [SerializeField]
    SkillShootingImplementationData[] implementationDatas;

    private SkillHelper skillHelper;
    private int skill;

    // Use this for initialization
    void Start() {

        var control = GetComponent<ControllableObject>();
        control.AddInputScript(this);

        invoker = GetComponent<Invoker>();
        rigidBody = GetComponent<Rigidbody>();
        atb = MainDataGirl.Instance.MainCharaBattle.Atb;
        closestTarget = GetComponent<ClosestTarget>();

        skillHelper = GetComponent<SkillHelper>();

        var attackInput = GetComponent<AttackInput>();

        for (int i = 0; i < implementationDatas.Length; i++) {
            attackInput.SetAttackMethod(implementationDatas[i].skillId, startShoot);
        }

    }

    private bool IsCorrectSkill(int currentS) {
        for (int i = 0; i < implementationDatas.Length; i++) {
            
            if(currentS == (int) implementationDatas[i].skillId) return true;
        }
        return false;
    }

    internal void IncreaseBullets() {
        bullets += 3;
    }

    // Update is called once per frame
    void Update() {
            }

    private void startShoot() {
        if (closestTarget.ChosenTarget != null)
            target = closestTarget.ChosenTarget.transform;
        else
            target = null;
        skillHelper.AttackStateWithMove(0.22f);
        //skillHelper.AttackState(2f);
        //skillHelper.BlockAttacking(0.11f);
        //skillHelper.ZeroSpeed();
        skill = skillHelper.CurrentSkill;
        bullets--;
        //EffectSingleton.Instance.ShowAndAddEffect("charge2", shootingSource.transform);
        //anim.SetTrigger("shoot1pre");
        //invoker.AddAction(Shoot, 0.1f);
        
        Shoot();
         anim.SetTrigger("shoot1"); 
        //invoker.AddAction(() => { anim.SetTrigger("shoot1"); }, 0.1f);
        FaceTarget();

    }

    internal void NoBullets() {
        bullets = 0;
    }

    void Shoot() {
        SkillShootingImplementationData data = GetImplementationData();
        int projCount = data.projCount;
        SoundSingleton.Instance.PlaySound("shoot1");
        Rigidbody clone;
        Vector3 p = transform.position;
        p.y += 5;
        p += transform.forward*5;
        EffectSingleton.Instance.ShowEffectHere("shoteffect", p);
        skillHelper.AnimationTrigger("normalslow", 0.1f);
        //skillHelper.AttackState(0.05f);
        skillHelper.BlockAttacking(0.05f);
        for (int i = 0; i < projCount; i++) {
            //clone = ((GameObject)Instantiate(bullet, p, transform.rotation)).GetComponent<Rigidbody>();
            
            var obj = skillHelper.GetProj(data.proj, (SkillInfoProvider.SkillId)skill, 2.5f);
            
            obj.transform.position = p;
            clone = obj.GetComponent<Rigidbody>();
            var pd = clone.GetComponent<ProjectilData>();
            skillHelper.Damages(pd, skill);
            pd.Damage *= 1f/projCount;
            pd.DamageMind *= 1f/projCount;

            pd.DestroyOnImpact = true;
            pd.ImpactEffectId = 1;
            pd.KnockbackOnSpeed = 60;
            pd.KnockbackMoveDuration = 0.1f;



            clone.gameObject.tag = "HeroProj";
            FaceTarget();

            var r = this.transform.rotation;
            r *= Quaternion.Euler(
                RandomSpecial.randomFloatOnZero(data.RandomAngleVariationOnSpeed*0.25f),
                RandomSpecial.randomFloatOnZero(data.RandomAngleVariationOnSpeed),
                RandomSpecial.randomFloatOnZero(data.RandomAngleVariationOnSpeed*0.25f));
            clone.transform.rotation = r;

            var speed = data.speed + RandomSpecial.randomFloatOnZero(data.speedRandomVariation) + i * data.speedVariationPerProj;

            clone.velocity = r * Vector3.forward * speed;
        }

        //clone.velocity = v.normalized * 50;
    }

    private SkillShootingImplementationData GetImplementationData() {
        for (int i = 0; i < implementationDatas.Length; i++) {
            if ((int)implementationDatas[i].skillId == skill) {
                return implementationDatas[i];
            }
        }
        return null;
    }

    private Vector3 FaceTarget() {
        Vector3 v = transform.position;
        //skillHelper.FaceTarget();
        return v;
        if (target != null)
            v = target.position - v;
        Quaternion newRotation = Quaternion.LookRotation(v);
        //rigidbody.MoveRotation(newRotation);
        //rigidBody.MoveRotation(newRotation);
        transform.rotation = newRotation;
        return v;
    }

    [System.Serializable]
    class SkillShootingImplementationData {

        [SerializeField]
        public int projCount = 1;
        [SerializeField]
        public float RandomAngleVariationOnSpeed;
        [SerializeField]
        public SkillInfoProvider.SkillId skillId;
        [SerializeField]
        public ProjectilHolder.Projs proj;
        [SerializeField]
        public float speed = 50;
        [SerializeField]
        public float speedRandomVariation;
        [SerializeField]
        public float speedVariationPerProj;
    }


}
