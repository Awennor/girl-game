﻿using UnityEngine;
using System.Collections;
using System;
using Assets.ReusablePidroh.Scripts;

public class HeroBlast : SkillBehaviour{

    private SkillInfoProvider.SkillId skill;

    void Start() {
        base.skillHelperInit();
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.HeroExplosion, ExplodeStart);
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.MindHExplosion, ExplodeStart);
    }


    void ExplodeStart() {
        
        skillHelper.AnimationTrigger("blastready");
        
        skillHelper.StopMovement();
        const float explodeDelay = 0.56f;
        skillHelper.AnimationTrigger("blastdo", explodeDelay-0.167f);
        skillHelper.DelayedMethod(ActualBombing, explodeDelay);
        
        skillHelper.AttackState(explodeDelay+0.24f);
        skillHelper.AnimationTrigger("normal", explodeDelay+0.25f);
        skill = skillHelper.GetCurrentSkill();
    }

    private void ActualBombing() {
        var proj2 = skillHelper.GetProj(ProjectilHolder.Projs.EnemyBlast, skill, 0.3f);
        SoundSingleton.Instance.PlaySound("explosion");
        proj2.GetComponent<ProjectilData>().KnockbackOnDistanceDirection = 70;
        proj2.GetComponent<ProjectilData>().KnockbackMoveDuration = 0.2f;
    }




    // Update is called once per frame
    void Update () {

	}
}
