﻿using UnityEngine;
using System.Collections;
using System;
using Assets.ReusablePidroh.Scripts;

public class Bomb : SkillBehaviour{

    public float speed;
    public float speedY;
    private SkillInfoProvider.SkillId skill;

    void Start() {
        base.skillHelperInit();
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.Bomb, ThrowBomb);
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.MindBomb, ThrowBomb);
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.BombB, ThrowBomb);
        var chosenSkillHolder = GetComponent<ChosenSkillHolder>();
        //DrawLine();
        chosenSkillHolder.skillChange += (ChosenSkillHolder csh)=> {
            int currentS = csh.CurrentSkill;
            if(currentS == (int) SkillInfoProvider.SkillId.Bomb) {
                DrawLine();
            } else {
                var lineDraw = skillHelper.LineDrawManager;
                if(lineDraw.Skill == (int) SkillInfoProvider.SkillId.Bomb) {
                    lineDraw.Visible(false);
                }
            }
        };
    }

    private void DrawLine() {
        var lineDraw = skillHelper.LineDrawManager;
        if (lineDraw != null) {
            lineDraw.SkillShowing((int)SkillInfoProvider.SkillId.Bomb);
            lineDraw.UpdateTrajectory(new Vector3(0, 4, 0), new Vector3(speed, speedY), Physics.gravity, 0.2f);
            lineDraw.Visible(true);
        }
    }

    void ThrowBomb() {
        SoundSingleton.Instance.PlaySound("throw");
        skillHelper.StopMovement();
        skillHelper.AnimationTrigger("throwsetup");
        const float throwDelay = 0.1f;
        skillHelper.AnimationTrigger("throw", throwDelay);
        skillHelper.DelayedMethod(ActualBombing, throwDelay+0.05f);
        skillHelper.AttackState(throwDelay+0.24f);
        skillHelper.AnimationTrigger("normal", throwDelay+0.25f);
        skill = skillHelper.GetCurrentSkill();
    }

    private void ActualBombing() {
        
        GameObject obj = skillHelper.GetProj(ProjectilHolder.Projs.Bomb1, skill, 5f);
        
        skillHelper.DamagesDirect(obj, 0, 0);
        skillHelper.ImpactSound(obj, "explosion");
        
        obj.transform.position += new Vector3(0, 4, 0);

        ProjectilHolder.Projs enemyBlast = ProjectilHolder.Projs.EnemyBlast;
        if(skill == SkillInfoProvider.SkillId.BombB) {
            enemyBlast = ProjectilHolder.Projs.EnemyBlast2;
        }
        var proj2 = skillHelper.GetProj(enemyBlast, skill, 15f);

        proj2.SetActive(false);
        proj2.transform.SetParent(obj.transform);
        proj2.transform.position = obj.transform.position;
        skillHelper.AddImpactProj(obj, proj2);
        var r = this.transform.rotation;
        obj.transform.rotation = r;

        var rigidbody1 = obj.GetComponent<Rigidbody>();
        rigidbody1.useGravity = true;
        rigidbody1.velocity = r * Vector3.forward * speed + new Vector3(0, speedY);

        proj2.GetComponent<ProjectilData>().KnockbackOnDistanceDirection = 70;
        proj2.GetComponent<ProjectilData>().KnockbackMoveDuration = 0.2f;

        ProjectilData pD = obj.GetComponent<ProjectilData>();
        pD.activateOnImpactCallback += BlastOn;
        pD.DestroyOnImpact = true;

        skillHelper.ProjCollideWithGround(obj);
    }

    private void BlastOn(GameObject arg1, GameObject arg2) {
        Destroy(arg2, 0.3f);
    }



    // Update is called once per frame
    void Update () {

	}
}
