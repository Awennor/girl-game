﻿using UnityEngine;
using System.Collections;

public class TempThunderAttack : MonoBehaviour {
    private Animator animator;
    private SkillHelper skillHelper;
    public bool debugTest = false;

    // Use this for initialization
    void Start() {
        animator = GetComponentInChildren<Animator>();
        skillHelper = GetComponent<SkillHelper>();

        var attackInput = GetComponent<AttackInput>();
        attackInput.SetAttackMethod((int)SkillInfoProvider.SkillId.Lightning, doSkill);
        attackInput.SetAttackMethod((int)SkillInfoProvider.SkillId.LightningB, doSkill);
    }

    // Update is called once per frame
    void Update() {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.U) && debugTest) {
            doSkill();

            //get closest target
            //    spawn lightning right next to it

        }
        #endif
    }

    private void doSkill() {
        const float timeToNormal = 0.2f;
        skillHelper.StopMovement();
        skillHelper.SuperArmor(timeToNormal);
        skillHelper.AttackState(timeToNormal);
        //skillHelper.AnimationTrigger("summondownsetup");
        //skillHelper.DelayedMethod(SummonLightning,0.2f);
        SummonLightning();
        skillHelper.AnimationTrigger("normal", timeToNormal);
        //}
        //skillHelper.AttachProj(ProjectilHolder.Projs.Sword1, 4, mainObjectBone.transform, 3f / 24f);

    }

    private void SummonLightning() {
        skillHelper.AnimationTrigger("summondown");
        var closestTarget = skillHelper.ClosestTarget();
        //if (closestTarget != null) {
        SoundSingleton.Instance.PlaySound("thunder");
        GameObject proj = skillHelper.GetProj(ProjectilHolder.Projs.Lightning1, skillHelper.GetCurrentSkill(), 12f / 24f);
        proj.GetComponent<ProjectilData>().ImpactEffectId = 2;
        if (closestTarget == null)
            proj.transform.position = transform.position;
        else
            proj.transform.position = closestTarget.transform.position;
    }
}
