﻿using UnityEngine;
using System.Collections;
using System;

public class LaserAttack : SkillBehaviour{

    public GameObject attackHand;

	void Start() {
        base.skillHelperInit();
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.Laser, UseLaser);
        var chosenSkillHolder = GetComponent<ChosenSkillHolder>();
        //DrawLine();
        chosenSkillHolder.skillChange += (ChosenSkillHolder csh)=> {
            int currentS = csh.CurrentSkill;
            if(currentS == (int) SkillInfoProvider.SkillId.Bomb) {
                ///DrawLine();
            } else {
                /*var lineDraw = skillHelper.LineDrawManager;
                if(lineDraw.Skill == (int) SkillInfoProvider.SkillId.Bomb) {
                    lineDraw.Visible(false);
                }*/
            }
        };
    }
    ParticleSystem.Particle[] particles = new ParticleSystem.Particle[100];
    private void UseLaser() {
        skillHelper.AnimationTrigger("shoot1pre");
        EffectSingleton.Instance.ShowAndAddEffect("charge1.4s", attackHand.transform);
        const float timeCharge = 1.34f;
        SoundSingleton.Instance.PlaySound("charge4");
        skillHelper.DelayedMethod(ActualLaser, timeCharge);
        skillHelper.AnimationTrigger("shoot1b", timeCharge-0.1f);
        skillHelper.AttackState(timeCharge+1.2f);
        skillHelper.StopMovement();
        skillHelper.SuperArmor(timeCharge+1.2f);
        skillHelper.Zoom(0.65f, 1.33f);
        skillHelper.BlockAttacking(timeCharge+1.2f);
        //skillHelper.DelayedMethod(shootAnimation, 0.26f);
        
        //pD.DestroyOnImpact = true;


    }



    private void ActualLaser() {
        skillHelper.Zoom(1f, 0.1f);
        //skillHelper.ScreenShakeType1(0.4f);
        SoundSingleton.Instance.PlaySound("laser");
        var proj2 = skillHelper.GetProj(ProjectilHolder.Projs.Laser, SkillInfoProvider.SkillId.Laser, 2f);

        proj2.transform.rotation = this.transform.rotation;
        proj2.transform.position += new Vector3(0, 4, 0);
        ProjectilData pD = proj2.GetComponent<ProjectilData>();
        skillHelper.DelayedMethod(() => {
            if(proj2 == null)
                return;
            proj2.GetComponent<ParticleEnder>().EndParticles();
            pD.IgnoreCollision = true;
            //pE.startSpeed = 0;
        }, 1f);
        skillHelper.AnimationTrigger("normal", 1.2f);
        
        pD.ImpactEffectId = 1;
        pD.Damage *= 0.2f;
        pD.DamageMind *= 0.2f;
        pD.MultiHit = true;
        proj2.GetComponent<ProjectilContinuousDamage>().CollisionRepeatPeriod = 0.23f;

    }

    // Update is called once per frame


    void Update () {

	}
}
