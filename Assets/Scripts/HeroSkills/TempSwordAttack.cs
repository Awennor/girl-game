﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.spacepuppy.Collections;
using System;

[Serializable]
public class MyDicProjProjData : SerializableDictionaryBase<ProjectilHolder.Projs, ProjectilData> { };

public class TempSwordAttack : MonoBehaviour {
    public MyDicProjProjData swords;
    private Animator animator;
    private SkillHelper skillHelper;
    public GameObject mainObjectBone;
    private AttackInput attackInput;
    [SerializeField]
    SkillSwordImplementationData[] implementationDatas;
    int skill;
    public int debugDefaultSkill;
    
    // Use this for initialization
    void Start () {
        animator = GetComponentInChildren<Animator>();
        skillHelper = GetComponent<SkillHelper>();
        attackInput = GetComponent<AttackInput>();

        for (int i = 0; i < implementationDatas.Length; i++) {
            attackInput.SetAttackMethod(implementationDatas[i].skillId, doSkill);
        }
        
        
        /*for (int i = 1; i < 4; i++)
        {
            attackInput.SetAttackMethod(i, doSkill);
        }*/
	}
	
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Y)
            )
        {

            doSkill();
        }
#endif
    }

    private void doSkill() {
        //Time.timeScale = 0.05f;
        skill = skillHelper.CurrentSkill;
        skillHelper.StopMovement();
        var skillSword = GetImplementationData();
        if(skillSword == null) return;
        float d = skillSword.delay;
        skillHelper.AttackState(skillSword.slashTime+d+0.1f);
        skillHelper.BlockAttacking(skillSword.slashTime+d);
        
        if(d > 0) {
            skillHelper.AnimationTrigger(skillSword.animationTriggerCharge);
            skillHelper.DelayedMethod(d, Slash);
        } else
            Slash();
    }

    private SkillSwordImplementationData GetImplementationData() {
        for (int i = 0; i < implementationDatas.Length; i++) {
            if ((int)implementationDatas[i].skillId == skill) {
                return implementationDatas[i];
            }
        }
        if(debugDefaultSkill == -1) return null;
        return implementationDatas[debugDefaultSkill];
    }

    private void Slash() {
        //Time.timeScale = 0.3f;
        var skillSword = GetImplementationData();
        SoundSingleton.Instance.PlaySound("slash");
        
        ProjectilHolder.Projs projEnum = ProjectilHolder.Projs.Sword1;
        projEnum = skillSword.proj;

        float slashTime = skillSword.slashTime;
        skillHelper.AnimationTrigger("normalslow", slashTime+0.1f);
        animator.SetTrigger(skillSword.animationTrigger);
        var proj = swords[projEnum];
        proj.gameObject.SetActive(true);
        proj.IgnoreCollision = false;
        proj.DelayedIgnoreCollision(slashTime+0.1f);
        proj.ClearPreviousCollisions();
        proj.DelayedDeactivation(slashTime+0.1f);
        //var proj = skillHelper.AttachProj(sword1, skillHelper.GetCurrentSkill(), mainObjectBone.transform, slashTime+0.1f);
        skillHelper.PrepareProj(skillHelper.GetCurrentSkill(), proj);

        float velocity = skillSword.speedMove;
        var speedOfMove = skillHelper.MoveTowardsDirectionXZ(velocity, 2, skillSword.moveTime);
        skillHelper.projKnockback(proj.gameObject, speedOfMove.normalized * 160, 2f / 24f);
        
    }

    [System.Serializable]
    class SkillSwordImplementationData {

        [SerializeField]
        public SkillInfoProvider.SkillId skillId;
        [SerializeField]
        public string animationTrigger = "slash2";
        [SerializeField]
        public string animationTriggerCharge = "dashslashsetup";
        [SerializeField]
        public ProjectilHolder.Projs proj;
        [SerializeField]
        public float speedMove = 100;
        [SerializeField]
        public float moveTime = 3f/24f;
        [SerializeField]
        public float slashTime = -1;
        [SerializeField]
        public float delay = 0;
        
    }
}
