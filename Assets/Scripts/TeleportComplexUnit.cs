﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TeleportComplexUnit : MonoBehaviour {
    [SerializeField]
    private TeleportComplexUnit telepoTarget;
    [SerializeField]
    private TeleporterComplex telepoComplex;
    public MyTransformEvent actualTeleportTo;
    public UnityEvent receiveTeleportRequest;

    public void StartTeleportDone_External() {
        Debug.Log("START TELEPO DONE");
        telepoTarget.RequestTelepo();
        
    }

    private void RequestTelepo() {
        receiveTeleportRequest.Invoke();
    }

    public void ActualMoving() {
        actualTeleportTo.Invoke(transform);
        TelepoComplex.ActualMovingHappens(this);
    }

    internal TeleportComplexUnit TelepoTarget {
        get {
            return telepoTarget;
        }

        set {
            telepoTarget = value;
        }
    }

    public TeleporterComplex TelepoComplex {
        get {
            return telepoComplex;
        }

        set {
            telepoComplex = value;
        }
    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
