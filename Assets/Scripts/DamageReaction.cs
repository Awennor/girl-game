﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;

public class DamageReaction : MonoBehaviour {
    private Animator animator;
    private BattleStateMachine battleStateMachine;
    float defaultDamageTime = 0.5f;
    float defaultMoveTime = 0.27f;
    float defaultAnimation2Time = 0.18f;
    private Invoker invoker;
    float currentMoveTime;
    private bool damageCanHitAgain = true;


    // Use this for initialization
    void Start () {
        battleStateMachine = GetComponent<BattleStateMachine>();
        animator = GetComponentInChildren<Animator>();
        invoker = GetComponent<Invoker>();
    }

    public float DefaultDamage() {
        animator.SetTrigger("damage");
        battleStateMachine.ChangeState(BattleStateMachine.States.Damage,
            defaultDamageTime
            );
        damageCanHitAgain = false;
        invoker.AddAction(animationEnd,defaultAnimation2Time);
        invoker.AddAction(ProtectionEnd, defaultDamageTime+0.2f);
        currentMoveTime = defaultMoveTime;
        return defaultDamageTime;
    }

    private void ProtectionEnd() {
        damageCanHitAgain = true;
    }

    private void animationEnd()
    {
        animator.SetTrigger("damageend");
    }

    // Update is called once per frame
    void Update () {
	
	}

    internal float CurrentMoveTime() {
        return currentMoveTime;
    }

    internal bool isDamageProtectionTime() {
        return !damageCanHitAgain;
    }
}
