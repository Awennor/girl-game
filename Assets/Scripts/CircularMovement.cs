﻿using UnityEngine;
using System.Collections;
using System;
using Assets.ReusablePidroh.Scripts;

public class CircularMovement : MonoBehaviour {
    //private Rigidbody rigidBody;
    public Vector3 velocity = new Vector3();
    Vector3 velchanging = new Vector3();
    //float velocityModulus;
    float period = 2f;
    private Invoker invoker;
    private CharacterControllerWrapper charaCtrl;

    // Use this for initialization
    void Start () {
        //rigidBody = GetComponent<Rigidbody>();
        velchanging = velocity;
        invoker = GetComponent<Invoker>();
        charaCtrl = GetComponent<CharacterControllerWrapper>();
        velchanging = Quaternion.Euler(0, 360 * UnityEngine.Random.Range(0, 2f) / period, 0) * velchanging;
    }

    void FixedUpdate() {
        //Debug.Log("MESSING WITH VELOCITY");
        //rigidBody.velocity = new Vector3(200, 0, 0);
    }

	// Update is called once per frame
	void Update () {
        //rigidBody.AddForce(new Vector3(200, 0, 0));

        //Vector3 v = rigidBody.velocity;
        velchanging = Quaternion.Euler(0, 360 * Time.deltaTime / period, 0) * velchanging;
        charaCtrl.SetSpeed(velchanging);
        //rigidBody.velocity = velchanging;
        //Debug.Log("CIRCULAR VEL "+ rigidBody.velocity);
	
	}

    internal void renable(float v)
    {
        invoker.AddAction(Enable, v);
    }

    private void Enable()
    {
        enabled = true;
    }
}
