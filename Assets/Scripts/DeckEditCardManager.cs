﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class DeckEditCardManager : MonoBehaviour {

    public DeckEditCard unitMatrix;

    List<DeckEditCard> cards = new List<DeckEditCard>();

    int initialCardsLoaded = 40;
    public GameObject cardParent;
    private ScrollManager scrollManage;

    public MyIntEvent CardPressed;

    public Selectable DownwardSelectable;
    public Selectable LeftwardSelectable;
    public Selectable RightwardSelectable;

    // Use this for initialization
    void Awake () {
        scrollManage = GetComponent<ScrollManager>();
        for (int i = 0; i < initialCardsLoaded; i++) {
            NewCardUnit();

        }
    }

    private void NewCardUnit() {
        DeckEditCard unit = Instantiate(unitMatrix);
        unit.transform.SetParent(cardParent.transform);
        unit.transform.localScale = new Vector3(1, 1, 1);
        unit.gameObject.SetActive(true);
        int cardIndex = cards.Count;
        cards.Add(unit);
        //int cardIndex = i;
        unit.SkillInfo.GetComponent<SelectableWrapper>().Selected.AddListener(() => {
            Selected(unit);
        });
        unit.SkillInfo.Button.onClick.AddListener(()=> {
            CardPressed.Invoke(cardIndex);
        });
    }

    internal int SkillIdOfCardAtPos(int pos) {
        return (int)cards[pos].SkillInfo.Skill;
    }

    internal void CardAmount(int i, int amount) {
        string off = "x";
        if(amount < 10) {
            off = "x ";
        }
        cards[i].text.text = off+amount;
    }

    internal void ChangeSkill(int i, int skillId) {
        cards[i].SkillInfo.ChangeSkill((SkillInfoProvider.SkillId)skillId);
    }

    internal void CardsActive(int count) {
        while(cards.Count < count) {
            NewCardUnit();
        }
        int columnNumber =3;
        for (int i = 0; i < cards.Count; i++) {
            var cardActive = i < count;
            cards[i].gameObject.SetActive(cardActive);
            if(cardActive) {

                Selectable s = cards[i].SkillInfo.Button;
                var navi = s.navigation;
                navi.mode = Navigation.Mode.Explicit;
                if(i >= columnNumber) {
                    navi.selectOnUp = cards[i-columnNumber].SkillInfo.Button;
                }
                if(i % columnNumber != 0) {
                    navi.selectOnLeft = cards[i-1].SkillInfo.Button;
                } else {
                    navi.selectOnLeft = LeftwardSelectable;
                }
                if(i % columnNumber != columnNumber-1) {
                    navi.selectOnRight = cards[i+1].SkillInfo.Button;
                } else {
                    navi.selectOnRight = RightwardSelectable;
                }
                if(i + columnNumber < count) {
                    navi.selectOnDown = cards[i+columnNumber].SkillInfo.Button;
                } else {
                    navi.selectOnDown = DownwardSelectable;
                }
                s.navigation = navi;
            }
            
        }

        
    }

    private void Selected(DeckEditCard unit) {
        scrollManage.SnapTo(unit.GetComponent<RectTransform>());
    }

    internal void ShineCard(int cardpos) {
        cards[cardpos].SkillInfo.EffectInBackground1();
    }

    // Update is called once per frame
    void Update () {
	}

    internal void MakeSureToShowCardIn(int pos) {
        scrollManage.SnapTo(cards[pos].GetComponent<RectTransform>());
    }

    internal void CardWantsInteractionState(int i, bool v) {
        if(v) cards[i].SkillInfo.NormalColor();
        else cards[i].SkillInfo.ExtraColor();
    }
}
