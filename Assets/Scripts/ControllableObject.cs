﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ControllableObject : MonoBehaviour {

    List<MonoBehaviour> inputScripts = new List<MonoBehaviour>();
    List<MonoBehaviour> antiInputScripts = new List<MonoBehaviour>();
    private bool controlling;

    public bool ControllingFlag {
        get {
            return controlling;
        }

        set {
            controlling = value;
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AddInputScript(MonoBehaviour script) {
        inputScripts.Add(script);
    }
    public void AddAntiInputScript(MonoBehaviour script) {
        antiInputScripts.Add(script);
    }

    internal void Controlling(bool v)
    {
        ControllingFlag = v;
        for (int i = 0; i < inputScripts.Count; i++)
        {
            inputScripts[i].enabled = v;
        }
        for (int i = 0; i < antiInputScripts.Count; i++)
        {
            antiInputScripts[i].enabled = !v;
        }
    }
}
