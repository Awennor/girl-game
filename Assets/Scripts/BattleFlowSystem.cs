﻿using UnityEngine;
using System.Collections;
using System;

public class BattleFlowSystem {

    public event Action OnBattleStart;
    public event Action OnBattleEnd;
    public event Action OnBattleEnd_OnlyOnce;
    public event Action OnBattleEndUIClose_OnlyOnce;

    bool inBattle;
    int nEnemies;
    public Action<int> nEnemiesChange;
    bool abortedBattleNotVictory;
    bool heroDeath;

    public bool InBattle {
        get {
            return inBattle;
        }

        set {
            inBattle = value;
        }
    }

    public int NEnemies {
        get {
            return nEnemies;
        }

        set {

            nEnemies = value;
            if (nEnemiesChange != null)
                nEnemiesChange(nEnemies);
        }
    }

    public bool AbortedBattleNotVictory {
        get {
            return abortedBattleNotVictory;
        }

        set {
            abortedBattleNotVictory = value;
        }
    }

    public bool HeroDeath {
        get {
            return heroDeath;
        }

        set {
            heroDeath = value;
        }
    }

    public bool LatestBattleWon {
        get {
            return !AbortedBattleNotVictory;
        }
    }

    internal void BattleStarted() {
        inBattle = true;
        heroDeath = false;
        AbortedBattleNotVictory = false;
        if (OnBattleStart != null) {
            OnBattleStart();
        }

    }

    public void BattleEnded_BeforeResultUI() {
        inBattle = false;
        if (OnBattleEnd != null) {
            OnBattleEnd();
        }
        if (OnBattleEnd_OnlyOnce != null) {
            OnBattleEnd_OnlyOnce();
            OnBattleEnd_OnlyOnce = null;

        }

    }

    internal void BattleUIClose() {
        if(OnBattleEndUIClose_OnlyOnce != null) {
            OnBattleEndUIClose_OnlyOnce();
            OnBattleEndUIClose_OnlyOnce = null;
        }
            
    }
}
