
/// <summary>
/// Generates a safe wrapper for VariableEnum.
/// </summary>
public enum VariableEnum {
    /* to keep track of cutscenes */
    Handportal1Cutscene1 = 1001,
    Handportal1Cutscene2,
    Handportal1Cutscene3,
    Handportal1Cutscene4,
    Handportal1Cutscene5,

    /* old stuff*/
    handportal1 = 99991,
    battle1,
    boss1,
    tutorialdeckchangersdisappear,


}