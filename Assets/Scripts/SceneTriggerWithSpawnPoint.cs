﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
/*
    this loads a scene and also a spawn point
*/
public class SceneTriggerWithSpawnPoint : MonoBehaviour {

    public string sceneName;
    public bool ActivateOnTrigger = true;
    [SerializeField]
    private int spawnPoint;
    public bool graphicalTransition = true;
    

    void OnTriggerEnter(Collider col) {
        if (ActivateOnTrigger)
            LoadLevel();
    }

    public void LoadLevel() {
        if (!CentralResource.Instance.BattleFlowSys.InBattle) {
            DOVirtual.DelayedCall(0.1f, ActualScene);
            if(graphicalTransition)
                CentralResource.Instance.TransitionOverlay.SceneChangeStart();
        }

    }

    private void ActualScene() {

        CentralResource.Instance.SaveSlots.CurrentSlot.spawnPositionSave.SaveData(sceneName, spawnPoint);
        CentralResource.Instance.SaveSlots.SaveData();
        CentralResource.Instance.LevelChangeSystem.SpawnPoint = spawnPoint;
        Application.LoadLevelAdditive(sceneName);
        if(graphicalTransition)
            CentralResource.Instance.TransitionOverlay.SceneChangeEndRequest();

    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        
    }
}
