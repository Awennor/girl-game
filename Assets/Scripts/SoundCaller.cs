﻿using UnityEngine;
using System.Collections;

public class SoundCaller : MonoBehaviour {


    public void Play(string soundName) {
        SoundSingleton.Instance.PlaySound(soundName);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
