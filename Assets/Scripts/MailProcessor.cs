﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MailProcessor : MonoBehaviour {

    public EmailData emailData;
    public MyStringEvent date, sender, receiver, subject, body;
    public UnityEvent show;

    public void ShowEmail(string mailid) {
        
        gameObject.SetActive(true);
        SingleMail sM = emailData.GetMail(mailid);
        date.Invoke(string.Format("{0}/{1:00}/{2:00}", sM.Year, sM.Month, sM.Day));

        var mailBody = CentralResource.Instance.LocalizationHolder.MailBody(sM.Id);
        if(sM.SentFromMobile) {
            mailBody = string.Format("{0}\n\n\n{1}", mailBody, "<Sent from mobile>");
        }
        body.Invoke(mailBody);
        subject.Invoke(CentralResource.Instance.LocalizationHolder.MailSubject(sM.Id));
        sender.Invoke( string.Format("{0} <{1}>",
            CentralResource.Instance.LocalizationHolder.GetSpeakerName(sM.Sender),
            CentralResource.Instance.LocalizationHolder.MailAddress(sM.Sender)));
        receiver.Invoke( string.Format("To {0}",
            CentralResource.Instance.LocalizationHolder.GetSpeakerName(sM.Receiver)
            ));
        show.Invoke();
    }

	// Use this for initialization
	void Start () {
		//ShowEmail("silva1");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
