﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RandomEncounterSpawnPoint : MonoBehaviour {
    //UnityEventSequenceBehaviour
    public GameObject encounterTrigger;
    public Countdown countdown;
    private bool encounterVisible;
    public UnityEvent appearRequest_3dot5;
    public UnityEvent disappearRequest;
    //public UnityEvent disappearRequest;
    public UnityEventSequence OnActualAppear;

    bool disappearOnCountdown = false;

    // Use this for initialization
    void Start() {
        encounterVisible = RandomSpecial.RandomBool();
        if (encounterVisible) ActualAppear();
        else Disappear();
        UpdateVisible();
        countdown.StartCount(RandomSpecial.Range(12f / 2, 22f / 2));
    }

    private void UpdateVisible() {
        encounterTrigger.SetActive(encounterVisible);

    }

 

    public void CountdownOver() {

        if (!encounterVisible) {
            Appear();
        } else {
            if (disappearOnCountdown) {
                Disappear();
                encounterVisible = !encounterVisible;
                UpdateVisible();
            }

        }

        UpdateCountDown();
    }

    private void Appear() {
        appearRequest_3dot5.Invoke();
        DOVirtual.DelayedCall(3.5f, ActualAppear);
    }

    private void ActualAppear() {
        OnActualAppear.Invoke(); ;
        encounterVisible = true;
        UpdateVisible();
    }

    private void Disappear() {
        disappearRequest.Invoke();
    }

    private void UpdateCountDown() {
        countdown.StartCount(RandomSpecial.Range(12f, 22f));
    }

    public void EncounterWon() {
        CentralResource.Instance.OverworldRandomEncounter.ImmunityActivate();
        encounterVisible = false;
        Disappear();
        UpdateVisible();
        UpdateCountDown();
    }

    // Update is called once per frame
    void Update() {

    }
}
