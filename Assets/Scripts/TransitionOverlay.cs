﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class TransitionOverlay : MonoBehaviour {
    private Image image;

    public Color greenColor;
    public Color pinkColor;
    
    private int sceneChangeOverFrameDelay;

    // Use this for initialization
    void Awake () {
	    image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(sceneChangeOverFrameDelay >= 0) {
            sceneChangeOverFrameDelay--;
            if(sceneChangeOverFrameDelay == -1) {
                SceneChangeEnd();
            }
        }
	}

    public void GreenFadeout(float timeToFadeOut) {
        image.enabled = true;
        greenColor.a = 0;
        image.color = greenColor;
        

        image.DOFade(1,timeToFadeOut);
    }

    internal void DarkFadeout(float time) {
        image.enabled = true;
        image.color = new Color(0.05f,0.02f,0.06f,0);
        image.DOFade(1,time);
    }

    public void PinkFadeout(float timeToFadeOut) {
        image.enabled = true;
        pinkColor.a = 0;
        image.color = pinkColor;
        

        image.DOFade(1,timeToFadeOut);
    }

    public void FadeinScreen(float timeToFadeIn) {
        image.DOFade(0,timeToFadeIn).OnComplete(DisableImage);
    }

    internal void SceneChangeStart() {
        image.enabled = true;
        image.color = new Color(0.1f, 0.1f, 0.1f, 0);
        image.DOFade(1,0.1f);
    }

    internal void SceneChangeEndRequest() {
        
        sceneChangeOverFrameDelay = 3;
    }

    private void SceneChangeEnd() {
        
        image.color = new Color(0.1f, 0.1f, 0.1f, 1f);
        DOTween.Sequence().Append(image.DOFade(0,0.1f)).AppendCallback(DisableImage);
    }

    public void DisableImage() {
        image.enabled = false;
    }
}
