﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class HelpTextLabel : MonoBehaviour {

    public Text text;
    int source = -1;
    private bool battleHelpBeginner;
    public GameObject backGround;
    public LocalizationKey mindKey;
    public LocalizationKey suicideKey;

    // Use this for initialization
    void Start() {
        TutoHelpSingleton.Instance.MovementShow += MovementShow;
        TutoHelpSingleton.Instance.ExamineHelp += ExamineHelp;
        TutoHelpSingleton.Instance.DeckEdit += DeckHelp;
        TutoHelpSingleton.Instance.OptionsHelp += OptionsHelp;
        TutoHelpSingleton.Instance.BattleHelpBeginner += BattleBeginnerHelp;
        CurrentSkillDisplay.skillDisplayShow2 += (int id, ChosenSkillHolder skillh) => {
            if(!battleHelpBeginner) return;
            if (id >= 0) {
                source = 1;
                backGround.SetActive(true);
                text.text = CentralResource.Instance.LocalizationInfo.GetText(LocalizationEnum.ATTACKINSTRU);

            } else {
                if (skillh.SubSkill >= 0) {
                    backGround.SetActive(true);
                    text.text = CentralResource.Instance.LocalizationHolder.GetText(suicideKey);
                } else {
                    if (source == 1) {
                        EmptyText();

                    }
                }



            }

        };
        MindControlTemp.CanMindControl += (b, mC) => {
            if(!battleHelpBeginner) return;
            if (b) {
                source = 2;
                backGround.SetActive(true);
                text.text = CentralResource.Instance.LocalizationHolder.GetText(mindKey);
                //CentralResource.Instance.LocalizationInfo.GetText(LocalizationEnum.MINDCINSTRU);;
            } else {
                if (source == 2)
                    EmptyText();
            }

        };
        var mindCtrl = CentralResource.Instance.Hero.GetComponent<MindControlTemp>();
        mindCtrl.mindControllingE += () => {
            if(battleHelpBeginner)
                MovementShow(true);
        };
    }

    private void OptionsHelp(bool obj) {
         if (obj) {
            source = 5;
            //Debug.Log("MOVE INSTRUCTION");
            backGround.SetActive(true);
            text.text = CentralResource.Instance.LocalizationInfo.GetText(LocalizationEnum.ESCINSTRU);;;
        } else {
            if (source == 5)
                EmptyText();
        }
    }

    private void BattleBeginnerHelp(bool obj) {
        battleHelpBeginner = obj;
    }

    private void DeckHelp(bool obj) {
        if (obj) {
            source = 4;
            //Debug.Log("MOVE INSTRUCTION");
            backGround.SetActive(true);
            text.text = CentralResource.Instance.LocalizationInfo.GetText(LocalizationEnum.DECKEDITOPENINSTRU);;;
        } else {
            if (source == 4)
                EmptyText();
        }
    }

    private void ExamineHelp(bool obj) {
        if (obj) {
            source = 3;
            //Debug.Log("MOVE INSTRUCTION");
            backGround.SetActive(true);
            text.text = CentralResource.Instance.LocalizationInfo.GetText(LocalizationEnum.INTERACTINSTRU);;;
        } else {
            if (source == 3)
                EmptyText();
        }
    }

    private void TextEnabled(bool obj) {
        //gameObject.SetActive(obj);
        text.enabled = obj;

    }

    private void EmptyText() {
        text.text = "";
        backGround.SetActive(false);
        //Debug.Log("EMPTY TEXT");
    }

    private void MovementShow(bool obj) {
        if (obj) {
            source = 0;
            backGround.SetActive(true);
            Debug.Log("MOVE INSTRUCTION");
            text.text = CentralResource.Instance.LocalizationInfo.GetText(LocalizationEnum.MOVEINSTRU);;
        } else {
            if (source == 0)
                EmptyText();
        }
    }

    // Update is called once per frame
    void Update() {
        TextEnabled(Time.deltaTime != 0);

    }
}
