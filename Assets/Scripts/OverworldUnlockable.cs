﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverworldUnlockable : MonoBehaviour {

    public GameObject[] lockSource;
    public MyGameObjectEvent RequestClearLockEffect;
    //public MyGameObjectEvent RequestWallUpdate;
    public UnityEventSequence LockClear;
    public LogicGateEvent logicGateEvent;

	// Use this for initialization
	void Start () {
		logicGateEvent.OnInputTrue.AddListener(Input_True);
	}

    private void Input_True(int arg0) {
        LockClear.Invoke();
        RequestClearLockEffect.Invoke(lockSource[arg0]);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
