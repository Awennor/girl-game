﻿using UnityEngine;
using System.Collections;

public class EnemyDefaultSetup : MonoBehaviour {

	// Use this for initialization
	void Start () {
        UBattleCenterGirl.Instance.UBattleGameObjects.AddEnemy(gameObject);
        GetComponent<ClosestTarget>().PossibleTargets = UBattleCenterGirl.Instance.UBattleGameObjects.Heroes;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
