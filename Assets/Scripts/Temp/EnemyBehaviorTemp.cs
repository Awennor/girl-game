﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;

public class EnemyBehaviorTemp : MonoBehaviour {

	public GameObject bullet;
    public GameObject bullet2;
    public Transform target;
    public GameObject particlePrefab;
    public GameObject particlePrefab2;
    private ClosestTarget closestTarget;
    private string sideTag = "EnemyProj";
    private bool blastMode = false;
    private Invoker invoker;
    private FaceGameObject faceGameObject;
    private MovementLinearCtrl movementLinear;
    private CircularMovement movementCircular;
    private SkillHelper skillHelper;
    public float delayShoot = 2.2f;
    private ForceField forceField;
    private bool quickMode;
    public bool forceFieldCanUse = true;
    

    public bool BlastMode
    {
        get
        {
            return blastMode;
        }

        set
        {
            blastMode = value;
        }
    }

    // Use this for initialization
    void Start () {
        //DoCheck ();

        //Debug.Log ("Start called ENEMY");
        CharacterControlledSystem.Instance.controlledCharaChange+= (obj)=> {
            if(this == null)
                return;
            if(obj == gameObject) {
                forceField.FieldState(false);
            }
        };
        
        skillHelper = GetComponent<SkillHelper>();
        closestTarget = GetComponent<ClosestTarget>();
        invoker = GetComponent<Invoker>();
        var control = GetComponent<ControllableObject>();
        control.AddAntiInputScript(this);
        //characterBattle = new CharacterBattle();
        //characterBattle.AtbMenuOpen = false;
        //GetComponent<CharacterBattleHolder>().CharacterBattle = characterBattle;
        //CharacterBattleHolder
        var characterBattle = GetComponent<CharacterBattleHolder>().CharacterBattle;
        characterBattle.Atb.ATBFull += () => {
            OnATBFull(characterBattle);
        };
        

        faceGameObject = GetComponent<FaceGameObject>();
        
        closestTarget.targetChanged += (obj) => { faceGameObject.Target = obj; };
        movementLinear = GetComponent<MovementLinearCtrl>();
        movementCircular = GetComponent<CircularMovement>();
        GetComponent<AvailableAttackListTempOld>().AddAttack(StartShoot);
        var attackInput = GetComponent<AttackInput>();
        forceField = GetComponent<ForceField>();
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.Vermishoot,
            () => {
                blastMode = false;
                quickMode = true;
                StartShoot();
                quickMode = false;
            }
            );
        attackInput.SetAttackMethod(SkillInfoProvider.SkillId.Explosion,
            () => {
                blastMode = true;
                StartShoot();
            }
            );

    }

    void OnEnable() {
        
    }

    private void OnATBFull(CharacterBattle characterBattle) {
        if (this != null && this.enabled) {
            StartShoot();
            characterBattle.Atb.Empty();

            ForceFieldAndDodges();
        }
    }

    internal void HeroSide()
    {
        sideTag = "HeroProj";
    }

    internal void EnemySide()
    {
        sideTag = "EnemyProj";
    }

    // Update is called once per frame
    void Update () {
	
	}


    public void StartShoot()
    {
        if(faceGameObject != null)
            faceGameObject.enabled = true;
        if (blastMode) {
            invoker.AddAction(DoShoot, 0.1f);
            //  Debug.Log("BLAST START");
        } else {
            //Debug.Log("SHOTING...");
            float delayShot = delayShoot;
            if(quickMode) delayShoot = 0.2f;
            GameObject clone = ((GameObject)Instantiate(particlePrefab, gameObject.transform.position, transform.rotation));
            clone.transform.SetParent(gameObject.transform);
            GameObject clone2 = ((GameObject)Instantiate(particlePrefab2, gameObject.transform.position, transform.rotation));
            clone2.transform.SetParent(gameObject.transform);
            Destroy(clone, delayShoot - 0.4f);
            var timeToParticle2 = delayShoot - 0.4f;
            clone2.GetComponent<ParticleSystem>().startDelay = timeToParticle2;
            clone2.GetComponent<ParticleSystem>().Stop();
            clone2.GetComponent<ParticleSystem>().Play();
            invoker.AddAction(DoShoot, delayShoot, 1);
            var ps = clone.GetComponent<ParticleSystem>();
            
            SoundSingleton.Instance.PlaySound("charge1");
            //SoundSingleton.Instance.PlaySound("charge1", timeToParticle2);
            //SoundSingleton.Instance.StopSoundDelay("charge1", delayShoot);
            


        }

        
        
        
    }

    private void DoShoot() {
        //if (!enabled)
        //  return;
        //Debug.Log("ENEMY DO SHOOT");
        GameObject projUsed = bullet;
        if (BlastMode) {
            projUsed = bullet2;
            SoundSingleton.Instance.PlaySound("explosion");
        } else {
            SoundSingleton.Instance.PlaySound("enemysetup");
        }
        if (closestTarget.ChosenTarget != null)
            target = closestTarget.ChosenTarget.transform;
        else
            target = null;
        Vector3 p = transform.position;
        p.y += 5;
        Rigidbody clone = ((GameObject)Instantiate(projUsed, p, transform.rotation)).GetComponent<Rigidbody>();
        clone.gameObject.tag = sideTag;
        Vector3 v = transform.position;
        if (target != null)
            v = target.position - v;
        var projectilData = clone.GetComponent<ProjectilData>();
        projectilData.KnockbackOnSpeed = 40;
        if (BlastMode) {

            projectilData.DestroyOnImpact = false;
            //projectilData.Damage = SkillInfoProvider.Instance.GetBaseDamage(SkillInfoProvider.SkillId.Explosion);
            skillHelper.Damages(projectilData, SkillInfoProvider.SkillId.Explosion);
            clone.GetComponent<Invoker>().AddAction(() => {
                Destroy(clone.gameObject);
            }, 0.3f);
            Destroy(gameObject);

        } else {
            projectilData.DestroyOnImpact = true;
            //projectilData.Damage = SkillInfoProvider.Instance.GetBaseDamage(SkillInfoProvider.SkillId.Vermishoot);
            skillHelper.Damages(projectilData, SkillInfoProvider.SkillId.Vermishoot);

            clone.velocity = v.normalized * 65;
        }
        faceGameObject.enabled = false;
        
    }

    private void ForceFieldAndDodges() {
        invoker.AddAction(forceFieldOn, 1.6f+delayShoot, 1);
        invoker.AddAction(forceFieldOff, 3.5f+delayShoot);
        if (UnityEngine.Random.Range(0, 1f) < 0.9f)
            invoker.AddAction(Dodge, UnityEngine.Random.Range(0.4f, 1.2f)+delayShoot, 1);
        if (UnityEngine.Random.Range(0, 1f) < 0.5f)
            invoker.AddAction(Dodge, UnityEngine.Random.Range(1.4f, 2.3f)+delayShoot, 1);
    }

    private void forceFieldOn()
    {
        if(forceFieldCanUse)
            forceField.FieldState(true);
    }

    private void forceFieldOff()
    {
        forceField.FieldState(false);
    }

    private void Dodge()
    {
        movementCircular.enabled = false;
        movementCircular.renable(0.1f);
        movementLinear.enabled = true;
        movementLinear.Disable(0.1f);
        var vel = movementLinear.Velocity;
        
        vel.x = 50;
        vel = Quaternion.Euler(0, UnityEngine.Random.Range(0, 360), 0) *vel;
        //if (UnityEngine.Random.Range(0, 1f) < 0.5f)
        //  vel.x = -50;
        movementLinear.Velocity = vel;
    }
}
