﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySummonerTemp : MonoBehaviour {

    List<GameObject> enemies = new List<GameObject>();
    public GameObject prefabEnemy;
    public GameObject prefabEnemyMinion;
    public GameObject hero;
    int enemiesDead;

	// Use this for initialization
	void Start ()
    {
        SummonEnemy(1);
    }

    private void SummonEnemy(int enemyN)
    {
        for (int i = 0; i < enemyN; i++)
        {
            Vector3 p = transform.position;
            p.x += Random.Range(-2, 5);
            p.z += Random.Range(-3, 4);
            if (i == 1) p.x += 46;
            if (i == 2) p.z+= 16;
            if (i == 3) {
                p.z += 32;
                p.x +=36;
            } 

            GameObject clone = (GameObject)Instantiate(prefabEnemy, p, transform.rotation);
            clone.GetComponent<EnemyBehaviorTemp>().target = hero.transform;
            enemies.Add(clone);
        }
        
    }

    // Update is called once per frame
    void Update () {
        if (enemies.Contains(null)) {
            while (enemies.Remove(null)) {
                enemiesDead++;
                if (enemiesDead == 1) {
                    SummonEnemy(2);

                }
                if (enemiesDead == 3)
                {
                    SummonEnemy(3);
                }
                if (enemiesDead == 6)
                {
                    SummonEnemy(4);
                }
            }
        }
	}
}
