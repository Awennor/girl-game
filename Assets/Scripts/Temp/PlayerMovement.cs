﻿using System;
using Assets.ReusablePidroh.Scripts;
using UnityEngine;
using DG.Tweening;

public class PlayerMovement : MonoBehaviour {
    [SerializeField]
    private float speed = 90f;            // The speed that the player will move at.

    Vector3 movement;                   // The vector to store the direction of the player's movement.
    public Animator anim;                      // Reference to the animator component.
    Rigidbody playerRigidbody;          // Reference to the player's rigidbody.
    [SerializeField]
    private bool autoDisable = false;
    private CharacterControllerWrapper CharacterControllerWrapper;
    private BattleStateMachine battleState;
    private bool diagonalMovementBuffer;
    private float lastH;
    private float lastV;
    private Invoker invoker;
    private float myAng;
    private Quaternion targetRotation;
    private Tweener lastTweener;

    public float Speed {
        get {
            return speed;
        }

        set {
            speed = value;
        }
    }

    public bool AutoDisable {
        get {
            return autoDisable;
        }

        set {
            autoDisable = value;
        }
    }

    void Start() {
        var control = GetComponent<ControllableObject>();
        control.AddInputScript(this);
        this.enabled = !AutoDisable;
        playerRigidbody = GetComponent<Rigidbody>();
        CharacterControllerWrapper = GetComponent<CharacterControllerWrapper>();
        battleState = GetComponent<BattleStateMachine>();
        invoker = GetComponent<Invoker>();
        battleState.stateChanged += (bsm) => {
            if (anim != null)
                anim.SetBool("walking", false);
        };

    }


    void FixedUpdate() {

        if (!battleState.CanMove()) return;
        // Store the input axes.
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        var inputDisabled = !CentralResource.Instance.InputPlayerMovement.Enabled
            || (!CentralResource.Instance.BattleFlowSys.InBattle && !CentralResource.Instance.OverworldInput.InputActive);

        if (inputDisabled) {
            h = 0;
            v = 0;
            //Debug.Log("INPUT DISABLE");
            lastH = 0;
            lastV = 0;
        }

        bool thisFrameHasDiagonal = h != 0 && v != 0;
        if (thisFrameHasDiagonal) {
            diagonalMovementBuffer = true;
        }
        if (h == 0 && v == 0) {
            diagonalMovementBuffer = false;
        }
        if (!thisFrameHasDiagonal && diagonalMovementBuffer) {
            invoker.AddAction(turnOffDiagBuffer, 0.05f);
            if (h == 0 && lastH != 0 && (v == lastV)) {
                h = lastH;
            }
            if (v == 0 && lastV != 0 && h == lastH) {
                v = lastV;
            }
        }


        lastH = h;
        lastV = v;
        //Debug.Log(h+" move "+v);
        // Move the player around the scene.
        Move(h, v);

        // Turn the player to face the movement direction, but only if not move attacking.
        
        if(!battleState.IsMoveAttacking()) {
            Turning(h, v);
        }
        
        

        // Animate the player.
        Animating(h, v);
    }

    private void turnOffDiagBuffer() {
        diagonalMovementBuffer = false;
    }

    void OnControllerColliderHit(ControllerColliderHit hit) {
        myAng = Vector3.Angle(Vector3.up, hit.normal); //Calc angle between normal and character

    }

    void Move(float h, float v) {

        //*

        /*if (!flying)
        {
            for (int i = 0; i < 2; i++)
            {
                Vector3 forRayCast = transform.position;
                forRayCast.y += 4;
                //forRayCast += movement * 0.5f;
                if (i == 0) {
                    forRayCast.x += h * 0.5f;
                    if (!Physics.Raycast(forRayCast, Vector3.down))
                    {
                        h = 0;
                        //return;
                    }
                }
                if (i == 1)
                {
                    forRayCast.z += v * 0.5f;
                    if (!Physics.Raycast(forRayCast, Vector3.down))
                    {
                        v = 0;
                        //return;
                    }
                }
            }
        }//*/

        //*/
        // Set the movement vector based on the axis input.
        movement.Set(h, 0, v);

        float tempSpeed = Speed;
        if (myAng > 30) {
            tempSpeed *= 1.5f;
        }
        float speedY = CharacterControllerWrapper.getSpeedY();
        //float limiter = 0.1f;

        //Debug.Log(speedY+"bla");

        //if(!!CharacterControllerWrapper.isGround() && speedY <0 && -tempSpeed < speedY*limiter) {
        //  tempSpeed = speedY*limiter;
        //}

        movement = movement.normalized * tempSpeed;

        movement.y = speedY;

        //movement.y = playerRigidbody.velocity.y;
        //playerRigidbody.velocity = movement;
        // Move the player to it's current position plus the movement.
        //playerRigidbody.MovePosition (transform.position + movement);
        //CharacterControllerWrapper.SimpleMove(movement);

        CharacterControllerWrapper.SetSpeed(movement);
        movement.y = 0;

    }

    internal void StopRotations() {
        if(lastTweener != null) {
            lastTweener.Kill();
        }
    }

    void Turning(float h, float v) {
        movement.x = h;
        movement.z = v;
        //Debug.Log("Try turn");
        if (movement.sqrMagnitude != 0) {
            var targetRotationNew = Quaternion.LookRotation(movement);
            bool rotate = false;
            //Debug.Log("Try turn2");
            var rotationNotEqual = targetRotationNew != playerRigidbody.rotation;
            var rotationNotEqual2 = targetRotationNew != transform.rotation;
            var tweenerIsOk = lastTweener == null || !lastTweener.IsPlaying();
            if(rotationNotEqual || rotationNotEqual2) {
                //Debug.Log(rotationNotEqual2+"Rotation not equal ..."+rotationNotEqual+"tween"+tweenerIsOk);
            }
            
            if (tweenerIsOk && rotationNotEqual) {
                rotate = true;
                //Debug.Log("Try turn should succeed");
            }
            if (targetRotation != targetRotationNew) {
                rotate = true;
                //Debug.Log("Try turn should succeed type II");
            } else {
                //Debug.Log("no need to turn type II");
            }
            if (rotate) {
                if(lastTweener != null) {
                    lastTweener.Complete();
                }
                lastTweener = playerRigidbody.DORotate(targetRotationNew.eulerAngles, 0.15f);
                targetRotation = targetRotationNew;
                playerRigidbody.MoveRotation(targetRotationNew);
            }


            //playerRigidbody.MoveRotation(newRotation);


        }
    }

    void Animating(float h, float v) {
        // Create a boolean that is true if either of the input axes is non-zero.
        bool walking = h != 0f || v != 0f;

        // Tell the animator whether or not the player is walking.
        if (anim != null)
            anim.SetBool("walking", walking);

    }
}