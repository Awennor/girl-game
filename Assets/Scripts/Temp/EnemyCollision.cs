﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyCollision : MonoBehaviour {

    int life = 5;

    bool enemySide = true;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    void OnTriggerEnter(Collider col) {

        if ((col.gameObject.tag.Equals("HeroProj") && enemySide) ||
            (col.gameObject.tag.Equals("EnemyProj") && !enemySide)
            ) {
            ProjectilData pD = col.GetComponent<ProjectilData>();
            if (pD != null) {
                //life--;
                life -= (int) pD.Damage;
                if(pD.DestroyOnImpact)
                    Destroy(col.gameObject);
            }
			
            
            if (life < 0){
				Destroy(gameObject);
			}
		}
			
	}

    internal void ChangeSide()
    {
        enemySide = !enemySide;
        
    }
}
