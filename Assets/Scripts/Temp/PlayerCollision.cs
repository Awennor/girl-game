﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerCollision : MonoBehaviour {

	int life = 5;

	// Use this for initialization
	void Start () {
        
        //Debug.Log("PLAYER COLLISION INIT"+ UBattleCenterGirl.Instance.UBattleGameObjects.Enemies);

    }

    internal int getHP()
    {
        return life;
    }

    internal int getMaxHP()
    {
        return 5;
    }

    // Update is called once per frame
    void Update () {
	
	}


	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag.Equals ("EnemyProj")) {
			Destroy (col.gameObject);
			life--;
			if(life <= 0){
				Destroy(gameObject);
			}
		}
	}
}
