﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets.Scripts.LogicScript;
using Assets.ReusablePidroh.Scripts.Logic;
using Assets.ReusablePidroh.Scripts;
using UnityStandardAssets.Utility;
using System;

public class GUITemp : MonoBehaviour {

    public RectTransform image;

    public GameObject commandMenu;
    private ATBManager atb;
    public GameObject hero;
    public BattleCharaGUI mainCharaGUI;
    public BattleCharaGUI controlledCharaGUI;
    public SmoothFollowC cameraFollow;
    private MindControlTemp mindControl;



    // Use this for initialization
    void Start() {
        if (hero.GetComponent<CharacterBattleHolder>().CharacterBattle == null) {
            hero.GetComponent<CharacterBattleHolder>().CharacterBattleSet += () =>
            {
                mainCharaGUI.ChangeChara(hero);
            };
        }
        else{
            mainCharaGUI.ChangeChara(hero);
        }
        
        TutoHelpSingleton.Instance.ShowHeroUI += (b)=> {
            mainCharaGUI.Visible(b);
        };

        CentralResource.Instance.BattleFlowSys.OnBattleStart += ()=> {
            mainCharaGUI.BattleStart();
            atb.ToFull();
        };
        CentralResource.Instance.BattleFlowSys.OnBattleEnd += ()=> {
            mainCharaGUI.BattleEnd();
            hero.GetComponent<ChosenSkillHolder>().Reset();
        };


        mindControl = hero.GetComponent<MindControlTemp>();
        mindControl.characterControlledChanged += () =>
        {
            atb = mindControl.ControllingObject.GetComponent<CharacterBattleHolder>().CharacterBattle.Atb;

        };
        //Debug.Log("MIND CONTROLLED WIRE");
        mindControl.mindControllingTargetChosen += (obj) => {
            mindControl.SkillHelper.DelayedMethodUnique(CameraToMindControlTarget, 0.2f);
        };
        mindControl.mindControllingE += () => {
            var controlled = mindControl.ControllingObject;
            cameraFollow.Target = controlled.transform;
            controlledCharaGUI.gameObject.SetActive(true);
            controlledCharaGUI.ChangeChara(controlled);
            controlledCharaGUI.BattleStart();
        };
        mindControl.mindControllingBackToHero += () =>
        {
            controlledCharaGUI.gameObject.SetActive(false);
            var controlled = mindControl.ControllingObject;
            cameraFollow.Target = controlled.transform;
        };

        atb = MainDataGirl.Instance.MainCharaBattle.Atb;

        

    }

    private void CameraToMindControlTarget() {
        if(mindControl.Target != null)
            cameraFollow.Target = mindControl.Target.transform;
    }

    // Update is called once per frame
    void Update () {

    }

    private void CloseCommandMenu()
    {
        Time.timeScale = 1;
        commandMenu.SetActive(false);
    }
}
