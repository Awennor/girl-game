﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.LogicScript;

public class EnemyAttackInputTemp : MonoBehaviour {

    EnemyBehaviorTemp enemyBehavior;
    int bullets = 1;
    public bool autoDisable = true;
    private ATBManager atb;

    // Use this for initialization
    void Start () {
        var control = GetComponent<ControllableObject>();
        enemyBehavior = GetComponent<EnemyBehaviorTemp>();
        control.AddInputScript(this);
        this.enabled = !autoDisable;
        GetComponent<CharacterBattleHolder>().CharaCallbackOnce((CharacterBattle chara) =>
        {
            atb = chara.Atb;
        });
    }
	
	// Update is called once per frame
	void Update () {



    }
}
