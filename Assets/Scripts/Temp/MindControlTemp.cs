﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.ReusablePidroh.Scripts;
using DG.Tweening;

public class MindControlTemp : MonoBehaviour {
    private ClosestTarget closestT;
    int bullet = 0;
    bool mindControlling = false;
    public Action characterControlledChanged;
    public Action mindControllingE;
    public Action<GameObject> mindControllingTargetChosen;
    public Action mindControllingBackToHero;
    private GameObject controllingObject;
    public ParticleSystem soulEnter;
    public ParticleSystem soulLeave;
    private Invoker invoker;
    private GameObject target;
    private bool startedMindControl;
    static public Action<bool, MindControlTemp> CanMindControl;
    private BattleFlowSystem battleFlow;
    private bool mindControllingUnderWay;
    private SkillHelper skillHelper;
    private GameObject mindEffect;

    public GameObject ControllingObject {
        get {
            return controllingObject;
        }

        set {
            controllingObject = value;
        }
    }

    public SkillHelper SkillHelper {
        get {
            return skillHelper;
        }

        set {
            skillHelper = value;
        }
    }

    public GameObject Target {
        get {
            return target;
        }

        set {
            target = value;
        }
    }

    // Use this for initialization
    void Start() {
        //Debug.Log("Mind control start...");
        SkillHelper = GetComponent<SkillHelper>();
        var control = GetComponent<ControllableObject>();
        closestT = GetComponent<ClosestTarget>();
        ControllingObject = gameObject;
        invoker = GetComponent<Invoker>();
        battleFlow = CentralResource.Instance.BattleFlowSys;
        battleFlow.OnBattleEnd += CheckMindControlStatus;


        var attackInput = GetComponent<AttackInput>();
        attackInput.SetAttackMethod((int)
            SkillInfoProvider.SkillId.Braincontrol, StartMindControl);

        mindControllingE += () => {
            if (characterControlledChanged != null)
                characterControlledChanged();
            CheckMindControlStatus();
        };
        mindControllingBackToHero += () => {
            if (characterControlledChanged != null)
                characterControlledChanged();
            CheckMindControlStatus();
        };
        CollisionProj.damageMindHappen += (cP, mindD) => {
            CheckMindControlStatus();
        };

        HPSystem.deathOnHPZeroDestroy += (g) => {
            CheckMindControlStatus();
        };
    }

    private void CheckMindControlStatus() {
        var mindControlOk = IsMindControlPossible();
        if (mindControlOk && controllingObject == gameObject) {
            StartMindControl_Closest();
        }
        if (CanMindControl != null) {

            CanMindControl(mindControlOk, this);
        }
    }

    private bool IsMindControlPossible() {
        if (controllingObject != gameObject) {
            return false;
        }
        if (Time.deltaTime <= 0) {
            return false;
        }
        if (mindControllingUnderWay)
            return false;
        var possibleTargs = closestT.PossibleTargets;
        for (int i = 0; i < possibleTargs.Count; i++) {
            var mindHP = possibleTargs[i].GetComponent<MindHPSystem>();
            if(mindHP == null) {
                continue;
            }
            var hpSystem = possibleTargs[i].GetComponent<HPSystem>();

            if (mindHP.canMindControl() && !hpSystem.IsZero()) {
                return true;
                break;
            }
        }
        return false;
    }

    // Update is called once per frame
    void Update() {
        if (controllingObject == null && gameObject != null) {

            backToHero();

        }
        if (Input.GetButtonDown("Mind")
            ) {
            if (IsMindControlPossible()) {
                if (controllingObject == gameObject) {
                    StartMindControl();
                }
            } else {
                /*
                if (controllingObject != gameObject && !mindControllingUnderWay && !startedMindControl
                    && Time.timeScale != 0) {
                    controllingObject.GetComponent<MindHPSystem>().GoToRatio(0.5f);
                    controllingObject.GetComponent<MindHPSystem>().Recover(1);
                    GivebackControlEnemy();
                    backToHero();
                }
                */

            }


        }
        if (Input.GetButtonDown("Cancel") && startedMindControl) {
            startedMindControl = false;
            var targetChoosingSystem = CentralResource.Instance.TargetChoosingSystem;
            targetChoosingSystem.CancelTargetting();
        }
    }

    private void backToHero() {
        if (mindEffect != null) {
            mindEffect.SetActive(false);
        }
        gameObject.GetComponent<CollisionProj>().StopCollisionsForATime(0.4f);
        skillHelper.Zoom(0.9f, 1.2f);
        skillHelper.Zoom(1f, 2f, 1.6f);

        SkillHelper.AnimationTrigger("normal");
        var lineDrawManager = GetComponent<LineDrawManager>();
        lineDrawManager.DrawLineFromOriginToThisObj = null;
        lineDrawManager.Visible(false);
        GetComponent<CharacterBattleHolder>().CharacterBattle.Atb.Pause(false);
        gameObject.GetComponent<ControllableObject>().Controlling(true);
        GetComponent<CollisionProj>().SetCollisionTransferObject(null);
        controllingObject = gameObject;
        UBattleCenterGirl.Instance.UBattleGameObjects.Heroes.Add(gameObject);
        mindControlling = false;
        CharacterControlledSystem.Instance.ChangeControlledChara(gameObject);
        if (mindControllingBackToHero != null) {
            mindControllingBackToHero();
        }
    }

    private void StartMindControl() {


        bullet--;

        var targetChoosingSystem = CentralResource.Instance.TargetChoosingSystem;
        startedMindControl = true;
        targetChoosingSystem.ClearTargets();
        var possibleTargs = closestT.PossibleTargets;
        for (int i = 0; i < possibleTargs.Count; i++) {
            var mindHP = possibleTargs[i].GetComponent<MindHPSystem>();
            if (mindHP.canMindControl()) {
                targetChoosingSystem.AddTargetPossible(mindHP.gameObject);
            }
        }
        targetChoosingSystem.
            ActivateTargetWithCallback(TargetChosen);


        GetComponent<CharacterControllerWrapper>().ZeroSpeed();
    }

    private void StartMindControl_Closest() {
        
        //var targetChoosingSystem = CentralResource.Instance.TargetChoosingSystem;
        var possibleTargs = closestT.PossibleTargets;
        for (int i = 0; i < possibleTargs.Count; i++) {
            var mindHP = possibleTargs[i].GetComponent<MindHPSystem>();
            if (mindHP != null && mindHP.canMindControl()) {
                TargetChosen(possibleTargs[i]);
                break;
            }
        }
        GetComponent<CharacterControllerWrapper>().ZeroSpeed();


        
    }

    private void TargetChosen(GameObject obj) {


        SkillHelper.AnimationTrigger("focus");

        if (mindEffect == null) {
            mindEffect = EffectSingleton.Instance.ShowAndAddEffect("mindcontrolling", transform);
        } else {
            mindEffect.SetActive(true);
        }

        startedMindControl = false;
        mindControllingUnderWay = true;
        //ParticleHelper.SummonParticle(soulLeave.gameObject, gameObject);
        EffectSingleton.Instance.ShowEffectHere("brainleave", transform);
        gameObject.GetComponent<ControllableObject>().Controlling(false);

        skillHelper.Zoom(0.8f, 0.8f, 0.2f);

        Target = obj;
        target.GetComponent<CollisionProj>().StopCollisionsForATime(1f);
        if (mindControllingTargetChosen != null)
            mindControllingTargetChosen(Target);
        //particleOnEnemy();
        invoker.AddAction(particleOnEnemy, 0.1f);

        GetComponent<CharacterBattleHolder>().CharacterBattle.Atb.Pause(true);

        GetComponent<CollisionProj>().SetCollisionTransferObject(Target);
        invoker.AddAction(TakeControlEnemy, 0.2f);

        var lineDrawManager = GetComponent<LineDrawManager>();
        lineDrawManager.DrawLineFromOriginToThisObj = Target;
        lineDrawManager.Visible(true);

    }

    private void particleOnEnemy() {
        if (Target != null) {
            EffectSingleton.Instance.ShowAndAddEffect("brainarrive", Target.transform);
            var materialChanger = Target.GetComponent<MaterialChanger>();
            if (materialChanger.HasRenderer()) {
                var mainMaterial = materialChanger.MainMaterial;

                mainMaterial.DOColor(new Color(55f / 255f, 255f / 255f, 166f / 255f, 0.3f), "_ColorEmi", 0.3f).SetDelay(0.2f);
                mainMaterial.DOColor(new Color(0, 0, 0, 0f), "_ColorEmi", 0.4f).SetDelay(0.8f);
            }
        }

        //ParticleHelper.SummonParticle(soulEnter.gameObject, target);

    }
    private void TakeControlEnemy() {
        if (Target == null) {
            backToHero();
            mindControllingUnderWay = false;
            return;
        }


        mindControllingUnderWay = false;
        skillHelper.Zoom(1f, 1.2f, 0.4f);

        List<GameObject> enemies = UBattleCenterGirl.Instance.UBattleGameObjects.Enemies;
        enemies.Remove(Target);
        UBattleCenterGirl.Instance.UBattleGameObjects.Heroes.Add(Target);
        UBattleCenterGirl.Instance.UBattleGameObjects.Heroes.Remove(gameObject);

        Target.GetComponent<ControllableObject>().Controlling(true);
        Target.GetComponent<ClosestTarget>().PossibleTargets = UBattleCenterGirl.Instance.UBattleGameObjects.Enemies;
        Target.GetComponent<CollisionProj>().HeroSide();

        var enemyBehavior = Target.GetComponent<EnemyBehaviorTemp>();
        if (enemyBehavior)
            enemyBehavior.HeroSide();
        Target.GetComponent<CharacterControllerWrapper>().ZeroSpeed();
        Target.GetComponent<FaceGameObject>().Target = null;
        Target.GetComponent<SkillHelper>().HeroSide();
        ControllingObject = Target;
        mindControlling = true;
        Target.GetComponent<Invoker>().ClearAllTagged(1);
        Target.GetComponent<BattleStateMachine>().ChangeState(BattleStateMachine.States.Normal);
        
        if (mindControllingE != null)
            mindControllingE();
        CharacterControlledSystem.Instance.ChangeControlledChara(Target);

        Target.GetComponent<CharacterBattleHolder>().CharacterBattle.Atb.ToRatio(0.9999f);
        
    }

    private void GivebackControlEnemy() {
        Target = ControllingObject;
        List<GameObject> enemies = UBattleCenterGirl.Instance.UBattleGameObjects.Enemies;
        enemies.Add(Target);
        UBattleCenterGirl.Instance.UBattleGameObjects.Heroes.Remove(Target);

        Target.GetComponent<ControllableObject>().Controlling(false);
        Target.GetComponent<ClosestTarget>().PossibleTargets = UBattleCenterGirl.Instance.UBattleGameObjects.Heroes;
        Target.GetComponent<CollisionProj>().EnemySideSet();
        var enemyBehavior = Target.GetComponent<EnemyBehaviorTemp>();
        if (enemyBehavior)
            enemyBehavior.EnemySide();
        Target.GetComponent<SkillHelper>().EnemySide();
        var atb = Target.GetComponent<CharacterBattleHolder>().CharacterBattle.Atb;
        atb.TryCallbackRefresh();
    }

    private bool isMindControlling() {
        return mindControlling;
    }

    internal void NoBullets() {
        bullet = 0;
    }

    internal void ResetBullets() {
        bullet = 1;
    }
}
