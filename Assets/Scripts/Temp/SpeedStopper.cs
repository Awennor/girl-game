﻿using UnityEngine;
using System.Collections;

public class SpeedStopper : MonoBehaviour {
    private Rigidbody rigidBody;

    // Use this for initialization
    void Start () {
        rigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        //rigidBody.velocity = Vector3.zero;
	}
    void FixedUpdate() {
        rigidBody.velocity = Vector3.zero;
    }
}
