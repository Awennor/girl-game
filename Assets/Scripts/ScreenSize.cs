﻿using UnityEngine;
using System.Collections;

public class ScreenSize : MonoBehaviour {

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if(Camera.main == null) return;
	    float height = Camera.main.orthographicSize * 2;
        float width = height * Screen.width/ Screen.height; 
        transform.localScale = new Vector3(width, height, 1);
	}
}
