﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;

public class BattleStateMachine : MonoBehaviour {

    public enum States { Normal, Damage, Attacking};
    public enum StatesAttackBlock { Can, Cannot};
    public enum StatesAttackAndMove { Can, Cannot};
    States currentState = States.Normal;
    StatesAttackBlock currentStateAttackBlock = StatesAttackBlock.Can;
    StatesAttackAndMove currentStateAttackAndMove = StatesAttackAndMove.Cannot;
    public Action<BattleStateMachine> stateChanged;
    public Action<BattleStateMachine> OnCurrentAttackBlockSet;
    private Invoker invoke;
    
    public void AllowAttackAndMove(float timeToNormal) {
        currentStateAttackAndMove = StatesAttackAndMove.Can;
        invoke.AddActionUniqueReplaceHigherDelay(BlockAttackAndMove, timeToNormal);
    }

    private void BlockAttackAndMove() {
        currentStateAttackAndMove = StatesAttackAndMove.Cannot;
    }

    public void BlockAttacks(float timeToNormal) {
        CurrentStateAttackBlock = StatesAttackBlock.Cannot;
        Debug.Log("Block attack");
        invoke.AddActionUniqueReplaceHigherDelay(UnblockAttack, timeToNormal);
    }

    public void UnblockAttack() {
        CurrentStateAttackBlock = StatesAttackBlock.Can;
        Debug.Log("Unblock attack");
    }

    public void ChangeState(States newState, float timeToNormal)
    {
        ChangeState(newState);
        invoke.AddActionUniqueReplaceHigherDelay(ToNormal, timeToNormal);
        //invoke.AddAction(ToNormal, timeToNormal);

    }

    public void ChangeState(States newState)
    {

        currentState = newState;
        if (stateChanged != null)
            stateChanged(this);
    }

    public void ToNormal() {
        ChangeState(States.Normal);
        
    }

    public States CurrentState
    {
        get
        {
            return currentState;
        }

    }

    public StatesAttackBlock CurrentStateAttackBlock {
        get {
            return currentStateAttackBlock;
        }

        set {
            currentStateAttackBlock = value;
            if(OnCurrentAttackBlockSet!= null) {
                OnCurrentAttackBlockSet(this);
            }
        }
    }

    void Start () {
        invoke = GetComponent<Invoker>();
	}
	
	
	void Update () {
	
	}

    internal bool CanMove()
    {
        var normalState = (currentState == States.Normal);

        return normalState || (currentStateAttackAndMove == StatesAttackAndMove.Can && currentState == States.Attacking);
    }

    internal void RemoveChangesToNormal() {
        invoke.removeAllActionsLikeThis(ToNormal);
    }

    internal bool IsMoveAttacking() {
        return currentStateAttackAndMove == StatesAttackAndMove.Can && currentState == States.Attacking;
    }
}
