﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneStartRequestVariable : MonoBehaviour {

    public VariableEnum cutsceneVariable;

	// Use this for initialization
	public void Request () {
		CentralResource.Instance.CutsceneVar.RequestCutscene(cutsceneVariable);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
