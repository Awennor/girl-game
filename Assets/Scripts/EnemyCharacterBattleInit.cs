﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class EnemyCharacterBattleInit : MonoBehaviour {

    public float ATBchargeTime;

	// Use this for initialization
	void Start () {
        CharacterBattle characterBattle = new CharacterBattle();
        GetComponent<CharacterBattleHolder>().CharacterBattle = characterBattle;
        characterBattle.Atb.TimeBar.Max = 25 * ATBchargeTime;
        characterBattle.Atb.TimeBar.Value = 25 * RandomSpecial.Range(ATBchargeTime*0.1f, ATBchargeTime-0.2f);
        var hp = GetComponent<HPSystem>();
        hp.HPZeroHappens += hpZero;
        hp.DestroyOnHPZero = false;
    }

    private void hpZero(GameObject obj) {
        //Time.timeScale = 0;

        DOVirtual.DelayedCall(0.1f, DeathStart, false);

        //DeathStart();
    }

    private void DeathStart() {
        CentralResource.Instance.BattleDeathTracker.Death(this.gameObject);
        EffectSingleton.Instance.ShowEffectHere("enemydeath", transform.position);
        Destroy(gameObject, 0.2f);
        SoundSingleton.Instance.PlaySound("enemydeath1");
        EffectSingleton.Instance.SpawnParticleHere("enemyblood", 80, transform.position+new Vector3(0,2));
    }

    // Update is called once per frame
    void Update () {
	
	}
}
