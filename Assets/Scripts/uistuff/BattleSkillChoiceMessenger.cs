﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class BattleSkillChoiceMessenger : MonoBehaviour {

    public Action RequestingEnd;

    public UnityEvent BattleStartEvent = new UnityEvent();
    public UnityEvent ReopenEvent = new UnityEvent();

    public void BattleStart() {
        BattleStartEvent.Invoke();
        
    }

    public void Reopen() {
        ReopenEvent.Invoke();
    }

    public void RequestEnd() {
        if(RequestingEnd != null) {
            RequestingEnd();
        }
    }


}