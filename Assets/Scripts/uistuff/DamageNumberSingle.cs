﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
//using UnityEngine.UI;

public class DamageNumberSingle : MonoBehaviour {

    public Text text;
    public Animator anim;
    bool wantToplay;
    String[] damageTriggers  = {"damage1", "damage2", "damage3" };
    private Vector3 lastOffset;
    private bool useLastOffSet;

    void Hide() {
        text.text = "";
    }

    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if(wantToplay) {
            
            //wantToplay = false;
        }

	
	}

    internal void show(Vector3 pos, float damage, int type) {
        gameObject.SetActive(true);
        pos.y += 1;

        
        pos = Camera.main.WorldToScreenPoint(pos);
        
        transform.position = pos;
        text.text = ""+(int)damage;
        //anim.Play();
        
        anim.SetTrigger(damageTriggers[type]);
        wantToplay = false;
        
    }
}
