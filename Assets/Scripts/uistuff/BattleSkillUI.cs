﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.LogicScript;
using System;

public class BattleSkillUI : MonoBehaviour {

    //public BattleSkillButton battleSkillButton;
    public BattleSkillChoiceMessenger battleSkillChoiceMessenger;
    public HidableGUI interfaceObj;
    public GameObject heroTemp;
    private ATBManager atb;
    public static Action<bool> Visible;
    private BattleFlowSystem battleFlow;
    public CameraFocusGroup cameraFocusGroup;
    private AttackInput attackInput;
    private BattleStateMachine battleStateMachine;
    private ActorStats actorStats;
    private CharacterBattleHolder characterBattleHolder;

    // Use this for initialization
    void Start() {

        //temp init
        var characterBattleHolder = heroTemp.GetComponent<CharacterBattleHolder>();
        characterBattleHolder.CharaCallbackOnce((chara) => {
            atb = chara.Atb;
            UpdateObj(heroTemp);
        });
        battleFlow = CentralResource.Instance.BattleFlowSys;
        battleFlow.OnBattleStart += BattleStarted;
        battleSkillChoiceMessenger.RequestingEnd += () => {
            Time.timeScale = 1;
            if (Visible != null) {
                Visible(false);
            }
            //cameraFocusGroup.ReturnToCharaFocus();
            interfaceObj.Hide(true);
        };

        CharacterControlledSystem.Instance.controlledCharaChange += (obj) => {
            UpdateObj(obj);
        };

    }

    private void UpdateObj(GameObject obj) {
        characterBattleHolder = obj.GetComponent<CharacterBattleHolder>();
        atb = characterBattleHolder.CharacterBattle.Atb;
        attackInput = obj.GetComponent<AttackInput>();
        battleStateMachine = obj.GetComponent<BattleStateMachine>();
        actorStats =  obj.GetComponent<SkillHelper>().ActorStats;
    }

    private void BattleStarted() {
        //battleSkillButton.BattleStart();
        battleSkillChoiceMessenger.BattleStart();
    }

    // Update is called once per frame
    void Update() {
        /*if (Input.GetKeyDown(KeyCode.X){
            TryOpen();
        }*/
        TryOpen();
    }

    private void TryOpen() {
        if (
                characterBattleHolder.CharacterBattle.AtbMenuOpen
                && atb.TimeBar.IsFull()
                && interfaceObj.Hidden
                && battleFlow.InBattle
                && Time.deltaTime > 0
                && attackInput.NoMoreAttacks()
                && battleStateMachine.CurrentState == BattleStateMachine.States.Normal

                ) {
            actorStats.TurnStart();
            atb.Empty();
            interfaceObj.Hide(false);
            if (Visible != null) {
                Visible(true);
            }
           /* if (cameraFocusGroup != null && Camera.current != null) {
                cameraFocusGroup.Offset =
               -Camera.current.ScreenToWorldPoint(new Vector3(188, 0)) +
               Camera.current.ScreenToWorldPoint(new Vector3(0, 0));
                //Debug.Log(cameraFocusGroup.Offset+"___OFFSET");
                var focusObjs = cameraFocusGroup.GameObjects;
                focusObjs.Clear();
                focusObjs.AddRange(UBattleCenterGirl.Instance.UBattleGameObjects.Enemies);
                focusObjs.AddRange(UBattleCenterGirl.Instance.UBattleGameObjects.Heroes);
                //cameraFocusGroup.FocusOnAll();
                //cameraFocusGroup.ZoomToShowAll();
            }
            */

            //battleSkillButton.reopen();
            battleSkillChoiceMessenger.Reopen();
            Time.timeScale = 0;
        }
    }
}
