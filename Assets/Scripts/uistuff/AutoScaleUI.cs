﻿using UnityEngine;
using System.Collections;

public class AutoScaleUI : MonoBehaviour {

    public float referenceHeight;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    float v = Screen.height / referenceHeight;
        this.transform.localScale = new Vector3(v,v,v);
	}
}
