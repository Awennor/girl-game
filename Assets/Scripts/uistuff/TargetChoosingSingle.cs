﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TargetChoosingSingle : MonoBehaviour {
    private Button button;
    GameObject followObject;
    private UIFollowObj uiFollow;
    public GameObject graphic;
    public Canvas canvas;

    public Button Button {
        get {
            return button;
        }

        set {
            button = value;
        }
    }

    public GameObject FollowObject {
        get {
            return followObject;
        }

        set {
            followObject = value;
            if(uiFollow!=null)
                uiFollow.Target = followObject;
        }
    }

    // Use this for initialization
    void Awake () {
	    uiFollow = gameObject.AddComponent<UIFollowObj>();
        uiFollow.canvas = canvas;
        uiFollow.Target = followObject;
        Button = GetComponent<Button>();
        
	}
	
	// Update is called once per frame
	void Update () {

	}

    internal void visible(bool targetExist) {
        graphic.SetActive(targetExist);
        button.interactable = targetExist;
    }

    internal void Select() {
        button.Select();
    }
}
