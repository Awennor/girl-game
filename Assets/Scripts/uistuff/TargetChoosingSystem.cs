﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class TargetChoosingSystem : MonoBehaviour {
    private ClosestTarget closestTarget;
    [SerializeField]
    GameObject hero;
    public GameObject targetsParent;
    [SerializeField]
    TargetChoosingSingle[] targetChoosingSingles;
    private Action<GameObject> targetChosen;
    //Action<GameObject> targetChosen;
    private List<GameObject> targetsPossible = new List<GameObject>();
    public Action<bool> targetOn;
    

    // Use this for initialization
    void Awake () {
        //var t = targetsParent.transform;
        CentralResource.Instance.TargetChoosingSystem = this;
        closestTarget = hero.GetComponent<ClosestTarget>();
        
	}

    void Start() {
        for (int i = 0; i < targetChoosingSingles.Length; i++) {
            int j = i;
            targetChoosingSingles[i].Button.onClick.AddListener(
                ()=> {
                    Debug.Log("Target choosing SYS!! press");
                    targetPressed(j);
                }
                );
        }
    }

    private void targetPressed(int j) {
        EndTargetting();
        if (targetChosen != null) {
            targetChosen(targetsPossible[j]);
            targetChosen = null;

        }
    }

    private void EndTargetting() {

        Time.timeScale = 1;
        for (int i = 0; i < targetChoosingSingles.Length; i++) {
            targetChoosingSingles[i].visible(false);
        }
        if(targetOn != null) {
            targetOn(false);
        } 
    }

    public void ActivateTargetWithCallback(Action<GameObject> returnFunction) {
        targetChosen = returnFunction;
        ActivateTarget();
    }

    // Update is called once per frame
    void Update () {
    }

    private void ActivateTarget() {
        if(targetOn != null) {
            targetOn(true);
        } 
        Time.timeScale = 0;
        var targetsP = closestTarget.PossibleTargets;
        //targetsPossible.Clear();
        //targetsPossible.AddRange(targetsP);
        bool selected = false;
        for (int i = 0; i < targetChoosingSingles.Length; i++) {
            var targetExist = i < targetsPossible.Count && targetsPossible[i]!=null;
            if (targetExist) {
                targetChoosingSingles[i].FollowObject = targetsPossible[i];
                if(!selected) {
                    selected = false;
                    targetChoosingSingles[i].Select();
                }

            } else {
                targetChoosingSingles[i].FollowObject = null;
            }
            targetChoosingSingles[i].visible(targetExist);
        }
    }

    internal void ClearTargets() {
        targetsPossible.Clear();
    }

    internal void AddTargetPossible(GameObject gameObject) {
        targetsPossible.Add(gameObject);
    }

    internal void CancelTargetting() {
        EndTargetting();
    }
}
