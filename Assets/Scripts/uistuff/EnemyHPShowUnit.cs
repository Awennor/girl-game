﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class EnemyHPShowUnit : MonoBehaviour {

    public Text hpText;
    public Text mindText;
    private HPSystem hPSystem;
    private MindHPSystem mindSystem;
    private GameObject boundObj;
    public UIFollowObj uiFollow;

    public void Bind(GameObject obj) {
        this.boundObj = obj;
        if(hPSystem != null) {
            hPSystem.HPChange -= hpChange;
        }
        if(mindSystem != null) {
            mindSystem.changeMind -= MindHPChange;
        }
        uiFollow.Target = obj;
        hPSystem = obj.GetComponent<HPSystem>();
        mindSystem = obj.GetComponent<MindHPSystem>();
        hPSystem.HPChange += hpChange;
        mindSystem.changeMind += MindHPChange;
        hpChange(hPSystem);
        MindHPChange(mindSystem);
    }

    private void MindHPChange(MindHPSystem obj) {
        mindText.text = ""+(int)obj.GetMindHP();
    }

    internal void TryBind(GameObject child) {
        if(boundObj!= child) {
            Bind(child);
        } else {
            var hPSystem = child.GetComponent<HPSystem>();
            hpChange(hPSystem);
        }
    }

    internal void Show(bool v) {
        hpText.gameObject.SetActive(v);
        mindText.gameObject.SetActive(v);
    }

    private void hpChange(HPSystem obj) {
        hpText.text =""+ Mathf.CeilToInt(obj.GetHP());
    }

    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
