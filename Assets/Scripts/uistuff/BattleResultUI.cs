﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class BattleResultUI : MonoBehaviour {

    public GameObject graphicParent;
    public Button button;
    public DoubleTextField assassinationField;
    public DoubleTextField manipDeathField;
    public DoubleTextField damageField;

    public DoubleTextField excellencyField;
    public DoubleTextField styleField;

    public EnemyPrizeDataDefinition prizeData;
    List<EnemyPrizeUnit> prizeUnits = new List<EnemyPrizeUnit>();
    List<Prize> possiblePrizes = new List<Prize>();
    public SkillInfoUI skillInfoUi;
    public UnityEvent uiShow;
    //public GameObject lossUIGraphic;
    public UnityEvent heroDeathBattleEnd;
    public UnityEvent uiClose;

	// Use this for initialization
	void Start () {
        CentralResource.Instance.BattleFlowSys.OnBattleEnd += ()=> {
            if(!CentralResource.Instance.BattleFlowSys.AbortedBattleNotVictory)
                DOVirtual.DelayedCall(0.3f,OpenUI);
            if(CentralResource.Instance.BattleFlowSys.HeroDeath) {
                //lossUIGraphic.SetActive(true);
                Debug.Log("DEATH TRIGGER");
                heroDeathBattleEnd.Invoke();
            }
            //OpenUI();

        };
        button.onClick.AddListener(OkPress);
	}

    public void HeroDeathReload() {
        var sps = CentralResource.Instance.SaveSlots.CurrentSlot.spawnPositionSave;
        CentralResource.Instance.LevelChangeSystem.SpawnPoint = sps.spawnNumber;
        CentralResource.Instance.Hero.GetComponent<HPSystem>().FullHeal();
        Debug.Log(sps.spawnNumber+"SPAWN NUMBER");
        SceneManager.LoadScene(sps.sceneName, LoadSceneMode.Additive);
    }

    private void OpenUI() {
        uiShow.Invoke();
        Time.timeScale = 0;
        graphicParent.gameObject.SetActive(true);
        //button.Select();
        float destructedRatio = CentralResource.Instance.BattleDeathTracker.DestructedRatio();
        int destructedPer = (int)(destructedRatio * 100);
        int notDestructedPer = (100 - destructedPer);
        assassinationField.SetContent(destructedPer + "%");
        manipDeathField.SetContent(notDestructedPer + "%");
        int damage = (int)CentralResource.Instance.HeroDamageTracker.Damage;
        damageField.SetContent("" + damage);
        int aux = damage;
        int excellency = 100;
        while (aux > 10) {
            excellency -= 10;
            aux -= 10;
            aux *= 2;
            aux /= 3;
        }
        excellency -= aux;
        excellencyField.SetContent(excellency + "%");
        VictoryStyle vStyle = VictoryStyle.MIXED;
        string style = "Mixed";
        if (destructedPer > 70) {
            style = "Assassin";
            vStyle = VictoryStyle.ASSASSIN;
        }
        if (destructedPer < 30) {
            style = "Manipulator";
            vStyle = VictoryStyle.MANIPULATIVE;
        }
        styleField.SetContent(style);

        int excellencyForPrize = excellency + RandomSpecial.Range(0, 10);
        var enemyPrefabNames = CentralResource.Instance.BattleDeathTracker.EnemyObjectNames;
        for (int i = 0; i < enemyPrefabNames.Count; i++) {
            string s = enemyPrefabNames[i];

            for (int j = i+1; j < enemyPrefabNames.Count; j++) {
                if(s.Equals(enemyPrefabNames[j])) {
                    Debug.Log("Enemy name repeteated"+enemyPrefabNames[j]);
                    enemyPrefabNames.RemoveAt(j);
                    j--;
                }
            }
            enemyPrefabNames[i] = s.Replace("(Clone)", "");
            Debug.Log("Enemy name is "+enemyPrefabNames[i]);
        }
        prizeUnits.Clear();
        for (int i = 0; i < enemyPrefabNames.Count; i++) {
            var prizeOfEnemy = prizeData.GetPrizeOfEnemy(enemyPrefabNames[i]);
            //Debug.Log("Enemy name is "+enemyPrefabNames[i] + " prize is "+prizeOfEnemy);
            if(prizeOfEnemy == null) {
                //Debug.Log("No enemy of name "+enemyPrefabNames[i]+ " on Prize Data!");
            } else {
                prizeUnits.Add(prizeOfEnemy);
            }
        }

        int excellencyOfGottenPrizes = -1;
        possiblePrizes.Clear();
        for (int i = 0; i < prizeUnits.Count; i++) {
            //Debug.Log("Prize Unit loop! ");
            var prizes = prizeUnits[i].prizes;
            for (int j = 0; j < prizes.Length; j++) {
                //Debug.Log("Prize Loop ");
                var p = prizes[j];
                if(p.excellency < excellencyForPrize && p.excellency >= excellencyOfGottenPrizes) {
                    if(p.victoryStyle == VictoryStyle.MIXED || p.victoryStyle == vStyle) {
                        if(p.excellency > excellencyOfGottenPrizes) {
                  //          Debug.Log("Prizes reset ");
                            possiblePrizes.Clear();
                        }
                    //    Debug.Log("Prize Add!!! "+p.skillId +" "+p.excellency);
                        excellencyOfGottenPrizes = p.excellency;
                        possiblePrizes.Add(p);
                    }
                }
            }
        }
        if(possiblePrizes.Count >0) {
            Debug.Log("Prize GET!!!");
            var prizeGotten = possiblePrizes[RandomSpecial.Range(0, possiblePrizes.Count)];
            skillInfoUi.ChangeSkill(prizeGotten.skillId);
            Debug.Log("Prize GET!!!" +prizeGotten.skillId);
            CentralResource.Instance.SkillInventoryDeckManagement.AddToInventory(prizeGotten.skillId);
        } else {
            skillInfoUi.NoSkill();
        }
        

    }

    public void ShowOk() {
        button.gameObject.SetActive(true);
    }

    public void HideOk() {
        button.gameObject.SetActive(false);
    }

    private void OkPress() {
        if(!button.gameObject.activeSelf) return;
        Time.timeScale = 1;
        uiClose.Invoke();
        graphicParent.gameObject.SetActive(false);
        
    }

    // Update is called once per frame
    void Update () {
	    
	}
}
