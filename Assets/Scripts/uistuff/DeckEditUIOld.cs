﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class DeckEditUIOld : MonoBehaviour {
    private SkillInventoryDeckManagement skillInventoryDeckManagement;
    private SkillInventory skillInvent;
    [SerializeField]
    SkillButtonsControl inventorySkillButtons;
    [SerializeField]
    SkillButtonsControl deckSkillButtons;

    public Text textPrefab;
    List<Text> texts = new List<Text>();
    public GameObject graphicParent;

    public Button okButton;
    public Button returnButton;
    private bool uiShowing;
    private bool uiClosing;
    private int selectedSkillIdInventory = -1;
    private int selectedDeckPosition = -1;
    public SwapAnimationUI swapAnimation;
    private int lastPressedDeckButton;
    public GameObject textParent;

    internal void Start() {
        skillInventoryDeckManagement = CentralResource.Instance.SkillInventoryDeckManagement;
        skillInvent = skillInventoryDeckManagement.SkillInvenDeckData.SkillInventory;

        inventorySkillButtons.ButtonPressedAction += InventoryPress;
        deckSkillButtons.ButtonPressedAction += DeckPress;


        okButton.onClick.AddListener(OkClick);
        returnButton.onClick.AddListener(returnClick);

    }

    private void returnClick() {
        SoundSingleton.Instance.PlaySoundOnPause("cancel");
        Close();
    }

    private void OkClick() {
        skillInventoryDeckManagement.CopyTempToCurrent();
        CentralResource.Instance.SaveSlots.SaveData();
        SoundSingleton.Instance.PlaySoundOnPause("uigood2");
        Close();
    }

    public void Close() {
        if (uiClosing) return;
        Time.timeScale = 1;
        graphicParent.SetActive(false);
        uiShowing = false;
        uiClosing = true;
        DOVirtual.DelayedCall(0.2f, ClosingFinish);
    }

    private void ClosingFinish() {
        uiClosing = false;
    }

    private void Open() {
        if (uiClosing) return;
        Time.timeScale = 0;

        skillInventoryDeckManagement.DeckTempBecomeCurrentContent();
        int numberOfSkills = SkillInfoProvider.Instance.NumberOfSkillsHeroThisBuild;
        var heroAccess = SkillInfoProvider.Instance.SkillIdHeroAccessible;
        inventorySkillButtons.ShowThisAMountButtons(numberOfSkills);
        for (int i = 0; i < numberOfSkills; i++) {
            var text = (Text)Instantiate(textPrefab, inventorySkillButtons.GetButtonGeneralId(i).transform.position, Quaternion.identity);
            texts.Add(text);
            text.transform.SetParent(textParent.transform);
            text.text = "";
            text.alignment = TextAnchor.UpperCenter;
            text.fontSize = 14;
            text.transform.position += new Vector3(0, 16);

        }
        for (int i = 0; i < numberOfSkills; i++) {
            inventorySkillButtons.SetSkillOfButton(i, (int)heroAccess[i]);
        }

        UpdateSkillAmountInventory();
        UpdateDeckView();
        inventorySkillButtons.GetButtonGeneralId(0).Select();
        //inventorySkillButtons.GetButtonGeneralId(0).SkillButtonSelected();
    }

    internal void Update() {
        if (Input.GetKeyDown(KeyCode.X) &&
            !CentralResource.Instance.BattleFlowSys.InBattle && (Time.timeScale != 0 || uiShowing)) {
            if (uiShowing) {
                Close();
            } else {
                if (uiClosing) return;
                uiShowing = true;
                graphicParent.SetActive(true);
                DOVirtual.DelayedCall(0.07f, () => {
                    Open();

                });
            }

        }
    }

    private void UpdateSkillAmountInventory() {

        var heroAccess = SkillInfoProvider.Instance.SkillIdHeroAccessible;
        for (int i = 0; i < heroAccess.Length; i++) {
            var v = CalculateAvailableAmountSkill(heroAccess[i]);
            inventorySkillButtons.GetButtonGeneralId(i).SpinAnim(false);
            if (v > 0)
                texts[i].text = "" + v;
            else
                texts[i].text = "";
            bool hasSome = v > 0;
            //bool deckFull = skillInventoryDeckManagement.DeckTempFull();
            if (hasSome) {
                int amountSkill = skillInventoryDeckManagement.AmountInTempDeck((int)heroAccess[i]);
                if (amountSkill < 5) {
                    inventorySkillButtons.GetButtonGeneralId(i).EnableAnimation();
                } else {
                    inventorySkillButtons.GetButtonGeneralId(i).DisableAnimationForce();
                }

                inventorySkillButtons.SetSkillOfButton(i, (int)heroAccess[i]);
            } else {
                inventorySkillButtons.SetSkillOfButton(i, -1);
                inventorySkillButtons.GetButtonGeneralId(i).DisableAnimationForce();

            }
            if (selectedSkillIdInventory == (int)heroAccess[i])
                //inventorySkillButtons.GetButtonGeneralId(i).ForceSelectedAnimation();
                inventorySkillButtons.GetButtonGeneralId(i).SpinAnim(true);

        }
    }
    private int CalculateAvailableAmountSkill(SkillInfoProvider.SkillId skillId) {
        int i = (int)skillId;
        return CalculateAvailableAmountSkill(i);
    }

    private int CalculateAvailableAmountSkill(int skillid) {
        return skillInvent.AmountOf(skillid) - skillInventoryDeckManagement.AmountInTempDeck(skillid);
    }

    private void UpdateDeckView() {
        var deckContent = skillInventoryDeckManagement.GetDeckTempContent();
        int buttonAmount = deckSkillButtons.GetAMountButtons();
        for (int i = 0; i < buttonAmount; i++) {
            deckSkillButtons.GetButtonGeneralId(i).SpinAnim(false);
            if (deckContent.Count > i) {
                deckSkillButtons.SetSkillOfButton(i, deckContent[i]);
                deckSkillButtons.GetButtonGeneralId(i).EnableAnimation();
            } else
                deckSkillButtons.SetSkillOfButton(i, -1);
            if (i == selectedDeckPosition) {
                //deckSkillButtons.GetButtonGeneralId(selectedDeckPosition).ForceSelectedAnimation();
                deckSkillButtons.GetButtonGeneralId(selectedDeckPosition).SpinAnim(true);
                
            }
        }
    }

    private void DeckPress(SkillButton arg1, SkillButtonsControl arg2) {

        int gId = arg2.GeneralIdOfButton(arg1);
        lastPressedDeckButton = gId;

        if (selectedSkillIdInventory >= 0) { //card swap
            if (arg1.SkillId >= 0) {
                skillInventoryDeckManagement.RemoveSkillDeckTempPosition(gId);
            }
            //skillInventoryDeckManagement.AddToTempDeck(selectedInventory);
            skillInventoryDeckManagement.InsertOnTempDeck(selectedSkillIdInventory, gId);
            SoundSingleton.Instance.PlaySoundOnPause("draw");
            SoundSingleton.Instance.PlaySoundOnPause("uigood1");
            swapAnimation.Anim(
                deckSkillButtons.GetButtonGeneralId(gId).transform.position,
                GetInventoryButtonBySkillId(selectedSkillIdInventory).transform.position);
            SelectInventoryWindow();
            selectedSkillIdInventory = -1;

        } else {
            if (arg1.SkillId >= 0) {
                SoundSingleton.Instance.PlaySoundOnPause("uigood1");
                selectedDeckPosition = gId;
                SelectInventoryWindow();
                //Debug.Log("SET Selected deck pos " + selectedDeckPosition);
            }

        }
        UpdateDeckView();
        UpdateSkillAmountInventory();
    }



    private void InventoryPress(SkillButton arg1, SkillButtonsControl arg2) {
        int skillId = arg1.SkillId;
        if (skillId < 0) return;
        if (CalculateAvailableAmountSkill(skillId) > 0
            ) {
            //if (!skillInventoryDeckManagement.DeckTempFull()) {
            int amountSkill = skillInventoryDeckManagement.AmountInTempDeck(skillId);
            if (amountSkill < 5) {
                if (selectedDeckPosition >= 0) {
                    Debug.Log("TRy swap " + selectedDeckPosition);
                    skillInventoryDeckManagement.RemoveSkillDeckTempPosition(selectedDeckPosition);
                    skillInventoryDeckManagement.InsertOnTempDeck(skillId, selectedDeckPosition);
                    SoundSingleton.Instance.PlaySoundOnPause("draw");
                    SoundSingleton.Instance.PlaySoundOnPause("uigood1");
                    swapAnimation.Anim(
                        deckSkillButtons.GetButtonGeneralId(selectedDeckPosition).transform.position,
                        GetInventoryButtonBySkillId(skillId).transform.position);
                    SelectDeckWindow();
                    selectedDeckPosition = -1;  
                    
                } else {
                    if (!skillInventoryDeckManagement.DeckTempFull()) {
                        skillInventoryDeckManagement.AddToTempDeck(skillId);
                        SoundSingleton.Instance.PlaySoundOnPause("draw");
                        SoundSingleton.Instance.PlaySoundOnPause("uigood1");
                    } else {
                        SoundSingleton.Instance.PlaySoundOnPause("uigood1");
                        selectedSkillIdInventory = skillId;
                        SelectDeckWindow();
                    }
                }
                UpdateDeckView();
                UpdateSkillAmountInventory();

            }
            //}


        }


    }

    private void SelectDeckWindow() {
        deckSkillButtons.GetButtonGeneralId(lastPressedDeckButton).Select();
    }

    private void SelectInventoryWindow() {
        if(selectedSkillIdInventory >= 0) {
            GetInventoryButtonBySkillId(selectedSkillIdInventory).Select();
        } else {
            inventorySkillButtons.GetButtonGeneralId(0).Select();
        }
        
    }

    private SkillButton GetInventoryButtonBySkillId(int skillId) {
        var heroAccess = SkillInfoProvider.Instance.SkillIdHeroAccessible;
        for (int i = 0; i < heroAccess.Length; i++) {
            if ((int)heroAccess[i] == skillId) return inventorySkillButtons.GetButtonGeneralId(i);
        }
        return null;
    }

    internal void Awake() { }



}
