﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ImageButton : MonoBehaviour {

    public Image image;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void ChangeImage(Sprite s)
    {
        image.sprite = s;
    }
}
