﻿using UnityEngine;
using System.Collections;

public class AlwaysAnimateOnEnable : MonoBehaviour {
    private Animator animator;
    public string triggerName;

    void Awake() {
        animator = GetComponent<Animator>();
        animator.SetTrigger(triggerName);
    }

	// Use this for initialization
	void Start () {
	    
	}

    void OnEnable() {
        if(animator!= null) {
            animator.SetTrigger(triggerName);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
