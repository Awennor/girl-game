﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class HPGuiScript : MonoBehaviour {

    public Text hpText;
    public Text hpMaxText;
    public GameObject hero;
    int displayedHP = -1;
    int displayedMaxHP = -1;
    private HPSystem hp;

    public HPSystem Hp
    {
        get
        {
            return hp;
        }

        set
        {
            hp = value;
        }
    }

    // Use this for initialization
    void Start () {
        //pC = hero.GetComponent<PlayerCollision>();
        if(hero != null)
            Hp = hero.GetComponent<HPSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        if (displayedHP != Hp.GetHP()) {
            UpdateHP(Mathf.CeilToInt(Hp.GetHP()));
        }
        if (displayedMaxHP != Hp.GetMaxHP())
        {
            UpdateMaxHP((int)Hp.GetMaxHP());
        }
    }

    private void UpdateMaxHP(int v)
    {
        hpMaxText.text = v + "";
    }

    private void UpdateHP(int v)
    {
        hpText.text = v + "";
    }
}
