﻿using UnityEngine;
using System.Collections;
using System;

public class DamageShowNumber : MonoBehaviour {

    public DamageNumberSingle prefab;
    DamageNumberSingle[] objects = new DamageNumberSingle[10];
    private int currentIndex;
    float degrees;
    Transform parent;

    // Use this for initialization
    void Start () {
        parent = new GameObject().transform;
        parent.SetParent(this.transform);
        for (int i = 0; i < objects.Length; i++) {
            objects[i] = Instantiate(prefab);
            objects[i].gameObject.SetActive(false);
            objects[i].transform.SetParent(parent);
        }
        CollisionProj.damageHappen += damageHappenMethod;
        CollisionProj.damageMindHappen += damageHappenMethod2;
        DamageActions.DamageHappens += damageHappenMethod3;
	}

    private void damageHappenMethod3(float damage, GameObject arg2) {
        int type = 0;
        DamageHappen(damage, type, arg2);
    }

    private void damageHappenMethod2(CollisionProj arg1, float damage) {
        int type = 1;
        if(damage != 0)
            DamageHappen(arg1, damage, type);
    }

    private void damageHappenMethod(CollisionProj arg1, float damage) {

        int type = 0;
        if(damage != 0)
            DamageHappen(arg1, damage, type);
    }



    private void DamageHappen(CollisionProj arg1, float damage, int type) {
        var gameObject1 = arg1.gameObject;
        DamageHappen(damage, type, gameObject1);
    }

    private void DamageHappen(float damage, int type, GameObject gameObject1) {
        var pos = gameObject1.transform.position;

        pos += Quaternion.Euler(0, degrees, 0) * Vector3.forward * 4;
        degrees += 60;
        if(damage < 0 && type == 0) {
            damage *=-1;
            type = 2;
        }
        objects[currentIndex].show(pos, damage, type);

        currentIndex++;
        if (currentIndex >= objects.Length) {
            currentIndex = 0;
        }
    }

    // Update is called once per frame
    void Update () {
	    //parent.gameObject.SetActive(Time.deltaTime != 0);
	}
}
