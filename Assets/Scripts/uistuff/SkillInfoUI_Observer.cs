﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface SkillInfoUI_Observer {
    
    void OnChangeSkill(SkillInfoProvider.SkillId skill);
    void OnEnabledColor();
    void OnDisableColor();
    void OnSpecialColorBullets();
    void OnEffect1Request();
    void OnPositiveEnergies();
    void OnNegativeEnergies();
    void OnMightUseEnergies();
    void OnStopMightUseEnergies();
    void OnClickEffect();
    void OnNoSkill();
    void OnNormalColorBullets(int amount);
}
