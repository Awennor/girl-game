﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using Assets.Scripts;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class BattleSkillButton : MonoBehaviour{
    private const int MaxPowersChosable = 7;
    public SkillButtonsControl skillButtonsCtrl;
    ChosenSkillHolder chosenSkillHolder;
    List<int> chosenBuffer = new List<int>();
    public SkillButtonsControl chosenBufferIcons;
    public Button okButton;
    public Action requestingEnd;
    public ObjectHolder energies;
    SkillPerTurnProvider skillPerTurnProvider;
    List<SkillButton> chosenButtons = new List<SkillButton>();
    List<int> recentlyAdded = new List<int>();
    private bool turnOpenEnergyAnimation;
    public EventSystem eventSystem;
    public CursorLogic cursor;

    public UnityEvent EnergyUse;
    public UnityEvent EnergyCreate;

    public Image noEnoughEnergyImage;
    private int latestEnergyCost;

    //public GameObject heroTEMP; //until you create the player change logic

    public ChosenSkillHolder ChosenSkillHolder {
        get {
            return chosenSkillHolder;
        }

        set {
            chosenSkillHolder = value;
        }
    }

    public Action RequestingEnd {
        get {
            return requestingEnd;
        }

        set {
            requestingEnd = value;
        }
    }



    internal void GoRecentlyAdded() {
        //Debug.Log("Should called skill button");
        var id = 0;
        //Time.timeScale = 0;
        if (id >= 0) {
            SoundSingleton.Instance.PlaySoundOnPause("draw");
            int skillPosIdRecentlyAdded = recentlyAdded[id];
            int line = (skillPosIdRecentlyAdded / 3) + 1;
            int row = skillPosIdRecentlyAdded % 3;
            var turnSkillIds = skillPerTurnProvider.GetTurnSkillIds();
            skillButtonsCtrl.SetSkillOfButton(line, row, turnSkillIds[skillPosIdRecentlyAdded]);
            recentlyAdded.RemoveAt(id);
        }
        UpdateEnergy();


    }

    public void Reopen() {
        chosenButtons.Clear();
        chosenBuffer.Clear();
        updateChosenBuffer();
        chosenSkillHolder.Clear();
        //cursor.PauseSeek();
        SkillButton.StopSelectSound = 2;
        skillButtonsCtrl.SelectButton(1, 0);
        cursor.TeleportTo(skillButtonsCtrl.GetButton(1, 0).gameObject);

        int energyCurrent = skillPerTurnProvider.Energy + 2;


        skillButtonsCtrl.AfterStartCallback(() => {
            skillPerTurnProvider.NewTurn();
            var turnSkillIds = skillPerTurnProvider.GetTurnSkillIds();

            recentlyAdded.Clear();
            recentlyAdded.AddRange(skillPerTurnProvider.RecentlyAdded);
            //Debug.Log("START CALLBACK!!!");
            //var seq = DOTween.Sequence();
            for (int i = 0; i < recentlyAdded.Count; i++) {
                DOVirtual.DelayedCall((i + 1) * 0.1f + 0.33f, GoRecentlyAdded, true);
            }
            if (recentlyAdded.Count > 0)
                DOVirtual.DelayedCall(0.1f + (1) * 0.35f, ButtonSelect, true);

            int turnSId = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (turnSId < turnSkillIds.Count) {

                        if (!recentlyAdded.Contains(turnSId))
                            skillButtonsCtrl.SetSkillOfButton(i + 1, j, turnSkillIds[turnSId]);
                        turnSId++;
                    } else {
                        skillButtonsCtrl.SetSkillOfButton(i + 1, j, -1);
                        //goto EndTwoLoops;
                    }
                }
            }
            var turnSkillIds2 = skillPerTurnProvider.GetTurnSkillIds2();
            for (int i = 0; i < 3; i++) {
                if (i < turnSkillIds2.Count) {
                    skillButtonsCtrl.SetSkillOfButton(0, i, turnSkillIds2[i]);
                } else {
                    skillButtonsCtrl.SetSkillOfButton(0, i, -1);
                }
            }

        });
        if (recentlyAdded.Count <= 0)
            ButtonSelect();
        turnOpenEnergyAnimation = true;
        ChangeEnergy(energyCurrent);
        turnOpenEnergyAnimation = false;
        //eventSystem.enabled =false;
        //DOVirtual.DelayedCall(0.6f, eventSystemOn, true);;

    }



    private void ButtonSelect() {
        skillButtonsCtrl.SelectButton(1, 1);
        skillButtonsCtrl.SelectButton(1, 0);
        UpdateEnergy();
    }

    public void BattleStart() {
        if (skillPerTurnProvider != null) {
            skillPerTurnProvider.BattleStart();
        }
    }

    private void UpdateEnergy() {
        ChangeEnergy(skillPerTurnProvider.Energy);
    }

    public void EnergyCost(int energyCost) {
        latestEnergyCost = energyCost;
        if(chosenButtons.Contains(skillButtonsCtrl.ShownButton)) {
            energyCost = 0;
        }
        
        int energyNow = skillPerTurnProvider.Energy;
        //Debug.Log(energyCost + "energy cost");
        noEnoughEnergyImage.gameObject.SetActive(energyNow < energyCost);

        if (noEnoughEnergyImage.gameObject.activeSelf) {
            noEnoughEnergyImage.DOKill();
            
            var seq = DOTween.Sequence();
            seq.SetTarget(noEnoughEnergyImage);
            seq
                .Append(noEnoughEnergyImage.DOFade(1, 0.1f))
                .AppendInterval(0.15f)
                .Append(noEnoughEnergyImage.DOFade(0, 0.1f))
                .AppendInterval(0.15f)
                .SetLoops(-1).SetUpdate(true);
        }

        for (int i = 0; i < energyNow; i++) {
            if (i < energyCost) {
                energies.GetHoldedObject(energyNow - i - 1).GetComponent<RayGraphic>().MightUse();
                Debug.Log(i + " Energy might use");
            } else {
                Debug.Log(i + " Energy KILL might use");
                energies.GetHoldedObject(energyNow - i - 1).GetComponent<RayGraphic>().TryKillMightUse();
            }

        }

    }

    private void ChangeEnergy(int energyCurrent) {
        if (energyCurrent < 0) energyCurrent = 0;
        if (energyCurrent > 9) energyCurrent = 9;
        int difference = energyCurrent - skillPerTurnProvider.Energy;
        skillPerTurnProvider.Energy = energyCurrent;
        var appearingBalls = skillPerTurnProvider.Energy;
        if (difference < 0) appearingBalls += -difference;
        energies.AppearingBalls(appearingBalls);
        if (difference > 0) {
            EnergyCreate.Invoke();
            for (int i = 0; i < difference; i++) {
                //GameObject gameObject =energies.GetHoldedObject(i);
                GameObject gameObject = energies.GetHoldedObject(energyCurrent - difference + i);

                if (turnOpenEnergyAnimation) {
                    gameObject.GetComponent<RayGraphic>().Summon(i * 0.2f + 0.3f);
                    //gameObject.GetComponent<RayGraphic>().Summon(i*0.2f +0.3f);
                } else {
                    gameObject.GetComponent<RayGraphic>().Summon(0.1f);
                    //gameObject.GetComponent<RayGraphic>().Summon(0.1f);
                }

            }
        }

        if (difference < 0) {
            EnergyUse.Invoke();
            int diffAux = -difference;
            Debug.Log("Diff aux " + diffAux + " energy curr" + energyCurrent);
            for (int i = 0; i < diffAux; i++) {
                GameObject gameObject = energies.GetHoldedObject(energyCurrent + diffAux - 1 - i);
                gameObject.GetComponent<RayGraphic>().DisappearAnim(0f);

            }
        }

        int rows = skillButtonsCtrl.GetNumberRows();
        for (int i = 0; i < rows; i++) {
            var group = skillButtonsCtrl.buttonGroups[i];
            var sBs = group.SkillButtons;
            for (int j = 0; j < sBs.Count; j++) {
                var sB = sBs[j];
                int skillId = sB.SkillId;
                if (skillId < 0) {
                    sB.GetComponent<Button>().transition = Selectable.Transition.None;
                    sB.GetComponent<Animator>().SetTrigger("Disabled");
                    continue;
                }
                int energyC = SkillInfoProvider.Instance.GetEnergyCost(skillId);
                if ((skillPerTurnProvider.Energy >= energyC && chosenBuffer.Count < MaxPowersChosable) || chosenButtons.Contains(sB)) {
                    sB.GetComponent<Button>().transition = Selectable.Transition.Animation;
                } else {
                    sB.GetComponent<Button>().transition = Selectable.Transition.None;
                    sB.GetComponent<Animator>().SetTrigger("Disabled");
                }

                //sB.Interactable(energy >= energyC);
            }
        }
        EnergyCost(latestEnergyCost);
    }

    void Awake() {
        CharacterControlledSystem.Instance.controlledCharaChange += (obj) => {
            chosenSkillHolder = obj.GetComponent<ChosenSkillHolder>();
            skillPerTurnProvider = obj.GetComponent<SkillPerTurnProvider>();
        };

    }


    // Use this for initialization
    void Start() {

        skillButtonsCtrl.ButtonPressedAction += (SkillButton button, SkillButtonsControl buttonGroup) => {

            int skillId = button.SkillId;
            var skillId1 = (SkillInfoProvider.SkillId)skillId;
            var skillInfo = SkillInfoProvider.Instance;
            int energyC = skillInfo.GetEnergyCost(skillId);
            if (!chosenButtons.Contains(button)) {

                if (energyC <= skillPerTurnProvider.Energy) {
                    SoundSingleton.Instance.PlaySoundOnPause("uigood1");
                    if (skillInfo.IsUtilSkill(skillId1)) {
                        var util = skillInfo.GetSkillUtilData(skillId);

                        ChangeEnergy(skillPerTurnProvider.Energy + util.EnergyAmount);

                        skillButtonsCtrl.SetSkillOfButton(-1, button);
                        button.SelectNext();
                        UsedUpSkillInThisButton(button);
                        if (util.DeckRefresh) {

                            ChangeEnergy(0);
                            chosenBuffer.Clear();
                            OkButtonPress();
                            skillPerTurnProvider.DeckRefreshUsed();
                        }

                    } else {
                        if (chosenBuffer.Count < MaxPowersChosable) {
                            chosenBuffer.Add(skillId);
                            updateChosenBuffer();
                            button.icon.gameObject.SetActive(false);
                            chosenButtons.Add(button);
                        }


                    }
                    ChangeEnergy(skillPerTurnProvider.Energy - energyC);
                }
            } else {
                SoundSingleton.Instance.PlaySoundOnPause("uicancel");
                //Debug.Log("BUTTON PRESSED 4");
                chosenButtons.Remove(button);
                chosenBuffer.Remove(skillId);
                updateChosenBuffer();
                button.icon.gameObject.SetActive(true);
                //skillButtonsCtrl.RefreshButtonShown();
                ChangeEnergy(skillPerTurnProvider.Energy + energyC);
            }


            //chosenSkillHolder.AddSkill(skillId);
            //Debug.Log("Added Skill "+skillId);
            //chosenSkillHolder.Add(button.);
        };

        okButton.onClick.AddListener(() => {
            OkButtonPress();
        });
    }

    private void OkButtonPress() {
        for (int i = 0; i < chosenBuffer.Count; i++) {
            chosenSkillHolder.AddSkill(chosenBuffer[i]);
        }
        for (int i = 0; i < chosenButtons.Count; i++) {
            UsedUpSkillInThisButton(chosenButtons[i]);
        }
        SoundSingleton.Instance.PlaySoundOnPause("uigood2");
        if (RequestingEnd != null)
            RequestingEnd();
    }

    private void updateChosenBuffer() {
        chosenBufferIcons.SetSkillOfButtons(0, -1);
        //chosenBufferIcons.DisableButtonsForceAnimation(0);
        int amount = chosenBufferIcons.GetAMountButtons();
        //for (int i = 0; i < chosenBuffer.Count; i++) {
        for (int i = 0; i < amount; i++) {
            var skillButton = chosenBufferIcons.GetButtonGeneralId(i);
            skillButton.ShowFrame(i < chosenBuffer.Count);
            if (i < chosenBuffer.Count) {
                chosenBufferIcons.SetSkillOfButton(0, i, chosenBuffer[i]);
                skillButton.NormalAnimation();

            } else {
                skillButton.DisableAnimationForce();
            }

            //if (chosenBuffer[i] >= 0)
            //  chosenBufferIcons.GetButtonGeneralId(i).NormalAnimation();

        }

    }

    private void UsedUpSkillInThisButton(SkillButton sB) {
        int line = skillButtonsCtrl.GetLineOfButton(sB);
        int row = skillButtonsCtrl.GetRowOfButton(sB);
        if (line >= 1) {
            skillPerTurnProvider.UsedUpSkillInPosition((line - 1) * 3 + row);
        }
    }

    // Update is called once per frame
    void Update() {

    }


}

