﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets.Scripts.LogicScript;
using Assets.ReusablePidroh.Scripts.Logic;
using Assets.ReusablePidroh.Scripts;
using System;

public class BattleCharaGUI : MonoBehaviour {

    private HPGuiScript hpGuiScript;
    public RectTransform image;
    private ATBManager atb;
    private ScaleToRangeValue scaler = new ScaleToRangeValue();
    public GameObject graphicHolder;
    public GameObject atbHolder;
    public Animator animator;

    // Use this for initialization
    void Start ()
    {
        hpGuiScript = GetComponent<HPGuiScript>();
        //Debug.Log("HP GUI SCRIPT IS "+hpGuiScript);
        
        scaler.RectT = image;

    }

    public void ShowATBAnimationEnd() {
        atbHolder.SetActive(true);
    }

    public void ATBHide() {
        atbHolder.SetActive(false);
    }

    private void updateATB()
    {
        RangedValue rV = atb.TimeBar;
        scaler.RV = rV;
    }

    // Update is called once per frame
    void Update () {
	
	}

    internal void ChangeChara(GameObject controlled)
    {
        var hp = controlled.GetComponent<HPSystem>();
        if(hpGuiScript == null)
            hpGuiScript = GetComponent<HPGuiScript>();
        //Debug.Log(controlled+" change chara "+hp +" HP GUI SCRIPT IS..."+hpGuiScript);
        //Debug.Log("HP GUI SCRIPT IS " + hpGuiScript);
        if (hp != null)
            hpGuiScript.Hp = hp;
        atb = controlled.GetComponent<CharacterBattleHolder>().CharacterBattle.Atb;
        updateATB();

    }

    internal void BattleEnd() {
        animator.SetTrigger("battleend");
    }

    internal void BattleStart() {
        animator.SetTrigger("battlestart");
    }

    internal void Visible(bool b) {
        graphicHolder.SetActive(b);
    }
}
