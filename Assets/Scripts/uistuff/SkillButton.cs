﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
using DG.Tweening;

public class SkillButton : MonoBehaviour {

    public Image icon;
    int skillId;
    public Action<SkillButton> buttonPressed;
    public Action<SkillButton> buttonSelected;
    public Action<SkillButton> buttonMouseOver;
    public Action<SkillButton> buttonMouseOverExit;
    private bool selectableIfHasSkill;
    private Button button;
    private bool attackSelectedType;
    static int stopSelectSound;
    private Sequence spinTween;
    public Image frame;

    public int SkillId
    {
        get
        {
            return skillId;
        }

        set
        {
            skillId = value;
            enforceSelectableHasSkill();
        }
    }

    private void enforceSelectableHasSkill()
    {
        if (selectableIfHasSkill)
        {
            if (button != null)
                button.interactable = (skillId >= 0);
        }
    }

    public bool SelectableIfHasSkill
    {
        get
        {
            return selectableIfHasSkill;
        }

        set
        {
            selectableIfHasSkill = value;
        }
    }

    public bool AttackSelectedType {
        get {
            return attackSelectedType;
        }

        set {
            attackSelectedType = value;
        }
    }

    public static int StopSelectSound {
        get {
            return stopSelectSound;
        }

        set {
            stopSelectSound = value;
        }
    }

    // Use this for initialization
    void Start () {
        button = GetComponent<Button>();
        var onClick = button.onClick;
        onClick.AddListener(()=> {
            //stopSelectSound++;
            if(buttonPressed != null)
                buttonPressed(this);
        });
        enforceSelectableHasSkill();
        if(attackSelectedType) {
            button.animator.SetTrigger("AttackChosenAnim");
        }
        
    }

    internal void HideFrame() {
        frame.gameObject.SetActive(false);
    }

    public void SkillButtonSelected() {
        if(StopSelectSound <= 0)

            SoundSingleton.Instance.PlaySoundOnPause("uimove");
        else
            StopSelectSound--;
        if (buttonSelected != null)
            buttonSelected(this);
    }

    public void SkillButtonMouseOver() {
        if(StopSelectSound <= 0)

            SoundSingleton.Instance.PlaySoundOnPause("uimove");
        else
            StopSelectSound--;
        if (buttonMouseOver != null)
            buttonMouseOver(this);
    }

     public void SkillButtonMouseOverExit() {
        Debug.Log("MOUSE OVER EXIT");
        if (buttonMouseOverExit != null)
            buttonMouseOverExit(this);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void Interactable(bool v)
    {
        if (button != null)
            button.interactable = (v);
        
    }

    internal void SelectNext() {
        button.FindSelectableOnRight().Select();
    }

    internal void DisableAnimationForce() {
        if(button == null) return;
        button.transition = Selectable.Transition.None;
        button.animator.SetTrigger("Disabled");
    }

    internal void ForceSelectedAnimation() {
        button.transition = Selectable.Transition.None;
        button.animator.SetTrigger("Highlighted");
    }

    internal void EnableAnimation() {
        button.transition = Selectable.Transition.Animation;
    }

    internal void Select() {
        button.Select();
    }

    internal void SpinAnim(bool b) {
        if(b) {
            //Debug.Log("SPIN");
            //SpinAnim(false);

            spinTween = DOTween.Sequence();
            var transform = this.transform.GetChild(0);
            spinTween.Append(
                transform.DORotate(new Vector3(0,0,180), 0.2f).SetUpdate(true)
                ).Append(
                transform.DORotate(new Vector3(0,0,0), 0.2f).SetUpdate(true)
                ).SetLoops(-1);
            spinTween.SetUpdate(true);

        } else {
            var transform = this.transform.GetChild(0);
            transform.localRotation = Quaternion.identity;
            //Debug.Log("SPIN END");
            if(spinTween != null) {
                spinTween.Kill();
                spinTween = null;

            }
        }
    }

    internal void NormalAnimation() {
        button.animator.SetTrigger("Normal");
    }

    internal void ShowFrame(bool v) {
        frame.gameObject.SetActive(v);
    }
}
