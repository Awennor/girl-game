﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ObjectHolder : MonoBehaviour {

    public bool horizontal;
    public bool vertical;
    public int maxNumber;
    public int distance;
    public int offsetX = 0;
    public int offsetY = 0;

    public bool negativeDistance = false;

    public GameObject holdedObject;
    public int width = 23;
    public int height = 23;
    List<GameObject> holdedObjectList = new List<GameObject>();
    private bool started;
    private Action StartedAction;
    private int objectsActive = 0;

    public List<GameObject> HoldedObjectList
    {
        get
        {
            return holdedObjectList;
        }

        set
        {
            holdedObjectList = value;
        }
    }

    public void AppearingBalls(int amount)
    {
        objectsActive = amount;
        for (int i = 0; i < HoldedObjectList.Count; i++)
        {
            HoldedObjectList[i].SetActive(i < amount);
        }
    }

    void Awake() {
        //Debug.Log("Object Holder Awake");
        started = true;
        for (int i = 0; i < maxNumber; i++)
        {
            GameObject obj = (GameObject)Instantiate(holdedObject, transform.position, Quaternion.identity);
            obj.transform.SetParent(this.transform);
            obj.transform.localScale = new Vector3(1,1,1);
            HoldedObjectList.Add(obj);
            FixPositions();


            //SendMessage it to the transform then fix position in a loop and send it to different method
        }
        FixPositions();
        AppearingBalls(objectsActive);
        if (StartedAction != null)
            StartedAction();

    }

    // Use this for initialization
    void Start () {
        //Debug.Log("Object Holder Start");
    }
    public void AddStartedListener(Action a) {
        if (started)
            a();
        else
            StartedAction += a;
    }
    private void FixPositions()
    {
        for (int i = 0; i < HoldedObjectList.Count; i++)
        {
            int x = 0;
            int y = 0;
            
            if (horizontal) x = i * (width+distance) + offsetX;
            if (vertical) y = i * (height + distance) + offsetY;
            if (negativeDistance)
            {
                x *= -1;
                y *= -1;
            }
            HoldedObjectList[i].transform.localPosition = new Vector3(x, y);

        }
    }

    // Update is called once per frame
    void Update () {
	
	}

    internal GameObject GetHoldedObject(int v) {
        return HoldedObjectList[v];
    }
}
