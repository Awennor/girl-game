﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class SkillButtonGroupControl : MonoBehaviour {

    List<SkillButton> skillButtons = new List<SkillButton>();
    public bool unselectableButtons;
    public bool selectableIfHasSkill;
    public bool normalAnimation;
    public bool hideFrame = false;
    Action<SkillButtonGroupControl> startedCallback;

    public List<SkillButton> SkillButtons
    {
        get
        {
            return skillButtons;
        }

        set
        {
            skillButtons = value;
        }
    }

    // Use this for initialization
    void Start ()
    {
        GetComponent<ObjectHolder>().AddStartedListener(InitSkillButtons);
        
    }

    internal void CallbackStart(Action<SkillButtonGroupControl> setupButtonGroup)
    {
        if (started)
        {
            setupButtonGroup(this);
        }
        else {
            startedCallback += setupButtonGroup;
        }
    }
    bool started;
    private void InitSkillButtons()
    {
        var objects = GetComponent<ObjectHolder>().HoldedObjectList;

        for (int i = 0; i < objects.Count; i++)
        {
            SkillButtons.Add(objects[i].GetComponent<SkillButton>());
            var button = objects[i].GetComponent<Button>();
            button.interactable = !unselectableButtons;

            var skillButton = button.gameObject.GetComponent<SkillButton>();
            skillButton.AttackSelectedType = normalAnimation;
            skillButton.SelectableIfHasSkill = selectableIfHasSkill;
            if(hideFrame) {
                skillButton.HideFrame();
            }
            
            //Debug.Log("Here... HAPPENS?");
        }
        if(startedCallback != null)
            startedCallback(this);
        started = true;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
