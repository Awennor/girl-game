﻿using UnityEngine;
using System.Collections;

public class SkillIconHolder : MonoBehaviour {

    [SerializeField]
    private Sprite[] sprite;

    [SerializeField]
    private Color[] colors;

    public Sprite[] Sprite
    {
        get
        {
            return sprite;
        }

        set
        {
            sprite = value;
        }
    }

    public Color[] Colors {
        get {
            return colors;
        }

        set {
            colors = value;
        }
    }

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
