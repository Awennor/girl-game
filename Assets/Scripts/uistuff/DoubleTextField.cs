﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DoubleTextField : MonoBehaviour {

    public Text header;
    public Text content;

    public void SetHeader(string s) {
        header.text = s;
    }

    public void SetContent(string s)
    {
        content.text = s;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Visible(bool v) {
        gameObject.SetActive(v);
    }
}
