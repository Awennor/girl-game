﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyHPShowManager : MonoBehaviour {

    public EnemyHPShowUnit[] enemyHPShowUnits;
    public Transform objectHolder;
    bool pausing = false;
    public GameObject parent;
    public bool OnlyOnPause = true;

    void Start() {
        CentralResource.Instance.BattleFlowSys.OnBattleStart += ()=>{
            parent.SetActive(true);
        };
        CentralResource.Instance.BattleFlowSys.OnBattleEnd += ()=>{
            parent.SetActive(false);
        };
    }

	// Update is called once per frame
	void Update () {
	    if(pausing) {
            if(Time.deltaTime != 0) {
                pausing = false;
            }
        } else {
            if(Time.deltaTime == 0) {
                pausing = true;
                //UpdateHPs();
            }
        }
        UpdateHPs();
        //parent.SetActive(pausing || OnlyOnPause);
	}

    private void UpdateHPs() {
        int cc = objectHolder.childCount;
        for (int i = 0; i < enemyHPShowUnits.Length; i++) {
            if(i < cc) {
                var child = objectHolder.GetChild(i).gameObject;
                enemyHPShowUnits[i].TryBind(child);
                enemyHPShowUnits[i].Show(true);
            } else {
                enemyHPShowUnits[i].Show(false);
            }
        }
    }
}
