﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using DG.Tweening;
using System.Collections.Generic;

public class SkillInfoUI : MonoBehaviour, SkillInfoUI_Observer {

    [SerializeField]
    private DoubleTextField attackField, mindField, usesField;
    [SerializeField]
    public Text skillName;
    [SerializeField]
    public Image skillIcon;
    [SerializeField]
    public Image background;
    [SerializeField]
    Color normalColor, extraColor;
    [SerializeField]
    private SkillIconHolder skillIconHolder;
    [SerializeField]
    public ObjectHolder energyHolder;
    [SerializeField]
    String nothing = "-";
    [SerializeField]
    public Text generalText;
    [SerializeField]
    bool skillNeverSet = true;
    [SerializeField]
    private SkillInfoProvider.SkillId skill;
    [SerializeField]
    public ObjectHolder bulletHolder;

    internal void EnergyChange(int energyCost) {
        energyChange.Invoke(energyCost);
    }

    [SerializeField]
    public MyIntEvent energyChange;
    [SerializeField]
    private Button button;

    List<SkillInfoUI_Observer> observers = new List<SkillInfoUI_Observer>();
    private bool defaultFuncionalityOn = true;

    public Button Button {
        get {
            return button;
        }

        set {
            button = value;
        }
    }

    public SkillInfoProvider.SkillId Skill {
        get {
            return skill;
        }

        set {
            skill = value;
        }
    }

    public SkillIconHolder SkillIconHolder {
        get {
            return skillIconHolder;
        }

        set {
            skillIconHolder = value;
        }
    }

    public void NormalColor() {
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnEnabledColor();
        }
    }

    internal void AddObservable(SkillInfoUI_Observer skillInfoUI_obs) {
        observers.Add(skillInfoUI_obs);
    }

    public void ExtraColor() {
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnDisableColor();
        }

    }

    public void ChangeSkill(SkillInfoProvider.SkillId skill) {
        this.skill = skill;
        skillNeverSet = false;
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnChangeSkill(skill);
        }
        

    }

    internal void SpecialColorBullets() {
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnSpecialColorBullets();
        }

    }

    internal void NormalColorBullets(int amount) {
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnNormalColorBullets(amount);
        }

    }

    internal void EffectInBackground1() {
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnEffect1Request();
        }



    }

    internal void BlueEnergies() {
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnPositiveEnergies();
        }

    }

    internal void RedEnergies() {
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnNegativeEnergies();
        }


    }

    internal void MightUseEnergies() {
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnMightUseEnergies();
        }

    }

    internal void StopMightUseEnergies() {
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnStopMightUseEnergies();
        }

    }

    // Use this for initialization
    void Start() {
        if (button != null)
            button.onClick.AddListener(() => {
                //background.gameObject.SetActive(true);
                ClickEffect();

            });
        //Debug.Log("Skill Info Start");
        if (skillNeverSet)
            ChangeSkill(SkillInfoProvider.SkillId.FocusSphere);
        else
            ChangeSkill(Skill);
        if(defaultFuncionalityOn)
            this.observers.Add(this);
    }

    public void DisableDefaultFuncionality() {
        defaultFuncionalityOn = false;
        while(this.observers.Remove(this));
    }

    public void ClickEffect() {
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnClickEffect();
        }

    }

    // Update is called once per frame
    void Update() {

    }

    public void NoSkill() {
        for (int i = 0; i < observers.Count; i++) {
            observers[i].OnNoSkill();
        }

    }

    public void OnEnabledColor() {

        background.color = normalColor;
    }

    public void OnDisableColor() {
        background.color = extraColor;
    }

    public void OnSpecialColorBullets() {
        var listBullets = bulletHolder.HoldedObjectList;
        for (int i = 0; i < listBullets.Count; i++) {
            listBullets[i].GetComponent<RayGraphic>().noGoodColorMode();
        }
    }

    public void OnNormalColorBullets(int amount) {
        var listBullets = bulletHolder.HoldedObjectList;
        for (int i = 0; i < amount; i++) {
            listBullets[i].GetComponent<RayGraphic>().normalColorMode();
        }
    }

    public void OnEffect1Request() {
        background.DOComplete();
        Color c = background.color;
        DOTween.Sequence().Append(background.DOColor(new Color(0, 1, 1), 0.2f))
            .Append(background.DOColor(c, 0.6f)).SetUpdate(true).SetTarget(background);
        ;
    }

    public void OnPositiveEnergies() {
        var holdedObjects = energyHolder.HoldedObjectList;
        for (int i = 0; i < holdedObjects.Count; i++) {
            holdedObjects[i].GetComponent<RayGraphic>().normalColorMode(); ;
        }
    }

    public void OnNegativeEnergies() {
        var holdedObjects = energyHolder.HoldedObjectList;
        for (int i = 0; i < holdedObjects.Count; i++) {
            holdedObjects[i].GetComponent<RayGraphic>().noGoodColorMode(); ;
        }
    }

    public void OnMightUseEnergies() {
        var holdedObjects = energyHolder.HoldedObjectList;
        for (int i = 0; i < holdedObjects.Count; i++) {
            holdedObjects[i].GetComponent<RayGraphic>().MightUse();
        }
    }

    public void OnStopMightUseEnergies() {
        var holdedObjects = energyHolder.HoldedObjectList;
        for (int i = 0; i < holdedObjects.Count; i++) {
            holdedObjects[i].GetComponent<RayGraphic>().TryKillMightUse();
        }
    }

    public void OnClickEffect() {
        background.DOComplete();
        Color c = background.color;
        DOTween.Sequence().AppendInterval(0.15f).Append(background.DOColor(c, 0))
        .SetUpdate(true).SetTarget(background);
        background.color = new Color(0, 1, 1);
    }

    public void OnNoSkill() {
        energyChange.Invoke(0);
        skillName.text = nothing;
        attackField.SetContent(nothing);
        mindField.SetContent(nothing);
        skillIcon.gameObject.SetActive(false);
        energyHolder.AppearingBalls(0);
        bulletHolder.AppearingBalls(0);
    }

    public void OnChangeSkill(SkillInfoProvider.SkillId skill) {
        //Debug.Log("Skill Changed"+skill);
        if ((int)skill == -1) return;
        this.Skill = skill;
        skillNeverSet = false;
        var skillInfoProvider = SkillInfoProvider.Instance;
        skillName.text = skillInfoProvider.GetName(skill);
        skillIcon.sprite = SkillIconHolder.Sprite[(int)skill];
        //skillIcon.color = skillIconHolder.Colors[(int)skill];
        var energyCost = skillInfoProvider.GetEnergyCost(skill);
        energyHolder.AppearingBalls(energyCost);
        energyChange.Invoke(energyCost);

        skillIcon.gameObject.SetActive(true);
        bulletHolder.AppearingBalls(0);
        var battleSkill = skillInfoProvider.IsBattleSkill(skill);
        if (battleSkill) {
            int damage = skillInfoProvider.GetBaseDamage(skill);
            attackField.SetContent("" + damage);
            mindField.SetContent("" + skillInfoProvider.GetMindDamage(skill));
            usesField.SetContent("" + skillInfoProvider.GetRepetition(skill));
            bulletHolder.AppearingBalls(skillInfoProvider.GetRepetition(skill));
        }
        var utilSkill = skillInfoProvider.IsUtilSkill(skill);
        if (utilSkill) {
            SkillUtilDataUnit utilU = skillInfoProvider.GetSkillUtilData(skill);
            generalText.text = "\n";
            if (utilU.DrawAmount > 0)
                generalText.text += "Draws " + utilU.DrawAmount + " Power(s)\n";
            if (utilU.EnergyAmount > 0)
                generalText.text += "Generates " + utilU.EnergyAmount + " of Energy\n";
        }
        var buff = skillInfoProvider.GetSkillBuffData((int)skill);
        if (buff != null) {
            generalText.text = "";
            var atks = buff.Buff.AttackMultipliers;
            if (atks.Length >= 1 && atks[0] > 0) {
                generalText.text += "Attack +" + atks[0] * 100 + "%";
                generalText.text += "\n";
            }

            if (buff.Buff.DefenseMultiplier > 0) {
                generalText.text += "Defense +" + buff.Buff.DefenseMultiplier * 100 + "%";
                generalText.text += "\n";
            }

            if (buff.Buff.TimeDuration > 0) {

                generalText.text += "Lasts for " + buff.Buff.TimeDuration + "s";
                generalText.text += "\n";
            }
            if (buff.Buff.TurnDuration > 0) {

                generalText.text += "Lasts for " + buff.Buff.TurnDuration + " turns";
                generalText.text += "\n";
            }
        }

        generalText.gameObject.SetActive(!battleSkill);
        attackField.Visible(battleSkill);
        mindField.Visible(battleSkill);
        usesField.Visible(battleSkill);
    }
}


