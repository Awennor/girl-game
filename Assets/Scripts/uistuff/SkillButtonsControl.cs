﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class SkillButtonsControl : MonoBehaviourExtended {

    public List<SkillButtonGroupControl> buttonGroups = new List<SkillButtonGroupControl>();
    public SkillInfoUI skillInfo;
    public SkillIconHolder skillIconHolder;

    public Action<SkillButton, SkillButtonsControl> ButtonPressedAction;
    private int lastSelectedSkillId;
    private SkillButton shownButton;

    public SkillButton ShownButton {
        get {
            return shownButton;
        }

        set {
            shownButton = value;
        }
    }

    void Start() {
        Setup();

    }

    private void Setup() {
        for (int i = 0; i < buttonGroups.Count; i++) {
            buttonGroups[i].CallbackStart(SetupButtonGroup);
            //SetupButtonGroup(buttonGroups[i]);

        }
    }

    internal void ShowThisAMountButtons(int numberOfSkills) {
        int bId = 0;

        for (int i = 0; i < buttonGroups.Count; i++) {
            var buttonsOfGroup = buttonGroups[i].SkillButtons;

            for (int j = 0; j < buttonsOfGroup.Count; j++) {

                buttonsOfGroup[j].gameObject.SetActive(bId < numberOfSkills);
                bId++;
            }
        }
    }

    internal int GetAMountButtons() {
        int bId = 0;

        for (int i = 0; i < buttonGroups.Count; i++) {
            var buttonsOfGroup = buttonGroups[i].SkillButtons;

            for (int j = 0; j < buttonsOfGroup.Count; j++) {
                bId++;
            }
        }
        return bId;
    }

    internal SkillButton GetButtonGeneralId(int generalId) {
        int bId = 0;

        for (int i = 0; i < buttonGroups.Count; i++) {
            var buttonsOfGroup = buttonGroups[i].SkillButtons;

            for (int j = 0; j < buttonsOfGroup.Count; j++) {
                if (bId == generalId)
                    return buttonsOfGroup[j];
                bId++;
            }
        }
        return null;
    }

    internal int GeneralIdOfButton(SkillButton sb) {
        int bId = 0;

        for (int i = 0; i < buttonGroups.Count; i++) {
            var buttonsOfGroup = buttonGroups[i].SkillButtons;

            for (int j = 0; j < buttonsOfGroup.Count; j++) {
                if (sb == buttonsOfGroup[j])
                    return bId;
                bId++;
            }
        }
        return -1;
    }

    internal SkillButton GetButton(int row, int column) {
        return buttonGroups[row].SkillButtons[column];
    }

    private void SetupButtonGroup(SkillButtonGroupControl skillButtonGroupCtrl) {
        var skillButtons = skillButtonGroupCtrl.SkillButtons;

        for (int j = 0; j < skillButtons.Count; j++) {
            skillButtons[j].buttonPressed = ButtonPressed;
            skillButtons[j].buttonSelected = ButtonSelected;
            skillButtons[j].buttonMouseOver = ButtonMouseOver;
            skillButtons[j].buttonMouseOverExit = ButtonMouseOverExit;
            skillButtons[j].SkillId = -1;

            updateButtonIcon(skillButtons[j]);

        }
        base.EndStart();
    }



    internal void SelectButton(int buttonRow, int buttonId) {
        var sButtons = buttonGroups[buttonRow].SkillButtons;
        if(sButtons.Count > buttonId)
            sButtons[buttonId].GetComponent<Button>().Select();
    }

    internal void SetSkillOfButtons(int buttonRow, int skillId) {
        var sButtons = buttonGroups[buttonRow].SkillButtons;
        for (int i = 0; i < sButtons.Count; i++) {
            SetSkillOfButton(skillId, sButtons[i]);

        }
    }

    internal void SetSkillOfButton(int buttonGeneralId, int skillId) {

        SetSkillOfButton(skillId, GetButtonGeneralId(buttonGeneralId));

    }

    internal void SetSkillOfButton(int buttonRow, int buttonId, int skillId) {
        var sButtons = buttonGroups[buttonRow].SkillButtons;
        var skillButton = sButtons[buttonId];
        SetSkillOfButton(skillId, skillButton);

    }


    public void SetSkillOfButton(int skillId, SkillButton skillButton) {
        skillButton.SkillId = skillId;
        updateButtonIcon(skillButton);
    }

    public void updateButtonIcon(SkillButton skillButton) {
        int skillId = skillButton.SkillId;
        if (skillId >= 0) {
            skillButton.icon.sprite = skillIconHolder.Sprite[skillId];
            //skillButton.icon.color = skillIconHolder.Colors[skillId];
        }
            
        //skillButton.icon.enabled = skillId >= 0;
        skillButton.icon.gameObject.SetActive(skillId >= 0);
        //Debug.Log("UPDATED BUTTON ICON");
    }

    private void ButtonMouseOver(SkillButton obj) {
        ShownButton = obj;
        if (skillInfo == null) {
            return;
        }
        ShownButton = obj;
        int id = obj.SkillId;
        if (id >= 0)
            skillInfo.ChangeSkill((SkillInfoProvider.SkillId)id);
        else {
            skillInfo.NoSkill();
        }
    }

    private void ButtonMouseOverExit(SkillButton obj) {
        if (skillInfo == null) {
            return;
        }
        Debug.Log("MOUSE EXIT CONTROL");
        skillInfo.NoSkill();
        if(lastSelectedSkillId >= 0) {
            //skillInfo.ChangeSkill((SkillInfoProvider.SkillId)lastSelectedSkillId);
        }
    }

    private void ButtonSelected(SkillButton obj) {
        if (skillInfo == null) {
            return;
        }
        int id = obj.SkillId;
        ShownButton = obj;
        if (id >= 0) {
            lastSelectedSkillId = id;
            skillInfo.ChangeSkill((SkillInfoProvider.SkillId)id);
        }
            
        else {
            lastSelectedSkillId = -1;
            skillInfo.NoSkill();
        }
            
    }

    private void ButtonPressed(SkillButton obj) {
        //int skillId = obj.SkillId;
        if (ButtonPressedAction != null)
            ButtonPressedAction(obj, this);
    }

    // Update is called once per frame
    void Update() {

    }

    public int GetNumberRows() {
        return buttonGroups.Count;
    }

    internal int GetLineOfButton(SkillButton sB) {
        for (int i = 0; i < buttonGroups.Count; i++) {
            if (buttonGroups[i].SkillButtons.Contains(sB))
                return i;
        }
        return -1;
    }

    internal int GetRowOfButton(SkillButton sB) {
        for (int i = 0; i < buttonGroups.Count; i++) {
            if (buttonGroups[i].SkillButtons.Contains(sB))
                return buttonGroups[i].SkillButtons.IndexOf(sB);
        }
        return -1;
    }

    internal void DisableButtonsForceAnimation(int buttonRow) {
         var sButtons = buttonGroups[buttonRow].SkillButtons;
        for (int i = 0; i < sButtons.Count; i++) {
            sButtons[i].DisableAnimationForce();

        }
    }

    internal void RefreshButtonShown() {
        throw new NotImplementedException();
    }
}
