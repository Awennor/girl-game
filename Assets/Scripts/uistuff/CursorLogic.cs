﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class CursorLogic : MonoBehaviour {
    Vector3 destination;
    private MoveTowardsTarget move;
    //private MoveTowardsTargetRectTransform move;

    // Use this for initialization
    void Awake () {
        move = GetComponent<MoveTowardsTarget>();
        //move = GetComponent<MoveTowardsTargetRectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!isActiveAndEnabled) return;
        if (EventSystem.current == null)
            return;
        var obj = EventSystem.current.currentSelectedGameObject;
        DecideTarget(obj);
        //move.Target = obj.transform;
    }

    private void DecideTarget(GameObject obj) {
        if (obj != null) {
            //this.transform.position = obj.transform.position + new Vector3(0, 10);
            //move.Target = obj.transform;
            move.Target = obj.GetComponent<RectTransform>();
            move.OffsetLocal = new Vector3(0,30,0);
            //move.SetWorldOffset(0, 30);
        }
    }

    internal void PauseSeek() {
        move.enabled = false;
    }

    internal void TeleportTo(GameObject gameObject) {
        DecideTarget(gameObject);
        move.Teleport();
    }
}
