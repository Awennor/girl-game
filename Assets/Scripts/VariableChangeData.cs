﻿using System;
using UnityEngine;

[Serializable]
public class VariableChangeData {
    [SerializeField]
    VariableEnum variableEnum;
    [SerializeField]
    float newValue;

    internal void ChangeValue() {
        CentralResource.Instance.VariableSystem.SetVariableValue(variableEnum, newValue);
    }
}