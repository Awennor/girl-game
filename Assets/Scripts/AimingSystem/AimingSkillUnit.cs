﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class AimingSkillUnit {

    [SerializeField]
    SkillInfoProvider.SkillId[] skillIds;

    [SerializeField]
    float length;

    [SerializeField]
    AimingType aimingType;

    [SerializeField]
    Vector2 speed;

    public float Length {
        get {
            return length;
        }

        set {
            length = value;
        }
    }

    public Vector2 Speed {
        get {
            return speed;
        }

        set {
            speed = value;
        }
    }

    public AimingType AimingType {
        get {
            return aimingType;
        }

        set {
            aimingType = value;
        }
    }

    internal bool Contains(int skillId) {
        for (int i = 0; i < skillIds.Length; i++) {
            if (skillId == (int)skillIds[i]) {
                return true;
            }
        }
        return false;
    }

}

