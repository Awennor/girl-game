﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class AimingSkillHelper {
    private AttackInput attackInput;
    private ChosenSkillHolder chosenSkillHolder;
    private SkillHelper skillHelper;

    public AimingSkillHelper(SkillHelper skillHelper) {
        this.skillHelper = skillHelper;
        chosenSkillHolder = skillHelper.gameObject.GetComponent<ChosenSkillHolder>();
        attackInput = skillHelper.gameObject.GetComponent<AttackInput>();
        attackInput.CanAttackChangeCallback += CanAttackCallback;
        CharacterControlledSystem.Instance.controlledCharaChange += (obj) => {
            if (obj == null) return;
            if (skillHelper == null) return;
            if (obj == skillHelper.gameObject) {
                int s = chosenSkillHolder.CurrentSkill;

                DrawLine(s);
                UpdateLineVisibility();
            }
        };
        chosenSkillHolder.skillChange += (ChosenSkillHolder csh) => {
            //Debug.Log("Try 1");
            if (skillHelper.LineDrawManager != null) {
              //  Debug.Log("Try 2");
                int currentS = csh.CurrentSkill;
                if (currentS >= 0) {
                    DrawLine(currentS);
                    UpdateLineVisibility();
                } else {
                    UpdateLineVisibility();
                }
            }

        };
    }

    private void CanAttackCallback() {
        UpdateLineVisibility();
    }

    private void UpdateLineVisibility() {
        var lineDraw = skillHelper.LineDrawManager;
        if (lineDraw == null) return;
        if (attackInput.CanAttack() && CentralResource.Instance.BattleFlowSys.InBattle) {

            if (chosenSkillHolder.CurrentSkill >= 0) {
                lineDraw.Visible(true);
    
            } else {
                lineDraw.Visible(false);
              //  Debug.Log("LINE DRAW DISAPP");
            }
        } else {
            //if (lineDraw.Skill >= 0) {
            lineDraw.Visible(false);
            //Debug.Log("LINE DRAW DISAPP2");
            //}
        }
    }

    private void DrawLine(int skillId) {
        //Debug.Log("Try 3");
        var lineDraw = skillHelper.LineDrawManager;
        AimingSkillUnit asu = CentralResource.Instance.AimingInfo.AimingUnitOfSkill(skillId);

        if (lineDraw != null) {
            if (asu == null) {
                lineDraw.Visible(false);
                lineDraw.NothingOnLine();
                return;
            }
            
            lineDraw.Visible(true);
            //Debug.Log("Try 4");
            lineDraw.SkillShowing(skillId);
            if(asu.AimingType == AimingType.STRAIGHTFORWARDLINE) {
                lineDraw.UpdateTrajectory(new Vector3(0, 4, 0), new Vector3(asu.Length, 4, 0));
            }
            if(asu.AimingType == AimingType.GRAVITYLINE) {
                lineDraw.UpdateTrajectory(new Vector3(0, 4, 0), new Vector3(asu.Speed.x, asu.Speed.y), Physics.gravity, 0.2f);
                //lineDraw.UpdateTrajectory(new Vector3(0, 4, 0), new Vector3(speed, speedY), Physics.gravity, 0.2f);
            }    
            //lineDraw.UpdateTrajectory(new Vector3(0, 4, 0), new Vector3(20, 20), Physics.gravity, 0.2f);
        }
    }
}


