﻿using System;

public class AimingInfoHolder {

    AimingSkillUnit[] aimingSkillUnits;
    bool mouseAiming;

    public AimingSkillUnit[] AimingSkillUnits {
        get {
            return aimingSkillUnits;
        }

        set {
            aimingSkillUnits = value;
        }
    }

    public bool MouseAimingActive {
        get {
            if (
                CentralResource.Instance.SaveSlots.CurrentSlot.settingsPersistent.aimingMode == AimingMode.NEVERMOUSE
                )
                mouseAiming = false;
            if (
                CentralResource.Instance.SaveSlots.CurrentSlot.settingsPersistent.aimingMode == AimingMode.ALWAYSMOUSE
                )
                mouseAiming = true;
            return mouseAiming;
        }

        set {
            mouseAiming = value;
        }
    }

    public bool AutomaticAim {
        get {
            return true;
        }

        set {
            //mouseAiming = value;
        }
    }

    internal AimingSkillUnit AimingUnitOfSkill(int skillId) {
        for (int i = 0; i < aimingSkillUnits.Length; i++) {
            if (aimingSkillUnits[i].Contains(skillId)) {
                return aimingSkillUnits[i];
            }

        }
        return null;
    }

    internal void KeyboardAttackTry() {
        mouseAiming = false;
    }

    internal void MouseAttackTry() {
        mouseAiming = true;
    }
}