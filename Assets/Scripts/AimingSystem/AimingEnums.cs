﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum AimingType {
    STRAIGHTFORWARDLINE, GRAVITYLINE,
}

public enum AimingMode {
    AUTOMATIC, ALWAYSMOUSE, NEVERMOUSE
}