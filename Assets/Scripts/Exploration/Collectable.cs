﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Examinable))]
[RequireComponent(typeof(HideShowInBattle))]
public class Collectable : MonoBehaviour {

    [SerializeField]
    SkillInfoProvider.SkillId skillId;
    Examinable examin;
    [SerializeField]
    int uniqueId = -1;

    public int UniqueId {
        get {
            return uniqueId;
        }

        set {
            uniqueId = value;
        }
    }

    public SkillInfoProvider.SkillId SkillId {
        get {
            return skillId;
        }

        set {
            skillId = value;
        }
    }

    void Awake() {
        if (uniqueId >= 0 && CentralResource.Instance.SaveSlots.CurrentSlot.AlreadyGottenCollectable(uniqueId)) {

            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start() {
        examin = GetComponent<Examinable>();
        examin.ExamineTry += Examined;
    }

    private void Examined(Examinable obj) {
        GetItem();
    }

    public void GetItem() {
        CentralResource.Instance.SkillInventoryDeckManagement.AddToInventory(SkillId);
        var infoCon = CentralResource.Instance.InfoConfirmAccess;
        if (uniqueId >= 0) {
            CentralResource.Instance.SaveSlots.CurrentSlot.AddCollectableGotten(uniqueId);
            CentralResource.Instance.SaveSlots.SaveData();
        }

        infoCon.SkillId = SkillId;
        infoCon.ShowSkill = true;
        infoCon.Text = "You got a New Power";
        infoCon.RequestShow();
        infoCon.DoneCallback = Done;
        //Destroy(this.gameObject);
        gameObject.SetActive(false);
    }

    private void Done() {

    }



    // Update is called once per frame
    void Update() {

    }
}
