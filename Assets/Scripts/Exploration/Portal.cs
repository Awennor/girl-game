﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.Events;
using System;

public class Portal : MonoBehaviour {
    private GameObject effect;
    public UnityEvent PortalParticleEnd;
    //private Tween callback1;
    private Tween fadeTween;
    private Tween shake;

    void OnTriggerEnter(Collider col) {
        effect = EffectSingleton.Instance.ShowEffectHere("particleportal1", transform);
        fadeTween = DOVirtual.DelayedCall(3.4f, StartTeleportUnstoppable);//CentralResource.Instance.FadeHolder.FadeOutAndFadeIn(3.4f, 0.3f, 0.1f, 0.4f, Color.white);
        var shake = DOTween.Sequence();
        //CentralResource.Instance.CameraControl.Shake(10f, new Vector3(3f, 3f, 0), 50, 50).SetEase(Ease.InQuart);
        ///*
        shake
            //.Append(CentralResource.Instance.CameraControl.Shake(4.3f, new Vector3(5f,5f, 0), 50, 50))
            ///*
            /// 
            //.Append(CentralResource.Instance.CameraControl.Shake(3f, new Vector3(1f, 1f, 0), 90, 50))
            .Append(CentralResource.Instance.CameraControl.Shake(0.2f, new Vector3(0.2f, 0.2f, 0), 40, 50))
            .AppendInterval(0.8f)
            .Append(CentralResource.Instance.CameraControl.Shake(1f, new Vector3(0.7f, 0.2f, 0), 70, 50))
            .Append(CentralResource.Instance.CameraControl.Shake(0.5f, new Vector3(1f, 1f, 0), 90, 50))
            .Append(CentralResource.Instance.CameraControl.Shake(1.1f, new Vector3(5f, 5f, 0), 400, 50))
            //.Append(CentralResource.Instance.CameraControl.Shake(0.3f, new Vector3(5f, 2f, 0), 150, 50))
            //*/
            ;
        //shake.SetEase(Ease.InQuint);
            //*/
        this.shake = shake;
            //.Append(CentralResource.Instance.CameraControl.Shake(0.5f, new Vector3(0.1f, 0.1f, 0), 70, 50));
        //shake = CentralResource.Instance.CameraControl.Shake(3.7f, new Vector3(1,0,0), 200, 50);
        //callback1 = DOVirtual.DelayedCall(0.3f, CallEvent);
    }

    private void StartTeleportUnstoppable() {
        fadeTween = null;
        CentralResource.Instance.FadeHolder.FadeOutAndFadeIn(0, 0.3f, 0.1f, 0.4f, Color.white);
        DOVirtual.DelayedCall(0.34f, CallEvent);
        
        effect = null;
    }

    private void CallEvent() {
        PortalParticleEnd.Invoke();
    }

    void OnTriggerExit(Collider col) {
        //callback1.Kill();
        if (shake != null)
            shake.Kill();
        if (effect != null) {
            effect.GetComponent<ParticleEnder>().EndParticles();
        }
        if (fadeTween != null)
            fadeTween.Kill();
    }


    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
