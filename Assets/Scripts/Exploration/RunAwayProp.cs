﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;

public class RunAwayProp : MonoBehaviour {
    private Rigidbody rigidBody;
    private Invoker invoker;
    private Vector3 lastSpeed;
    

    // Use this for initialization
    void Start() {
        rigidBody = GetComponent<Rigidbody>();
        invoker = GetComponent<Invoker>();
    }

    void OnTriggerEnter(Collider other) {
        rigidBody.velocity = new Vector3(UnityEngine.Random.Range(-40,40),
            0,//UnityEngine.Random.Range(20,30), 
            UnityEngine.Random.Range(-40,40));
        lastSpeed = rigidBody.velocity;
        invoker.ClearAll();
        var delay = UnityEngine.Random.Range(1, 2f);
        invoker.AddAction(slowDown, delay);
        var delay2 = delay + UnityEngine.Random.Range(1, 2f);

        if(RandomSpecial.RandomBool())
            invoker.AddAction(returnSpeed, delay2);
        
        invoker.AddAction(normalBehaviour, delay2+delay*2);
    }

    private void slowDown() {
        rigidBody.velocity = lastSpeed *0.1f;
    }

    private void normalBehaviour() {
        if(RandomSpecial.RandomBool()) {
            rigidBody.velocity = new Vector3(
                UnityEngine.Random.Range(-3,3), 
                0,
                UnityEngine.Random.Range(-3,3) );;
        } else {
            stop();
        }
        

        invoker.AddAction(normalBehaviour, UnityEngine.Random.Range(0.4f, 2f));
    }

    private void returnSpeed() {
        rigidBody.velocity = lastSpeed *-0.5f;
    }

    private void stop() {
        rigidBody.velocity = Vector3.zero;
    }

    // Update is called once per frame
    void Update() {

    }
}
