﻿using UnityEngine;
using System.Collections;

public class TeleporterScenario : MonoBehaviour {

    
    public bool ActivateOnTrigger = true;
    public GameObject targetPosition;

    void OnTriggerEnter(Collider col) {
        if(ActivateOnTrigger)
            Teleport();
    }

    public void Teleport() {
        
        if (!CentralResource.Instance.BattleFlowSys.InBattle) {
            var heroObj = CentralResource.Instance.Hero;
            heroObj.GetComponent<PlayerMovement>().StopRotations();
            heroObj.transform.position = targetPosition.transform.position;
            heroObj.transform.rotation = targetPosition.transform.rotation;
            //CentralResource.Instance.InputPlayer.Disable(0.3f);
            CentralResource.Instance.InputPlayerMovement.Disable(0.5f);
            
        }
            
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
