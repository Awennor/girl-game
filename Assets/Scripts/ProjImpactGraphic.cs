﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ProjImpactGraphic : MonoBehaviour {

    public List<string> impactEffects = new List<string>();
    public Material hitMaterial;


    // Use this for initialization
    void Start() {
        CollisionProj.collisionProjAfterStatic += CollisionAfter;
        CollisionProjSelfManaged.collidedWithScenarioStatic += CollisionGroundProj;
    }

    private void CollisionGroundProj(CollisionProjSelfManaged obj) {
        var pD = obj.ProjectilData;
        var transformSource = pD.transform;
        SummonImpact(pD, transformSource);
        //CentralResource.Instance.CameraControl.Shake
    }

    private void CollisionAfter(CollisionProj collisionProj) {
        var projData = collisionProj.LastCollidedProjData;
        var transformHitTarget = collisionProj.gameObject.transform;
        SummonImpact(projData, transformHitTarget);
        var materialChanger = transformHitTarget.GetComponent<MaterialChanger>();
        materialChanger.ChangeMaterial(hitMaterial);
        materialChanger.revertMaterial(0.1f);
    }

    private void SummonImpact(ProjectilData projData, Transform transformHitTarget) {
        int effectId = projData.ImpactEffectId;
        if (effectId >= 0) {
            //Debug.Log("PROJ IMPACT START 3");
            var position = transformHitTarget.position;
            //position.z += 5;
            EffectSingleton.Instance.ShowEffectHere(impactEffects[effectId], position);
            //var impact = (GameObject) GameObject.Instantiate(impactEffects[effectId], position, Quaternion.identity);
            //impact.SetActive(true);
            //Destroy(impact, 0.2f);

        }
    }

    // Update is called once per frame
    void Update() {

    }
}
