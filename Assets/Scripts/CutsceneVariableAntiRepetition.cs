﻿using UnityEngine;
using System.Collections;

public class CutsceneVariableAntiRepetition : MonoBehaviour {

    [SerializeField]
    private VariableEnum variableEnum;

    public void OnEnable() {
        if(CentralResource.Instance.VariableSystem.GetVariableValue(variableEnum) != -1) {
            gameObject.SetActive(false);
        }
    }

    public VariableEnum VariableEnum {
        get {
            return variableEnum;
        }

        set {
            variableEnum = value;
        }
    }
}
