﻿using UnityEngine;
using System.Collections;
using System;

public class CharacterBattleHolder : MonoBehaviour {

    public Action CharacterBattleSet;
    private Action<CharacterBattle> callbackCharaOnce;

    CharacterBattle characterBattle;

    public CharacterBattle CharacterBattle
    {
        get
        {
            return characterBattle;
        }

        set
        {
            characterBattle = value;
            if (CharacterBattleSet != null)
                CharacterBattleSet();
            if (callbackCharaOnce != null && characterBattle != null) {
                callbackCharaOnce(CharacterBattle);
                callbackCharaOnce = null;
                callbackCharaOnce = null;
            }

        }
    }

    internal void CharaCallbackOnce(Action<CharacterBattle> setupChara)
    {
        if (characterBattle != null)
            setupChara(characterBattle);
        else
            callbackCharaOnce += setupChara;

    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
