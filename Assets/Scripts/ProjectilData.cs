﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;

public class ProjectilData : MonoBehaviour {

    float damage;
    float damageMind;
    [SerializeField]
    bool destroyOnImpact;
    [SerializeField]
    int impactEffectId;
    bool multiHit;
    bool ignoreCollision;

    string impactSound;

    GameObject owner;

    List<GameObject> collidedWith = new List<GameObject>();

    [SerializeField]
    List<GameObject> prefabsToInstatiateAsChild;

    float knockbackOnSpeed = 0;
    float knockbackMoveDuration = -1;
    float knockbackOnDistanceDirection = 0;
    Vector3 knockbackNormal = new Vector3(0,0,0);

    List<GameObject> activateOnImpact = new List<GameObject>(); 
    public Action<GameObject, GameObject> activateOnImpactCallback;
    public Action<ProjectilData> Destroying;

    public float Damage {
        get {
            return damage;
        }

        set {
            damage = value;
        }
    }

    public bool DestroyOnImpact {
        get {
            return destroyOnImpact;
        }

        set {
            destroyOnImpact = value;
        }
    }

    public int ImpactEffectId {
        get {
            return impactEffectId;
        }

        set {
            impactEffectId = value;
        }
    }

    public float KnockbackOnSpeed {
        get {
            return knockbackOnSpeed;
        }

        set {
            knockbackOnSpeed = value;
        }
    }

    public Vector3 KnockbackNormal {
        get {
            return knockbackNormal;
        }

        set {
            knockbackNormal = value;
        }
    }

    public float KnockbackMoveDuration {
        get {
            return knockbackMoveDuration;
        }

        set {
            knockbackMoveDuration = value;
        }
    }

    public List<GameObject> ActivateOnImpact {
        get {
            return activateOnImpact;
        }

        set {
            activateOnImpact = value;
        }
    }

    public float KnockbackOnDistanceDirection {
        get {
            return knockbackOnDistanceDirection;
        }

        set {
            knockbackOnDistanceDirection = value;
        }
    }

    public float DamageMind {
        get {
            return damageMind;
        }

        set {
            damageMind = value;
        }
    }

    public List<GameObject> CollidedWith {
        get {
            return collidedWith;
        }

        set {
            collidedWith = value;
        }
    }

    public string ImpactSound {
        get {
            return impactSound;
        }

        set {
            impactSound = value;
        }
    }

    public bool MultiHit {
        get {
            return multiHit;
        }

        set {
            multiHit = value;
        }
    }

    internal void DelayedIgnoreCollision(float v) {
        DOVirtual.DelayedCall(v, IgnoreCollisionOn, false);
    }

    internal void ClearPreviousCollisions() {
        collidedWith.Clear();
    }

    internal void DelayedDeactivation(float v) {
        DOVirtual.DelayedCall(v, Deactivate, false);
    }

    private void Deactivate() {
        gameObject.SetActive(false);
    }

    private void IgnoreCollisionOn() {
        ignoreCollision = true;
    }

    public bool IgnoreCollision {
        get {
            return ignoreCollision;
        }

        set {
            ignoreCollision = value;
        }
    }

    public void DelayedDestroy(float time) {
        DOVirtual.DelayedCall(time, DestroyProj, false);
    }

    private void DestroyProj() {
        //Debug.Log("DESTROY PROJ CALLED");
        if(Destroying != null) {
            Destroying(this);
        }
        if(this == null) return;
        Destroy(this.gameObject);
        
    }

    public Action<ProjectilData, GameObject> SpecialCollisionCase { get; internal set; }

    public GameObject Owner {
        get {
            return owner;
        }

        set {
            owner = value;
        }
    }

    public void Awake() {
        var colP = gameObject.AddComponent<CollisionProjSelfManaged>();
        colP.ProjectilData = this;
        for (int i = 0; i < prefabsToInstatiateAsChild.Count; i++) {
            var obj = (GameObject)Instantiate(prefabsToInstatiateAsChild[i]);
            obj.transform.SetParent(transform);
            obj.transform.position = transform.position;
        }
    }
    
    //  this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
