﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class InfoConfirmAccess {
    public Action RequestShow = ()=> {};
    bool showSkill;
    SkillInfoProvider.SkillId skillId;
    string text;
    private Action doneCallback;

    public bool ShowSkill {
        get {
            return showSkill;
        }

        set {
            showSkill = value;
        }
    }

    public SkillInfoProvider.SkillId SkillId {
        get {
            return skillId;
        }

        set {
            skillId = value;
        }
    }

    public string Text {
        get {
            return text;
        }

        set {
            text = value;
        }
    }

    public Action DoneCallback {
        get {
            return doneCallback;
        }

        set {
            doneCallback = value;
        }
    }

    public void RequestShowNow() {
        RequestShow();
    }
}
