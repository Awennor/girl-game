﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//rename class if you start to use it for more things
public class InputPlayerStatus_Movement {
    bool enabled = true;

    float timeToEnable;

    public bool Enabled {
        get {
            return enabled;
        }

        set {
            enabled = value;
        }
    }

    public void Disable(float timeDisabling) {
        timeToEnable = timeDisabling;
        enabled = false;
    }

    public void Update(float f) {
        if(timeToEnable >0 ) {
            timeToEnable -= f;
            if(timeToEnable <= 0) {
                enabled = true;
            }
        }
    }
}

