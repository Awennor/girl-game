﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Utility;
using System;

public class CameraFocusGroup : MonoBehaviour {

    List<GameObject> gameObjects = new List<GameObject>();
    public SmoothFollowC smoothFollowC;
    Vector3 offset;
    //private Vector3 focusedPoint;
    public float minSize;

    public List<GameObject> GameObjects {
        get {
            return gameObjects;
        }

        set {
            gameObjects = value;
        }
    }

    public Vector3 Offset {
        get {
            return offset;
        }

        set {
            offset = value;
        }
    }

    // Use this for initialization
    void Start () {
	    
	}



    public bool FocusOnAll() {
        if(gameObjects.Count == 0) {
            
            return false;
        }
        Vector3 sum = new Vector3();
        
        for (int i = 0; i < GameObjects.Count; i++) {
            if(GameObjects[i]!= null)
                sum += GameObjects[i].transform.position;

        }
        sum /= GameObjects.Count;
        sum += offset;
        smoothFollowC.FollowPosition = true;
        smoothFollowC.TargetPosition = sum;
        //focusedPoint = sum;
        return true;
    }

    public void ZoomToShowAll() {
        float maxX = 0;
        float maxZ = 0;
        var focusedPoint = smoothFollowC.GetFocusPoint();
        for (int i = 0; i < GameObjects.Count; i++) {
            if(GameObjects[i]) {
                float x = GameObjects[i].transform.position.x - focusedPoint.x;
                float z = GameObjects[i].transform.position.z - focusedPoint.z;
                if(Mathf.Abs(x) > maxX) {
                    maxX = Mathf.Abs(x);
                }
                if(Mathf.Abs(z) > maxZ) {
                    maxZ = Mathf.Abs(z);
                }
            }
        }
        //maxX += offset.x;
        float size = Mathf.Max(maxX, maxZ) *1.3f;
        float factor =  minSize / (minSize - offset.x);
        size *= factor;

        if(size > minSize) {
            if(Camera.current != null)
                Camera.current.orthographicSize = size;
        }

    }

    internal void ReturnToCharaFocus() {
        smoothFollowC.FollowObject = true;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
