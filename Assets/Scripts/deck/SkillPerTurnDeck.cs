﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SkillPerTurnDeck : SkillPerTurnProvider {
    [SerializeField]
    List<SkillInfoProvider.SkillId> deckOriginal = new List<SkillInfoProvider.SkillId>();
    List<SkillInfoProvider.SkillId> deck = new List<SkillInfoProvider.SkillId>();
    int drawAmount = 2;
    int drawAmountFirstTurn = 5;
    private bool firstDraw;

    public int DrawAmount {
        get {
            return drawAmount;
        }

        set {
            drawAmount = value;
        }
    }

    public int DrawAmountFirstTurn {
        get {
            return drawAmountFirstTurn;
        }

        set {
            drawAmountFirstTurn = value;
        }
    }

    internal void DefaultDrawAmount() {
        drawAmount = 2;
        DrawAmountFirstTurn = 5;
    }

    internal override void UsedUpSkillInPosition(int v) {
        skillList[v] = -1;
    }

    protected override void FillUpSkillList2() {
        skillList2.Clear();
        skillList2.Add(-1);
        skillList2.Add(-1);
        if(deck.Count <= 0)
            skillList2.Add((int)SkillInfoProvider.SkillId.DeckReset);
    }

    internal override void DeckRefreshUsed() {
        firstDraw = true;
        ResetDeckAndHand();

    }

    protected override void FillUpSkillListNewTurn() {
        recentlyAdded.Clear();
        if (firstDraw) {
            firstDraw = false;
            Draw(DrawAmountFirstTurn);
        } else {
            Draw(DrawAmount);
            //Debug.Log(RecentlyAdded.Count+" RECENT ADDED COUNT");
        }
    }

    private void Draw(int v) {
        int powersHolding = 0;
        for (int i =  0; i < skillList.Count; i++) {
            if(skillList[i] >= 0)
                powersHolding++;
        }
        if(powersHolding >= 9) return;
        for (int i = 0; i < v; i++) {
            if(deck.Count <= 0) return;
            int skill = (int)deck[deck.Count - 1];
            deck.RemoveAt(deck.Count - 1);
            bool added = false;
            for (int j = 0; j < skillList.Count; j++) {
                if (skillList[j] < 0) {
                    skillList[j] = skill;
                    added = true;
                    recentlyAdded.Add(j);
                    break;
                }
            }
            if (!added) {
                recentlyAdded.Add(skillList.Count);
                skillList.Add(skill);
            }
            powersHolding++;
            if(powersHolding >= 9) return;
        }

    }

    override internal void BattleStartChildClass() {
        firstDraw = true;
        ResetDeckAndHand();
    }

    private void ResetDeckAndHand() {
        deck.Clear();
        skillList.Clear();
        deck.AddRange(deckOriginal);
        deck.Shuffle();
    }

    // Use this for initialization
    void Start() {
        

        var skillInventoryDeckManagement = CentralResource.Instance.SkillInventoryDeckManagement;
        skillInventoryDeckManagement.DeckChanged += UpdateDeck;
        UpdateDeck(skillInventoryDeckManagement);
    }

    private void UpdateDeck(SkillInventoryDeckManagement skillInventoryDeckManagement) {
        deckOriginal.Clear();
        var skillIds = skillInventoryDeckManagement.GetCurrentDeckContent();
        for (int i = 0; i < skillIds.Count; i++) {
            deckOriginal.Add((SkillInfoProvider.SkillId)skillIds[i]);
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
