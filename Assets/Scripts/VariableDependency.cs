﻿using UnityEngine;
using System.Collections;
using System;

public class VariableDependency : MonoBehaviour {

    [SerializeField]
    VariableEnum variableName;
    [SerializeField]
    float value;

    void OnEnable() {
        TestValue();
    }

    // Use this for initialization
    void Start() {
        CentralResource.Instance.VariableSystem.variableChange += VariableChange;
    }

    private void VariableChange(int arg1, float arg2) {
        TestValue();
    }

    private void TestValue() {
        if (CentralResource.Instance.VariableSystem.GetVariableValue(variableName) == value) {

        } else {
            if(this != null)
                gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
