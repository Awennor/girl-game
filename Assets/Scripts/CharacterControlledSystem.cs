﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


    class CharacterControlledSystem
    {
        static CharacterControlledSystem instance;
        public Action<GameObject> controlledCharaChange;
    private GameObject controlled;

    static internal CharacterControlledSystem Instance
        {
            get
            {
                if(instance == null)
                    instance = new CharacterControlledSystem();
                return instance;
            }

            set
            {
                instance = value;
            }
        }

    public GameObject Controlled {
        get {
            return controlled;
        }

        set {
            controlled = value;
        }
    }

    public void ChangeControlledChara(GameObject obj) {
            this.Controlled = obj;
            if (controlledCharaChange != null)
                controlledCharaChange(obj);

        }
    }
