﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;

public class AttackInput : MonoBehaviour {
    private AvailableAttackListTempOld attackList;
    private ChosenSkillHolder chosenSkillHolder;

    List<Action> attackMethods = new List<Action>();
    private BattleFlowSystem battleFlow;
    private int currentSkill;
    private SkillHelper skillHelper;
    private BattleStateMachine battleState;
    public Action overworldAttackTry;
    bool canAttackLastState;
    public Action CanAttackChangeCallback;
    public event Action<bool> CanAttackChangeCallback_Bool;
    private LineDrawManager lineDrawManager;
    private ClosestTarget closestTarget;
    public int mouseAimHelpMinDis = 5;

    public event Action OnAttackStartSuccess;

    public int CurrentSkill {
        get {
            return currentSkill;
        }

        set {
            currentSkill = value;
        }
    }

    void Awake() {
        var controllableObject = GetComponent<ControllableObject>();
        controllableObject.AddInputScript(this);

        chosenSkillHolder = GetComponent<ChosenSkillHolder>();
        attackList = GetComponent<AvailableAttackListTempOld>();

    }
    // Use this for initialization
    void Start() {
        closestTarget = GetComponent<ClosestTarget>();
        lineDrawManager = GetComponent<LineDrawManager>();
        battleFlow = CentralResource.Instance.BattleFlowSys;
        skillHelper = GetComponent<SkillHelper>();
        battleState = GetComponent<BattleStateMachine>();
        battleState.OnCurrentAttackBlockSet += OnAttackBlockChange;
        battleState.stateChanged += OnAttackBlockChange;
    }

    private void OnAttackBlockChange(BattleStateMachine obj) {
        CheckAttackStateUpdate();
    }

    internal void SetAttackMethod(Enum e, Action doSkill) {
        SetAttackMethod(Convert.ToInt32(e), doSkill);
    }

    internal void SetAttackMethod(int i, Action doSkill) {
        while (i >= attackMethods.Count) {
            attackMethods.Add(null);
        }
        attackMethods[i] = doSkill;

    }

    [ExposeMethodInEditor]
    public void DebugCanAttackAndAttakLast() {
        Debug.Log(""+canAttackLastState + CanAttack());
    }

    // Update is called once per frame
    void Update() {
        CheckAttackStateUpdate();
        if (Input.GetButtonDown("Mind")
            ) {
            TryAttackSub();
            Debug.Log("attack try 1");
        }
        if (CanAttack() && lineDrawManager != null) {
            if (battleFlow.InBattle) {
                if (CentralResource.Instance.AimingInfo.MouseAimingActive) {
                    if (CentralResource.Instance.AimingInfo.AutomaticAim) {
                        var heroPlanePosition = RotationToMouse.GetPositionInTransformPlane(this.transform);
                        var t = closestTarget.ClosestTargetTo(heroPlanePosition, mouseAimHelpMinDis);

                        lineDrawManager.RotateLineToThisObject = t;
                    } else {
                        lineDrawManager.RotateLineToThisObject = null;
                    }

                } else {
                    if (CentralResource.Instance.AimingInfo.AutomaticAim) {
                        lineDrawManager.RotateLineToThisObject = closestTarget.ChosenTarget;
                    } else {
                        lineDrawManager.RotateLineToThisObject = null;
                    }
                }

            }
        }
        if (Input.GetButtonDown("Attack")
            ) {

            CentralResource.Instance.AimingInfo.KeyboardAttackTry();
            TryAttack();

        }
        if (Input.GetButtonDown("AttackMouse")
            ) {

            CentralResource.Instance.AimingInfo.MouseAttackTry();


            TryAttack();

        }
    }

    private void CheckAttackStateUpdate() {
        if (canAttackLastState != CanAttack()) {
            canAttackLastState = CanAttack();
            if (CanAttackChangeCallback != null) CanAttackChangeCallback();
            if (CanAttackChangeCallback_Bool != null) CanAttackChangeCallback_Bool(canAttackLastState);
            Debug.Log("Can attack update " + canAttackLastState);
        }
    }

    public void TryAttackSub() {
        if (
                    CanAttack()
                    ) {
            Debug.Log("attack try 2");
            if (battleFlow.InBattle) {
                Debug.Log("attack try 3");
                int skill = chosenSkillHolder.SubSkill;
                if (skill >= 0) {
                    Debug.Log("attack try 4");
                    currentSkill = skill;
                    skillHelper.CurrentSkill = skill;
                    attackMethods[skill]();
                }
            }

        }
    }

    public void TryAttackAsAI(int skill) {
        LookAtClosestTarget();
        currentSkill = skill;
        if(skillHelper == null)
            skillHelper = GetComponent<SkillHelper>();
        skillHelper.CurrentSkill = skill;
        if (OnAttackStartSuccess != null)
            OnAttackStartSuccess();
        if (attackMethods.Count > skill) {
            attackMethods[skill]();
        }
        chosenSkillHolder.UseUp();
    }

    public void TryAttack() {
        if (
            CanAttack()
            ) {

            if (battleFlow.InBattle) {

                int skill = chosenSkillHolder.CurrentSkill;
                currentSkill = skill;
                chosenSkillHolder.DebugSkillList();
                if (skill >= 0) {
                    if (CentralResource.Instance.AimingInfo.MouseAimingActive) {
                        //LookAtMouse();
                        if (CentralResource.Instance.AimingInfo.AutomaticAim) {
                            LookAtClosestTargetToMouse();
                        } else {
                            LookAtMouse();
                        }
                    } else {
                        if (CentralResource.Instance.AimingInfo.AutomaticAim) {
                            LookAtClosestTarget();
                        }
                    }
                    skillHelper.CurrentSkill = skill;
                    if (OnAttackStartSuccess != null)
                        OnAttackStartSuccess();
                    if (attackMethods.Count > skill) {
                        attackMethods[skill]();
                    }
                    chosenSkillHolder.UseUp();
                }
            } else {
                if (overworldAttackTry != null && CentralResource.Instance.OverworldInput.InputActive) {
                    if (CentralResource.Instance.AimingInfo.MouseAimingActive) {
                        LookAtMouse();
                    }
                    overworldAttackTry();
                }
            }

        }
    }



    public bool CanAttack() {
        if(battleState == null) battleState = GetComponent<BattleStateMachine>();
        return Time.deltaTime > 0
                            && battleState.CurrentStateAttackBlock == BattleStateMachine.StatesAttackBlock.Can
                            && battleState.CurrentState != BattleStateMachine.States.Damage;
    }

    internal bool NoMoreAttacks() {
        int skill = chosenSkillHolder.CurrentSkill;

        return skill < 0;


    }

    public void LookAtMouse() {
        RotationToMouse.LookAtMouse(this.transform);
        GetComponent<PlayerMovement>().StopRotations();
    }

    private void LookAtClosestTargetToMouse() {
        var heroPlanePosition = RotationToMouse.GetPositionInTransformPlane(this.transform);
        var closestTarget = GetComponent<ClosestTarget>();
        var t = closestTarget.ClosestTargetTo(heroPlanePosition, mouseAimHelpMinDis);
        if (t == null) {
            LookAtMouse();
            return;
        }

        //var t = closestTarget.ChosenTarget;

        Quaternion targetRotation = Quaternion.LookRotation(t.transform.position - transform.position);
        var euler = targetRotation.eulerAngles;
        euler.z = 0;
        euler.x = 0;

        targetRotation = Quaternion.Euler(euler);
        //euler.

        // Smoothly rotate towards the target point.
        transform.rotation = targetRotation;
        GetComponent<PlayerMovement>().StopRotations();
    }

    private void LookAtClosestTarget() {
        var closestTarget = GetComponent<ClosestTarget>();
        var t = closestTarget.ChosenTarget;
        if (t == null) return;
        Quaternion targetRotation = Quaternion.LookRotation(t.transform.position - transform.position);
        var euler = targetRotation.eulerAngles;
        euler.z = 0;
        euler.x = 0;

        targetRotation = Quaternion.Euler(euler);
        //euler.

        // Smoothly rotate towards the target point.
        transform.rotation = targetRotation;
        GetComponent<PlayerMovement>().StopRotations();
    }
}
