﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableSystemAccessor : MonoBehaviour {

    public VariableEnum variable;

    public void ChangeVariable(float f) {
        CentralResource.Instance.VariableSystem.SetVariableValue(variable, f);
    }



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
