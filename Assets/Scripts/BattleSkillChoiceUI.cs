﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BattleSkillChoiceUI : MonoBehaviour {

    SkillPerTurnProvider skillPerTurnProvider;
    private ChosenSkillHolder chosenSkillHolder;
    public SkillInfoUI[] cards;
    private List<SkillInfoUI> chosenCards = new List<SkillInfoUI>();
    List<int> chosenBuffer = new List<int>();
    private int MaxPowersChosable = 7;

    public ObjectHolder energies;

    public Image[] orderNumberBacks;
    List<Text> orderNumberTexts = new List<Text>();

    public float positionChosenY;
    public float positionNotChosenY;

    const float scaleSkillInfo = 0.8f;
    //public Button buttonOk;
    public UnityEvent NoCardToSelect;
    public MyBoolEvent canDeckRecharge;
    private bool needToSelectCard;


    void Awake() {
        CharacterControlledSystem.Instance.controlledCharaChange += (obj) => {
            chosenSkillHolder = obj.GetComponent<ChosenSkillHolder>();
            skillPerTurnProvider = obj.GetComponent<SkillPerTurnProvider>();
        };

        for (int i = 0; i < orderNumberBacks.Length; i++) {
            orderNumberTexts.Add(orderNumberBacks[i].transform.GetChild(0).GetComponent<Text>());
        }

        for (int i = 0; i < cards.Length; i++) {
            int buttonId = i;
            int i2 = i;
            cards[i].transform.parent.gameObject.SetActive(false);
            cards[i].Button.onClick.AddListener(() => {
                ButtonPressed(buttonId);
            });
            var selectableWrapper = cards[i].GetComponent<SelectableWrapper>();
            selectableWrapper.MouseOver.AddListener(()=> {
                SelectedCard(i2);
            });
            selectableWrapper.MouseOverLeave.AddListener(()=> {
                SelectedCard(-1);
            });
            selectableWrapper.Selected.AddListener(() => {
                SelectedCard(i2);
            });
        }

    }

    private void SelectedCard() {
        SelectedCard(-1);
    }

    private void SelectedCard(int cardIndex) {
        if (cardIndex == -1) {
            for (int i = 0; i < cards.Length; i++) {
                if (EventSystem.current.currentSelectedGameObject == cards[i].gameObject) {
                    cardIndex = i;
                    break;
                }

            }
        }
        if(cardIndex == -1) cardIndex = 0;
        var card = cards[cardIndex];
        var skill = card.Skill;
        int energyCost = SkillInfoProvider.Instance.GetEnergyCost(skill);
        int totalEnergy = skillPerTurnProvider.Energy;
        for (int j = 0; j < cards.Length; j++) {
            cards[j].StopMightUseEnergies();
            cards[j].BlueEnergies();
        }

        if (chosenCards.Contains(card)) {

            energies.AppearingBalls(totalEnergy + energyCost);
            for (int j = 0; j < energyCost + totalEnergy; j++) {
                var holdedObjects = energies.HoldedObjectList;
                var rayGraphic = holdedObjects[j].GetComponent<RayGraphic>();
                rayGraphic.normalColorMode();
                rayGraphic.TryKillMightUse();
                if (j < energyCost)
                    rayGraphic.MightGet();
            }
        } else {

            energies.AppearingBalls(totalEnergy);
            //gameObject.GetComponent<RayGraphic>().MightUse();
            var holdedObjects = energies.HoldedObjectList;
            card.MightUseEnergies();
            var canUse = totalEnergy >= energyCost;
            if (canUse) {

            } else {
                card.RedEnergies();
            }

            for (int j = 0; j < holdedObjects.Count; j++) {
                var rayGraphic = holdedObjects[j].GetComponent<RayGraphic>();
                rayGraphic.ResetScaleAndRotation();
                if (!canUse) {
                    rayGraphic.noGoodColorMode();
                } else{
                    rayGraphic.normalColorMode();
                }
                if (j < energyCost) {
                    
                    rayGraphic.MightUse();

                } else {
                    rayGraphic.TryKillMightUse();
                }

            }


        }
    }

    private void ButtonPressed(int buttonId) {
        //Debug.Log("BUTTON PRESS " + buttonId);
        var card = cards[buttonId];
        var skillId1 = card.Skill;
        int skillId = (int)skillId1;
        var skillInfo = SkillInfoProvider.Instance;
        int energyC = skillInfo.GetEnergyCost(skillId);

        if (!chosenCards.Contains(card)) {
            //Debug.Log("Here1");
            if (energyC <= skillPerTurnProvider.Energy) {
                //Debug.Log("Here1");
                SoundSingleton.Instance.PlaySoundOnPause("uigood1");
                if (skillInfo.IsUtilSkill(skillId1)) {
                    //Debug.Log("Here2");

                    card.transform.DOLocalMove(card.transform.localPosition + new Vector3(0, 200, 0), 0.15f).SetUpdate(true);
                    /*DOVirtual.DelayedCall(0.05f, () => {
                    //    Debug.Log("Here3");
                        
                    });*/

                    DOVirtual.DelayedCall(0.25f, () => {
                        //  Debug.Log("Here4");
                        SoundSingleton.Instance.PlaySoundOnPause("uigood3");
                        var util = skillInfo.GetSkillUtilData(skillId);

                        ChangeEnergy(skillPerTurnProvider.Energy + util.EnergyAmount);

                        //card.Button.;
                        //TODO has to select next though
                        skillPerTurnProvider.UsedUpSkillInPosition(buttonId);
                        SelectNext(buttonId);
                        UpdateCards();
                        if (util.DeckRefresh) {
                            DeckRefresh();
                        }
                    });
                    //return;

                } else {

                    //Debug.Log("Here3");
                    if (chosenBuffer.Count < MaxPowersChosable) {
                      //  Debug.Log("Here4");
                        card.transform.DOComplete();
                        chosenBuffer.Add(skillId);
                        updateChosenBuffer();
                        //button.icon.gameObject.SetActive(false);
                        chosenCards.Add(card);
                    }


                }

                //if(energyC > 0) {

                ChangeEnergy(skillPerTurnProvider.Energy - energyC);
                //}

            }
        } else {
            Debug.Log("Here5");
            SoundSingleton.Instance.PlaySoundOnPause("uicancel");
            //Debug.Log("BUTTON PRESSED 4");
            chosenCards.Remove(card);
            chosenBuffer.Remove(skillId);
            updateChosenBuffer();
            //button.icon.gameObject.SetActive(true);
            //skillButtonsCtrl.RefreshButtonShown();
            card.transform.DOComplete();
            ChangeEnergy(skillPerTurnProvider.Energy + energyC);
        }
        updateChosenBuffer();
        DOVirtual.DelayedCall(0.2f, SelectedCard);
        //SelectedCard();
    }

    public void DeckRefresh() {
        ChangeEnergy(0);
        chosenBuffer.Clear();
        OkButtonPress();

        skillPerTurnProvider.DeckRefreshUsed();
    }

    private void SelectNext(int buttonId) {
        for (int i = buttonId + 1; i < cards.Length; i++) {
            if (cards[i].gameObject.activeInHierarchy) {
                cards[i].Button.Select();
                return;
            }
        }

        for (int i = 0; i < buttonId; i++) {
            if (cards[i].gameObject.activeInHierarchy) {
                cards[i].Button.Select();
                return;
            }
        }
        NoCardToSelect.Invoke();

    }

    public void OkButtonPress() {
        for (int i = 0; i < chosenBuffer.Count; i++) {
            chosenSkillHolder.AddSkill(chosenBuffer[i]);
        }
        for (int i = 0; i < chosenCards.Count; i++) {
            for (int j = 0; j < cards.Length; j++) {
                if (cards[j] == chosenCards[i]) {
                    cards[j].transform.parent.gameObject.SetActive(false);
                    skillPerTurnProvider.UsedUpSkillInPosition(j);
                }
            }
        }
        SoundSingleton.Instance.PlaySoundOnPause("uigood2");

        //if (RequestingEnd != null)
        //  RequestingEnd();
    }

    private void CompleteCardTweens() {
        for (int i = 0; i < cards.Length; i++) {

            cards[i].transform.DOComplete();
        }
    }

    private void ChangeEnergy(int energyCurrent) {
        if (energyCurrent < 0) energyCurrent = 0;
        if (energyCurrent > 9) energyCurrent = 9;
        int difference = energyCurrent - skillPerTurnProvider.Energy;
        skillPerTurnProvider.Energy = energyCurrent;
        var appearingBalls = skillPerTurnProvider.Energy;
        if (difference < 0) appearingBalls += -difference;
        energies.AppearingBalls(appearingBalls);
        if (difference > 0) {
            //EnergyCreate.Invoke();
            for (int i = 0; i < difference; i++) {
                //GameObject gameObject =energies.GetHoldedObject(i);
                var inversedId = energyCurrent - difference + i;
                int normalId = i;
                GameObject gameObject = energies.GetHoldedObject(normalId);

                gameObject.GetComponent<RayGraphic>().Summon(0.1f);

            }
        }

        if (difference < 0) {
            //EnergyUse.Invoke();
            int diffAux = -difference;
            Debug.Log("Diff aux " + diffAux + " energy curr" + energyCurrent);
            for (int i = 0; i < diffAux; i++) {
                var inversedId = energyCurrent + diffAux - 1 - i;
                int normalId = i;
                GameObject gameObject = energies.GetHoldedObject(normalId);
                gameObject.GetComponent<RayGraphic>().DisappearAnim(0f);

            }
        }


        for (int j = 0; j < cards.Length; j++) {
            var card = cards[j];
            int skillId = (int)card.Skill;
            if (skillId < 0) {
                //TODO disable card here

                continue;
            }

            int energyC = SkillInfoProvider.Instance.GetEnergyCost(skillId);
            if ((skillPerTurnProvider.Energy >= energyC && chosenBuffer.Count < MaxPowersChosable) || chosenCards.Contains(card)) {
                //card can receive input
                card.NormalColor();
            } else {
                card.ExtraColor();
                //card.Button.interactable = false;
                //TODO disable card here
            }

            //card.Button.interactable = (energyCurrent >= energyC);
        }
        //EnergyCost(latestEnergyCost); //TODO this could be used to animate the energy cost
    }

    private void updateChosenBuffer() {
        for (int j = 0; j < cards.Length; j++) {
            var card = cards[j];

            bool chosen = chosenCards.Contains(card);
            //orderNumberBacks[j].gameObject.SetActive(chosen);
            orderNumberBacks[j].gameObject.SetActive(false);
            //card.transform.localScale = new Vector3(scaleSkillInfo,scaleSkillInfo,1);

            if (chosen) {
                var chosenIndex = chosenCards.IndexOf(card) + 1;
                orderNumberTexts[j].text = "" + chosenIndex;
                //if(card.transform.localPosition.y < positionChosenY)
                //card.transform.localPosition = new Vector3(0, positionChosenY);

                var finalPosition = positionChosenY + (chosenIndex - 1) * 20;

                if (card.transform.localPosition.y < finalPosition) {
                    card.transform.localPosition = new Vector3(0, finalPosition);
                } else {
                    card.transform.DOLocalMoveY(finalPosition, 5f).SetUpdate(true).SetEase(Ease.InOutQuad);
                }


            } else {
                card.transform.localPosition = new Vector3(0, positionNotChosenY);

            }

            //card.Button.interactable = (energyCurrent >= energyC);
        }
    }

    public void Reopen() {
        //Debug.Log("CALLED REOPEN");
        needToSelectCard = true;
        chosenCards.Clear();
        chosenBuffer.Clear();

        int energyCurrent = skillPerTurnProvider.Energy + 3;

        skillPerTurnProvider.NewTurn();

        //CompleteCardTweens();

        UpdateCards();
        ChangeEnergy(energyCurrent);
        updateChosenBuffer();


    }

    private void UpdateCards() {
        var turnSkillIds = skillPerTurnProvider.GetTurnSkillIds();


        int newCardCount = 0;
        float drawDelay = 0;
        for (int i = 0; i < cards.Length; i++) {
            var cardExist = turnSkillIds.Count > i && turnSkillIds[i] >= 0;
            bool cardIsNew = cardExist && (!cards[i].transform.parent.gameObject.activeSelf || ((int)cards[i].Skill) != turnSkillIds[i]);
            cards[i].transform.parent.gameObject.SetActive(cardExist);

            if (cardExist) {
                int skillId = turnSkillIds[i];
                cards[i].ChangeSkill((SkillInfoProvider.SkillId)skillId);
                cards[i].transform.localScale = new Vector3(scaleSkillInfo, scaleSkillInfo, scaleSkillInfo);
            }
            if (cardIsNew) {
                //cards[i].transform.parent.gameObject.SetActive(false);
                var delayScale = 0.2f + newCardCount * 0.1f;
                int cardIndex = i;
                /*DOVirtual.DelayedCall(delayScale-0.2f, ()=> {
                     cards[cardIndex].gameObject.transform.parent.gameObject.SetActive(true);
                }); */
                cards[i].transform.localScale = new Vector3(scaleSkillInfo, 0, scaleSkillInfo);

                cards[i].transform.DOScaleY(scaleSkillInfo, 0.3f).
                    SetEase(Ease.OutBounce).SetUpdate(true).SetDelay(delayScale);

                SoundSingleton.Instance.PlaySoundOnPause("draw", delayScale);

                newCardCount++;
                drawDelay = delayScale + 0.3f;
            }

        }
        if (needToSelectCard) {
            needToSelectCard = false;
            DOVirtual.DelayedCall(drawDelay - 0.2f, SelectCardOpenTurn);
        }
        var skills2 = skillPerTurnProvider.GetTurnSkillIds2();
        bool deckResetOn = false;
        for (int i = 0; i < skills2.Count; i++) {
            int sId = skills2[i];
            if (sId >= 0) {
                SkillInfoProvider.SkillId s = (SkillInfoProvider.SkillId)sId;
                if (s == SkillInfoProvider.SkillId.DeckReset) {
                    deckResetOn = true;
                    break;
                }
            }
        }
        canDeckRecharge.Invoke(deckResetOn);

    }

    private void SelectCardOpenTurn() {
        SelectNext(-1);

    }

    public void BattleStart() {
        //Debug.Log("CALLED BATTLESTART");
        if (skillPerTurnProvider != null) {
          //  Debug.Log("CALLED BATTLESTART 2");
            skillPerTurnProvider.BattleStart();
        }
    }

    void Start() {
        for (int i = 0; i < cards.Length; i++) {

            cards[i].transform.parent.gameObject.SetActive(false);

        }
    }

    void Update() {

    }
}

