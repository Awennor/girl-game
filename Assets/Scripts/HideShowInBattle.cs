﻿using UnityEngine;
using System.Collections;
using System;

public class HideShowInBattle : MonoBehaviour {

	// Use this for initialization
	void Start () {
	    UpdateState();
        CentralResource.Instance.BattleFlowSys.OnBattleStart += UpdateState;
        CentralResource.Instance.BattleFlowSys.OnBattleEnd += UpdateState;
	}

    private void UpdateState() {
        if(this != null) {
            this.gameObject.SetActive(!CentralResource.Instance.BattleFlowSys.InBattle);
            //Debug.Log(this.gameObject.name+"OBJ NAME WHAT");
        }
        
    }

    // Update is called once per frame
    void Update () {
	
	}
}
