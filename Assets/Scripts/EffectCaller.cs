﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectCaller : MonoBehaviour {

	public GameObject CallEffect(string e) {
        return EffectSingleton.Instance.ShowEffectHere(e, transform.position);
    }

    public void CallEffectVoid(string e) {
        EffectSingleton.Instance.ShowEffectHere(e, transform.position);
    }

    public void CallEffectTimeToLive(string e, float timeToLive) {
        if(timeToLive >= 0) {
            Destroy(CallEffect(e).gameObject, timeToLive);
            
        } else {
            CallEffect(e);
        }

    }

	void Start () {
		
	}
	
	
	void Update () {
		
	}
}
