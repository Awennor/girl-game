﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;




public class SettingsAccess : MonoBehaviour {

    public MyIntEvent MouseInitial;

    public void AimingMode(int mode) {
        CentralResource.Instance.SaveSlots.
            CurrentSlot.settingsPersistent.aimingMode = (AimingMode)mode;
    }

    public void AutomaticModeAim(bool state) {
        if(state)
            AimingMode(0);
    }

    public void MouseModeAim(bool state) {
        if(state)
            AimingMode(1);
    }

    public void NoMouseModeAim(bool state) {
        if(state)
            AimingMode(2);
    }

	// Use this for initialization
	void Start () {
	    MouseInitial.Invoke((int)CentralResource.Instance.SaveSlots.
           CurrentSlot.settingsPersistent.aimingMode);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
