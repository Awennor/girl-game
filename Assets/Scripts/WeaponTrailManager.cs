﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Xft;

public class WeaponTrailManager : MonoBehaviour {

    public List<XWeaponTrail> list = new List<XWeaponTrail>();
    public bool AutoDeactivate = false;
    public bool AutoDeactivateOnDisable = true;
    public bool AutoActivateOnEnable = false;

    // Use this for initialization
    void Start() {
        if (AutoDeactivate)
            deactivateAll();
    }

    public void deactivateAll() {
        for (int i = 0; i < list.Count; i++) {
            list[i].Deactivate();
        }
    }

    public void ActivateAll() {
        for (int i = 0; i < list.Count; i++) {
            list[i].Activate();
        }
    }

    // Update is called once per frame
    void Update() {

    }

    void OnDestroy() {
        deactivateAll();
    }

    void OnEnable() {
        if (AutoActivateOnEnable)
            ActivateAll();
    }

    void OnDisable() {
        if (AutoDeactivateOnDisable)
            deactivateAll();

        for (int i = 0; i < list.Count; i++) {
            //list[i].StopSmoothly(0.2f);
        }
    }

}
