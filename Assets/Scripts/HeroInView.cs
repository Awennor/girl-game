﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HeroInView : MonoBehaviour {

    [SerializeField]
    float angle;
    [SerializeField]
    float maxDistance;

    bool previouslySeen;

    public UnityEvent OnSightEnter;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		var hero = CentralResource.Instance.Hero;
        var selfLook = this.transform.forward;
        var heroPos = hero.transform.position;
        heroPos.y += 3;
        var heroDirectionFromMe = heroPos - transform.position;
        float angle = Vector3.Angle(selfLook, heroDirectionFromMe);
        if(angle < this.angle && heroDirectionFromMe.magnitude < maxDistance) {
            if(!previouslySeen) {
                OnSightEnter.Invoke();

            }
            previouslySeen = true;
        } else {
            previouslySeen = false;
        }
        //var rotation = this.transform.rotation;
        //var selfEuler = rotation.eulerAngles;
        
        
	}
}
