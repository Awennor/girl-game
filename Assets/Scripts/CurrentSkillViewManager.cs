﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class CurrentSkillViewManager : MonoBehaviour {

    public SkillInfoUI skillInfo;
    bool hiding;
    private int currentSkill;
    int hiddenX = -160;

    // Use this for initialization
    void Start() {



        DOTween.Sequence()
            .Append(skillInfo.transform.DOLocalMoveY(10, 0.8f).SetEase(Ease.InOutQuad))
            .Append(skillInfo.transform.DOLocalMoveY(0, 0.8f).SetEase(Ease.InOutQuad))
            .SetLoops(-1);
        ;
    }

    // Update is called once per frame
    void Update() {

    }

    internal void UseAnimation() {
        skillInfo.ClickEffect();
        /*DOTween.Sequence()
            .Append(skillInfo.transform.DOLocalMoveX(80, 0.1f))
            .Append(skillInfo.transform.DOLocalMoveX(0, 0.1f))
            ;
            */
    }

    internal void ChangeSkillId(int s) {
        bool skillChange = false;
        if(currentSkill >= 0 && s >= 0) {
            skillChange = true;
        }
        currentSkill = s;
        if(skillChange) {
            if(!hiding)
                Hide();
            return;
        }

        if (s < 0) {

            if (skillInfo != null && skillInfo.gameObject.activeSelf && !hiding) {
                Hide();

            }
            //skillInfo.gameObject.SetActive(false);
        } else {
            if (!hiding) {
                AppearAnimation();
            }


        }
    }

    private void Hide() {
        hiding = true;
        DOTween.Sequence()
                            .Append(skillInfo.transform.DOLocalMoveX(hiddenX, 0.5f))
                            .AppendCallback(EndHide)
                    ;
    }

    private void EndHide() {
        skillInfo.gameObject.SetActive(false);
        hiding = false;
        if (currentSkill >= 0) {
            AppearAnimation();
        }
    }

    private void AppearAnimation() {
        skillInfo.gameObject.SetActive(true);
        var p = skillInfo.transform.position;
        p.x = hiddenX;
        skillInfo.transform.localPosition = p;
        skillInfo.ChangeSkill((SkillInfoProvider.SkillId)currentSkill);
        skillInfo.transform.DOLocalMoveZ(0, 0f);
        DOTween.Sequence()
                    .Append(skillInfo.transform.DOLocalMoveX(0, 0.5f))
                    
            ;
    }
}
