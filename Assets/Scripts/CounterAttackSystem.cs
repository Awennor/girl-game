﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CounterAttackSystem : MonoBehaviour {

    public UnityEvent OnCounterShield;
    public UnityEvent OnCounterShieldSuccess;
    bool shielding;
    public float shieldTime;
    public float afterShieldImmunityTime;
    public float buffDuration;
    private bool immunityTime;
    private float shieldCounterTime, immunityCounterTime;
    private float buffCounterTime;
    public ActorStats.Buff counterSuccessBuff;
    public MyBoolEvent OnCounterBuffActive;

    // Use this for initialization
    void Start() {
        BuffState(false);
        var attackInput = GetComponent<AttackInput>();
        CentralResource.Instance.BattleFlowSys.OnBattleEnd += BattleFlowSys_OnBattleEnd;
        attackInput.OnAttackStartSuccess += AttackInput_OnAttackStartSuccess;
        var collisionProj = GetComponent<CollisionProj>();
        collisionProj.CollisionInterruptors.Add(CollisionProj_CollisionInterruptTry);
        GetComponent<SkillHelper>().ActorStats.AddIndependentBuff(counterSuccessBuff);
    }

    private void BattleFlowSys_OnBattleEnd() {
        BuffState(false);
    }

    private bool CollisionProj_CollisionInterruptTry(CollisionProj arg1) {

        if (shielding && !immunityTime) {
            OnCounterShieldSuccess.Invoke();
            immunityTime = true;
            immunityCounterTime = afterShieldImmunityTime;
            bool state = true;
            BuffState(state);
            buffCounterTime = buffDuration;
            //shielding = false;
            //shieldCounterTime = 0.0001f;
            //DOVirtual.DelayedCall(afterShieldImmunityTime, ImmunityEnd, false);
            return true;
        }
        if (immunityTime) {
            return true;
        }
        BuffState(false);
        return false;
    }

    private void BuffState(bool state) {
        counterSuccessBuff.Enabled = state;
        OnCounterBuffActive.Invoke(state);
    }

    private void ImmunityEnd() {
        immunityTime = false;
    }

    private void AttackInput_OnAttackStartSuccess() {
        if(!enabled) return;
        OnCounterShield.Invoke();
        shielding = true;
        shieldCounterTime = shieldTime;

    }

    private void CounteringDisable() {
        shielding = false;
    }

    // Update is called once per frame
    void Update() {

        if (shieldCounterTime >= 0) {
            shieldCounterTime -= Time.deltaTime;
            if(shieldCounterTime < 0) {
                CounteringDisable();
            } 
        } 
        if (immunityCounterTime >= 0) {
            immunityCounterTime -= Time.deltaTime;
            if(immunityCounterTime < 0) {
                ImmunityEnd();
            }
        }
        if (buffCounterTime >= 0) {
            buffCounterTime  -= Time.deltaTime;
            if(buffCounterTime  < 0) {
                BuffState(false);
            }
        }
    }
}
