﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts.Logic;
using System;

public class HPSystem : MonoBehaviour {
    [SerializeField]
    RangedValue hp = new RangedValue();
    [SerializeField]
    private bool destroyOnHPZero = false;
    public static  Action<GameObject> deathOnHPZeroDestroy;
    public Action<GameObject> HPZeroHappens;
    public Action<HPSystem> HPChange;

    public bool DestroyOnHPZero {
        get {
            return destroyOnHPZero;
        }

        set {
            destroyOnHPZero = value;
        }
    }

    internal void FullHeal() {
        hp.Value = hp.Max;
    }

    // Use this for initialization
    void Start () {
        hp.ValueChanged += (rv) => {
            if(HPChange != null) {
                HPChange(this);
            }
            if(rv.isZero()) {
                if(HPZeroHappens != null) {
                    HPZeroHappens(gameObject);
                }
            }
            if (DestroyOnHPZero && rv.isZero()) {
                
                Destroy(gameObject);
                if(deathOnHPZeroDestroy !=null) {
                    deathOnHPZeroDestroy(this.gameObject);
                }
            }
        };
	}

    internal void SetMaxHPFullHeal(int v)
    {
        hp.Max = v;
        hp.Value = v;
    }

    internal float GetHP()
    {
        return hp.Value;
    }

    internal float GetMaxHP()
    {
        return hp.Max;
    }

    // Update is called once per frame
    void Update () {
	
	}

    internal void DealDamage(float damage)
    {
        hp.Value -= damage;
    }

    internal bool IsZero() {
        return hp.Value == 0;
    }

    internal bool RatioGoneBelow(float v) {
        return hp.RatioGoneBelow(v);
    }
}
