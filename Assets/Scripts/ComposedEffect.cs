﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComposedEffect : MonoBehaviour {
    [SerializeField]
    ComposedEffectUnit[] units;
    private EffectCaller graphicRun;
    private SoundCaller soundRun;
    private UnityEventSequenceBehaviour runner;
    public bool RunOnStart = true;
    public bool RunOnEnable = true;

    public void Awake() {
        graphicRun = this.GetOrAddComponent<EffectCaller>();
        soundRun = this.GetOrAddComponent<SoundCaller>();
        runner = this.GetOrAddComponent<UnityEventSequenceBehaviour>();
        runner.RunOnStart = RunOnStart;
        for (int i = 0; i < units.Length; i++) {
            var u = units[i];

            runner.AddUnit(() => { RunUnit(u); }, u.Delay);
        }
    }

    public void OnEnable() {
        if(RunOnEnable) {
            runner.Invoke();
        }
    }

    private void RunUnit(ComposedEffectUnit u) {

        Debug.Log("RUNNING THE UNIT!!!");

        if(u.Graphic) {
            graphicRun.CallEffectTimeToLive(u.Name, u.TimeToStop);
        }

        if(u.Sound) {
            soundRun.Play(u.Name);
            if(u.TimeToStop > 0) {
                SoundSingleton.Instance.StopSoundDelay(u.Name, u.TimeToStop);
            }
        }
    }


    public void Run() {
        runner.Invoke();
    }

}

[Serializable]
public class ComposedEffectUnit {
    [SerializeField]
    float delay;
    [SerializeField]
    string name;
    [SerializeField]
    bool sound, graphic;
    [SerializeField]
    float timeToStop = -1;

    public float Delay {
        get {
            return delay;
        }

        set {
            delay = value;
        }
    }

    public bool Graphic {
        get {
            return graphic;
        }

        set {
            graphic = value;
        }
    }

    public string Name {
        get {
            return name;
        }

        set {
            name = value;
        }
    }

    public bool Sound {
        get {
            return sound;
        }

        set {
            sound = value;
        }
    }

    public float TimeToStop {
        get {
            return timeToStop;
        }

        set {
            timeToStop = value;
        }
    }
}
