﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EmailData", menuName = "Data/Email Data", order = 2)]
public class EmailData : ScriptableObject {
    [SerializeField]
    SingleMail[] mails;

    internal SingleMail GetMail(string mailid) {
        for (int i = 0; i < mails.Length; i++) {
            if(mails[i].Id.Equals(mailid)) {
                return mails[i];
            }
        }
        return null;
    }
}

[Serializable]
public class SingleMail {
    
    [SerializeField]
    private string id, sender, receiver;
        //, senderMail, receiverMail/*, bodyAndSubjectIdentifier*/;
    [SerializeField]
    int year, month, day;
    [SerializeField]
    bool sentFromMobile;

    public int Day {
        get {
            return day;
        }

        set {
            day = value;
        }
    }

    public string Id {
        get {
            return id;
        }

        set {
            id = value;
        }
    }

    public int Month {
        get {
            return month;
        }

        set {
            month = value;
        }
    }

    public string Receiver {
        get {
            return receiver;
        }

        set {
            receiver = value;
        }
    }

    public string Sender {
        get {
            return sender;
        }

        set {
            sender = value;
        }
    }

    public int Year {
        get {
            return year;
        }

        set {
            year = value;
        }
    }

    public bool SentFromMobile {
        get {
            return sentFromMobile;
        }

        set {
            sentFromMobile = value;
        }
    }
}
