﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ArticleData", menuName = "Data/Article Data", order = 2)]
public class ArticleData : ScriptableObject {
    [SerializeField]
    SingleArticle[] articles;

    internal SingleArticle GetArticle(string mailid) {
        for (int i = 0; i < articles.Length; i++) {
            if (articles[i].Id.Equals(mailid)) {
                return articles[i];
            }
        }
        return null;
    }
}

[Serializable]
public class SingleArticle {

    [SerializeField]
    private string id;
    //, senderMail, receiverMail/*, bodyAndSubjectIdentifier*/;
    [SerializeField]
    int year, month, day;

    public int Day {
        get {
            return day;
        }

        set {
            day = value;
        }
    }

    public string Id {
        get {
            return id;
        }

        set {
            id = value;
        }
    }

    public int Month {
        get {
            return month;
        }

        set {
            month = value;
        }
    }


    public int Year {
        get {
            return year;
        }

        set {
            year = value;
        }
    }

}
