﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class InputHandleEvent : MonoBehaviour {

    public UnityEvent ButtonPressed;

    public bool OutOfBattleOnly;
    public bool OnPausedOnly;
    public bool OnNotPausedOnly;
    public bool OnOverworldActiveOnly;
    public bool OnEditorOnly;

    public string InputKey;
    public KeyCode InputKey_Code;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(InputKey.Length > 0 && Input.GetButtonDown(InputKey)) {
            KeyPressed();
        }
        if(Input.GetKeyDown(InputKey_Code)) {
            KeyPressed();
        }
    }

    private void KeyPressed() {
        if (
                        (!OutOfBattleOnly || !CentralResource.Instance.BattleFlowSys.InBattle)
                        &&
                        (!OnPausedOnly || Time.timeScale == 0)
                        &&
                        (!OnNotPausedOnly || Time.timeScale != 0)
                        &&
                        (!OnEditorOnly || Application.isEditor)
                        &&
                        (!OnOverworldActiveOnly || CentralResource.Instance.OverworldInput.InputActive)
                        ) {

            ButtonPressed.Invoke();
        }
    }
}


