﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(CharacterControllerWrapper))]
[RequireComponent(typeof(SkillHelper))]
public class CinematicFall : MonoBehaviour {

    public string fallingTrigger = "falling";
    public string risingTrigger = "fallrising";
    public string lyingTrigger = "lying";
    public float reboundScale = 0.5f;
    public float minimumSpeedYRebound = -50;
    public float minimumSpeedYRisingTrigger = 40;

    bool falling = false;
    private CharacterControllerWrapper characterControllerWrap;
    private float lastSpeedY;
    private SkillHelper skillHelper;
    public Action fallEnd;

    public void StartFall(Vector3 initialSpeed) {
        falling = true;
        GetComponent<CollisionProj>().IgnoreCollision = true;
        if(initialSpeed.y >0) {
            skillHelper.AnimationTrigger(fallingTrigger);
        } else {
            skillHelper.AnimationTrigger(fallingTrigger);
        }
        characterControllerWrap.Speed = initialSpeed;
    }

    internal void Update() {
        if (falling) {
            float speedY = characterControllerWrap.getSpeedY();
            if(lastSpeedY >= 0 && speedY < 0) {
                skillHelper.AnimationTrigger(fallingTrigger);
            }
            if (speedY < 0 && characterControllerWrap.isGround()) {
                Vector3 spd = characterControllerWrap.Speed;
                if (speedY< minimumSpeedYRebound) {

                    spd *= reboundScale;
                    spd.y = -spd.y;
                    characterControllerWrap.Speed = spd;
                    if(spd.y >= minimumSpeedYRisingTrigger) {
                        //SoundSingleton.Instance.PlaySound("impact");
                        SoundSingleton.Instance.PlaySound("hitground");
                        skillHelper.AnimationTrigger(risingTrigger);
                    } else {
                        //SoundSingleton.Instance.PlaySound("impact");
                        SoundSingleton.Instance.PlaySound("hitground2");
                    }
                        
                } else {
                    //Debug.Log("FALL END");

                    falling = false;
                    skillHelper.AnimationTrigger(lyingTrigger);
                    if(fallEnd != null) fallEnd();
                }

            }
            lastSpeedY = speedY;
        }
    }

    internal void Start() {
        characterControllerWrap = GetComponent<CharacterControllerWrapper>();
        skillHelper = GetComponent<SkillHelper>();
    }
    internal void Awake() { }
}
