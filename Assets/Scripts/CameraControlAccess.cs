﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControlAccess : MonoBehaviour {

    public void ChangeTargetInstant(GameObject obj) {
        CentralResource.Instance.CameraControl.ChangeTargetInstant(obj);
    }

    public void BackToNormalTarget() {
        CentralResource.Instance.CameraControl.BackToNormalTarget();
    }

    public void ChangeTarget(GameObject obj) {
        CentralResource.Instance.CameraControl.ChangeTargetTemp(obj);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
