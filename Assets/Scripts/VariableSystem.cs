﻿using System;

internal class VariableSystem {

    public Action<int,float> variableChange;

    internal void SetVariableValue(VariableEnum variableEnum, float newValue) {
        CentralResource.Instance.SaveSlots.CurrentSlot.SetVariableValue(variableEnum, newValue);
        if(variableChange != null) {
            variableChange((int)variableEnum, newValue);
        }
    }

    internal float GetVariableValue(VariableEnum variableName) {
        return CentralResource.Instance.SaveSlots.CurrentSlot.GetVariableValue(variableName);
    }

}