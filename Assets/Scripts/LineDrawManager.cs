﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class LineDrawManager : MonoBehaviour {

    [SerializeField]
    LineRenderer lineRender;
    private int skill = -1;
    [SerializeField]
    GameObject followObject;
    [SerializeField]
    GameObject rotateLineToThisObject;
    public Vector3 rotationForTrajectory;
    public BoxCollider boxColliderObj;
    public Colliding colliding;
    [SerializeField]
    bool rotationToMouse = true;
    [SerializeField]
    float yOffSetFollowObject = 4;
    bool disableRotationForTrajectory = true;
    private Quaternion latestTargetRotation;

    public int Skill {
        get {
            return skill;
        }

        set {
            skill = value;
        }
    }

    public GameObject DrawLineFromOriginToThisObj {
        get {
            return followObject;
        }

        set {
            followObject = value;
            Update();
        }
    }

    public GameObject RotateLineToThisObject {
        get {
            return rotateLineToThisObject;
        }

        set {
            rotateLineToThisObject = value;
        }
    }

    // Use this for initialization
    void Start() {
        //UpdateTrajectory(new Vector3(0,0,0), new Vector3(20, 30,0), Physics.gravity);
        //Visible(false);
    }

    public void NothingOnLine() {
        lineRender.SetVertexCount(0);

    }

    public void UpdateTrajectory(Vector3 initialPosition, Vector3 finalPoint) {
        int numSteps = 2;
        LineRenderer lineRenderer = lineRender;
        lineRenderer.SetVertexCount(numSteps);

        lineRenderer.SetPosition(0, initialPosition * 0.9f + finalPoint * 0.1f);
        lineRenderer.SetPosition(1, finalPoint);

        boxColliderObj.transform.localPosition = initialPosition / 2 + finalPoint / 2;
        var lS = boxColliderObj.transform.localScale;
        lS.x = (finalPoint - initialPosition).magnitude;
        lS.y = 3.2f;
        lS.z = 3.2f;
        boxColliderObj.transform.localScale = lS;
        colliding.Clear();
        lineRender.gameObject.SetActive(false);
        DOVirtual.DelayedCall(0.1f, LineActive);
        if (!disableRotationForTrajectory)
            lineRenderer.transform.localRotation = Quaternion.Euler(
                rotationForTrajectory.x,
                rotationForTrajectory.y,
                rotationForTrajectory.z
                );
    }

    private void LineActive() {
        lineRender.gameObject.SetActive(true);
    }

    public void UpdateTrajectory(Vector3 initialPosition, Vector3 initialVelocity, Vector3 gravity, float extraTime = 0.0f) {
        int numSteps = 15; // for example
        float timeDelta; // for example
        float timeForGround = (-initialVelocity.y / gravity.y) * 2 + extraTime;
        timeDelta = timeForGround / (float)numSteps;


        LineRenderer lineRenderer = lineRender;
        lineRenderer.SetVertexCount(numSteps);

        Vector3 position = initialPosition;
        Vector3 velocity = initialVelocity;
        for (int i = 0; i < numSteps; ++i) {
            lineRenderer.SetPosition(i, position);
            if (i == numSteps - 2) {
                colliding.Clear();
                boxColliderObj.transform.localPosition = position;
                var lS = boxColliderObj.transform.localScale;
                lS.x = 3;
                lS *= 3;
                boxColliderObj.transform.localScale = lS;
            }
            position += velocity * timeDelta + 0.5f * gravity * timeDelta * timeDelta;
            velocity += gravity * timeDelta;

        }
        if (!disableRotationForTrajectory)
            lineRenderer.transform.localRotation = Quaternion.Euler(
                rotationForTrajectory.x,
                rotationForTrajectory.y,
                rotationForTrajectory.z
                );
    }

    internal void Visible(bool s) {
        lineRender.gameObject.SetActive(s);
    }

    internal void SkillShowing(int skill) {
        this.Skill = skill;
    }

    // Update is called once per frame
    void Update() {
        if (colliding != null) {
            if (colliding.CollidingSomething()) {
                lineRender.SetColors(new Color(1, 1, 1), new Color(0, 1, 1));
            } else {
                lineRender.SetColors(new Color(0.5f, 0.5f, 0.5f), new Color(5f, 0, 0));
            }
        }
        if (!disableRotationForTrajectory)
            lineRender.transform.localRotation = Quaternion.Euler(
                rotationForTrajectory.x,
                rotationForTrajectory.y,
                rotationForTrajectory.z
                );
        if (DrawLineFromOriginToThisObj != null) {
            Vector3 pos = DrawLineFromOriginToThisObj.transform.position;
            Vector3 dis = pos - transform.position;

            dis.y += yOffSetFollowObject;
            lineRender.SetVertexCount(2);
            lineRender.SetPosition(0, new Vector3(0, yOffSetFollowObject, 0));
            lineRender.SetPosition(1, dis);
            //lineDrawmanager.gameObject.transform.localRotation = -transform.localRotation;
            lineRender.gameObject.transform.rotation = Quaternion.identity;
        }

        if (rotationToMouse && followObject == null &&
            CentralResource.Instance.AimingInfo.MouseAimingActive) {

            RotationToMouse.LookAtMouse(lineRender.gameObject.transform, -90);
        }
        if (RotateLineToThisObject != null && followObject == null) {
            //lineRender.transform.RotateYAxisToTarget(RotateLineToThisObject.transform);
            var transform = lineRender.transform;
            var t = RotateLineToThisObject.transform;
            Quaternion targetRotation = Quaternion.LookRotation(t.position - transform.position);
            var originalEuler = transform.rotation.eulerAngles;
            var euler = targetRotation.eulerAngles;
            euler.z = originalEuler.z;
            euler.x = originalEuler.x;
            var targetY = euler.y - 90;
            //var originY = originalEuler.y - 90;
            //var disY = targetY - originY;
            //var lerpY = Time.deltaTime * disY * 4 + originY;
            //euler.y = lerpY;
            euler.y = targetY;
            //euler.y = originalEuler.y;
            //Debug.Log(originalEuler.y + "before Y rot");

            targetRotation = Quaternion.Euler(euler);
            //euler.

            // Smoothly rotate towards the target point.

            //transform.rotation = targetRotation;
            
            transform.rotation= Quaternion.Lerp(latestTargetRotation, targetRotation, 0.1f);//targetRotation;
            latestTargetRotation = targetRotation;
        }
    }
}
