﻿using UnityEngine;
using System.Collections;

public class AutoAddCameraGirl : MonoBehaviour {

	// Use this for initialization
	void Start () {
        transform.SetParent(CentralResource.Instance.CameraControl.Camera.transform);
	    transform.localPosition = new Vector3(0,0,0);
	}
	
	// Update is called once per frame
	void Update () {
        transform.localRotation = Quaternion.identity;	
	}
}
