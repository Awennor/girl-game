﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class CaretFixer : MonoBehaviour {

    Text text;

	// Use this for initialization
	void Start () {
        DOVirtual.DelayedCall(0.5f, FixCaret);
    }

    public void FixCaret() {
        text = GetComponent<Text>();
        text.rectTransform.pivot = new Vector2(0f, -0.02f);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
