﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DeckEditCard : MonoBehaviour {

    [SerializeField]
    private SkillInfoUI skillInfo;
    [SerializeField]
    public Text text;

    public SkillInfoUI SkillInfo {
        get {
            return skillInfo;
        }

        set {
            skillInfo = value;
        }
    }


    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    
}
