﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;
using UnityEngine.UI;

public class DeckEditUI : MonoBehaviour {

    public DeckEditCardManager stockManager;
    public DeckEditCardManager deckManager;
    private SkillInventoryDeckManagement skillInventoryDeckManagement;
    public GameObject graphicParent;

    public Text cardsInDeckLabel;

    List<int> skillIdsVisibleStock = new List<int>();
    List<int> skillIdsVisibleDeck = new List<int>();
    private int skillIdAddedToDeck;
    public MyBoolEvent DeckIsUsable;

    // Use this for initialization
    void Start() {
        stockManager.CardPressed.AddListener(StockCardPressed);
        deckManager.CardPressed.AddListener(DeckCardPressed);
        skillInventoryDeckManagement = CentralResource.Instance.SkillInventoryDeckManagement;
        OpenUI();

    }



    public void OkClick() {
        skillInventoryDeckManagement.CopyTempToCurrent();
        CentralResource.Instance.SaveSlots.SaveData();
        SoundSingleton.Instance.PlaySoundOnPause("uigood2");
        Close();
    }

    public void ReturnClick() {
        SoundSingleton.Instance.PlaySoundOnPause("cancel");
        Close();
    }



    public void Close() {

        Time.timeScale = 1;
        graphicParent.SetActive(false);

        //DOVirtual.DelayedCall(0.2f, ClosingFinish);
    }

    private void OpenUI() {
        skillInventoryDeckManagement.DeckTempBecomeCurrentContent();
        var skillInvent = skillInventoryDeckManagement.SkillInvenDeckData.SkillInventory;
        var quantities = skillInvent.SkillQuantities;
        skillIdsVisibleStock.Clear();
        for (int i = 0; i < quantities.Count; i++) {
            if (quantities[i] > 0) {
                skillIdsVisibleStock.Add(i);
            }
        }
        stockManager.CardsActive(skillIdsVisibleStock.Count);
        UpdateDeck();

        for (int i = 0; i < skillIdsVisibleStock.Count; i++) {
            var skillId = skillIdsVisibleStock[i];
            stockManager.ChangeSkill(i, skillId);
        }
        UpdateStockQuantities();
    }

    private void UpdateDeck() {
        skillInventoryDeckManagement.FillSkillIdsVisibleOnTempDeck(skillIdsVisibleDeck);
        skillIdsVisibleDeck.Sort();
        deckManager.CardsActive(skillIdsVisibleDeck.Count);

        cardsInDeckLabel.text = skillInventoryDeckManagement.DeckTempAmount() + "/" + SkillInventoryDeckManagement.MAXCARDSDECK;
        DeckIsUsable.Invoke(skillInventoryDeckManagement.DeckTempAmount() >= SkillInventoryDeckManagement.MAXCARDSDECK);


        for (int i = 0; i < skillIdsVisibleDeck.Count; i++) {
            var skillId = skillIdsVisibleDeck[i];

            deckManager.ChangeSkill(i, skillId);
            deckManager.CardAmount(i,
                skillInventoryDeckManagement.AmountInTempDeck(skillId));
            Canvas.ForceUpdateCanvases();
            if (skillIdAddedToDeck == skillId) {
                skillIdAddedToDeck = -1;
                deckManager.MakeSureToShowCardIn(i);
                int cardpos = i;
                deckManager.ShineCard(cardpos);
            }
        }
    }

    private void UpdateStockQuantities() {
        var skillInvent = skillInventoryDeckManagement.SkillInvenDeckData.SkillInventory;
        var quantities = skillInvent.SkillQuantities;
        for (int i = 0; i < skillIdsVisibleStock.Count; i++) {
            stockManager.CardAmount(i, GetStockQuantity(i));
            stockManager.CardWantsInteractionState(i, StockCardCanBeAdded(i));

        }


    }

    public int GetStockQuantity(int cardIdPos) {
        var skillId = skillIdsVisibleStock[cardIdPos];
        var skillInvent = skillInventoryDeckManagement.SkillInvenDeckData.SkillInventory;
        var quantities = skillInvent.SkillQuantities;
        int q = (quantities[skillId] - skillInventoryDeckManagement.AmountInTempDeck(skillId));
        return q;
    }

    private void StockCardPressed(int cardPosId) {

        if (StockCardCanBeAdded(cardPosId)) {
            SoundSingleton.Instance.PlaySoundOnPause("draw");
            SoundSingleton.Instance.PlaySoundOnPause("uigood1");
            skillIdAddedToDeck = skillIdsVisibleStock[cardPosId];
            skillInventoryDeckManagement.AddToTempDeck(skillIdAddedToDeck);
            UpdateStockQuantities();
            UpdateDeck();
        } else {
            SoundSingleton.Instance.PlaySoundOnPause("uicancel");
        }
    }

    private void DeckCardPressed(int cardPos) {
        skillInventoryDeckManagement.RemoveSkillIdDeckTempFromEnd(deckManager.SkillIdOfCardAtPos(cardPos));
        UpdateStockQuantities();
        SoundSingleton.Instance.PlaySoundOnPause("draw");
        UpdateDeck();
    }

    private bool StockCardCanBeAdded(int cardPosId) {
        var skillId = skillIdsVisibleStock[cardPosId];
        return GetStockQuantity(cardPosId) > 0
                    && skillInventoryDeckManagement.AmountInTempDeck(skillId) < 5;
    }

    // Update is called once per frame
    internal void Update() {
        
    }

    public void TryOpenDeckInterface() {
        if (skillInventoryDeckManagement.CanEditDeck) {
            if (graphicParent.activeSelf) {
                Close();
            } else {

                graphicParent.SetActive(true);
                Time.timeScale = 0;
                OpenUI();
                DOVirtual.DelayedCall(0.07f, () => {


                });
            }
        }
    }
}
