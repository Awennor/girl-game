﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class RayGraphic : MonoBehaviour {

    public Image sprite;
    private Sequence MightUseTween;
    public Color normalColor;
    public Color noGoodColor;

    public void Summon(float delay) {
        //TryKillMightUse();
        fullAlpha();
        TryPauseMightUse(true);
        sprite.transform.localRotation = Quaternion.Euler(0,0, 0);
        sprite.transform.localScale = new Vector3(100f,0f,1);
        //if(delay > 0)
            //sprite.rectTransform.DOScaleX(0,0);
        DOTween.Sequence().
            AppendInterval(delay).Append(sprite.rectTransform.DOScale(new Vector3(4,4,1), 0.05f))
            .Append(sprite.rectTransform.DOScale(new Vector3(0.8f,0.8f), 0.05f))
            .Append(sprite.rectTransform.DOScale(new Vector3(1f,1f), 0.05f))
            .SetUpdate(true);
    }

    public void TryPauseMightUse(bool pause) {
        if(MightUseTween != null) {
            //MightUseTween.Restart();
            if(pause )MightUseTween.Pause();
            else MightUseTween.Play();
        }
        fullAlpha();
    }

    public void TryKillMightUse() {
        if(MightUseTween != null) {
            MightUseTween.Kill();
            MightUseTween = null;
            
        }
        fullAlpha();
    }

    private void fullAlpha() {
        var c = sprite.color;
        c.a = 1;
        sprite.color = c;
    }

    public void DisappearAnim(float delay) {
        //fullAlpha();
        TryPauseMightUse(true);
        if(delay > 0)
            sprite.rectTransform.DOScaleX(1,1);
        sprite.transform.localRotation = Quaternion.Euler(0,0, 45);
        DOTween.Sequence().
            AppendInterval(delay)
            .Append(sprite.rectTransform.DOScale(new Vector3(10f, 5f, 1f), 0.05f))
            .Append(sprite.rectTransform.DOScale(new Vector3(4f, 1f, 1f), 0.05f))
            .Append(sprite.rectTransform.DOScale(new Vector3(0,100f,1), 0.05f))
            //.Append(sprite.rectTransform.DOScaleX(1, 0.1f))
            .AppendCallback(()=> {
                ResetScaleAndRotation();
                gameObject.SetActive(false);

            })
            .SetUpdate(true);
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void MightUse() {
        TryKillMightUse();
        if(MightUseTween != null) {
            TryPauseMightUse(pause:false);
            return;
        }
        fullAlpha();
        ResetScaleAndRotation();
        MightUseTween = DOTween.Sequence()
            
            .AppendInterval(0.15f)
            .Append(sprite.DOFade(0f,0.05f))
            .AppendInterval(0.15f)
            .Append(sprite.DOFade(1,0.05f))
            .SetUpdate(true);
        MightUseTween.SetLoops(-1);
    }

    public void ResetScaleAndRotation() {
        sprite.transform.localScale = new Vector3(1,1,1);
        sprite.transform.localRotation = Quaternion.identity;
    }

    internal void MightGet() {
        ResetScaleAndRotation();
        sprite.DOFade(0.5f, 0f).SetUpdate(true);
        sprite.transform.localScale = new Vector3(0.5f,0.5f,0.5f);
    }

    internal void noGoodColorMode() {
        sprite.color = noGoodColor;
    }

    internal void normalColorMode() {
        sprite.color = normalColor;
    }
}
