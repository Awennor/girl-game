﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EffectSingleton {

    static EffectSingleton instance;

    Dictionary<string, GameObject> effects = new Dictionary<string, GameObject>();
    Dictionary<string, EffectSingletonNode> effectNodes = new Dictionary<string, EffectSingletonNode>();
    List<GameObject> effectsMarkedAsUnique = new List<GameObject>();

    public bool hasAny() {
        return effects.Count > 0;
    }

    public static EffectSingleton Instance {
        get {
            if (instance == null) {
                instance = new EffectSingleton();
            }
            return instance;
        }

        set {
            instance = value;
        }
    }

    internal void SpawnParticleHere(string effectName, int particleAmount, Vector3 position) {
        var effectObj = effects[effectName].GetComponent<ParticleSystem>();
        effectObj.transform.position = position;
        effectObj.gameObject.SetActive(true);
        effectObj.Emit(particleAmount);

    }

    public void AddEffect(string key, GameObject aS) {
        const string uniqueTag = "_U";
        if (key.Contains(uniqueTag)) {
            key = key.Replace(uniqueTag, "");
            effectsMarkedAsUnique.Add(aS);
        }
        var effectSingletonNode = aS.GetComponent<EffectSingletonNode>();
        if (effectSingletonNode != null) {
            effectNodes.Add(key, effectSingletonNode);
        }
        effects.Add(key, aS);
    }

    internal GameObject ShowAndAddEffect(string name, Transform transform) {
        var obj = ShowEffectHere(name, transform);

        obj.transform.SetParent(transform);
        return obj;
    }

    internal GameObject ShowEffectHere(string name, Transform transform, Vector3 offset = new Vector3()) {
        var position = transform.position;
        position += offset;
        return ShowEffectHere(name, position);
    }

    public GameObject ShowEffectHere(string name, Vector3 position) {
        if (!effects.ContainsKey(name)) {
            Debug.Log("Effect not found: " + name);
        }

        if (effectNodes.ContainsKey(name)) {
            var node = effectNodes[name];
            node.EffectSummon(position);
            return node.gameObject;
        } else {
            var effect = effects[name];
            var obj = effect;
            if (effectsMarkedAsUnique.Contains(effect)) {

            } else {
                obj = GameObject.Instantiate(effect);
            }



            obj.transform.position = position;
            obj.SetActive(false);
            obj.SetActive(true);
            return obj;
        }


    }
}
