﻿using UnityEngine;
using System.Collections;

public class EffectLoader : MonoBehaviour {

    public Transform[] parents;

    void Awake() {
        if (!EffectSingleton.Instance.hasAny()) {
            foreach (Transform transform in parents) {
                int childCount = transform.childCount;
                for (int i = 0; i < childCount; i++) {
                    var obj = transform.GetChild(i).gameObject;
                    string n = obj.name;
                    EffectSingleton.Instance.AddEffect(n, obj);
                }
            }

        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
