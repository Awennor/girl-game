﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class CameraControl : MonoBehaviour {

    [SerializeField]
    new private Camera camera;
    
    

    private float defaultOrthoSize;
    private SmoothFollowC smoothFollow;
    [SerializeField]
    private Transform realTarget;

    private Color backGroundColor = new Color(0,0,0,0);
    

    public Camera Camera {
        get {
            return camera;
        }

        set {
            camera = value;
        }
    }

    public SmoothFollowC SmoothFollow {
        get {
            return smoothFollow;
        }

        set {
            smoothFollow = value;
        }
    }

    public float DefaultOrthoSize {
        get {
            return defaultOrthoSize;
        }

        set {
            defaultOrthoSize = value;
        }
    }

    public Color BackGroundColor {
        get {
            return backGroundColor;
        }

        set {
            backGroundColor = value;
            camera.backgroundColor = value;
        }
    }




    // Use this for initialization
    void Start () {
	    DefaultOrthoSize = Camera.orthographicSize;    
        SmoothFollow = camera.GetComponent<SmoothFollowC>();
        //DOTween.Sequence().Append(camera.DOOrthoSize(28, 0.3f)).AppendInterval(1f).Append(camera.DOOrthoSize(15, 0.5f));
        //camera.DOOrthoSize(28);
        //animator.
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void Zoom(float ratio, float timeZooming, float delay=0) {
        Camera.DOOrthoSize(ratio*DefaultOrthoSize, timeZooming).SetDelay(delay);
    }

    internal Tween Shake(float time, Vector3 strength, int vibration, int randomness) {
        return Camera.DOShakeRotation(time, strength, vibration, randomness);
    }

    internal void PauseFollowingTarget(bool b) {
        SmoothFollow.Pause = b;
        
    }

    internal void ChangeTargetTemp(GameObject gameObject) {
        //realTarget = SmoothFollow.Target;
        SmoothFollow.Target = gameObject.transform;
    }

    public void ChangeTargetInstant(GameObject gameObject) {
        SmoothFollow.Target = gameObject.transform;
        SmoothFollow.AutomaticFollow = true;
    }

    public void BackToNormalTarget() {
        SmoothFollow.Target = realTarget;
        SmoothFollow.AutomaticFollow = false;
    }
}
