﻿using DG.Tweening;
using FlowCanvas;
using FlowCanvas.Nodes;
using ParadoxNotion.Design;
using ParadoxNotion.Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

///Latent Action Nodes do not return any value and can span within multiple frames and have up to 5 parameters. They need Flow execution.
[Name("Async Node Test")]
[Category("Events/Graph")]
[Description("Called per-frame")]
public class NodeTest : SimplexNode {

    private FlowOutput outFlow;
    private FlowOutput doing;
    private FlowOutput done;

    private Queue<IEnumerator> queue = new Queue<IEnumerator>();
    private UnityEngine.Coroutine currentCoroutine;
    private bool graphPaused;

    public override string name {
        get { return queue.Count > 0 ? string.Format("{0} [{1}]", base.name, queue.Count.ToString()) : base.name; }
    }

    sealed public override void OnGraphStarted() { queue = new Queue<IEnumerator>(); currentCoroutine = null; }
    sealed public override void OnGraphStoped() { Break(); }
    sealed public override void OnGraphPaused() { graphPaused = true; }
    sealed public override void OnGraphUnpaused() { graphPaused = false; }

    //begins a new coroutine


    public void Invoke() {
    }

    //breaks all coroutine queues
    protected void Break() {
        if (currentCoroutine != null) {
            MonoManager.current.StopCoroutine(currentCoroutine);
            queue = new Queue<IEnumerator>();
            currentCoroutine = null;
            outFlow.parent.SetStatus(NodeCanvas.Status.Resting);
            OnBreak();
            done.Call(new Flow());
        }
    }

    public void Begin(Flow f) {
        outFlow.Call(f);
    }

    public void End(Flow f) {
        DOVirtual.DelayedCall(2f, TestMethod);
        done.Call(f);
    }

    private void TestMethod() {
        doing.Call(new Flow());
    }

    IEnumerator InternalCoroutine(IEnumerator enumerator, Flow f) {
        var parentNode = outFlow.parent;
        parentNode.SetStatus(NodeCanvas.Status.Running);
        outFlow.Call(f);

        while (enumerator.MoveNext()) {
            while (graphPaused) {
                yield return null;
            }
            doing.Call(f);
            yield return enumerator.Current;
        }

        parentNode.SetStatus(NodeCanvas.Status.Resting);
        done.Call(f);
        currentCoroutine = null;

    }

    protected override void OnRegisterPorts(FlowNode node) {
        //to make update safe from previous version, the ID (2nd string), is same as the old version. The first string, is the actual name.
        outFlow = node.AddFlowOutput("Start", "Out");
        doing = node.AddFlowOutput("Update", "Doing");
        done = node.AddFlowOutput("Finish", "Done");
        node.AddFlowInput("Start", Begin);
        node.AddFlowInput("End", End);
        node.AddValueInput<SkillInfoProvider.SkillId>("Skill Enum");
    }

    ///Called when Break input is called.
    virtual public void OnBreak() { }
}
