﻿using DG.Tweening;
using FlowCanvas;
using FlowCanvas.Nodes;
using ParadoxNotion.Design;
using ParadoxNotion.Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using NodeCanvas.Framework;

namespace FlowCanvas.Nodes {
    ///Latent Action Nodes do not return any value and can span within multiple frames and have up to 5 parameters. They need Flow execution.
    [Name("Skill Use")]
    [Category("Battle Behavior/")]
    [Description("Starts up a skill attack")]
    public class SkillNode : SimplexNode, IUpdatable {

        private FlowOutput start;
        //private FlowOutput doing;
        private FlowOutput success;

        private bool graphPaused;
        private ValueInput<SkillInfoProvider.SkillId> skillValue;
        private ValueInput<AttackInput> attack;
        private bool waitForAttackEnd;

        public override string name {
            get { return base.name; }
        }

        sealed public override void OnGraphStarted() { }
        sealed public override void OnGraphStoped() { Break(); }
        sealed public override void OnGraphPaused() { graphPaused = true; }
        sealed public override void OnGraphUnpaused() { graphPaused = false; }

        //begins a new coroutine


        public void Invoke() {
        }

        //breaks all coroutine queues
        protected void Break() {

        }

        public void Begin(Flow f) {
            Debug.Log("Begin Attack!!" + TimeTag());
            if (attack.value.CanAttack() && !waitForAttackEnd) {
                waitForAttackEnd = true;
                attack.value.TryAttackAsAI((int)skillValue.value);
                attack.value.CanAttackChangeCallback_Bool -= OnCanAttack;
                attack.value.CanAttackChangeCallback_Bool += OnCanAttack;
                start.Call(f);
            } else {
                Debug.Log("Cannot Attack sorry!!" + TimeTag());
            }


        }

        private static string TimeTag() {
            return DateTime.Now.Second + "," + DateTime.Now.Millisecond;
        }

        private void OnCanAttack(bool b) {
            if (waitForAttackEnd) {
                if (b) {
                    Debug.Log("Can Attack!!" + TimeTag());
                    if (waitForAttackEnd) {
                        Debug.Log("Can Attack 2!!" + TimeTag());
                        End(new Flow());

                    }
                } else {
                    Debug.Log("Cannot Attack!!" + TimeTag());
                }
            }



        }

        public void End(Flow f) {
            //DOVirtual.DelayedCall(2f, TestMethod);
            attack.value.CanAttackChangeCallback_Bool -= OnCanAttack;
            waitForAttackEnd = false;
            success.Call(f);


        }

        private void TestMethod() {
            //doing.Call(new Flow());
        }



        protected override void OnRegisterPorts(FlowNode node) {
            //to make update safe from previous version, the ID (2nd string), is same as the old version. The first string, is the actual name.
            start = node.AddFlowOutput("Start", "Out");
            //doing = node.AddFlowOutput("Update", "Doing");
            success = node.AddFlowOutput("Success", "Done");
            node.AddFlowInput("Start", Begin);
            attack = node.AddValueInput<AttackInput>("Attack Input");
            skillValue = node.AddValueInput<SkillInfoProvider.SkillId>("Skill Enum");
            //node.AddValueOutput<bool>("Attacking", GetAttacking);
        }

        private bool GetAttacking() {
            return waitForAttackEnd;
        }

        ///Called when Break input is called.
        virtual public void OnBreak() {
            waitForAttackEnd = false;
            attack.value.CanAttackChangeCallback_Bool -= OnCanAttack;
        }

        void IUpdatable.Update() {

        }
    }

}