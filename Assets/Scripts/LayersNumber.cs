﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class LayersNumber {

    public static int HERO = 11;
    public static int ENEMY = 8;
    public static int PROJHEROSIMPLE = 13;
    public static int PROJHEROGROUND = 14;
    public static int PROJENEMYSIMPLE= 15;
    public static int PROJENEMYGROUND = 16;

    public static int SCENARIO = 17;
    
}
