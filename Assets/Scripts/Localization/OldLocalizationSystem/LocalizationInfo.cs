﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LocalizationInfo  {
    private LocalizationDataDefinition locData;
    int currentLanguage = 0;
    Font[] fonts;

    public Font[] Fonts {
        get {
            return fonts;
        }

        set {
            fonts = value;
        }
    }

    public int CurrentLanguage {
        get {
            return currentLanguage;
        }

        set {
            currentLanguage = value;
        }
    }

    internal Font GetFont() {
        return fonts[CurrentLanguage];
    }

    public LocalizationInfo(LocalizationDataDefinition locInfo) {
        this.locData = locInfo;
    }

    public string GetText(int key) {
        if(locData.locUnits.Length < key) return null;
        return locData.locUnits[key].values[CurrentLanguage];
    }

    public string GetText(LocalizationEnum key) {
        return locData.locUnits[(int)key].values[CurrentLanguage];
    }

}
