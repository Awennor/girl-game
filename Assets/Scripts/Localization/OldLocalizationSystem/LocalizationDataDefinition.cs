﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "LocalizationDataOld", menuName = "Data/Localization DataOld", order = 2)]
public class LocalizationDataDefinition : ScriptableObject {

    public LocalizationUnit[] locUnits;
    public string[] languageCodes;
    
}

[Serializable]
public class LocalizationUnit {
    public string key;
    public string[] values;

}
