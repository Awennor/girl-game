﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "LocalizationData new", menuName = "Data/Localization Data New", order = 2)]
public class LocalizationDataDefine : ScriptableObject {

    public List<LocalizationGroup> groups = new List<LocalizationGroup>();
    public string[] languageCodes;
    public int SpeakerNameGroupIndex;

    public List<Folder> folders = new List<Folder>();

    private string[] groupNames;
    private Dictionary<LocalizationGroup, string[]> messageArraysFirstLanguage;
    
    public string[] MessageArrayFirstLanguage(int group) {
        if(messageArraysFirstLanguage == null) {
            messageArraysFirstLanguage = new Dictionary<LocalizationGroup, string[]>();
        }
        var g = groups[group];
        var contain = messageArraysFirstLanguage.ContainsKey(g);
        string[] messages;
        if (contain) {
            messages = messageArraysFirstLanguage[g];
        } else {
            var ns = g.Nodes;
            messages = new string[ns.Count];
            for (int i = 0; i < messages.Length; i++) {
                messages[i] = ns[i].values[0];
            }
        }
        return messages;
    }


    public string[] GroupNames() {
        if(groupNames != null && groupNames.Length == groups.Count) {

            return groupNames;
        } 
        groupNames = new string[groups.Count];
        for (int i = 0; i < groupNames.Length; i++) {
            groupNames[i] = groups[i].Name;
        }
        return groupNames;
    }

    public int FindIndexOfGroupName(string v) {
        for (int i = 0; i < groups.Count; i++) {
            if (groups[i].Name.Equals(v)) return i;
        }
        return -1;
    }

    public LocalizationGroup GetGroup(string groupId) {
        for (int i = 0; i < groups.Count; i++) {
            if (groups[i].Name.Equals(groupId)) return groups[i];
        }
        return null;
    }
}

[Serializable]
public class LocalizationGroup {
    [SerializeField]
    private string name;
    [SerializeField]
    private string folderName;
    [SerializeField]
    private List<LocalizationNode> nodes = new List<LocalizationNode>();

    public string Name {
        get {
            return name;
        }

        set {
            name = value;
        }
    }

    public List<LocalizationNode> Nodes {
        get {
            return nodes;
        }

        set {
            nodes = value;
        }
    }

    public string FolderName {
        get {
            return folderName;
        }

        set {
            folderName = value;
        }
    }

    public void AddNode(LocalizationNode n) {
        Nodes.Add(n);
    }
}

[Serializable]
public class LocalizationNode {
    public string key;
    public string[] values;
}

[Serializable]
public class Folder {
    public string name;
    public string parentName;
}
