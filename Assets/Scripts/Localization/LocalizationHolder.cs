﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LocalizationHolder {

    LocalizationDataDefine localizationDataDefine;
    Dictionary<string,Dictionary<string,LocalizationNode>> localizationNodes;
    int currentLanguage = 0;

    public LocalizationHolder(LocalizationDataDefine[] locals) {
        localizationNodes = new Dictionary<string, Dictionary<string, LocalizationNode>>();
        for (int j = 0; j < locals.Length; j++) {
            LocalizationDataAdd(locals[j]);
        }
        
    }

    private void LocalizationDataAdd(LocalizationDataDefine define) {
        var groups = define.groups;
        this.localizationDataDefine = define;
        
        for (int i = 0; i < groups.Count; i++) {
            var nodes = groups[i].Nodes;
            string name = groups[i].Name;
            var nodeHolder = new Dictionary<string, LocalizationNode>();
            localizationNodes.Add(name, nodeHolder);
            for (int j = 0; j < nodes.Count; j++) {
                var n = nodes[j];
                nodeHolder.Add(n.key, n);
            }
        }
    }

    internal string MailAddress(string sender) {
        return GetText("@EmailAdresses", sender);
    }

    internal string MailSubject(string id) {
        return GetText("@EmailSubjects", id);
    }

    internal string MailBody(string id) {
        return GetText("@EmailBodies", id);
    }

    internal string ArticleBody(string id) {
        return GetText("@ArticleBodies", id);
    }

    internal string ArticleTitle(string id) {
        return GetText("@ArticleTitles", id);
    }

    public string GetText(string groupKey, string nodeKey) {
        if(!localizationNodes.ContainsKey(groupKey)) {
            Debug.Log("GROUP KEY NOT FOUND!!! "+groupKey);
            List<string> keyList = new List<string>(this.localizationNodes.Keys);
            for (int i = 0; i < keyList.Count; i++) {
                Debug.Log("key "+i +keyList[i]);;
            }
        }
        var groupDic = localizationNodes[groupKey];
        if(!groupDic.ContainsKey(nodeKey)) {
            Debug.Log("NODE KEY NOT FOUND!!!"+nodeKey+groupKey);
        }
        return groupDic[nodeKey].values[currentLanguage];
    }

    internal string GetSpeakerName(string charaKey) {
        return GetText("@SpeakerNames", charaKey);
        //return localizationNodes[localizationDataDefine.groups[localizationDataDefine.SpeakerNameGroupIndex].Name][charaKey].values[currentLanguage];
            
    }

    internal string GetText(LocalizationKey mindKey) {
        return GetText(mindKey.groupKey, mindKey.entryKey);
    }

    //Dictionary<Dictionary<string, LocalizationNode>> localizationNodes;
}

[Serializable]
public class LocalizationKey {
    public LocalizationDataDefine localizationData;
    public string groupKey;
    public string entryKey;

}