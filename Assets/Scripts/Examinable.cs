﻿using UnityEngine;
using System.Collections;
using System;

public class Examinable : MonoBehaviour {

    public Action<Examinable> ExamineTry;
    bool TipActive;

    // Use this for initialization
    void Start() {

    }


    void OnTriggerEnter(Collider col) {

        TutoHelpSingleton.Instance.ExamineTipActive(true);
        TipActive = true;
        //Destroy(this.gameObject);
    }

    void OnTriggerExit(Collider col) {
        TutoHelpSingleton.Instance.ExamineTipActive(false);
        TipActive = false;
    }

    void OnDestroy() {
        if(TipActive)
            TutoHelpSingleton.Instance.ExamineTipActive(false);
    }

    // Update is called once per frame
    void Update() {
        if (TipActive) {
            if (Input.GetButtonDown("Attack")) {
                if (ExamineTry != null) {
                    ExamineTry(this);
                }
            }
        }

    }
}
