﻿using UnityEngine;
using System.Collections;
using System;

public class ProjectilHolder : MonoBehaviour {

    public GameObject[] projs;

    [SerializeField]
    private Transform projectilParent;

    public Transform ProjectilParent {
        get {
            return projectilParent;
        }

        set {
            projectilParent = value;
        }
    }

    public enum Projs { Sword1, EnemyBall1, HeroBall1, EnemyBlast,
        Lightning1, Bomb1, EnemyBall2,
        Laser, Sword2, HeroBallSmall, 
        EnemyBlast2, EnemyBall3, DarkBall1, HeroExamine, HeroBallMind, HeroBallBig,
        EnemyBallCool, Spear,
    }

    internal GameObject GetProjectil(Projs proj)
    {
        GameObject obj = projs[(int)proj];
        GameObject clone = Instantiate(obj);
        return clone;
        //throw new NotImplementedException();
    }
}
