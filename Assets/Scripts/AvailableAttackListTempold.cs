﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AvailableAttackListTempOld : MonoBehaviour {

    List<int> uses = new List<int>();
    List<Action> attackMethods = new List<Action>();
    int chosenAttackId = -1;
    int currentUses;


	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void AddAttack(Action a)
    {
        attackMethods.Add(a);
        uses.Add(1);
    }

    internal void tryAttack()
    {
        if (chosenAttackId >= 0) {
            currentUses--;
            
            attackMethods[chosenAttackId]();
            if (currentUses == 0)
            {
                chosenAttackId = -1;
            }
        }
    }

    internal void ChooseAttack(int id)
    {
        chosenAttackId = id;
        currentUses = uses[id];
    }
}
