﻿using UnityEngine;
using System.Collections;
using System;

public class MovementConstraint : MonoBehaviour, CharacterControllerWrapperModifier {

    

    // Use this for initialization
    void Start () {
        GetComponent<CharacterControllerWrapper>().AddModifier(this);
	}

    public Action<bool, bool> constrainedMove;

    public Vector3 MovementTryModify(Vector3 motion) {
        //Debug.Log("TRY TRY MODIFIY!!!");
        if (motion.x == 0 && motion.z == 0) return motion;
        var motionForTest = motion;
        float h = 0;
        float v = 0;
        const float extraMovement = 2f;
        if (motionForTest.x > 0) h = motionForTest.x * Time.deltaTime + extraMovement;
        if (motionForTest.x < 0) h = motionForTest.x * Time.deltaTime - extraMovement;
        if (motionForTest.z > 0) v = motionForTest.z * Time.deltaTime + extraMovement;
        if (motionForTest.z < 0) v = motionForTest.z * Time.deltaTime - extraMovement;

        bool constrainMoveX = false;
        bool constrainMoveZ = false;
        for (int i = 0; i < 2; i++)
            {
                Vector3 forRayCast = transform.position;
                forRayCast.y += 4;
                //forRayCast += movement * 0.5f;
                if (i == 0 && motion.x != 0)
                {
                    forRayCast.x += h * 1f;
                    
                    if (!Physics.Raycast(forRayCast, Vector3.down, 20f))
                    {
                        motion.x = 0;
                    constrainMoveX = true;
                        //return;
                    }
                }
                if (i == 1 && motion.z != 0)
                {
                    forRayCast.z += v * 1f;
                    if (!Physics.Raycast(forRayCast, Vector3.down, 20f))
                    {
                    constrainMoveZ = true;
                    //v = 0;
                    motion.z = 0;
                        //return;
                    }
                }
            }
        if (constrainMoveX || constrainMoveZ) {
            if (constrainedMove != null) {
                constrainedMove(constrainMoveX, constrainMoveZ);
            }
        }
        return motion;
    }

	// Update is called once per frame
	void Update () {
	
	}

    public Vector3 TryModify(Vector3 motion)
    {
        return MovementTryModify(motion);
    }
}
