﻿using UnityEngine;
using System.Collections;

public class MovementAnimatedActor : MonoBehaviour {

    [SerializeField]
    private float speed = 90f;            // The speed that the player will move at.

    Vector3 movement;                   // The vector to store the direction of the player's movement.
    public Animator anim;                      // Reference to the animator component.
    Rigidbody playerRigidbody;          // Reference to the player's rigidbody.
    private CharacterControllerWrapper CharacterControllerWrapper;
    private BattleStateMachine battleState;
    float h;
    float v;

    public float H {
        get {
            return h;
        }

        set {
            h = value;
        }
    }

    public float V {
        get {
            return v;
        }

        set {
            v = value;
        }
    }

    public float Speed {
        get {
            return speed;
        }

        set {
            speed = value;
        }
    }

    void Start() {
        var control = GetComponent<ControllableObject>();
        playerRigidbody = GetComponent<Rigidbody>();
        CharacterControllerWrapper = GetComponent<CharacterControllerWrapper>();
        battleState = GetComponent<BattleStateMachine>();
        if (control != null) control.AddAntiInputScript(this);
        if (battleState != null)
            battleState.stateChanged += (bsm) => {
                if (anim != null)
                    anim.SetBool("walking", false);
            };

    }


    void FixedUpdate() {
        if (battleState != null)
            if (!battleState.CanMove()) {
                if (anim != null)
                    anim.SetBool("walking", false);
                return;
            }


        // Move the player around the scene.
        Move(H, V);

        // Turn the player to face the mouse cursor.
        Turning(H, V);

        // Animate the player.
        Animating(H, V);
    }

    void Move(float h, float v) {
        movement.Set(h, 0, v);
        movement = movement.normalized * Speed;
        CharacterControllerWrapper.SetSpeed(movement);

    }

    void Turning(float h, float v) {
        movement.x = h;
        movement.z = v;
        if (movement.sqrMagnitude != 0) {
            Quaternion newRotation = Quaternion.LookRotation(movement);
            playerRigidbody.rotation = newRotation;;
            transform.rotation = newRotation;
        }
    }

    void Animating(float h, float v) {
        // Create a boolean that is true if either of the input axes is non-zero.
        bool walking = h != 0f || v != 0f;

        // Tell the animator whether or not the player is walking.
        if (anim != null)
            anim.SetBool("walking", walking);

    }
}
