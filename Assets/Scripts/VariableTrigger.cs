﻿using UnityEngine;
using System.Collections;

public class VariableTrigger : MonoBehaviour {

    public VariableChangeData variableChangeData;

     void OnTriggerEnter(Collider other) {
        variableChangeData.ChangeValue();
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
