﻿using UnityEngine;
using System.Collections;

public class DeckChangerTrigger : MonoBehaviour {

    public SkillInfoProvider.SkillId[] skillIds;
    public int drawPower = -1;
    public int drawPowerFirstTurn = -1;
    

    // Use this for initialization
    void Start () {
	    
	}

    void OnTriggerEnter(Collider other) {
        var deckManager = CentralResource.Instance.SkillInventoryDeckManagement;
        deckManager.ClearCurrentDeck();;
        deckManager.CanEditDeck = false;
        if(skillIds.Length == 0) {
            deckManager.AddStarterInvenAndDeck();
            deckManager.CanEditDeck = true;

        }
        for (int i = 0; i < skillIds.Length; i++) {
            deckManager.AddToCurrentDeck((int)skillIds[i]);
        }
        var skillDeck = CentralResource.Instance.Hero.GetComponent<SkillPerTurnDeck>();
        if(drawPower >= 0) {
            skillDeck.DrawAmount = drawPower;
            skillDeck.DrawAmountFirstTurn = drawPowerFirstTurn;
        } else {
            skillDeck.DefaultDrawAmount();
        }
        
        deckManager.AlertDeckChanged();
        
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
