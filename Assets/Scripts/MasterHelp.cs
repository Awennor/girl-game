﻿using UnityEngine;
using System.Collections;

public class MasterHelp : MonoBehaviour {

    bool masterVisible = true;

	// Use this for initialization
	void Awake () {
	    TutoHelpSingleton.Instance.MasterHelpOptionAvailable+= (b)=> {
            if(gameObject) enabled = b;
        };
        TutoHelpSingleton.Instance.MasterHelpOption += (b)=> {
            masterVisible = b;
        };
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetButtonDown("Help")) {
            TutoHelpSingleton.Instance.MasterHelpOption(!masterVisible);
        }
	}
}
