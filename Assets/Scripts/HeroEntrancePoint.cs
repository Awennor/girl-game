﻿using UnityEngine;
using System.Collections;

public class HeroEntrancePoint : MonoBehaviour {

    //public GameObject heroObj;
    [SerializeField]
    public int spawnPoint;

    // Use this for initialization
    void Start() {
        //Debug.Log("HERO ENTRANCE POINT 1");
        if (CentralResource.Instance.LevelChangeSystem.SpawnPoint == spawnPoint) {
            //Debug.Log("HERO ENTRANCE POINT 2");
            var heroObj = CentralResource.Instance.Hero;
            CentralResource.Instance.InputPlayerMovement.Disable(0.89f);
            heroObj.GetComponent<PlayerMovement>().StopRotations();
            heroObj.transform.position = this.transform.position;
            heroObj.transform.rotation = this.transform.rotation;
            
        }


    }

    // Update is called once per frame
    void Update() {

    }
}
