﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class FadeHolder : MonoBehaviour {

    public Image screenImage;
    
    //const Color initialColor = new Color(0,0,0);

    // Use this for initialization
    void Start() {
        CentralResource.Instance.FadeHolder = this;
    }

    // Update is called once per frame
    void Update() {

    }

    public Tween FadeOutAndFadeIn(float delay, float fadeOutTime, float holdFade, float fadeInTime, Color c) {
        screenImage.gameObject.SetActive(true);
        c.a = 0;
        screenImage.color = c;
        c.a = 1;
        var seq = DOTween.Sequence();
        if(delay >0 ) seq.AppendInterval(delay);
        seq.Append(screenImage.DOColor(c,fadeOutTime));
        if(holdFade >0 ) seq.AppendInterval(holdFade);
        seq.Append(screenImage.DOColor(screenImage.color,fadeInTime));
        seq.Append(DOVirtual.DelayedCall(0, DeactivateObject));
        return seq;
    }

    private void DeactivateObject() {
        screenImage.gameObject.SetActive(false);    
    }
}
