﻿using UnityEngine;
using System.Collections;
using System;

public class ProjectilDestroyEffect : MonoBehaviour {
    [SerializeField]
    string effectName;
    [SerializeField]
    int particleAmount = 30;
    [SerializeField]
    string soundName = "disappear1";

	// Use this for initialization
	void Start () {
	    GetComponent<ProjectilData>().Destroying += DestroyBefore;
	}

    private void DestroyBefore(ProjectilData obj) {
        //Debug.Log("DESTROY BEFORE CALLED!!");
        if(this == null) return;
        var position = transform.position;
        var viewportPoint = Camera.main.WorldToViewportPoint(position);
        if(viewportPoint.x > 0 && viewportPoint.x < 1 &&
            viewportPoint.y > 0 && viewportPoint.y < 1) {
            EffectSingleton.Instance.SpawnParticleHere(effectName, particleAmount, position);
            SoundSingleton.Instance.PlaySound(soundName);
        }
        

    }

    // Update is called once per frame
    void Update () {
	
	}
}
