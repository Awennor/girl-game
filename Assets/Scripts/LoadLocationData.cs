﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLocationData : MonoBehaviour {

    public bool ForceTutorial;
    public bool forceAreaForDebug;
    public Object debugForceSceneName;
    public int debugForceSpawnNumber;

    // Use this for initialization
    void Start() {
        var sps = CentralResource.Instance.SaveSlots.CurrentSlot.spawnPositionSave;
        if (!sps.sceneName.Equals("") && !ForceTutorial) {

        } else {
            sps.sceneName = "temple";
            sps.spawnNumber = 0;
            CentralResource.Instance.LevelChangeSystem.SpawnPoint = 0;
            //Application.LoadLevelAdditive("temple");
            //Application.LoadLevelAdditive("scenetest2");

        }

        if(forceAreaForDebug) {
            sps.sceneName = debugForceSceneName.name;
            sps.spawnNumber = debugForceSpawnNumber;
            CentralResource.Instance.LevelChangeSystem.SpawnPoint = 0;
        }

        CentralResource.Instance.LevelChangeSystem.SpawnPoint = sps.spawnNumber;
        //Application.LoadLevelAdditive(sps.sceneName);

        var scene = SceneManager.GetSceneByName(sps.sceneName);
        if (scene != null && scene.isLoaded) {
            SceneManager.UnloadSceneAsync(sps.sceneName);
        }
            
        SceneManager.LoadScene(sps.sceneName, LoadSceneMode.Additive);
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sps.sceneName));

    }

    // Update is called once per frame
    void Update() {

    }
}
