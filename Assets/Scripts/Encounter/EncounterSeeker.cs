﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class EncounterSeeker : MonoBehaviour {

    public DetectorCallback detectorCallback;
    public DetectorCallback detectorCallbackProj;
    public FollowTargetWithSpeed targetWithSpeed;
    public float maxDistanceFromParent = 110;
    Vector3 initialPosition;
    private GameObject registeredTarget;
    private Tween delayedCall;
    private bool respawning;
    public EncounterTrigger encounterTrigger;
    private bool battleStarted;
    private bool JustStarted = true;

    // Use this for initialization
    void Start() {
        detectorCallback.detected += heroDetected;
        initialPosition = transform.position;
        CentralResource.Instance.BattleFlowSys.OnBattleEnd += BattleEnd;
        encounterTrigger.encounterStarted += EncounterStarted;
        detectorCallbackProj.detected += projInteract;
        DOVirtual.DelayedCall(2f, JustStartedPeriodDone);
    }

    private void JustStartedPeriodDone() {
        JustStarted = false;
    }

    private void EncounterStarted() {
        battleStarted = true;
    }

    private void projInteract(DetectorCallback arg1, GameObject arg2) {
        if(JustStarted) return;
        if(arg2.tag.Equals("examineproj")) {
            encounterTrigger.EncounterStart();
        }
        //Debug.Log("PROJ INTERACT!!!" + arg2.name);
        
        
    }

    private void BattleEnd() {
        if (this != null) {
            gameObject.SetActive(false);
            if (!battleStarted) {
                respawning = true;
                ClearTarget();
                delayedCall = DOVirtual.DelayedCall(5f 
                    //+ RandomSpecial.randomFloat(5f)
                    ,respawn, false);
            }

        }

    }

    private void respawn() {
        if(this == null) return;
        if (gameObject != null) {
            gameObject.SetActive(true);
            transform.position = initialPosition;
            EffectSingleton.Instance.ShowEffectHere("darkappear", transform);
            SoundSingleton.Instance.PlaySound("darkdisappear");
        }


    }

    private void heroDetected(DetectorCallback arg1, GameObject arg2) {
        registeredTarget = arg2;
        EffectSingleton.Instance.ShowEffectHere("redwarning", transform, new Vector3(3, 12));
        SoundSingleton.Instance.PlaySound("found");
        DOVirtual.DelayedCall(0.4f, FollowTarget, false);
    }

    private void FollowTarget() {
        GameObject arg2 = registeredTarget;
        targetWithSpeed.Target = arg2;
        detectorCallback.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        if (transform.localPosition.sqrMagnitude > maxDistanceFromParent * maxDistanceFromParent) {
            ClearTarget();
            DOVirtual.DelayedCall(0f, ReturnToInitialPositionTry, false);
        }

    }

    private void ClearTarget() {
        targetWithSpeed.Target = null;
        detectorCallback.gameObject.SetActive(true);
    }

    private void ReturnToInitialPositionTry() {
        if (targetWithSpeed.Target == null) {
            SoundSingleton.Instance.PlaySound("darkdisappear");
            EffectSingleton.Instance.ShowEffectHere("darkappear", transform);
            transform.position = initialPosition;
            ClearTarget();
        }
    }
}
