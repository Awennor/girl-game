﻿using UnityEngine;
using System.Collections;

public class EncounterSingleton {

    static EncounterSingleton instance;
    EncounterManager encounterManager;

    public void RegisterGroup(EncounterGroup encounterGroup) {
        encounterGroup.encounterStartCallback += encounterManager.EncounterStart;
    }

    public void EndAnyEncounter() {
        
        encounterManager.AbortEncounterTry();
        
    }

    public static EncounterSingleton Instance
    {
        get
        {
            if (instance == null) instance = new EncounterSingleton();
            return instance;
        }
        set
        {
            instance = value;
        }
    }

    public EncounterManager EncounterManager
    {
        get
        {
            return encounterManager;
        }

        set
        {
            encounterManager = value;
        }
    }
}
