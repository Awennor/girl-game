﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public enum EnemyIdEncounterType {
    SIMPLE, SPECIAL, BOSS
}


public class EncounterSingle : MonoBehaviour {

    [SerializeField]
    FixedEnemyInEncounter[] fixedEncounters;

    [SerializeField]
    private int[] enemiesRandom;

    [SerializeField]
    bool hideOnEnd = true;

    [SerializeField]
    GameObject heroPos;

    [SerializeField]
    bool barrier = true;

    [SerializeField]
    Transform relevantPoints;
    Dictionary<string, Transform> relevantPointDictionary;

    [SerializeField]
    VariableChangeData[] variableChangeData;

    [SerializeField]
    List<EncounterWave> normalWaves;

    List<GameObject> spawnPoints = new List<GameObject>();

    public int[] EnemiesRandom {
        get {
            return enemiesRandom;
        }

        set {
            enemiesRandom = value;
        }
    }

    public GameObject HeroPos {
        get {
            return heroPos;
        }

        set {
            heroPos = value;
        }
    }

    public List<GameObject> SpawnPoints {
        get {
            return spawnPoints;
        }

        set {
            spawnPoints = value;
        }
    }

    public FixedEnemyInEncounter[] FixedEncounters {
        get {
            return fixedEncounters;
        }

        set {
            fixedEncounters = value;
        }
    }

    public bool HideOnEnd {
        get {
            return hideOnEnd;
        }

        set {
            hideOnEnd = value;
        }
    }

    public bool Barrier {
        get {
            return barrier;
        }

        set {
            barrier = value;
        }
    }

    public Dictionary<string, Transform> RelevantPointDictionary {
        get {
            return relevantPointDictionary;
        }

        set {
            relevantPointDictionary = value;
        }
    }

    public VariableChangeData[] VariableChangeData {
        get {
            return variableChangeData;
        }

        set {
            variableChangeData = value;
        }
    }

    public List<EncounterWave> NormalWaves {
        get {
            return normalWaves;
        }

        set {
            normalWaves = value;
        }
    }

    public Action<EncounterSingle> StartBattleCallback;

    void Awake() {
        int cC = transform.childCount;

        for (int i = 0; i < cC; i++) {
            var child = transform.GetChild(i).gameObject;
            if (child.name.Contains("spawn") && child.activeSelf) {
                SpawnPoints.Add(child);
            }
        }
        if (relevantPoints != null) {
            RelevantPointDictionary = new Dictionary<string, Transform>();
            int c = relevantPoints.childCount;
            for (int i = 0; i < c; i++) {
                var child = relevantPoints.GetChild(i);
                RelevantPointDictionary.Add(child.name, child);
            }
        }
    }

    [System.Serializable]
    public class FixedEnemyInEncounter {

        [SerializeField]
        EnemyIdEncounterType typeEncounter;
        [SerializeField]
        int enemyWhich;
        [SerializeField]
        GameObject spawnPoint;






        public EnemyIdEncounterType TypeEncounter {
            get {
                return typeEncounter;
            }

            set {
                typeEncounter = value;
            }
        }

        public int EnemyWhich {
            get {
                return enemyWhich;
            }

            set {
                enemyWhich = value;
            }
        }

        public GameObject SpawnPoint {
            get {
                return spawnPoint;
            }

            set {
                spawnPoint = value;
            }
        }


    }

    // Use this for initialization
    void Start() {
        //BattleStart();
    }

    public void BattleStart() {
        if (StartBattleCallback != null) {
            StartBattleCallback(this);
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
