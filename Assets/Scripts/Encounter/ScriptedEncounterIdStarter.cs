﻿using System;
using UnityEngine;

internal class ScriptedEncounterIdStarter {
    string scriptedEncounterId;

    public event Action<string> EncounterStartId;

    public ScriptedEncounterIdStarter() {
    }

    public void EncounterStart(string id) {
        scriptedEncounterId = id;
        if(EncounterStartId != null) {
            EncounterStartId(id);
        } else {
            //Debug.Log("CALL BACKNULL... WHY");
        }
            
    }

    public string ScriptedEncounterId {
        get {
            return scriptedEncounterId;
        }

        set {
            scriptedEncounterId = value;
        }
    }



    internal void SuccessStart() {
        //EncounterStartId = null;
    }
}