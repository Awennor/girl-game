﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public class EncounterNameTrigger : MonoBehaviour {

    [SerializeField]
    private string encounterName;
    public UnityEvent battleComplete;
    bool skipOnce;
    [SerializeField]
    bool skipOnceActive = true;
    public bool byCollision;
    public UnityEvent backAttackSuccess;
    public UnityEvent OnEncounterStart;
    public bool immuneToEncounterStateIgnore = true;

    public string EncounterName {
        get {
            return encounterName;
        }

        set {
            encounterName = value;
        }
    }


    void Awake() {


    }

    void OnCollisionEnter(Collision col) {
        //if(!col.collider.gameObject.tag.Equals("Player")) return;
        //Debug.Log("COLLISION ENTER");
        if (byCollision)
            EncounterStart();

    }

    void OnTriggerEnter(Collider other) {
        //if (!other.gameObject.tag.Equals("Player")) return;
        //Debug.Log("COLLISION ENTER2");
        if (byCollision) {
            EncounterStartByCollision();
        }
    }

    public void AttemptBackattack() {
        if (!EncounterSingleton.Instance.EncounterManager.EncounterStarting) {
            backAttackSuccess.Invoke();
            EncounterSingleton.Instance.EncounterManager.BackAttack = true;
            EncounterStart();

        }

    }

    public void EncounterStartByCollision() {
        if (!CentralResource.Instance.OverworldRandomEncounter.ImmuneToEncounterState || immuneToEncounterStateIgnore) {
            EncounterStart();
            Debug.Log("Collision start");
        }
    }

    public void EncounterStart() {
        if (skipOnce) {
            skipOnce = false;
            if (skipOnceActive)
                return;
        }
        OnEncounterStart.Invoke();
        //Time.timeScale = 0.05f;
        Debug.Log("regular start");
        //if(CentralResource.Instance.OverworldRandomEncounter.)
        SoundSingleton.Instance.PlaySoundOnPause("darktouch");
        CentralResource.Instance.ScriptedEncounterIdStarter.EncounterStart(EncounterName);
        CentralResource.Instance.BattleFlowSys.OnBattleEndUIClose_OnlyOnce += BattleFlowSys_OnBattleEnd;
    }

    private void BattleFlowSys_OnBattleEnd() {
        if (CentralResource.Instance.BattleFlowSys.LatestBattleWon) {
            Debug.Log("Immunity active");
            CentralResource.Instance.OverworldRandomEncounter.ImmunityActivate();
            battleComplete.Invoke();
            skipOnce = true;
        }
    }



    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
