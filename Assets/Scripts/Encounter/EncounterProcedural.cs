﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterProcedural : MonoBehaviour {

    public EncounterSingle encounterSingle;
    public EncounterPosition[] spawnPositions;
    public List<EncounterWave> encounterWaves = new List<EncounterWave>();
    public List<int> numberOfWaveProbDistribution;
    public List<int> enemyDifficulty;
    public List<int> enemyProbPriority;
    public List<int> aux;


    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        //encounterSingle.
    }

    [ExposeMethodInEditor]
    public void GenerateTest5() {
        Generate(5);
    }

    public void Generate(int difficulty) {
        int nWaves = RandomSpecial.PriorityArray(numberOfWaveProbDistribution);
        encounterSingle.NormalWaves.Clear();
        for (int w = 0; w < nWaves; w++) {
            var wave0 = GetWave(w);
            wave0.Fixedenemies.Clear();

            int waveDifficulty = 0;
            if (w == nWaves - 1) {
                if (nWaves == 1) {
                    waveDifficulty = difficulty;
                } else {
                    waveDifficulty = difficulty - 2;
                }
            } else {
                waveDifficulty = (int)(difficulty - nWaves * RandomSpecial.Range(1.6f, 2.4f));
            }
            if (waveDifficulty <= 0) {
                waveDifficulty = 1;
            }
            encounterSingle.NormalWaves.Add(wave0);
            FillWave(waveDifficulty, wave0);


        }

    }

    private void FillWave(int waveDifficulty, EncounterWave wave) {
        int spawnPos = 0;
        int buffer = waveDifficulty;
        wave.Fixedenemies.Clear();

        while (buffer > 0) {
            aux.Clear();
            for (int i = 0; i < enemyDifficulty.Count; i++) {
                if (enemyDifficulty[i] <= buffer) {
                    aux.Add(enemyProbPriority[i]);
                } else {
                    aux.Add(0);
                }
            }

            int enemyWhich = RandomSpecial.PriorityArray(aux);
            int enemyD = enemyDifficulty[enemyWhich];
            buffer -= enemyD;

            wave.Fixedenemies.Add(new EncounterWave.FixedEnemyEncounter(enemyWhich, spawnPositions[spawnPos]));

            spawnPos++;
            if(spawnPositions.Length <= spawnPos) {
                return;
            }
        }

    }



    private EncounterWave GetWave(int v) {
        while (encounterWaves.Count <= v) {
            var go = new GameObject();
            go.transform.SetParent(this.transform);
            var ew = go.AddComponent<EncounterWave>();
            encounterWaves.Add(ew);
            go.name = "wave" + v;
        }
        return encounterWaves[v];
    }


}
