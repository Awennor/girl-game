﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EncounterWave : MonoBehaviour {

    [SerializeField]
    List<FixedEnemyEncounter> fixedenemies = new List<FixedEnemyEncounter>();

    public List<FixedEnemyEncounter> Fixedenemies {
        get {
            return fixedenemies;
        }

        set {
            fixedenemies = value;
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }



    [Serializable]
    public struct FixedEnemyEncounter {
        [SerializeField]
        private int enemyWhich;
        [SerializeField]
        private EncounterPosition encounterPosition;

        public FixedEnemyEncounter(int enemyWhich, EncounterPosition encounterPosition) {
            this.enemyWhich = enemyWhich;
            this.encounterPosition = encounterPosition;
        }

        public int EnemyWhich {
            get {
                return enemyWhich;
            }

        }

        public EncounterPosition EncounterPosition {
            get {
                return encounterPosition;
            }

        }
    }

}
