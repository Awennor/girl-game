﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterNameStarter : MonoBehaviour {

    public EncounterSingle encounter;

    // Use this for initialization
    void Start() {
        string scriptedEncounterId = CentralResource.Instance.ScriptedEncounterIdStarter.ScriptedEncounterId;
        CentralResource.Instance.ScriptedEncounterIdStarter.EncounterStartId += TryStart;
        //TryStart(scriptedEncounterId);
    }

    private void TryStart(string scriptedEncounterId) {
        if (this != null) {
            if (scriptedEncounterId != null && scriptedEncounterId.Equals(gameObject.name)) {
                CentralResource.Instance.ScriptedEncounterIdStarter.SuccessStart();
                encounter.BattleStart();
            }
        }

    }

    // Update is called once per frame
    void Update() {

    }
}
