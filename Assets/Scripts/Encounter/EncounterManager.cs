﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;
using UnityEngine.Events;

public class EncounterManager : MonoBehaviour {

    [SerializeField]
    private GameObject enemyParent;
    private bool inEncounter;
    private bool encounterStarting;
    public GameObject barrier;
    private EncounterGroup lastGroup;
    private EncounterSingle lastEncounter;
    private int nEnemies;
    public GameObject hero;

    private Vector3 savedHeroPosition;
    bool backAttack = false;
    public UnityEventSequence OnEncounterStart_BackAttack;
    public UnityEventSequence OnEncounterStart_NormalEncounter;
    public UnityEventSequence OnEncounterStart;
    public UnityEvent OnSummonEnemyRequest;
    public UnityEvent OnHeroBattleStartRequest;
    public UnityEvent OnEncounterEnd;
    public UnityEvent OnEnemiesAllDead;
    private bool enemySummoningState;

    public EncounterSingle LastEncounter {
        get {
            return lastEncounter;
        }

        set {
            lastEncounter = value;
        }
    }



    public bool EncounterStarting {
        get {
            return encounterStarting;
        }

        set {
            encounterStarting = value;
        }
    }

    public bool BackAttack {
        get {
            return backAttack;
        }

        set {
            backAttack = value;
        }
    }

    public EncounterGroup LastGroup {
        get {
            return lastGroup;
        }

        set {
            lastGroup = value;
        }
    }

    public GameObject EnemyParent {
        get {
            return enemyParent;
        }

        set {
            enemyParent = value;
        }
    }

    internal void AbortEncounterTry() {
        if (inEncounter) {
            CentralResource.Instance.BattleFlowSys.AbortedBattleNotVictory = true;
            var transform1 = EnemyParent.transform;
            DestroyChildrenOfTransform(transform1);
        }
    }

    private static void DestroyChildrenOfTransform(Transform transform1) {
        int childCount = transform1.childCount;
        for (int i = 0; i < childCount; i++) {
            Destroy(transform1.GetChild(i).gameObject);
        }
    }

    void Awake() {
        EncounterSingleton.Instance.EncounterManager = this;
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

        TryUpdateEnemyCount();
        if (inEncounter && EnemyParent.transform.childCount == 0 && !enemySummoningState) {
            OnEnemiesAllDead.Invoke();

        }
        if (inEncounter) {
            EncounterSingle encounterSingle = LastEncounter;
            if (encounterSingle.HeroPos != null) {
                float yDis = encounterSingle.HeroPos.transform.position.y - hero.transform.position.y;
                if (yDis > 20) {
                    HeroPos();
                }
                //hero.transform.position = encounterSingle.HeroPos.transform.position;
                //EffectSingleton.Instance.ShowEffectHere("darkappear", encounterSingle.HeroPos.transform);
            }

        }
    }

    public void SummoningEnemies_External() {
        enemySummoningState = true;
    }

    public void SummoningEnemiesOver_External() {
        enemySummoningState = false;
    }

    public void NoMoreEnemiesToSummon_External() {
        inEncounter = false;
        encounterEnd();
    }


    internal void TryUpdateEnemyCount() {
        if (nEnemies != EnemyParent.transform.childCount) {
            nEnemies = EnemyParent.transform.childCount;
            CentralResource.Instance.BattleFlowSys.NEnemies = nEnemies;
        }
    }

    private void encounterEnd() {
        CentralResource.Instance.BattleFlowSys.BattleEnded_BeforeResultUI();
        if (LastGroup != null) {
            LastGroup.gameObject.SetActive(true);
            LastGroup = null;
            DestroyChildrenOfTransform(CentralResource.Instance.ProjectilHolder.ProjectilParent);
            if (!CentralResource.Instance.BattleFlowSys.AbortedBattleNotVictory) {
                var variables = LastEncounter.VariableChangeData;
                if (variables != null) {
                    for (int i = 0; i < variables.Length; i++) {
                        variables[i].ChangeValue();
                    }
                }


            }
            if (LastEncounter.HideOnEnd)
                LastEncounter.gameObject.SetActive(false);
        }
        OnEncounterEnd.Invoke();
        barrier.SetActive(false);
    }

    internal void EncounterStart(EncounterGroup group, EncounterSingle encounterSingle
        ) {
        if (!encounterStarting && !inEncounter) {
            this.LastEncounter = encounterSingle;
            this.LastGroup = group;
            encounterStarting = true;

            LastGroup.gameObject.SetActive(false);
            if (backAttack)
                OnEncounterStart_BackAttack.Invoke();
            else
                OnEncounterStart_NormalEncounter.Invoke();

            OnEncounterStart.Invoke();

            if (encounterSingle.HeroPos != null) {
                //EffectSingleton.Instance.ShowEffectHere("darkappear", hero.transform);
                CentralResource.Instance.InputPlayerMovement.Disable(0.16f);
                //DOVirtual.DelayedCall(0.65f, BattlePrestart);
                //DOVirtual.DelayedCall(0.15f, HeroPos);
            } else {
                BattlePrestart();
            }
        }



        //BattleStart();
    }

    public void ScreenTransitionFadedOut_External() {
        HeroPos();
    }

    public void ScreenTransitionOver_External() {
        BattlePrestart();
    }

    private void HeroPos() {
        EncounterSingle encounterSingle = LastEncounter;
        if (encounterSingle.HeroPos != null) {
            savedHeroPosition = hero.transform.position;
            hero.transform.position = encounterSingle.HeroPos.transform.position;
            EffectSingleton.Instance.ShowEffectHere("darkappear", encounterSingle.HeroPos.transform);
        }
    }

    private void BattlePrestart() {

        //Time.timeScale = 0.05f;

        EncounterGroup group = LastGroup;
        EncounterSingle encounterSingle = LastEncounter;

        OnSummonEnemyRequest.Invoke();

        //SummonEnemies(group, encounterSingle);
        //DOVirtual.DelayedCall(0.1f, BackAttackDamageStep);
        //DOVirtual.DelayedCall(0.4f, SignalizeBattleStart);
    }

    public void EnemySummonFinished_External() {
        if (encounterStarting) {
            BackAttackDamageStep();
            HeroBattleStartAnimationStep();
        } else {
            SummoningEnemiesOver_External();
        }

        //DOVirtual.DelayedCall(0.3f, SignalizeBattleStart);
    }

    private void HeroBattleStartAnimationStep() {
        OnHeroBattleStartRequest.Invoke();
    }

    public void HeroBattleStartAnimationFinished_External() {
        DOVirtual.DelayedCall(0.3f, SignalizeBattleStart);
    }

    private void BackAttackDamageStep() {
        if (backAttack) {
            EnemyParent.ActionOnAllChildrenComponent<HPSystem>(BackAttackDamageUnit);
            backAttack = false;
        }

        //var hpSystems = enemyParent.transform.GetComponentsInChildren<HPSystem>();
        //for (int i = 0; i < hpSystems.Length; i++) {

        //}
    }

    private void BackAttackDamageUnit(HPSystem obj) {
        obj.DealDamage(obj.GetMaxHP() * 0.33f);

    }

    public void CloseBattleResultUIEvent() {
        CentralResource.Instance.BattleFlowSys.BattleUIClose();
        TryReturnHero();
    }

    public void TryReturnHero() {
        Debug.Log("TryReturnHero HERO");
        hero.transform.position = savedHeroPosition;
    }

    private void SignalizeBattleStart() {
        EncounterGroup group = LastGroup;
        EncounterSingle encounterSingle = LastEncounter;

        barrier.SetActive(encounterSingle.Barrier);
        barrier.transform.position = encounterSingle.transform.position + new Vector3(-40, 0, -40);
        Time.timeScale = 1f;
        CentralResource.Instance.BattleFlowSys.BattleStarted();
        inEncounter = true;
        encounterStarting = false;
    }



}
