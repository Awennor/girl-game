﻿using UnityEngine;
using System.Collections;
using System;

public class EncounterGroup : MonoBehaviour {

    [SerializeField]
    GameObject[] enemies;
    [SerializeField]
    GameObject[] enemiesSpecial;
    [SerializeField]
    GameObject[] enemiesBoss;


    public GameObject barrier;
    public GameObject enemyHolder;
    public Action<EncounterGroup, EncounterSingle> encounterStartCallback;

    public GameObject[] Enemies
    {
        get
        {
            return enemies;
        }

        set
        {
            enemies = value;
        }
    }

    public void Awake() {


        int cC = transform.childCount;
        for (int i = 0; i < cC; i++)
        {
            var child = transform.GetChild(i).gameObject;
            var eS = child.GetComponent<EncounterSingle>();
            if (eS != null) {
                eS.StartBattleCallback += encounterStart;
            }
        }
    }

    private void encounterStart(EncounterSingle obj)
    {
        if (encounterStartCallback != null)
        {
            encounterStartCallback(this, obj);
        }
        
    }

    public void callBarrier(Transform t)
    {
        
    }

    // Use this for initialization
    void Start () {
       EncounterSingleton.Instance.RegisterGroup(this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
