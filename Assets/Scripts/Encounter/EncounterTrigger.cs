﻿using UnityEngine;
using System.Collections;
using System;

public class EncounterTrigger : MonoBehaviour {
    private EncounterSingle encounter;
    public Action encounterStarted;

    void Awake() {
        encounter = GetComponentInParent<EncounterSingle>();

    }

    void OnCollisionEnter(Collision col) {
        if(!col.collider.gameObject.tag.Equals("Player")) return;
        //Debug.Log("COLLISION ENTER");
        EncounterStart();
        
    }

    void OnTriggerEnter(Collider other) {
        if (!other.gameObject.tag.Equals("Player")) return;
        //Debug.Log("COLLISION ENTER2");
        EncounterStart();
    }

    public void EncounterStart() {
        SoundSingleton.Instance.PlaySoundOnPause("darktouch");
        if(encounterStarted != null) {
            encounterStarted();
        }
        encounter.BattleStart();
    }



    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
