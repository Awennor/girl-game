﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(EncounterWave.FixedEnemyEncounter))]
public class FixedEnemyEncounterDrawer : PropertyDrawer {

    List<string> aux = new List<string>();

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        //EditorGUI.PropertyField(position, property);
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.

        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        float rect1Width = position.width/2;
        var unitRect = new Rect(position.x, position.y, rect1Width, position.height);
        var unitRect2 = new Rect(position.x + rect1Width + 5, position.y, position.width - (rect1Width + 5), position.height);

        // Draw label
        EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("encounterPosition"), GUIContent.none);
        
        EncounterWave ew = property.serializedObject.targetObject as EncounterWave;
        var eG = ew.gameObject.GetComponentInParent<EncounterGroup>();
        var enemies = eG.Enemies;
        aux.Clear();
        foreach(var e in enemies) {
            aux.Add(e.name);
        }
        property.FindPropertyRelative("enemyWhich").intValue = EditorGUI.Popup(unitRect2, property.FindPropertyRelative("enemyWhich").intValue, aux.ToArray());
        //Debug.Log();

        EditorGUI.EndProperty();
    }


}
