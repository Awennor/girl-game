﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EncounterWaveGizmoDrawing  {

    [DrawGizmo(GizmoType.Pickable | GizmoType.Active | GizmoType.InSelectionHierarchy)]
    public static void GizmoMeshDrawing(EncounterWave ew, GizmoType gizmoType) {
        foreach (var fe in ew.Fixedenemies) {
            Gizmos.color = new Color(1, 1, 0, 0.75F);
            var enemyPrefab = ew.GetComponentInParent<EncounterGroup>().Enemies[fe.EnemyWhich];
            if (fe.EncounterPosition != null) {
                var skinnedMeshRenderer = enemyPrefab.GetComponentInChildren<SkinnedMeshRenderer>();
                var lossyScale = skinnedMeshRenderer.transform.lossyScale;
                //Debug.Log(lossyScale);
                var sharedMesh = skinnedMeshRenderer.sharedMesh;
                
                Gizmos.DrawMesh(
                    sharedMesh, fe.EncounterPosition.transform.position, fe.EncounterPosition.transform.rotation, skinnedMeshRenderer.transform.localScale);
                //Debug.Log("DRAW MESH");
            }


        }
        // Display the explosion radius when selected

    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
