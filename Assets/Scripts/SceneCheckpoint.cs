﻿using UnityEngine;
using System.Collections;

public class SceneCheckpoint : MonoBehaviour {

    public string sceneName;
    public bool ActivateOnTrigger = true;
    [SerializeField]
    private int spawnPoint;

    void OnTriggerEnter(Collider col) {
        if(ActivateOnTrigger)
            SaveLocation();
    }

    public void SaveLocation() {
        
        if (!CentralResource.Instance.BattleFlowSys.InBattle) {
            CentralResource.Instance.SaveSlots.CurrentSlot.spawnPositionSave.SaveData(sceneName, spawnPoint);
        }
            
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
