﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using System;

public class SuperArmor : MonoBehaviour {
    private Invoker invoker;
    int activeLevel;

    public bool SuperArmorActive { get {
            return activeLevel > 0;
        } }

   

    // Use this for initialization
    void Start () {
	    invoker = GetComponent<Invoker>();
	}

    public void Activate(float timeToTurnOff) {
        activeLevel = 5;
        invoker.AddActionUnique( deactivate, timeToTurnOff);
    }

    private void deactivate() {
        activeLevel = -1;
    }

    

    // Update is called once per frame
    void Update () {
	
	}
}
