﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScreenPauseEffect : MonoBehaviour {

    public List<MonoBehaviour> behaviorActiveOnPause = new List<MonoBehaviour>();
    private float timeScale;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
        if(timeScale != Time.timeScale) {
            for (int i = 0; i < behaviorActiveOnPause.Count; i++) {
                behaviorActiveOnPause[i].enabled =  (Time.timeScale == 0);
            }
            if((Time.timeScale == 0)) {
                AudioListener.pause = true;
            }
            if((timeScale == 0)) {
                AudioListener.pause = false;
                //AudioListener.
            }
        }
        timeScale = Time.timeScale;
	}
}

