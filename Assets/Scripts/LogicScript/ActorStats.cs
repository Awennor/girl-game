﻿using com.spacepuppy.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ActorStats {

    public Action<ActorStats> battleBuffsChange;



    internal void UpdateTime(float f) {

        for (int i = 0; i < BattleTempBufs.Count; i++) {

            if (timeLeftOnBuff[i] > 0) {
                Debug.Log("ACTIVE BUFF " + timeLeftOnBuff[i]);
                timeLeftOnBuff[i] -= f;

                if (timeLeftOnBuff[i] <= 0) {
                    RemoveBuffAt(i);

                    i--;
                }
            }
        }
    }

    private void RemoveBuffAt(int i) {
        BattleTempBufs.RemoveAt(i);
        timeLeftOnBuff.RemoveAt(i);
        turnLeftOnBuff.RemoveAt(i);
        if (battleBuffsChange != null)
            battleBuffsChange(this);
        UpdateResultStats();
    }

    internal void AddIndependentBuff(Buff buff) {
       independentBuffs.Add(buff); 
    }

    List<Buff> battleTempBufs = new List<Buff>();
    List<Buff> independentBuffs = new List<Buff>();
    List<float> timeLeftOnBuff = new List<float>();
    List<float> turnLeftOnBuff = new List<float>();

    Buff resultStats = new Buff();

    public List<Buff> BattleTempBufs {
        get {
            return battleTempBufs;
        }

        set {
            battleTempBufs = value;
        }
    }

    internal void TurnStart() {
        for (int i = 0; i < BattleTempBufs.Count; i++) {

            if (turnLeftOnBuff[i] > 0) {
                turnLeftOnBuff[i] -= 1;


                if (turnLeftOnBuff[i] <= 0) {
                    RemoveBuffAt(i);

                    i--;
                }
            }
        }
    }

    internal void NewBattleBuff(Buff buffOfSkill) {
        timeLeftOnBuff.Add(buffOfSkill.TimeDuration);
        turnLeftOnBuff.Add(buffOfSkill.TurnDuration);
        BattleTempBufs.Add(buffOfSkill);
        if (battleBuffsChange != null)
            battleBuffsChange(this);
        UpdateResultStats();
    }


    public ActorStats() {
        UpdateResultStats();
        CentralResource.Instance.BattleFlowSys.OnBattleEnd += BattleEnd;
    }

    private void BattleEnd() {
        while (battleTempBufs.Count != 0) {
            RemoveBuffAt(0);
        }
    }

    private void UpdateResultStats() {
        resultStats.Reset();
        Buff battleBuff = resultStats;

        var buffList = this.BattleTempBufs;

        for (int i = 0; i < buffList.Count; i++) {
            var buff = buffList[i];
            if (buff.Enabled) {
                battleBuff.AttackMultipliers.AEXCopyElementsThatAreBigger(buff.AttackMultipliers);
                if (buff.DefenseMultiplier > battleBuff.DefenseMultiplier) {
                    battleBuff.DefenseMultiplier = buff.DefenseMultiplier;
                }
            }
        }

        for (int i = 0; i < independentBuffs.Count; i++) {
            var buff = independentBuffs[i];
            if (buff.Enabled) {
                battleBuff.AttackMultipliers.AEXMultiply(buff.AttackMultipliers);
                battleBuff.DefenseMultiplier *= buff.DefenseMultiplier;
            }

        }
    }

    public float GetAttackMultiplier() {
        UpdateResultStats();
        return resultStats.AttackMultipliers[0];
    }

    internal float GetDefenseMultiplier() {
        UpdateResultStats();
        return resultStats.DefenseMultiplier;
    }


    [System.Serializable]
    public class Buff {
        [SerializeField]
        float[] attackMultipliers;
        [SerializeField]
        float defenseMultiplier;
        [SerializeField]
        float speedMultiplier;
        [SerializeField]
        float hpChangePerSecond;
        [SerializeField]
        int turnDuration;
        [SerializeField]
        float timeDuration;
        //[SerializeField]
        bool enabled = true;

        internal void Reset() {
            speedMultiplier = 1;
            defenseMultiplier = 1;
            attackMultipliers.AEXSetArray(1);
            hpChangePerSecond = 0;
            Enabled = true;
        }

        public Buff() {
            attackMultipliers = new float[(int)AttackTypes.NumberOfAttacks];

        }

        public int TurnDuration {
            get {
                return turnDuration;
            }

            set {
                turnDuration = value;
            }
        }

        public float TimeDuration {
            get { return timeDuration; }
            set { timeDuration = value; }
        }

        public float[] AttackMultipliers {
            get { return attackMultipliers; }
            set { attackMultipliers = value; }
        }

        public float DefenseMultiplier {
            get {
                return defenseMultiplier;
            }
            set {
                defenseMultiplier = value;
            }
        }

        public bool Enabled {
            get {
                return enabled;
            }

            set {
                enabled = value;
            }
        }
    }
}
