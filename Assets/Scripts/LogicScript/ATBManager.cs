﻿using Assets.ReusablePidroh.Scripts.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.LogicScript
{
    class ATBManager
    {
        RangedValue timeBar = new RangedValue();
        TimedRangeValue timeRangeValue;
        public Action ATBFull;

        public ATBManager() {
            timeRangeValue = new TimedRangeValue(timeBar);
            timeRangeValue.SpeedRatio = 25;
            timeBar.Max = 25*7;
            timeBar.MaxValueReached += (rv) => {
                if (ATBFull != null) {
                    ATBFull();
                }
            };


            
        }

        internal void Empty()
        {
            timeBar.Value = 0;
        }

        public RangedValue TimeBar
        {
            get
            {
                return timeBar;
            }

            set
            {
                timeBar = value;
            }
        }

        public bool Full { get { return timeBar.IsFull();} }

        internal void TryCallbackRefresh() {
            if(timeBar.IsFull()) {
                if (ATBFull != null) {
                    ATBFull();
                }
            }
        }

        internal void ToFull() {
            timeRangeValue.Value.Value += 9000;
        }

        internal void Pause(bool v) {
            timeRangeValue.Pause = v;
        }

        internal void ToRatio(float ratio) {
            timeRangeValue.Value.Value = timeRangeValue.Value.Max*ratio;
        }
    }
}
