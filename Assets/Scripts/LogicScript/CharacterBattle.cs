﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts.Logic;
using Assets.Scripts.LogicScript;

public class CharacterBattle {

    int id;

    //RangedValue hP = new RangedValue();
    ATBManager atb = new ATBManager();
    bool atbMenuOpen = true;
    
    //public RangedValue HP { get; set; } 

    public CharacterBattle() {
        
        //timeBar.DebugB = true;
    }

    /*public RangedValue HP
    {
        get
        {
            return hP;
        }

        set
        {
            hP = value;
        }
    }*/

    internal ATBManager Atb
    {
        get
        {
            return atb;
        }

        set
        {
            atb = value;
        }
    }

    public bool AtbMenuOpen {
        get {
            return atbMenuOpen;
        }

        set {
            atbMenuOpen = value;
        }
    }
}
