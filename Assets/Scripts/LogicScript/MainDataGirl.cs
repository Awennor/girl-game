﻿using UnityEngine;
using System.Collections;

public class MainDataGirl  {

    CharacterBattle mainCharaBattle = new CharacterBattle();

    private MainDataGirl() {
        
    }

    private static MainDataGirl instance;
    
    static public MainDataGirl Instance {
        get{
            if (instance == null) instance = new MainDataGirl(); return instance;
        }  }

    public CharacterBattle MainCharaBattle
    {
        get
        {
            return mainCharaBattle;
        }

        set
        {
            mainCharaBattle = value;
        }
    }
}
