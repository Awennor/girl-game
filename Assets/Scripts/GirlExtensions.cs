﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

static class GirlExtensions {
    public static SkillHelper GetSkillHelper(this MonoBehaviour mono) {
        return mono.gameObject.GetComponent<SkillHelper>();
    }
}

