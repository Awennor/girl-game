﻿using UnityEngine;
using System.Collections;

public class SensorProjProvider : MonoBehaviour {

    public GameObject invisibleProj;
    Vector3 scaleV = new Vector3();

    public GameObject GetProj(float scale) {
        scaleV.x = 0;
        scaleV.y = 0;
        //scaleV.z = 0;
        GameObject obj = (GameObject)Instantiate(invisibleProj, scaleV, Quaternion.identity);
        
        obj.transform.localPosition = scaleV;
        scaleV.x = scale;
        scaleV.y = scale;
        scaleV.z = scale;
        obj.transform.localScale = scaleV;
        return obj;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
