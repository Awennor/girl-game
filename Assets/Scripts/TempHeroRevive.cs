﻿using UnityEngine;
using System.Collections;
using System;

public class TempHeroRevive : MonoBehaviour {
    private HPSystem hp;

    // Use this for initialization
    void Start() {
        hp = GetComponent<HPSystem>();
        hp.DestroyOnHPZero = false;
        hp.HPZeroHappens += HPZeroHappened;
    }

    private void HPZeroHappened(GameObject obj) {
        //obj.transform.position = new Vector3(0, 2,0);
        
        //hp.FullHeal();
        CentralResource.Instance.BattleFlowSys.HeroDeath = true;
        EncounterSingleton.Instance.EndAnyEncounter();
        
    }

    // Update is called once per frame
    void Update() {

    }
}
