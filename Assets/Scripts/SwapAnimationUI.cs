﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class SwapAnimationUI : MonoBehaviour {

    public GameObject graphicsHolder;
    public Image cover1;
    public Image cover2;
    public Image transfer1;
    public Image transfer2;
    private Tween endCallTween;

    public void Anim(Vector3 pos1, Vector3 pos2) {
        if(endCallTween != null) {
            endCallTween.Kill();
            endCallTween = null;
        }

        cover2.gameObject.SetActive(false);
        transfer1.gameObject.SetActive(false);

        graphicsHolder.SetActive(true);
        cover1.DOKill();
        cover2.DOKill();
        transfer1.DOKill();
        transfer2.DOKill();
        cover1.transform.localScale = new Vector3(1,1,1);
        cover2.transform.localScale = new Vector3(1,1,1);
        cover1.transform.position = pos1;
        transfer1.transform.position = pos1;
        cover2.transform.position = pos2;
        transfer2.transform.position = pos2;

        float maxTimeToMove = 0.2f;
        float timeToMove = (pos2 - pos1).magnitude / 100; 
        //timeToMove = 0.5f;
        if(timeToMove > maxTimeToMove) timeToMove = maxTimeToMove;
        const float timeToScale = 0f;


        cover1.transform.DOScale(1f, timeToScale).SetDelay(timeToMove).SetUpdate(true);
        cover2.transform.DOScale(1f, timeToScale).SetDelay(timeToMove).SetUpdate(true);
        transfer1.transform.DOMove(pos2, timeToMove).SetUpdate(true);
        transfer2.transform.DOMove(pos1, timeToMove).SetUpdate(true);

        endCallTween = DOVirtual.DelayedCall(timeToScale+timeToMove, End);
    }

    private void End() {
        graphicsHolder.SetActive(false);
    }
}
