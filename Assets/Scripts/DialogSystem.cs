﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using System;

public class DialogSystem : MonoBehaviour {

    public Text message;
    public Text speaker;
    public GameObject speakerHolder;
    public GameObject messageHolder;
    public UnityEvent wannaClose;
    public UnityEvent OnCloseStart;
    private bool showing;
    public Color normalColor;
    public Color grayColor;
    public RectTransform messagePanel;
    Vector3[] screenCorners = new Vector3[4];
    bool startingUp = false;

    public Camera Camera { get; internal set; }

    public void ShowMessage(string m, string speakerString) {
        showing = true;
        if (speakerString.Length == 0) {
            speakerHolder.SetActive(false);
        } else {
            speakerHolder.transform.DOScaleX(1, 0.1f);
            speakerHolder.SetActive(true);
            speaker.text = speakerString;
        }

        MessageVisible();
        message.text = m;
    }

    public void ShowMessage(string m) {
        showing = true;
        speakerHolder.SetActive(false);
        MessageVisible();
        message.text = m;
    }

    private void MessageVisible() {
        startingUp = true;
        messageHolder.SetActive(true);
        messageHolder.transform.DOScaleX(0, 0);
        messageHolder.transform.DOScaleX(1, 0.1f).OnComplete(FinishStartUp);
    }

    private void FinishStartUp() {
        startingUp = false;
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        var screenRect = new Rect(0f, 0f, Screen.width, Screen.height);
        if (messagePanel.gameObject.activeSelf) {
            var lrsave = messagePanel.localScale;

            var localp = messagePanel.localPosition;
            localp.x = 0;
            messagePanel.localPosition = localp;
            //if(!screenRect.Contains(screenCorners[0])) {

            //}
            messagePanel.localScale = new Vector3(1,1,1);
            Canvas.ForceUpdateCanvases();
            messagePanel.GetWorldCorners(screenCorners);
            var worldCamera = message.canvas.worldCamera;
            if(worldCamera !=null)
                for (int i = 0; i < screenCorners.Length; i++) {

                    screenCorners[i] = worldCamera.WorldToScreenPoint(screenCorners[i]);
                }
            //messagePanel.GetWorldCorners
            if (screenCorners[0].x < 0) {
                var lp = messagePanel.localPosition;
                lp.x = -screenCorners[0].x + 30;
                messagePanel.localPosition = lp;
            }
            if (screenCorners[3].x > Screen.width) {
                var lp = messagePanel.localPosition;
                lp.x = -screenCorners[3].x+Screen.width;
                messagePanel.localPosition = lp;
            }
            //Debug.Log("SCREEN RECT " + screenRect);
            //for (int i = 0; i < screenCorners.Length; i++) {
            //  Debug.Log("IN SCREEN CORNER " + i + " " + screenCorners[i]);
            //}
            messagePanel.localScale = lrsave;
        }


        if (Input.GetButtonDown("Submit") && !startingUp) {
            if (showing) {
                showing = false;
                OnCloseStart.Invoke();

                speakerHolder.transform.DOScaleX(0, 0.1f);
                messageHolder.transform.DOScaleX(0, 0.1f);
                DOVirtual.DelayedCall(0.2f, FinishClosing);

            }
        }
    }

    private void FinishClosing() {
        wannaClose.Invoke();
    }

    internal void CenterOn(GameObject obj) {
        var lp = messagePanel.localPosition;
        lp.x = 0;
        messagePanel.localPosition = lp;
        if (obj == null) {
            Debug.Log(obj + "Dialog System center on NULL");
            transform.localPosition = new Vector3(0, 0, 0);
            return;
        }
        if (message.canvas.renderMode == RenderMode.ScreenSpaceCamera) {
            //var main = message.canvas.worldCamera;
            var main = Camera;
            var scrPos = main.WorldToScreenPoint(obj.transform.position);
            scrPos.z = 0;
            this.gameObject.transform.position = message.canvas.worldCamera.ScreenToWorldPoint(scrPos);
            //this.gameObject.transform.position = message.canvas.transform.TransformPoint(scrPos);
            //this.gameObject.transform.localPosition = scrPos;
            /*
            var position = obj.transform.position;
            //position.z = this.gameObject.transform.position.z;
            this.gameObject.transform.position = position;
            var localPosition = this.gameObject.transform.localPosition;
            localPosition.z = 0;
            this.gameObject.transform.localPosition = localPosition;
            */
        }
        if (message.canvas.renderMode == RenderMode.ScreenSpaceOverlay) {
            var main = Camera;
            var scrPos = main.WorldToScreenPoint(obj.transform.position);
            scrPos.z = 0;
            this.gameObject.transform.position = scrPos;

            /*for (int i = 0; i < screenCorners.Length; i++) {
                screenCorners[i] = main.WorldToScreenPoint(screenCorners[i]);
            }
            for (int i = 0; i < screenCorners.Length; i++) {
                Debug.Log("IN SCREEN CORNER T2 "+i+" "+screenCorners[i]);
            }*/
            /*
            if(!screenRect.Contains(screenCorners[0])) {
                var aux = this.gameObject.transform.position;
                screenCorners[0].x *= -1;
                aux.x += main.ScreenToWorldPoint(screenCorners[0]).x;
                this.gameObject.transform.position = aux;
            }  
            if(!screenRect.Contains(screenCorners[3])) {
                var aux = this.gameObject.transform.position;
                //aux.x += -screenCorners[3].x + screenRect.width;
                screenCorners[3].x *= -1;
                screenCorners[3].x += screenRect.width;
                aux.x += main.ScreenToWorldPoint(screenCorners[3]).x;
                this.gameObject.transform.position = aux;
            }*/

        }


    }

    internal void GrayColor() {
        message.color = grayColor;
    }
    internal void NormalColor() {
        message.color = normalColor;
    }
}
