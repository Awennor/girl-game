﻿using UnityEngine;
using System.Collections;
using Assets.ReusablePidroh.Scripts;
using DG.Tweening;
using System;

public class ActorSetupDefault : MonoBehaviour {

    public bool hero;
    public int maxHP = 10;
    public int maxMindHP = 0;
    private ClosestTarget closestTarget;
    private CollisionProj collisionProj;
    private HPSystem hpSystem;
    private bool createdComps = false;
    public bool DestroyOnHPZero = false;
    public bool Flying = false;
    public Renderer mainRenderer;
    public float attackMultiplier = 1;
    public MyGameObjectEvent OnSetupDone;
    public bool initializeOnAwake = true;

    void Awake() {
        if (initializeOnAwake) {
            Initialize();
        }

    }

    public void Initialize() {
        if(createdComps) return;
        ActorStats actorStats = new ActorStats();
        if (maxMindHP > 0) {
            var mindSystem = gameObject.AddComponent<MindHPSystem>();
            mindSystem.SetMaxHPFullHeal(maxMindHP);
            mindSystem.zeroMind += () => {
                EffectSingleton.Instance.ShowEffectHere("braincontrol", transform);
            };
        }
        if (attackMultiplier != 1) {

            ActorStats.Buff buff = new ActorStats.Buff();
            buff.AttackMultipliers[0] = attackMultiplier - 1;
            actorStats.NewBattleBuff(buff);
        }

        var characterControllerWrapper = gameObject.AddComponent<CharacterControllerWrapper>();
        characterControllerWrapper.GravityAffect = !Flying;
        hpSystem = gameObject.AddComponent<HPSystem>();
        hpSystem.DestroyOnHPZero = DestroyOnHPZero;
        collisionProj = gameObject.AddComponent<CollisionProj>();
        collisionProj.ActorStats = actorStats;
        if (hero) {
            collisionProj.DuringDamageProtection = true;
            collisionProj.HurtSoundDefault = "hurthero";
        }
        closestTarget = gameObject.AddComponent<ClosestTarget>();
        gameObject.AddComponent<ControllableObject>();
        var update = gameObject.AddComponent<UpdateSupport>();
        update.Updates += actorStats.UpdateTime;
        gameObject.AddComponent<ChosenSkillHolder>();
        gameObject.AddComponent<Invoker>();
        gameObject.AddComponent<SuperArmor>();
        gameObject.AddComponent<DamageReaction>();
        var charaBattle = gameObject.AddComponent<CharacterBattleHolder>();
        charaBattle.CharaCallbackOnce((cb) => {
            cb.AtbMenuOpen = hero;
        });
        gameObject.AddComponent<MovementLinearCtrl>().enabled = false;
        //gameObject.AddComponent<MovementConstraint>().enabled = !Flying;
        gameObject.AddComponent<AttackInput>().enabled = hero;
        var sH = gameObject.AddComponent<SkillHelper>();
        new AimingSkillHelper(sH);
        sH.ActorStats = actorStats;
        var battleStateMachine = gameObject.AddComponent<BattleStateMachine>();
        gameObject.AddComponent<MaterialChanger>().RendererMain = mainRenderer;
        var fallCinematic = gameObject.AddComponent<CinematicFall>();
        fallCinematic.fallEnd += () => {
            DOVirtual.DelayedCall(1f, () => {
                battleStateMachine.ToNormal();
                sH.AnimationTrigger("gettingup");
                collisionProj.IgnoreCollision = false;
                //sH.AnimationTrigger("battlepose");
                //sH.AnimationTrigger("normal");
            });
        };

        if (!hero) {
            characterControllerWrapper.GravityScale = 4f;
            gameObject.AddComponent<AvailableAttackListTempOld>();
            gameObject.AddComponent<BlastAttack>();

        }

        var faceGameObject = gameObject.AddComponent<FaceGameObject>();
        faceGameObject.enabled = false;
        //CentralResource.Instance.AddProjectilImpactGraphics(gameObject);

        createdComps = true;
        OnSetupDone.Invoke(this.gameObject);
    }




    // Use this for initialization
    void Start () {
        
        
        hpSystem.SetMaxHPFullHeal(maxHP);
        collisionProj.EnemySide = !hero;

            
        if (hero)
        {
            GetComponent<SkillHelper>().HeroSide();
            UBattleCenterGirl.Instance.UBattleGameObjects.AddHero(gameObject);
            closestTarget.PossibleTargets = UBattleCenterGirl.Instance.UBattleGameObjects.Enemies;
            CharacterControlledSystem.Instance.ChangeControlledChara(this.gameObject);
        }

        else {
            GetComponent<SkillHelper>().EnemySide();
            UBattleCenterGirl.Instance.UBattleGameObjects.AddEnemy(gameObject);
            closestTarget.PossibleTargets = UBattleCenterGirl.Instance.UBattleGameObjects.Heroes;
        }
            

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
