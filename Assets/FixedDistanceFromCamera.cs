﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedDistanceFromCamera : MonoBehaviour {

    public int dis; 
    public Transform worldPosition;

	// Use this for initialization
	void Start () {
		if(worldPosition == null)
            worldPosition = transform;
	}
	
	// Update is called once per frame
	void Update () {
		var mainCamera = Camera.main;
        if(mainCamera !=null) {
            var posInCamera = mainCamera.transform.InverseTransformPoint(worldPosition.position);
            posInCamera.z = dis;
            this.transform.position = mainCamera.transform.TransformPoint(posInCamera);
        }
	}
}
