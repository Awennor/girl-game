﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneHolder : MonoBehaviour {
    Dictionary<string, CutsceneComp> cutscenes = new Dictionary<string, CutsceneComp>();
    public Transform[] cutsceneHolders;


    public Dictionary<string, CutsceneComp> Cutscenes {
        get {
            return cutscenes;
        }

        set {
            cutscenes = value;
        }
    }

    internal CutsceneComp GetCutscene(string cutsceneName) {
        return cutscenes[cutsceneName];
    }



    // Use this for initialization
    void Awake () {
        foreach(var t in cutsceneHolders) {
            t.gameObject.FillDictionaryWithChildrenComponent<CutsceneComp>(Cutscenes);
        }
		
        CentralResource.Instance.CutsceneHolder = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
