﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaleController : MonoBehaviour {

    List<TimeScaleChanger> changers = new List<TimeScaleChanger>();

    public float superSlowTime = 0.01f;

    // Use this for initialization
    void Start() {
        CentralResource.Instance.TimeScaleController = this;
    }

    public void ChangeTime(float timeToEnd, float timeScale) {
        changers.Add(new TimeScaleChanger(timeScale, timeToEnd));

    }

    public void SuperSlowTime(float timeToEnd) {
        ChangeTime(timeToEnd, superSlowTime);

    }

    // Update is called once per frame
    void Update() {
        if (Time.timeScale > 0) {
            float smallestTimeScale = 999999f;
            bool hasAnyTimeScaleActive = false;
            for (int i = 0; i < changers.Count; i++) {
                var c = changers[i];
                if (c.TimeToEnd >= 0) {
                    var deltaTime = Time.unscaledDeltaTime;
                    //Debug.Log(c.TimeToEnd);
                    //Debug.Log(deltaTime);
                    c.TimeToEnd = c.TimeToEnd - deltaTime;
                    //Debug.Log(c.TimeToEnd+"AFTER");
                    if (c.TimeToEnd < 0) {
                        changers.RemoveAt(i);
                        i--;
                    } else {
                        changers[i] = c;
                        if (smallestTimeScale > c.TimeScale) {
                            smallestTimeScale = c.TimeScale;
                            hasAnyTimeScaleActive = true;
                        }
                    }
                }
            }
            if (hasAnyTimeScaleActive)
                Time.timeScale = smallestTimeScale;
            else
                Time.timeScale = 1;
        }

    }

    struct TimeScaleChanger {
        float timeToEnd;
        float timeScale;

        public TimeScaleChanger(float timeScale, float timeToEnd) : this() {
            this.timeScale = timeScale;
            this.timeToEnd = timeToEnd;
        }

        public float TimeToEnd {
            get {
                return timeToEnd;
            }

            set {
                timeToEnd = value;
            }
        }

        public float TimeScale {
            get {
                return timeScale;
            }

            set {
                timeScale = value;
            }
        }
    }
}
