﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverworldInput : MonoBehaviour {

    [SerializeField]
    bool inputActive = true;

    public bool InputActive {
        get {
            return inputActive;
        }

        set {
            inputActive = value;
            Debug.Log("OVERWORLD "+value);
        }
    }

    // Use this for initialization
    void Start () {
		CentralResource.Instance.OverworldInput = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
