﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TeleporterComplex : MonoBehaviour {

    [SerializeField]
    List<GameObject> teleportPositions;
    [SerializeField]
    TeleportComplexUnit teleportUnit_Original;
    [SerializeField]
    UnityEventSequence eventSeq;

    public GameObjectSwitched_Event OnActualMoveToPosition;

    public Connection[] connections;
    GameObject generateHolder;
    [SerializeField]
    List<TeleportComplexUnit> generatedUnits = new List<TeleportComplexUnit>();

    void OnDrawGizmosSelected() {
        for (int i = 0; i < connections.Length; i++) {
            var from = connections[i].from;
            var to = connections[i].to;
            if (from != null && to != null)
                Gizmos.DrawLine(
                    from.transform.position,
                    to.transform.position);
        }
    }

    internal void ActualMovingHappens(TeleportComplexUnit teleportComplexUnit) {
        OnActualMoveToPosition.CallEvent(teleportPositions[generatedUnits.IndexOf(teleportComplexUnit)]);
    }

    [ExposeMethodInEditor]
    public void Generate() {
        if(generateHolder == null)
            generateHolder = transform.GetOrAddChildWithName("Automatically_Generated");
        generateHolder.transform.DestroyAllChildren();
        generatedUnits.Clear();
        for (int i = 0; i < teleportPositions.Count; i++) {
            var telepoUnit = Instantiate(teleportUnit_Original);
            telepoUnit.gameObject.SetActive(true);
            telepoUnit.transform.position = teleportPositions[i].transform.position;
            telepoUnit.transform.SetParent(generateHolder.transform);
            telepoUnit.TelepoComplex = this;
            generatedUnits.Add(telepoUnit);
        }
        for (int i = 0; i < connections.Length; i++) {
            var from = connections[i].from;
            int indexFrom = teleportPositions.IndexOf(from);
            var telepoFrom = generatedUnits[indexFrom];
            var telepoTo = generatedUnits[teleportPositions.IndexOf(connections[i].to)];
            
            telepoFrom.TelepoTarget = telepoTo;
        }
    }

    void OnDrawGizmos() {
        Gizmos.color = new Color(1,1,1, 0.4f);
        for (int i = 0; i < teleportPositions.Count; i++) {
            Gizmos.DrawCube(teleportPositions[i].transform.position, new Vector3(5, 5, 5));

        }

    }

    //create teleportercomplex unit which is a dumb teleport thing that has a bunch of events so that the real implementation can cover it

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    [Serializable]
    public class Connection {
        public GameObject from, to;
    }
}
